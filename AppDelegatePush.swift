//
//  AppDelegatePush.swift
//  Mew_iOS
//
//  Created by ficow on 2017/8/8.
//  Copyright © 2017年 MewMewStudio. All rights reserved.
//

import Foundation

extension AppDelegate:JPUSHRegisterDelegate{
    
    // MARK: - JPush Setup  https://docs.jiguang.cn/jpush/client/iOS/ios_sdk/
    func setupJPush(withApplication application: UIApplication,options:[UIApplicationLaunchOptionsKey: Any]?){
        
        mpush.register(forRemoteNotificationDelegate: self)
        mpush.application(application, didFinishLaunchingWithOptions: options)
    }
    // DeviceToken
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        mpush.application(application, didRegisterForRemoteNotificationsWithDeviceToken: deviceToken)
    }
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        
        DLOG(error.localizedDescription)
    }
    
    // MARK: - iOS 7-9 Remote Notification
    // APP进程未终结时点击推送提醒的回调
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        if APP.applicationState == UIApplicationState.active {
            // 从前台接受消息app
            playSoundAndVibration()
        }else{
            // 从后台接受消息后进入app
            handleNotificationWithInfo(userInfo)
        }
        mpush.handleRemoteNotification(userInfo)
        completionHandler(UIBackgroundFetchResult.newData)
    }
    // APP进程终结后点击推送提醒，读取启动项获取推送内容
    func loadNotification(withOptions launchOptions:[UIApplicationLaunchOptionsKey: Any]?){
        
        let notifyDict = launchOptions?[UIApplicationLaunchOptionsKey.remoteNotification]
        if let userInfo = notifyDict as? Dictionary<String,AnyObject>{
            DLOG(userInfo)
//            handleNotificationWithInfo(userInfo)
//            sendMsgToDebugServer(userInfo.debugDescription)
        }
    }
    
    
    // MARK: - iOS 10 Remote Notification
    // APP内部收到推送时的回调
    @available(iOS 10.0, *)
    func jpushNotificationCenter(_ center: UNUserNotificationCenter!, willPresent notification: UNNotification!, withCompletionHandler completionHandler: ((Int) -> Void)!) {
        
        //FIXME: <FICOW> Comment
//        sendMsgToDebugServer(notification.debugDescription)
        
        if APP.applicationState == UIApplicationState.active {
            // 从前台接受消息app
            let userInfo = notification.request.content.userInfo
            
            playSoundAndVibration()
            
            DLOG(userInfo)
            completionHandler(Int(UNNotificationPresentationOptions.badge.union(.sound).rawValue))
        }else{
            // 从后台接受消息后进入app
            completionHandler(Int(UNNotificationPresentationOptions.badge.union(.sound).union(.alert).rawValue))
        }
        mpush.jpushNotificationCenter(center, willPresent: notification, withCompletionHandler: completionHandler)
    }
    // APP外部点击推送提醒进入时的回调(不管APP进程是否终结)
    @available(iOS 10.0, *)
    func jpushNotificationCenter(_ center: UNUserNotificationCenter!, didReceive response: UNNotificationResponse!, withCompletionHandler completionHandler: (() -> Void)!) {
        
        //FIXME: <FICOW> Comment
//        sendMsgToDebugServer(response.notification.debugDescription)
        DLOG(response.notification.debugDescription)
        
        if (response.notification.request.trigger?.isKind(of: UNPushNotificationTrigger.self))!{
            let userInfo = response.notification.request.content.userInfo
            handleNotificationWithInfo(userInfo)
            mpush.handleRemoteNotification(userInfo)
            
        }else{
            // 本地推送
        }
        completionHandler()
    }
    func handleNotificationWithInfo(_ infoDict:[AnyHashable : Any]){
        DLOG("infoDict\(infoDict)")
        
        APP.applicationIconBadgeNumber = 0
        MPushManager.shared.process(userInfo: infoDict)
    }
    func playSoundAndVibration(){
        
        if isSoundEnabled{
            GMessageNotifier.playNewMessageSound()
        }
        if isVibrationEnabled{
            GMessageNotifier.playVibration()
        }
    }
}
