

import UIKit

class MBaseTVCell: UITableViewCell {

    func addBottomSeparateLine(){
        
        let line = UIView.init(frame: CGRect.zero)
        line.backgroundColor = DefaultSeparateLineColor
        self.addSubview(line)
        line.snp.makeConstraints { (m) in
            m.leading.equalTo(self.snp.leading)
            m.trailing.equalTo(self.snp.trailing)
            m.bottom.equalTo(self.snp.bottom)
            m.height.equalTo(1/UIScreen.main.scale)
        }
    }

}
