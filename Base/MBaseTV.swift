

import UIKit
import SnapKit

protocol MBaseTVDelegate:UITableViewDataSource,UITableViewDelegate {
    
    /// 加载TableView的数据
    ///
    /// - Parameter pageIndex: 页索引
    func loadData(pageIndex:Int)
    
    /// 需要注册的UITableViewCell对应的Nib文件，在setup时进行注册
    ///
    /// - Returns: Nib文件文件名名/UITableViewCell的Identifier数组
    func nibsToRegister() -> [String]
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
}

/// 借助MJRefresh，实现了上拉加载更多和下拉刷新功能的TableView
class MBaseTV: BaseTV {
    
    /// 必要的代理
    weak var tvDelegate:MBaseTVDelegate?
    var showEmptyPropmt = true
    /// 数据源
    var dataArr = [Any]()
    /// 加载完成后的回调，参数：tableView自身、页索引
    var loadFinishBlock:((MBaseTV,Int) -> Void)?
    
    fileprivate var pageIndex = 0
    fileprivate var isReload = false
    fileprivate var nibNames = [String]()
    fileprivate var emptyPromptLB = UILabel()
    fileprivate var emptyPromptIV = UIImageView()
    
    /// 初始化基类MBaseTV
    ///
    /// - Parameter delegate: MBaseTV代理
    func setup(withDelegate delegate:MBaseTVDelegate){
        
        if #available(iOS 11.0, *) {
            self.contentInsetAdjustmentBehavior = .never
        }
        self.isAccessibilityElement = true
        self.separatorStyle = .none
        self.tvDelegate = delegate
        self.dataSource = tvDelegate
        self.delegate = tvDelegate
        
        if showEmptyPropmt {
            setupPrompt()
        }
        
        nibNames = (self.tvDelegate?.nibsToRegister())!
        for nibName in nibNames {
            self.register(UINib.init(nibName: nibName, bundle: nil), forCellReuseIdentifier: nibName)
        }
        let header = MJRefreshNormalHeader.init(refreshingBlock: { [weak self] in
            self?.reloadDataList()
        })
        header?.lastUpdatedTimeLabel.isHidden = true  // 隐藏时间
        header?.stateLabel.isHidden = true // 隐藏文字
        header?.isAutomaticallyChangeAlpha = true //自动更改透明度
        self.mj_header = header
        
        let footer = MJRefreshAutoNormalFooter.init(refreshingBlock: { [weak self] in
            self?.loadMoreDataList()
        })!
        footer.setTitle("", for: .idle)
        footer.setTitle("", for: .noMoreData)
        self.mj_footer = footer
        self.mj_footer.isAutomaticallyHidden = true
    }
    fileprivate func setupPrompt(){
        
        self.addSubview(emptyPromptLB)
        self.addSubview(emptyPromptIV)
        emptyPromptLB.snp.makeConstraints { (m) in
            m.centerX.equalTo(self.snp.centerX)
            m.centerY.equalTo(self.snp.centerY).offset(16)
        }
        emptyPromptIV.snp.makeConstraints { (m) in
            m.centerX.equalTo(emptyPromptLB.snp.centerX)
            m.bottom.equalTo(emptyPromptLB.snp.top).offset(-16)
        }
        emptyPromptLB.isHidden = true
        emptyPromptIV.isHidden = true
        emptyPromptLB.text = "暂无数据,下拉可刷新喔~"
        emptyPromptLB.font = UIFont.systemFont(ofSize: 15)
        emptyPromptIV.image = UIImage.init(named: "about_mew")
        emptyPromptIV.alpha = 0.6
        emptyPromptLB.sizeToFit()
        emptyPromptIV.sizeToFit()
        emptyPromptLB.textColor = DefaultColorGray
    }
}

extension MBaseTV{
    
    // MARK: - Delegate Process Data
    
    /// 重新加载数据
    func refreshData(){
        
        if !emptyPromptLB.isHidden{
            emptyPromptLB.isHidden = true
            emptyPromptIV.isHidden = true
        }
        self.mj_header?.beginRefreshing()
    }
    
    /// 提供数据来重置列表
    ///
    /// - Parameter dataArr: 数据源
    func resetData(withDataArray dataArr:[Any]?){
        
        if isReload{
            isReload = false
        }
        
        self.dataArr.removeAll()
        self.dataArr = dataArr ?? [Any]()
        
        endRefreshing()
    }
    
    /// 增加数据
    ///
    /// - Parameter dataArray: 若数据项个数为0，则显示已加载全部
    ///                        若为nil，则停止显示正在刷新状态
    func appendData(_ dataArray:[Any]?){
        
        guard let dataArr = dataArray else {
            endRefreshing()
            return
        }
        if isReload{
            self.dataArr.removeAll()
            isReload = false
        }
        self.dataArr.append(contentsOf: dataArr)
        if dataArr.count == 0 {
            endRefreshingWithNoMoreData()
        }else{
            endRefreshing()
        }
    }
    
    /// 加载失败
    func loadFailed(){
        
        endRefreshing()
        if isReload{
            isReload = false
        }
    }
    
    // MARK: - Reload
    fileprivate func reloadDataList(){
        
        isReload = true
        pageIndex = 0
        self.tvDelegate?.loadData(pageIndex: pageIndex)
    }
    fileprivate func loadMoreDataList(){
        
        pageIndex += 1
        self.tvDelegate?.loadData(pageIndex: pageIndex)
    }
    fileprivate func endRefreshing(){
        self.reloadData()
        self.mj_header?.endRefreshing()
        self.mj_footer?.endRefreshing()
        loadFinishBlock?(self,pageIndex)
        
        // 显示列表数据为空状态
        if self.dataArr.count == 0{
            emptyPromptLB.isHidden = false
            emptyPromptIV.isHidden = false
        }else{
            emptyPromptLB.isHidden = true
            emptyPromptIV.isHidden = true
        }
    }
    fileprivate func endRefreshingWithNoMoreData(){
        endRefreshing()
        self.mj_footer?.endRefreshingWithNoMoreData()
    }
}

