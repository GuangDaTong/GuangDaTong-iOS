//
//  BaseAnimationTransition.swift
//  Mew_iOS
//
//  Created by ficow on 2017/10/18.
//  Copyright © 2017年 MewMewStudio. All rights reserved.
//

import UIKit

class BaseAnimationTransition: NSObject,UIViewControllerAnimatedTransitioning {
    
    var interactivePopTransition:UIPercentDrivenInteractiveTransition?
    var transitionType:UINavigationControllerOperation = UINavigationControllerOperation.none
    var duration:TimeInterval = TimeInterval.init(0.6)
    
    init(transitionType:UINavigationControllerOperation){
        super.init()
        
        self.transitionType = transitionType
    }
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return self.duration
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        
        if self.transitionType == UINavigationControllerOperation.push{
            self.pushTransition(using: transitionContext)
        }
        else if self.transitionType == UINavigationControllerOperation.pop{
            self.popTransition(using: transitionContext)
        }
    }
    
    func pushTransition(using transitionContext: UIViewControllerContextTransitioning){
        
//        let fromView:UIView = transitionContext.view(forKey: UITransitionContextViewKey.from)!
        let toView:UIView = transitionContext.view(forKey: UITransitionContextViewKey.to)!
        let duration:TimeInterval = self.transitionDuration(using: transitionContext)
        
        toView.layer.shadowColor = UIColor.init(red: 0.2, green: 0.2, blue: 0.2, alpha: 0.8).cgColor
        toView.layer.shadowOffset = CGSize.init(width: -3, height: 0)
        toView.layer.shadowOpacity = 0.5
        
        transitionContext.containerView.addSubview(toView)
        toView.layer.transform = CATransform3DMakeTranslation(GetView_Width(view: toView), 0, 0)
        
        UIView.animate(withDuration: duration, delay: 0, usingSpringWithDamping: 1.0, initialSpringVelocity: 0, options: UIViewAnimationOptions.curveLinear, animations: {
            
            toView.layer.transform = CATransform3DIdentity
        }, completion: { (finish) in
            
            transitionContext.completeTransition(true)
        })
    }

    func popTransition(using transitionContext: UIViewControllerContextTransitioning){
        
        let toView:UIView = transitionContext.view(forKey: UITransitionContextViewKey.to)!
        let container:UIView = transitionContext.containerView
        let duration:TimeInterval = self.transitionDuration(using: transitionContext)
        
        let fromVC:MBaseVC = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from) as! MBaseVC
//        let toVC:UIViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)!
        
        // snapshot for fromView
        let fromSnapshot:UIView = UIView.init(frame: UIScreen.main.bounds)
        fromSnapshot.layer.contents = self.snapshotForView((fromVC.navigationController?.view)!).cgImage
        
        // add shadow
        fromSnapshot.layer.shadowColor = UIColor.init(red: 0.2, green: 0.2, blue: 0.2, alpha: 0.8).cgColor
        fromSnapshot.layer.shadowOffset = CGSize.init(width: -3, height: 0)
        fromSnapshot.layer.shadowOpacity = 0.5
        
        container.addSubview(toView)
        toView.layer.transform = CATransform3DMakeTranslation(SCREEN_WIDTH * -0.25, 0, 0)
        fromVC.navigationController?.view.addSubview(fromSnapshot)
        
        
        if fromVC.interactivePopTransition != nil{
            // 右滑手势返回
            // 正常的动画使用 SpringWithDamping 类型的动画;
            // 使用手势pop时候, 使用这个类, 使用普通样式的动画,
            // 因为这时候使用SpringWithDamping类型动画会导致滑动过快.
            UIView.animate(withDuration: duration, delay: 0, options: UIViewAnimationOptions.curveLinear, animations: {

                toView.layer.transform = CATransform3DIdentity
                fromSnapshot.layer.transform = CATransform3DMakeTranslation(GetView_Width(view: fromSnapshot), 0, 0)
            }, completion: { (finish) in
                
                if transitionContext.transitionWasCancelled{
                    toView.layer.transform = CATransform3DIdentity
                    toView.removeFromSuperview()
                }
                fromSnapshot.removeFromSuperview()
                transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
            })
        }
        else{
            
            UIView.animate(withDuration: duration, delay: 0, usingSpringWithDamping: 1.0, initialSpringVelocity: 0, options: UIViewAnimationOptions.curveLinear, animations: {
                
                toView.layer.transform = CATransform3DIdentity
                fromSnapshot.layer.transform = CATransform3DMakeTranslation(GetView_Width(view: fromSnapshot), 0, 0)
            }, completion: { (finish) in
                
                fromSnapshot.removeFromSuperview()
                
                transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
            })
        }
    }
    func snapshotForView(_ view:UIView) -> UIImage{
        
        let scale:CGFloat = view.window?.screen.scale ?? UIScreen.main.scale
        UIGraphicsBeginImageContextWithOptions(view.frame.size, true, scale)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let iamge = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return iamge
    }
}
