//
//  BaseNaviController.swift
//  Mew_iOS
//
//  Created by ficow on 2017/10/18.
//  Copyright © 2017年 MewMewStudio. All rights reserved.
//

import UIKit

class BaseNaviController: UINavigationController,UINavigationControllerDelegate {

    var interactivePopTransition:UIPercentDrivenInteractiveTransition = UIPercentDrivenInteractiveTransition.init()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.delegate = self
    }
    override init(rootViewController: UIViewController) {
        super.init(rootViewController: rootViewController)
        
        self.delegate = self
    }
    
    // MARK: - UINavigationControllerDelegate
    func navigationController(_ navigationController: UINavigationController, interactionControllerFor animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        
        let animation:BaseAnimationTransition = animationController as! BaseAnimationTransition
        return animation.interactivePopTransition
    }
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationControllerOperation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        if let from = fromVC as? MBaseVC,from.interactivePopTransition != nil{
            
            let animation:BaseAnimationTransition = BaseAnimationTransition.init(transitionType: operation)
            animation.interactivePopTransition = from.interactivePopTransition
            
            return animation //手势
        }
        else{
            let animation:BaseAnimationTransition = BaseAnimationTransition.init(transitionType: operation)
            return animation //非手势
        }
    }
}
