

import UIKit

class MBaseVC: UIViewController,UIGestureRecognizerDelegate {

    var interactivePopTransition:UIPercentDrivenInteractiveTransition?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hideNaviBackBtnTitle()
//        if self.navigationController != nil && self != self.navigationController?.viewControllers.first{
//            configNavigationTransition()
//        }
        
        self.edgesForExtendedLayout = UIRectEdge.init(rawValue: 0)
        if !isAfter_IOS10{
            self.automaticallyAdjustsScrollViewInsets = false
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        MobClick.beginLogPageView(self.title)
    }
    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(animated)
        MobClick.endLogPageView(self.title)
    }
    /// NavigationController Pop手势
    func configNavigationTransition(){
        
        let popGesture = UIPanGestureRecognizer.init(target: self, action: #selector(handleCustomPopGesture(_:)))
        self.view.addGestureRecognizer(popGesture)
        popGesture.delegate = self
    }
    @objc func handleCustomPopGesture(_ recognizer:UIPanGestureRecognizer){
        
        var progress:CGFloat = recognizer.translation(in: self.view).x / GetView_Width(view: self.view)
        progress = min(1.0, max(0,progress))
        
        switch recognizer.state {
        case .began:
            self.interactivePopTransition = UIPercentDrivenInteractiveTransition.init()
            self.navigationController?.popViewController(animated: true)
        case .changed:
            self.interactivePopTransition?.update(progress)
        case .ended,.cancelled:
            if progress > 0.25{
                self.interactivePopTransition?.finish()
            }else{
                self.interactivePopTransition?.cancel()
            }
            self.interactivePopTransition = nil
        default:
            break
        }
    }
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        let canBegin:Bool = gestureRecognizer.location(in: self.view).x < 64
        return canBegin
    }
    func hideNaviBackBtnTitle(){
        
        let backBtn = UIBarButtonItem.init(title: "", style: .plain, target: self, action: #selector(popViewController))
        self.navigationItem.backBarButtonItem = backBtn
    }
    @objc func popViewController(){
        
        self.navigationController?.popViewController(animated: true)
    }
    func toastSuccess(_ msg:String){
        
        if self.view.window == nil {
            return
        }
        TopToastView.shared.makeSuccessToast(msg)
    }
    func toastFail(_ msg:String){
        
        if self.view.window == nil {
            return
        }
        TopToastView.shared.makeFailToast(msg)
    }
    func showAlert(_ msg:String){
        
        let alert = MAlertView.loadViewWithType(type: .notice,msg: msg)
        alert.show()
    }
    func showSystemStyleAlert(title:String,msg:String,okBlock:(() -> Void)?){
        
        DispatchQueue.global().async {
            
            let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
            
            let okAction = UIAlertAction.init(title: "是的", style: .destructive) { (action) in
                
                DispatchQueue.main.async{
                    okBlock?()
                }
            }
            let cancelAction = UIAlertAction(title: "取消", style: .default, handler: nil)
            alert.addAction(okAction)
            alert.addAction(cancelAction)
            
            DispatchQueue.main.async{
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
}
