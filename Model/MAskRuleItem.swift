
import UIKit
import SwiftyJSON

class MAskSubRuleItem: NSObject {
    var college = 0
    var school = 0
    init(college:Int,school:Int) {
        self.college = college
        self.school = school
    }
}
class MAskRuleItem: NSObject {
    var normal:MAskSubRuleItem
    var urgent:MAskSubRuleItem
    init(normal:MAskSubRuleItem,urgent:MAskSubRuleItem) {
        self.normal = normal
        self.urgent = urgent
    }
}
