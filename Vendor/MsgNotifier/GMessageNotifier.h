//
//  GMessageNotifier.h
//  GGo
//
//  Created by ficow on 2017/6/6.
//  Copyright © 2017年 ficow. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AudioToolbox/AudioToolbox.h>

@interface GMessageNotifier : NSObject

// 播放接收到新消息时的声音
+ (SystemSoundID)playNewMessageSound;

// 震动
+ (void)playVibration;

@end
