//
//  GMessageNotifier.m
//  GGo
//
//  Created by ficow on 2017/6/6.
//  Copyright © 2017年 ficow. All rights reserved.
//

#import "GMessageNotifier.h"

/**
 *  系统铃声播放完成后的回调
 */
void EMSystemSoundFinishedPlayingCallback(SystemSoundID sound_id, void* user_data)
{
    AudioServicesDisposeSystemSoundID(sound_id);
}

@implementation GMessageNotifier

// 播放接收到新消息时的声音
+ (SystemSoundID)playNewMessageSound
{
//    // 要播放的音频文件地址
//    NSURL *bundlePath = [[NSBundle mainBundle] URLForResource:@"EaseMob" withExtension:@"bundle"];
//    NSURL *audioPath = [[NSBundle bundleWithURL:bundlePath] URLForResource:@"in" withExtension:@"caf"];
//    // 创建系统声音，同时返回一个ID
//    SystemSoundID soundID;
//    AudioServicesCreateSystemSoundID((__bridge CFURLRef)(audioPath), &soundID);
    
    // 铃声振动简略教程 http://blog.csdn.net/wang631106979/article/details/52282322
    // 铃声编号： http://iphonedevwiki.net/index.php/AudioServices
    SystemSoundID soundID = 1007;
    
    // Register the sound completion callback.
    AudioServicesAddSystemSoundCompletion(soundID,
                                          NULL, // uses the main run loop
                                          NULL, // uses kCFRunLoopDefaultMode
                                          EMSystemSoundFinishedPlayingCallback, // the name of our custom callback function
                                          NULL // for user data, but we don't need to do that in this case, so we just pass NULL
                                          );
    
    AudioServicesPlaySystemSound(soundID);
    
    return soundID;
}

// 震动
+ (void)playVibration
{
    // Register the sound completion callback.
    AudioServicesAddSystemSoundCompletion(kSystemSoundID_Vibrate,
                                          NULL, // uses the main run loop
                                          NULL, // uses kCFRunLoopDefaultMode
                                          EMSystemSoundFinishedPlayingCallback, // the name of our custom callback function
                                          NULL // for user data, but we don't need to do that in this case, so we just pass NULL
                                          );
    
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
}

@end
