

import UIKit

class MSignUpVCFooter: UIView {

    var showContractBlock:(() -> Void)?
    
    let selectImage = UIImage.init(named: "pushSetting_selected")
    
    class func loadFromNib() -> MSignUpVCFooter{
        
        let view = Bundle.main.loadNibNamed("MSignUpVCFooter", owner: nil, options: nil)?.last as! MSignUpVCFooter
        return view
    }
    @IBAction func contractBtnPressed(_ sender: Any) {
        
        showContractBlock?()
    }

}
