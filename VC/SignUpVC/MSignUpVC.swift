

import UIKit

enum SignupType:String {
    case partTime
    case training
    case activity
}

class MSignUpVC: MBaseVC {

    // MARK: - View
    let signupTV = BaseTV.init(frame: CGRect.zero, style: .grouped)
    let finishBtn = UIButton.init()
    let contractView = MSignUpVCFooter.loadFromNib()
    
    // MARK: - Property
    var discoveryID = ""
    var signupType = SignupType.partTime
    var name = ""
    var phone = uDefaults.string(forKey: "UserAccount") ?? ""
    var personCount = "1"
    var qqNum = ""
    var wechatNum = ""
    
    // MARK: - VC Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }
    // MARK: - Setup
    func setupWithDiscoveryID(_ ID:String,type:SignupType){
        
        discoveryID = ID
        signupType = type
    }
    private func setupUI(){
        
        self.title = "我要报名"
        self.view.backgroundColor = UIColor.white
        
        setupNaviBar()
        setupTV()
    }
    private func setupNaviBar(){
        
        finishBtn.setTitle("完成", for: .normal)
        finishBtn.sizeToFit()
        finishBtn.addTarget(self, action: #selector(finishSignup), for: .touchUpInside)
        let msgItem = UIBarButtonItem.init(customView: finishBtn)
        self.navigationItem.rightBarButtonItem = msgItem
    }
    private func setupTV(){
        
        signupTV.backgroundColor = UIColor.groupTableViewBackground
        signupTV.separatorStyle = .none
        signupTV.delegate = self
        signupTV.dataSource = self
        view.addSubview(signupTV)
        signupTV.snp.makeConstraints { (m) in
            m.leading.equalTo(view.snp.leading)
            m.trailing.equalTo(view.snp.trailing)
            m.top.equalTo(view.snp.top)
            m.bottom.equalTo(view.snp.bottom)
        }
        signupTV.register(UINib.init(nibName: MSignUpVCInputCellIdentifier, bundle: nil), forCellReuseIdentifier: MSignUpVCInputCellIdentifier)
        contractView.showContractBlock = {
            self.showContractVC()
        }
    }
    @objc func finishSignup(){
        
        guard name.count != 0 else {
            self.toastFail("请输入姓名")
            return
        }
        guard phone.count == 11 else {
            self.toastFail("请输入正确的手机号")
            return
        }
        if signupType == .activity && personCount.count == 0{
            self.toastFail("请输入活动参与人数")
            return
        }
        
        view.makeToastActivity()
        
        HttpController.shared.signUp(type: signupType, itemID: discoveryID, userName: name, phone: phone, personCount: personCount, qq: qqNum, wechat: wechatNum, successBlock: { (json) in
            
            self.view.hideToastActivity()
            self.toastSuccess("报名成功")
            delay(second: 0.5, block: {
                self.popViewController()
            })
            
        }, failure: { (err) in
            
            self.view.hideToastActivity()
            self.toastFail(err.localizedDescription)
            DLOG(err.localizedDescription)
        })
    }
    private func showContractVC(){
        
        self.view.endEditing(true)
        let webVC = self.loadViewControllerInMainStoryboard("MWebVC") as! MWebVC
        webVC.setup(title: "服务条款", url: BaseURL + "/contract")
        webVC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(webVC, animated: true)
    }
}

extension MSignUpVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch signupType {
        case .training:
            fallthrough
        case .partTime:
            return 4
        case .activity:
            return 5
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: MSignUpVCInputCellIdentifier) as! MSignUpVCInputCell
        var row = indexPath.row
        
        if signupType != .activity && row > 1{
            row += 1
        }
        switch row {
        case 0:
            cell.titleLB.text = "姓名"
            cell.inputTF.text = name
            cell.inputTF.placeholder = "请输入真实姓名"
            cell.textDidChangeBlock = { text in
                self.name = text
            }
        case 1:
            cell.titleLB.text = "手机号"
            cell.inputTF.text = phone
            cell.inputTF.placeholder = "请输入手机号"
            cell.textDidChangeBlock = { text in
                self.phone = text
            }
        case 2:
            cell.titleLB.text = "活动参与人数"
            cell.inputTF.text = personCount
            cell.inputTF.placeholder = "请输入活动参与人数"
            cell.textDidChangeBlock = { text in
                self.personCount = text
            }
        case 3:
            cell.titleLB.text = "QQ(可选)"
            cell.inputTF.text = qqNum
            cell.inputTF.placeholder = "请输入QQ"
            cell.textDidChangeBlock = { text in
                self.qqNum = text
            }
        case 4:
            cell.titleLB.text = "微信(可选)"
            cell.inputTF.text = wechatNum
            cell.inputTF.placeholder = "请输入微信"
            cell.textDidChangeBlock = { text in
                self.wechatNum = text
            }
        default:
            break
        }
        return cell
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return contractView
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 44
    }
}
