

import UIKit

let MSignUpVCInputCellIdentifier = "MSignUpVCInputCell"

class MSignUpVCInputCell: MBaseTVCell,UITextFieldDelegate {

    var textDidChangeBlock:((String) -> Void)?
    
    @IBOutlet weak var titleLB: UILabel!
    @IBOutlet weak var inputTF: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        inputTF.delegate = self
        inputTF.setPlaceholderTextToLightGray()
        addBottomSeparateLine()
        NotiCenter.addObserver(self, selector: #selector(textFieldDidChange), name: NSNotification.Name.UITextFieldTextDidChange, object: inputTF)
    }
    deinit {
        NotiCenter.removeObserver(self)
    }
    @objc func textFieldDidChange(){
        
        let text = inputTF.text!
        if text.count > DefaultSignupTextLength{
            inputTF.text = String(text.characters.prefix(DefaultSignupTextLength))
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        textDidChangeBlock?(inputTF.text!)
    }

}
