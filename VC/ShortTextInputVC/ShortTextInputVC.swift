

import UIKit

let ShortTextInputVCIdentifier = "ShortTextInputVCCell"

class ShortTextInputVC: MBaseVC {

    var oldText = ""
    var textInputFinishBlock:((String) -> Void)?
    var inputTV = BaseTV.init(frame: CGRect.zero, style: UITableViewStyle.plain)
    var inputTF = UITextField()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = UIColor.white
        setupNaviBar()
        setupTV()
        setupTextField()
        NotiCenter.addObserver(self, selector: #selector(textFieldDidChange), name: NSNotification.Name.UITextFieldTextDidChange, object: inputTF)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        inputTF.becomeFirstResponder()
    }
    deinit {
        NotiCenter.removeObserver(self)
    }
    func setupNaviBar(){
        
        let saveBtn = UIButton.init()
        saveBtn.setTitle("确定", for: .normal)
        saveBtn.sizeToFit()
        saveBtn.addTarget(self, action: #selector(finish), for: .touchUpInside)
        let saveItem = UIBarButtonItem.init(customView: saveBtn)
        self.navigationItem.rightBarButtonItem = saveItem
    }
    func setupTV(){
        
        self.view.addSubview(inputTV)
        inputTV.contentInset = UIEdgeInsetsMake(16, 0, 0, 0)
        inputTV.separatorStyle = .none
        inputTV.frame = self.view.frame
        inputTV.backgroundColor = UIColor.groupTableViewBackground
        inputTV.delegate = self
        inputTV.dataSource = self
        inputTV.register(UITableViewCell.self, forCellReuseIdentifier: ShortTextInputVCIdentifier)
    }
    func setupTextField(){
        
        inputTF.placeholder = "请输入1~15个字符作为您的昵称"
        inputTF.textColor = UIColor.darkGray
        inputTF.clearButtonMode = .whileEditing
        inputTF.text = oldText
    }
    @objc func textFieldDidChange(){
        
        let text = inputTF.text!
        if text.count > DefaultUserNameMaxLength{
            inputTF.text = String(text.characters.prefix(DefaultUserNameMaxLength))
        }
        oldText = inputTF.text!
    }
    @objc func finish(){
        
        let count = oldText.count
        if count == 0 || count > DefaultUserNameMaxLength{
            TopToastView.shared.makeFailToast("昵称应为1~15个字符")
            return
        }
        textInputFinishBlock?(oldText)
        self.popViewController()
    }
}

extension ShortTextInputVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: ShortTextInputVCIdentifier)!
        cell.selectionStyle = .none
        cell.addSubview(inputTF)
        inputTF.snp.makeConstraints { (m) in
            m.leading.equalTo(cell.snp.leading).offset(16)
            m.top.equalTo(cell.snp.top)
            m.trailing.equalTo(cell.snp.trailing).offset(-16)
            m.bottom.equalTo(cell.snp.bottom)
        }
        
        return cell
    }
}
