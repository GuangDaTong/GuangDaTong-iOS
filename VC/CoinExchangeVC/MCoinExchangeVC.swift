

import UIKit

class MCoinExchangeVC: MBaseVC {

    // MARK: - Property
    var isGive = false
    var userID = ""
    var coinStr = "0"
    var sendText = ""
    var operationFinishedBlock:((Int) -> Void)?
    let NormalCellIdentifier = "NormalCell"
    
    // MARK: - View
    @IBOutlet weak var exchangeTV: BaseTV!
    fileprivate var inputV = MInputView.loadFromNib()
    fileprivate var coinTF:UITextField?
    
    // MARK: - VC Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.coinTF?.becomeFirstResponder()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
    }
    // MARK: - Setup
    func setup(withUserID ID:String,isGive:Bool){
        
        userID = ID
        self.isGive = isGive
    }
    fileprivate func setupUI(){
        
        setupNaviBar()
        
        exchangeTV.contentInset = UIEdgeInsetsMake(16, 0, 0, 0)
        exchangeTV.register(UINib.init(nibName: MCoinExchangeVCCellIdentifier, bundle: nil), forCellReuseIdentifier: MCoinExchangeVCCellIdentifier)
        exchangeTV.register(UITableViewCell.self, forCellReuseIdentifier: NormalCellIdentifier)
        
        inputV.placeholder = "对TA说点什么吧~"
        inputV.textChangedBlock = { text in
            self.sendText = text
        }
        
        if isGive{
            self.title = "赠送TA金币"
        }else{
            self.title = "向TA索要金币"
        }
    }
    fileprivate func setupNaviBar(){
        
        let saveBtn = UIButton.init()
        saveBtn.setTitle("完成", for: .normal)
        saveBtn.sizeToFit()
        saveBtn.addTarget(self, action: #selector(finish), for: .touchUpInside)
        let saveItem = UIBarButtonItem.init(customView: saveBtn)
        self.navigationItem.rightBarButtonItem = saveItem
    }
    @objc func finish(){
        
        self.coinTF?.resignFirstResponder()
        
        if coinStr == "0"{
            self.toastFail("金币数不能为0")
            return
        }
        
        self.view.makeToastActivity()
        
        HttpController.shared.coinExchange(isGive: isGive, userID: userID, coinCount: coinStr, msg: sendText, successBlock: { (json) in
            
            self.view.hideToastActivity()
            if self.isGive{
                self.toastSuccess("赠送成功")
            }else{
                self.toastSuccess("索要成功")
            }
            let coins = (self.coinStr as NSString).integerValue
            self.operationFinishedBlock?(coins)

            delay(second: 0.5, block: {
                self.navigationController?.popViewController(animated: true)
            })
        }, failure: { (err) in
            
            self.view.hideToastActivity()
            self.toastFail(err.localizedDescription)
        })
    }
}

extension MCoinExchangeVC:UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let section = indexPath.section
        if section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: MCoinExchangeVCCellIdentifier) as! MCoinExchangeVCCell
            cell.coinEditFinishBlock = { coin in
                self.coinStr = coin
            }
            self.coinTF = cell.coinTF
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: NormalCellIdentifier)!
        cell.addSubview(inputV)
        inputV.snp.makeConstraints { (m) in
            m.leading.equalTo(cell.snp.leading)
            m.top.equalTo(cell.snp.top)
            m.trailing.equalTo(cell.snp.trailing)
            m.bottom.equalTo(cell.snp.bottom)
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
            return MCoinExchangeVCCell.DefaultHeight
        }
        
        var height = MInputView.DefaultHeight
        if SCREEN_HEIGHT < 667{
            height /= 2.0
        }
        return height
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 1{
            return MCoinExchangeTVHeader.loadFromNib()
        }
        return nil
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 1{
            return MCoinExchangeTVHeader.DefaultHeight
        }
        return 0.1
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 8
    }
}
