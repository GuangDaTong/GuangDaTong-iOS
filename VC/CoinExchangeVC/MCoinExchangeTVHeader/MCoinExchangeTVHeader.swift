

import UIKit

class MCoinExchangeTVHeader: UIView {

    static let DefaultHeight:CGFloat = 32
    
    class func loadFromNib() -> MCoinExchangeTVHeader{
        
        let view = Bundle.main.loadNibNamed("MCoinExchangeTVHeader", owner: nil, options: nil)?.last as! MCoinExchangeTVHeader
        return view
    }

}
