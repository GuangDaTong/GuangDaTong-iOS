

import UIKit

let MCoinExchangeVCCellIdentifier = "MCoinExchangeVCCell"

class MCoinExchangeVCCell: MBaseTVCell,UITextFieldDelegate {

    static let DefaultHeight:CGFloat = 44
    
    @IBOutlet weak var coinTF: UITextField!

    var coinEditFinishBlock:((String) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        addBottomSeparateLine()
        coinTF.delegate = self
        coinTF.text = "0"
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var text = textField.text!
        if text.count > DefaultCoinAmountDigitCount {
            coinTF.text = String(text.characters.prefix(DefaultCoinAmountDigitCount))
        }
        coinEditFinishBlock?(text)
        
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        var text = textField.text!
        if text.count == 0 {
            text = "0"
            textField.text = text
        }
        coinEditFinishBlock?(text)
    }
}
