

import UIKit

class MCompleteInfoVC: MBaseVC,UITextFieldDelegate {

    // MARK: - Views
    @IBOutlet weak var pwdTF: UITextField!
    @IBOutlet weak var pwdAgainTF: UITextField!
    @IBOutlet weak var finishBtn: UIButton!
    @IBOutlet weak var instituteLB: UILabel!
    
    // MARK: - Constraints
    @IBOutlet weak var pwdSepLineHeight: NSLayoutConstraint!
    @IBOutlet weak var pwdAgainSepLineHeight: NSLayoutConstraint!
    @IBOutlet weak var topDistance: NSLayoutConstraint!
    
    // MARK: - Data
    var phoneStr:String = ""
    var verifyToken = ""
    var instituteArr:[String] = [String]()
    var _listVC:MSelectionListVC! = nil
    var listVC:MSelectionListVC{
        get{
            if _listVC != nil {
                return _listVC
            }
            _listVC = self.loadViewControllerInMainStoryboard("MSelectionListVC") as! MSelectionListVC
            _listVC.title = "请选择学院"
            _listVC.selectedBlock = { [weak self] (title) in
                
                self?.isShowingList = false
                if title.count > 0 {
                    self?.instituteLB.text = title
                }else{
                    self?.instituteLB.text = "请选择学院"
                }
                self?.checkInputStatus()
            }
            return _listVC
        }
    }
    var isShowingList = false
    
    // MARK: - VC Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    deinit {
        NotiCenter.removeObserver(self)
    }
    // MARK: - setup
    func setupUI(){
        
        APP.setStatusBarHidden(false, with: .none)
        
        pwdSepLineHeight.setToOnePixel()
        pwdAgainSepLineHeight.setToOnePixel()
        
        finishBtn.isEnabled = false
        
        NotiCenter.addObserver(self, selector: #selector(checkInputStatus), name: NSNotification.Name.UITextFieldTextDidChange, object: nil)
        
        pwdTF.delegate = self
        pwdTF.setPlaceholderTextToLightGray()
        pwdAgainTF.delegate = self
        pwdAgainTF.setPlaceholderTextToLightGray()
    }
    @objc func checkInputStatus(){
        
        guard (pwdAgainTF.text?.count)! > 0 && (pwdTF.text?.count)! > 0 && instituteLB.text != "请选择学院" else {
            
            finishBtn.isEnabled = false
            return
        }
        finishBtn.isEnabled = true
    }
    func getInstitueData(){
        
        view.makeToastActivity()
        HttpController.shared.instituteList(success: { (json) in
            
            self.view.hideToastActivity()
            let arr = json["result"]["colleges"].array
            for institute in arr! {
                self.instituteArr.append(institute.stringValue)
            }
            self.listVC.list = self.instituteArr
            self.showListVC()
            
            }, failure: {  (err) in
                self.view.hideToastActivity()
                self.toastFail(err.localizedDescription)
        })
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == pwdTF{
            view.endEditing(true)
            pwdAgainTF.becomeFirstResponder()
        }else{
            view.endEditing(true)
        }
        return true
    }
    
    // MARK: - Touch Events
    @IBAction func viewDidTouchDown(_ sender: Any) {
        self.view.endEditing(true)
    }
    @IBAction func selectInstitueBtnPressed(_ sender: Any) {
        
        self.view.endEditing(true)
        guard self.instituteArr.count != 0 else {
            getInstitueData()
            return
        }
        showListVC()
    }
    @IBAction func finishBtnPressed(_ sender: Any) {
            
        self.view.endEditing(true)
        
        guard pwdTF.text == pwdAgainTF.text else {
            self.toastFail("两次输入的密码不匹配！😳")
            return
        }
        
        finishBtn.isEnabled = false
        
        view.makeToastActivity()
        DLOG(phoneStr)
        DLOG(pwdTF.text!)
        DLOG(instituteLB.text!)
        HttpController.shared.register(account: phoneStr, pwd: pwdTF.text!, institute: instituteLB.text!,token: verifyToken, success: { (json) in
            
            self.view.hideToastActivity()
            uDefaults.set(self.phoneStr, forKey: "UserAccount")
            uDefaults.set(self.pwdTF.text, forKey: "UserPassword")
            uDefaults.synchronize()
            self.toastSuccess("注册成功！")
            delay(second: 0.5, block: {
                self.navigationController?.popToRootViewController(animated: true)
            })
            
        }, failure: {  (err) in
            
            self.view.hideToastActivity()
            self.finishBtn.isEnabled = true
            self.toastFail(err.localizedDescription)
        })
    }
    
    func showListVC(){
        isShowingList = true
        view.endEditing(true)
        self.navigationController?.pushViewController(listVC, animated: true)
    }
}

