

import UIKit

let MAskWriteVCCostCellIdentifier = "MAskWriteVCCostCell"

class MAskWriteVCCostCell: MBaseTVCell {

    var helpBtnPressedBlock:(() -> Void)?
    @IBOutlet weak var costLB: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        addBottomSeparateLine()
    }
    
    @IBAction func helpBtnPressed(_ sender: Any) {
        
        helpBtnPressedBlock?()
    }

}
