

import UIKit
import AssetsLibrary
import TZImagePickerController

enum MAskCanSeeType:String {
    case sameCollege = "本校同学院"
    case sameSchool = "本校所有人"
}

class MAskWriteVC: MBaseVC,UITableViewDelegate,UITableViewDataSource,TZImagePickerControllerDelegate {

    var askWriteFinishBlock:((AskRangeType) -> Void)?
    
    @IBOutlet weak var askWriteTV: BaseTV!
    
    var _listVC:MSelectionListVC! = nil
    var listVC:MSelectionListVC{
        get{
            if _listVC != nil {
                return _listVC
            }
            _listVC = self.loadViewControllerInMainStoryboard("MSelectionListVC") as! MSelectionListVC
            return _listVC
        }
    }
    var imagePickerVC:TZImagePickerController! = nil
    
    var isFirstLoad = true
    let sendAskBtn = UIButton.init()
    var askSendView = MAskSendView.loadFromNib()
    
    var ruleItem:MAskRuleItem! = nil
    var coins = 0
    
    var askType = AskType.normal
    var askTag = AskTag.learn
    var askCanSee:MAskCanSeeType = .sameCollege
    var askReward = "0"
    var askCost = "0"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if isFirstLoad{
            isFirstLoad = false
            askSendView.titleTF.becomeFirstResponder()
            recountCost()
        }
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        askSendView.refreshImages()
    }
    
    // MARK: - UI
    func setupUI(){
        
        self.title = "发帖子"
        setupNaviBar()
        
        askWriteTV.register(UINib.init(nibName: MAskWriteVCSelectCellIdentifier, bundle: nil), forCellReuseIdentifier: MAskWriteVCSelectCellIdentifier)
        askWriteTV.register(UINib.init(nibName: MAskWriteVCRewardCellIdentifier, bundle: nil), forCellReuseIdentifier: MAskWriteVCRewardCellIdentifier)
        askWriteTV.register(UINib.init(nibName: MAskWriteVCCostCellIdentifier, bundle: nil), forCellReuseIdentifier: MAskWriteVCCostCellIdentifier)
        
        askSendView.setTextViewPlaceholder(text: "输入帖子内容吧...")
        askSendView.frame = CGRect.init(x: 0, y: 0, width: SCREEN_WIDTH, height: MAskSendView.DefaultHeight)
        askWriteTV.tableHeaderView = askSendView
        askSendView.imagePickBlock = {
            
            self.view.endEditing(true)
            self.showImagePickerVC()
        }
        askSendView.heightUpdateBlock = { height in
            self.askWriteTV.beginUpdates()
            self.askSendView.setFrameHeight(height)
            self.askWriteTV.tableHeaderView = self.askSendView
            self.askWriteTV.endUpdates()
        }
    }
    func setupNaviBar(){
        
        sendAskBtn.setImage(UIImage.init(named: "askWrite_send"), for: .normal)
        sendAskBtn.sizeToFit()
        sendAskBtn.addTarget(self, action: #selector(checkCoinsBeforeSend), for: .touchUpInside)
        let msgItem:UIBarButtonItem = UIBarButtonItem.init(customView: sendAskBtn)
        self.navigationItem.rightBarButtonItem = msgItem
        
        let backBtn:UIButton = UIButton.init(type: UIButtonType.custom)
        backBtn.setTitle("取消", for: UIControlState.normal)
        backBtn.sizeToFit()
        backBtn.addTarget(self, action: #selector(goBack), for: UIControlEvents.touchUpInside)
        let backItem:UIBarButtonItem = UIBarButtonItem.init(customView: backBtn)
        self.navigationItem.leftBarButtonItem = backItem
    }
    @objc func goBack(){
        
        self.showSystemStyleAlert(title: "提示", msg: "\n 放弃发帖子吗？") {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    // MARK: - Data
    func recountCost(){
        
        let reward = (self.askReward as NSString).integerValue
        var cost = reward
        
        switch askType {
        case .normal:
            let normal = ruleItem.normal
            switch askCanSee {
            case .sameCollege:
                cost += normal.college
            case .sameSchool:
                cost += normal.school
            }
        case .urgent:
            let urgent = ruleItem.urgent
            switch askCanSee {
            case .sameCollege:
                cost += urgent.college
            case .sameSchool:
                cost += urgent.school
            }
        }
        if cost > coins{
            self.toastFail("亲，您的金币不足喔，快去赚吧！")
        }
        self.askCost = "\(cost)"
        self.askWriteTV.reloadData()
    }
    func showImagePickerVC(){
        
        let authorization = ALAssetsLibrary.authorizationStatus()
        switch authorization {
        case .authorized:
            break
        case .notDetermined:
            loadImagePickerVC()
            delay(second: 0.5, block: {
                self.imagePickerVC.dismiss(animated: true, completion: nil)
            })
            return
        default:
            self.showAlert("无法打开相册，请在 设置-隐私-照片 选项中，允许应用访问您的相册")
            return
        }
        loadImagePickerVC()
    }
    func loadImagePickerVC(){
        
        let count:Int = 9 - self.askSendView.imageArr.count //最大9张图
        self.imagePickerVC = TZImagePickerController.init(maxImagesCount: count, delegate: self)
        self.imagePickerVC.didFinishPickingPhotosHandle = { photos,assets,isOriginal in
            self.askSendView.addPhotos(images: photos!)
        }
        self.imagePickerVC.naviBgColor = AppThemeColor
        self.imagePickerVC.allowPickingVideo = false
        self.present(self.imagePickerVC, animated: true, completion: nil)
    }
    @objc func checkCoinsBeforeSend(){
        
        let title:String = askSendView.titleTF.text!
        if title.count == 0 {
            self.view.hideToastActivity()
            self.toastFail("帖子标题不能为空")
            return
        }
        if title.count > DefaultAskTitleMaxLength {
            self.view.hideToastActivity()
            self.toastFail("帖子标题不能超过15个字符")
            return
        }
        let content:String = askSendView.textView.text!
        if content.count == 0 {
            self.view.hideToastActivity()
            self.toastFail("帖子内容不能为空")
            return
        }
        
        view.endEditing(true)
        view.makeToastActivity()
        
        HttpController.shared.getCoins(successBlock: { (coins,ruleItem) in
            
            self.coins = coins
            self.sendAsk()
            
        }, failure: { (err) in
            
            self.view.hideToastActivity()
            self.toastFail(err.localizedDescription)
            DLOG(err.localizedDescription)
        })
    }
    func sendAsk(){
        
        if (askCost as NSString).integerValue > coins{
            self.view.hideToastActivity()
            self.toastFail("亲，您的金币不足喔，快去赚吧！")
            return
        }
        
        self.sendAskBtn.isEnabled = false
        
        var range:AskRangeType = AskRangeType.college
        switch self.askCanSee {
        case .sameCollege:
            range = .college
        case .sameSchool:
            range = .school
        }
        
        let images:[UIImage] = askSendView.imageArr
        let title:String = askSendView.titleTF.text!
        let content:String = askSendView.textView.text!
        
        HttpController.shared.sendAsk(type: self.askType.rawValue,title:title,label: self.askTag.rawValue, range: range, reward: self.askReward, content: content, images: images, successBlock: { (json) in
            
            self.view.hideToastActivity()
            
            self.toastSuccess("发帖子成功")
            self.sendAskBtn.isEnabled = true
            DispatchQueue.global().async {
                self.askWriteFinishBlock?(range)
            }
            delay(second: 0.5, block: { 
                self.navigationController?.popViewController(animated: true)
            })
            
        }, failure: { (err) in
            
            self.sendAskBtn.isEnabled = true
            self.view.hideToastActivity()
            self.toastFail(err.localizedDescription)
        })
        
    }
    func showHelpVC(){
        
        let webVC = self.loadViewControllerInMainStoryboard("MWebVC") as! MWebVC
        webVC.setup(title: "帖子 - 使用帮助", url: BaseURL + "/help/ask")
        webVC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(webVC, animated: true)
    }

    
    // MARK: - TV Datasoure & Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var returnCell:UITableViewCell! = nil
        
        let row = indexPath.row
        switch row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: MAskWriteVCSelectCellIdentifier) as! MAskWriteVCSelectCell
            cell.iconIV.image = UIImage.init(named: "askWrite_askType")
            cell.titleLB.text = "帖子类型"
            cell.selectedLB.text = askType.rawValue
            returnCell = cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: MAskWriteVCSelectCellIdentifier) as! MAskWriteVCSelectCell
            cell.iconIV.image = UIImage.init(named: "askWrite_addTag")
            cell.titleLB.text = "添加标签"
            cell.selectedLB.text = askTag.rawValue
            returnCell = cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: MAskWriteVCSelectCellIdentifier) as! MAskWriteVCSelectCell
            cell.iconIV.image = UIImage.init(named: "askWrite_whoCanSee")
            cell.titleLB.text = "谁可以看"
            cell.selectedLB.text = askCanSee.rawValue
            returnCell = cell
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: MAskWriteVCRewardCellIdentifier) as! MAskWriteVCRewardCell
            cell.rewardTF.text = self.askReward
            cell.selectionStyle = .none
            cell.rewardEditFinishBlock = { reward in
                self.askReward = reward
                self.recountCost()
            }
            
            returnCell = cell
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: MAskWriteVCCostCellIdentifier) as! MAskWriteVCCostCell
            cell.costLB.text = askCost
            cell.helpBtnPressedBlock = {
                self.showHelpVC()
            }
            returnCell = cell
        default:
            assertionFailure()
        }
        
        returnCell.selectionStyle = .none
        
        return returnCell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 16
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        view.endEditing(true)
        
        let row = indexPath.row
        if row > 2 {
            return
        }
        
        var title = ""
        var selectedStr = ""
        var list:[String]! = nil
        var block:((String) -> Void)! = nil
        switch row {
        case 0:
            title = "帖子类型"
            list = [AskType.normal.rawValue,
                    AskType.urgent.rawValue]
            
            selectedStr = self.askType.rawValue
            block = { [weak self] (title) in
                if title == AskType.normal.rawValue{
                    self?.askType = .normal
                }else{
                    self?.askType = .urgent
                }
                self?.recountCost()
            }
        case 1:
            title = "添加标签"
            selectedStr = self.askTag.rawValue
            let tagList:[AskTag] = [.lost,.found,.learn,.practice,.master,
                                    .training,.shopping,.food,.traffic,.travel,
                                    .emotion,.other]
            var tagStrList = [String]()
            for tag in tagList{
                tagStrList.append(tag.rawValue)
            }
            list = tagStrList
            
            block = { [weak self] (title) in
                for tag in tagList{
                    if tag.rawValue != title {
                        continue
                    }
                    self?.askTag = tag
                    break
                }
                self?.recountCost()
            }
        case 2:
            title = "谁可以看"
            selectedStr = self.askCanSee.rawValue
            list = [MAskCanSeeType.sameCollege.rawValue,
                    MAskCanSeeType.sameSchool.rawValue]
            block = { [weak self] (title) in
                self?.askCanSee = MAskCanSeeType.init(rawValue: title)!
                self?.recountCost()
            }
        default:
            break
        }
        listVC.title = title
        listVC.selectedStr = selectedStr
        listVC.list = list
        listVC.selectedBlock = block
        
        self.navigationController?.pushViewController(listVC, animated: true)
        
    }
}
