

import UIKit

let MAskWriteVCSelectCellIdentifier = "MAskWriteVCSelectCell"

class MAskWriteVCSelectCell: MBaseTVCell {

    @IBOutlet weak var iconIV: UIImageView!
    @IBOutlet weak var titleLB: UILabel!
    @IBOutlet weak var selectedLB: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        addBottomSeparateLine()
    }
}
