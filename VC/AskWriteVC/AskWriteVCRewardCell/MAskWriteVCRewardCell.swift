

import UIKit

let MAskWriteVCRewardCellIdentifier = "MAskWriteVCRewardCell"

class MAskWriteVCRewardCell: MBaseTVCell,UITextFieldDelegate {

    @IBOutlet weak var rewardTF: UITextField!
    var rewardEditFinishBlock:((String) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        addBottomSeparateLine()
        rewardTF.delegate = self
        NotiCenter.addObserver(self, selector: #selector(textFieldDidChange), name: NSNotification.Name.UITextFieldTextDidChange, object: nil)
    }
    deinit {
        NotiCenter.removeObserver(self)
    }
    
    
    @objc func textFieldDidChange(){
        
        var text = rewardTF.text!
        if text.count > DefaultCoinAmountDigitCount {
            rewardTF.text = String(text.characters.prefix(DefaultCoinAmountDigitCount))
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        var text = textField.text!
        if text.count == 0 {
            text = "0"
            textField.text = text
        }
        rewardEditFinishBlock?(text)
    }
    
    
}
