

import UIKit
import SwiftyJSON
import SnapKit

class MUserInfoVC: MBaseVC{

    // MARK: - Property
    var userID:String = ""
    var userName:String = ""
    var coins:String = "0"
    var zanCount = 0
    var isSelf = false
    var mineNavigator:MMineNavigator! = nil
    
    fileprivate var isZan = false
    fileprivate var schoolText = DefaultSchool
    fileprivate var instituteText = "XX学院"
    fileprivate var gradeText = "XXXX级"
    fileprivate var iconURL = ""
    
    // MARK: - View
    @IBOutlet weak var infoTV: BaseTV!
    @IBOutlet var infoContainer: UIView!
    @IBOutlet weak var iconIV: UIImageView!
    @IBOutlet weak var levelBtn: UIButton!
    @IBOutlet weak var zanBtn: UIButton!
    @IBOutlet weak var nameLB: UILabel!
    @IBOutlet weak var sexLB: UILabel!
    @IBOutlet weak var mewCoinLB: UILabel!
    @IBOutlet weak var backgroundIV: UIImageView!
    
    fileprivate let infoContainerDefaultHeight:CGFloat = 219 / 667 * SCREEN_HEIGHT
    fileprivate var infoContainerHeight:Constraint?
    fileprivate var infoContainerTop:Constraint?
    
    // MARK: - VC Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        isSelf = (userID == uUserID ? true : false)
        
        if isSelf{
            NotiCenter.addObserver(self, selector: #selector(setupForUser), name: UserDidModifyInfoNotification, object: nil)
        }
        
        setupUI()
    }
    deinit {
        if isSelf{
            NotiCenter.removeObserver(self)
        }
    }
    func setupUI(){
        
        self.title = (self.isSelf ? "个人信息" : "\(userName)的资料")
        if self.isSelf{
            self.mineNavigator = MMineNavigator()
            self.mineNavigator.setupWith(ownerVC: self)
            self.setupNaviBar()
        }
        self.nameLB.text = self.userName
        self.levelBtn.setCornerRounded(radius: 8)
        self.iconIV.setCornerRounded(radius: 4)
        
        self.infoTV.snp.remakeConstraints { (m) in
            m.top.equalTo(self.view.snp.top)
            m.leading.equalTo(self.view.snp.leading)
            m.width.equalTo(self.view.snp.width)
            m.bottom.equalTo(self.view.snp.bottom)
        }

        self.infoTV.contentInset = UIEdgeInsetsMake(self.infoContainerDefaultHeight, 0, 0, 0)
        self.infoTV.setContentOffset(CGPoint.init(x: 0, y: -self.infoContainerDefaultHeight), animated: true)
        
        self.infoTV.addSubview(self.infoContainer)
        self.infoContainer.snp.remakeConstraints { (m) in
            self.infoContainerTop = m.top.equalTo(self.infoTV.snp.top).offset(-self.infoContainerDefaultHeight).constraint
            m.leading.equalTo(self.infoTV.snp.leading)
            m.width.equalTo(SCREEN_WIDTH)
            self.infoContainerHeight = m.height.equalTo(self.infoContainerDefaultHeight).constraint
        }
        
        self.infoTV.register(UINib.init(nibName: MUserInfoVCInstituteCellIdentifier, bundle: nil), forCellReuseIdentifier: MUserInfoVCInstituteCellIdentifier)
        self.infoTV.register(UINib.init(nibName: MMeVCCellIdentifier, bundle: nil), forCellReuseIdentifier: MMeVCCellIdentifier)
        
        let iconTap:UITapGestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(self.showUserIcon))
        self.iconIV.addGestureRecognizer(iconTap)
        let bgTap:UITapGestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(self.backgroundImagePressed))
        self.backgroundIV.addGestureRecognizer(bgTap)
        
        self.setupForUser()
    }
    func setupNaviBar(){
        
        let editBtn = UIButton.init()
        editBtn.setTitle("编辑", for: .normal)
        editBtn.sizeToFit()
        editBtn.addTarget(self, action: #selector(selfInfoVC), for: .touchUpInside)
        let msgItem = UIBarButtonItem.init(customView: editBtn)
        self.navigationItem.rightBarButtonItem = msgItem
    }
    @objc func selfInfoVC(){
        
        let selfVC = loadViewControllerInMainStoryboard("MSelfInfoVC") as! MSelfInfoVC
        selfVC.hidesBottomBarWhenPushed = true
        hideNaviBackBtnTitle()
        navigationController?.pushViewController(selfVC, animated: true)
    }
    @objc func setupForUser(){
        
        let id = userID
        view.makeToastActivity()
        HttpController.shared.userInfo(id: id, successBlock: { (json) in
            
            self.view.hideToastActivity()
            self.setup(withJSON: json["result"])
            
        }, failure: { (err) in
            
            self.view.hideToastActivity()
            self.toastFail(err.localizedDescription)
            delay(second: 1, block: {
                self.navigationController?.popViewController(animated: true)
            })
        })
    }
    fileprivate func setup(withJSON json:JSON){
        
        let name = json["nickname"].stringValue
        let icon = json["icon"].stringValue
        let level = json["level"].stringValue
        let zan = json["zan"].stringValue
        let gender = json["gender"].stringValue
        let coins = json["coins"].stringValue
        let isZan = json["isZan"].stringValue
        
        self.isZan = (isZan == "1" ? true : false)
        if self.isZan{
            zanBtn.setImage(UIImage.init(named: "userInfo_zan_blue"), for: .normal)
        }
        
        self.nameLB.text = name
        self.schoolText = json["school"].stringValue
        self.instituteText = json["college"].stringValue
        self.gradeText = json["grade"].stringValue
        self.iconURL = icon
        self.coins = coins
        
        if let url = URL.init(string: icon){
            self.iconIV.sd_setImage(with: url, placeholderImage: DefaultUserIcon)
        }else{
            self.iconIV.image = DefaultUserIcon
        }
        self.levelBtn.setTitle("LV \(level)", for: .normal)
        
        self.zanBtn.setTitle(" \(zan)", for: .normal)
        self.zanCount = (zan as NSString).integerValue
        
        let maleColor = UIColor.colorWithHex(value: 0x4990E2, alpha: 1)
        let femaleColor = UIColor.colorWithHex(value: 0xFE3824, alpha: 1)
        
        var genderText = ""
        var genderColor:UIColor! = nil
        if gender == "男"{
            genderText = "♂"
            genderColor = maleColor
        }else if gender == "女"{
            genderText = "♀"
            genderColor = femaleColor
        }else{
            genderText = ""
        }
        let genderAttrStr = NSMutableAttributedString.init(string: genderText)
        let genderRange = NSRange.init(location: 0, length: genderAttrStr.length)
        genderAttrStr.addAttributes([NSAttributedStringKey.foregroundColor:genderColor], range: genderRange)
        self.sexLB.attributedText = genderAttrStr
        
        self.setCoinsText(coins)
        
        infoTV.reloadData()
    }
    func setCoinsText(_ coins:String){
        
        let rewardColor = UIColor.colorWithHex(value: 0xFE3824, alpha: 1)
        let coinAttrStr = NSMutableAttributedString.init(string: "金币: \(coins)")
        let coinRange = NSRange.init(location: 3, length: coinAttrStr.length - 3)
        coinAttrStr.addAttributes([NSAttributedStringKey.foregroundColor:rewardColor], range: coinRange)
        mewCoinLB.attributedText = coinAttrStr
    }
}

// MARK: - Touch Event
extension MUserInfoVC{
    
    @objc func showUserIcon(){
        
        showImageBrowser(withURLStr: iconURL)
    }
    @objc func backgroundImagePressed(){
        DLOG("")
    }
    
    @IBAction func levelBtnPressed(_ sender: Any) {
        DLOG("")
    }
    @IBAction func zanBtnPressed(_ sender: Any) {
        
        if isUserNotLogin{
            self.toastFail("您尚未登录")
            self.presentVCInMainStoryboard("MLoginNaviVC")
            return
        }
        
        if isZan{
            return
        }
        isZan = true
        self.zanBtn.setTitle(" \(self.zanCount + 1)", for: .normal)
        
        let ID = self.userID
        HttpController.shared.zan(type: .user, ID: ID, successBlock: { (json) in
            DLOG("赞 帖子\(ID) 成功！")
        }, failure: { (err) in
            DLOG("赞 帖子\(ID) 失败 ！")
        })
        zanBtn.setImage(UIImage.init(named: "userInfo_zan_blue"), for: .normal)
        
        UIView.animate(withDuration: 0.15, animations: {
            self.zanBtn.imageView?.layer.transform = CATransform3DMakeScale(1.1, 1.1, 0)
        }, completion: { (finish) in
            UIView.animate(withDuration: 0.15, animations: {
                self.zanBtn.imageView?.layer.transform = CATransform3DIdentity
            })
        })
    }
}

// MARK: - UITableViewDelegate & UITableViewDataSource
extension MUserInfoVC:UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            return 1
        }
        return 6
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let section = indexPath.section
        
        if section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: MUserInfoVCInstituteCellIdentifier) as! MUserInfoVCInstituteCell
            cell.selectionStyle = .none
            cell.schoolLB.text = schoolText
            let grade = gradeText.count > 0 ? "∙" + gradeText : ""
            cell.instituteLB.text = instituteText + grade
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: MMeVCCellIdentifier) as! MMeVCCell
        cell.selectionStyle = .none
        
        configCell(cell, indexPath: indexPath, isSelf: isSelf)
        
        return cell
    }
    func configCell(_ cell:MMeVCCell,indexPath: IndexPath,isSelf:Bool){
        
        let row = indexPath.row
        var title = ""
        var icon:UIImage! = nil
        
        if isSelf{
            
            switch row {
            case 0:
                title = "通讯录"
                icon = UIImage.init(named: "mine_contact")
            case 1:
                title = "金币收益记录"
                icon = UIImage.init(named: "mine_icon_record")
            case 2:
                title = "我的帖子"
                icon = UIImage.init(named: "mine_ask")
            case 3:
                title = "我的收藏"
                icon = UIImage.init(named: "mine_collection")
            case 4:
                title = "我的消息"
                icon = UIImage.init(named: "mine_msg")
            case 5:
                title = "黑名单"
                icon = UIImage.init(named: "put_into_blacklist")
            default:
                break
            }
            cell.setup(title, image: icon)
            return
        }
        switch row {
        case 0:
            title = "添加TA为联系人"
            icon = UIImage.init(named: "userInfo_user")
        case 1:
            title = "赠送TA金币"
            icon = UIImage.init(named: "userInfo_mew")
        case 2:
            title = "向TA索要金币"
            icon = UIImage.init(named: "userInfo_mew")
        case 3:
            title = "给TA留言"
            icon = UIImage.init(named: "userInfo_msg")
        case 4:
            title = "举报TA"
            icon = UIImage.init(named: "userInfo_inform")
        case 5:
            title = "加入黑名单"
            icon = UIImage.init(named: "put_into_blacklist")
        default:
            break
        }
        cell.setup(title, image: icon)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 0 {
            return MUserInfoVCInstituteCell.DefaultHeight
        }
        return MMeVCCellHeight
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 16
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 0 {
            return
        }
        let row = indexPath.row
        
        if isSelf {
            switch row {
            case 0:
                mineNavigator.showVC(type: .contact)
            case 1:
                mineNavigator.showVC(type: .accountBook)
            case 2:
                mineNavigator.showVC(type: .ask)
            case 3:
                mineNavigator.showVC(type: .collection)
            case 4:
                mineNavigator.showVC(type: .msg)
            case 5:
                mineNavigator.showVC(type: .blacklist)
            default:
                break
            }
            return
        }
        
        if isUserNotLogin{
            self.toastFail("您尚未登录")
            self.presentVCInMainStoryboard("MLoginNaviVC")
            return
        }
        
        switch row {
        case 0:
            addContact()
        case 1:
            giveCoins()
        case 2:
            askForConins()
        case 3:
            leaveMsg()
        case 4:
            inform()
        case 5:
            putIntoBlacklist()
        default:
            break
        }
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let yOffset = -scrollView.contentOffset.y
        if yOffset >  infoContainerDefaultHeight {
            infoContainerTop?.layoutConstraints.first?.constant = -yOffset
            infoContainerHeight?.layoutConstraints.first?.constant = yOffset
        }
    }
}
