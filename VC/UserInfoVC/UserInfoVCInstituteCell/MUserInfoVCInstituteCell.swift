

import UIKit

let MUserInfoVCInstituteCellIdentifier = "MUserInfoVCInstituteCell"

class MUserInfoVCInstituteCell: MBaseTVCell {

    static let DefaultHeight:CGFloat = 86.0
    @IBOutlet weak var schoolLB: UILabel!
    @IBOutlet weak var instituteLB: UILabel!

}
