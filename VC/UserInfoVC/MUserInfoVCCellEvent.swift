

import Foundation

extension MUserInfoVC{
    
    // MARK: - 添加联系人
    func addContact(){
        
        self.showSystemStyleAlert(title: "提示", msg: "\n添加TA为联系人？") {
            
            self.view.makeToastActivity()
            
            if MAskFilter.shared.blacklist.contains(self.userID){
                self.view.hideToastActivity()
                self.toastFail("TA已被您拉黑")
                return
            }
            
            self.addContactFromServer()
        }
    }
    func addContactFromServer(){
        
        HttpController.shared.addToContact(userID: self.userID, successBlock: { (json) in
            
            self.view.hideToastActivity()
            self.toastSuccess("添加成功")
            NotiCenter.post(name: UserDidAlterContactNotification, object: nil)
        }, failure: { (err) in
            
            self.view.hideToastActivity()
            self.toastFail(err.localizedDescription)
        })
    }
    
    // MARK: - 赠送喵币
    func giveCoins(){
        
        let ceVC:MCoinExchangeVC = self.loadViewControllerInMainStoryboard("MCoinExchangeVC") as! MCoinExchangeVC
        ceVC.setup(withUserID: self.userID, isGive: true)
        ceVC.operationFinishedBlock = { coins in
            let coinStr:NSString? = self.coins as NSString?
            let oldCoin:Int = (coinStr == nil ? 0 : (coinStr!).integerValue)
            self.setCoinsText("\(oldCoin + coins)")
        }
        self.hideNaviBackBtnTitle()
        self.navigationController?.pushViewController(ceVC, animated: true)
    }
    
    // MARK: - 索要喵币
    func askForConins(){
        
        let ceVC:MCoinExchangeVC = self.loadViewControllerInMainStoryboard("MCoinExchangeVC") as! MCoinExchangeVC
        ceVC.setup(withUserID: self.userID, isGive: false)
        self.hideNaviBackBtnTitle()
        self.navigationController?.pushViewController(ceVC, animated: true)
    }
    
    // MARK: - 留言
    func leaveMsg(){
        
        let tsVC:MTextSendVC = self.loadViewControllerInMainStoryboard("MTextSendVC") as! MTextSendVC
        tsVC.setupWith(operationType: .leaveMsg, ID: self.userID,name: self.userName)
        self.hideNaviBackBtnTitle()
        self.navigationController?.pushViewController(tsVC, animated: true)
    }
    
    // MARK: - 举报
    func inform(){
        
        DispatchQueue.global().async {
            
            let ID:String = self.userID
            let sheet:UIAlertController = UIAlertController.init(title: "", message: "请选择", preferredStyle: .actionSheet)
            let cheat:String = "欺诈骗钱"
            let cheatAction:UIAlertAction = UIAlertAction.init(title: cheat, style: .default) { (action) in
                DispatchQueue.main.async{
                    self.informWithReason(cheat,userID: ID)
                }
            }
            sheet.addAction(cheatAction)
            let violence:String = "色情暴力"
            let violenceAction:UIAlertAction = UIAlertAction.init(title: violence, style: .default) {(action) in
                DispatchQueue.main.async{
                    self.informWithReason(violence,userID: ID)
                }
            }
            sheet.addAction(violenceAction)
            let ad:String = "广告骚扰"
            let adAction:UIAlertAction = UIAlertAction.init(title: "广告骚扰", style: .default) {(action) in
                DispatchQueue.main.async{
                    self.informWithReason(ad,userID: ID)
                }
            }
            sheet.addAction(adAction)
            let otherAction:UIAlertAction = UIAlertAction.init(title: "其他", style: .default) { (action) in
                DispatchQueue.main.async{
                    self.informWithReason("其他",userID: ID)
                }
            }
            sheet.addAction(otherAction)
            let cancelAction:UIAlertAction = UIAlertAction.init(title: "取消", style: .cancel, handler: nil)
            sheet.addAction(cancelAction)
            
            if sheet.popoverPresentationController != nil{
                let cell:UITableViewCell = self.infoTV.cellForRow(at: IndexPath.init(row: 4, section: 1))!
                sheet.popoverPresentationController?.sourceView = cell
                sheet.popoverPresentationController?.sourceRect = cell.bounds
            }
            
            DispatchQueue.main.async{
                
                self.present(sheet, animated: true, completion: nil)
            }
        }
    }
    
    func informWithReason(_ reason:String,userID:String){
        
        if reason == "其他" {
            
            let tsVC:MTextSendVC = self.loadViewControllerInMainStoryboard("MTextSendVC") as! MTextSendVC
            tsVC.hidesBottomBarWhenPushed = true
            tsVC.setupWith(operationType: .informUser, ID: userID)
            self.hideNaviBackBtnTitle()
            self.navigationController?.pushViewController(tsVC, animated: true)
            return
        }
        self.view.makeToastActivity()
        HttpController.shared.inform(type: .user,ID: userID, reason: reason, successBlock: { (json) in
            
            self.view.hideToastActivity()
            self.toastSuccess("举报成功")
            
            }, failure: { (err) in
                
                self.view.hideToastActivity()
                self.toastFail(err.localizedDescription)
        })
    }
    func putIntoBlacklist(){
        
        self.showSystemStyleAlert(title: "提示", msg: "\n将TA加入黑名单后，您将不会再收到TA发送的留言以及帖子。是否继续？") {
            
            self.putIntoBlacklistFromServer()
        }
    }
    func putIntoBlacklistFromServer(){
        
        self.view.makeToastActivity()
        HttpController.shared.addToBlacklist(userID: self.userID, successBlock: { (json) in
            
            MAskFilter.shared.appendBlacklist(userID: self.userID)
            MMsgLoader.removeMsgForBlacklist(userID: self.userID)
            
            self.view.hideToastActivity()
            self.toastSuccess("拉黑成功")
            NotiCenter.post(name: UserDidAlterContactNotification, object: nil)
            NotiCenter.post(name: UserAlteredBlacklistNotification, object: nil)
            self.popViewController()
        }, failure: { (err) in
            
            self.view.hideToastActivity()
            self.toastFail(err.localizedDescription)
        })
    }
}
