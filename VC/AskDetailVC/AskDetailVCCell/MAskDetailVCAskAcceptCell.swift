

import UIKit

let MAskDetailVCAskAcceptCellIdentifier = "MAskDetailVCAskAcceptCell"

class MAskDetailVCAskAcceptCell: MBaseTVCell {

    static let MinHeight:CGFloat = 72
    // 评论内容的label的最小高度
    static let ContentMinCountHeight:CGFloat = 30
    
    var iconBtnPressedBlock:(() -> Void)?
    // MARK: - View
    @IBOutlet weak var iconBtn: UIButton!
    @IBOutlet weak var nameLB: UILabel!
    @IBOutlet weak var genderLB: UILabel!
    @IBOutlet weak var timeLB: UILabel!
    @IBOutlet weak var contentLB: MCopyLabel!
    @IBOutlet weak var isAskSolvedLB: UILabel!
    
    fileprivate var _item:MAskCommentItem! = nil
    
    var isAskSolved = false
    var item:MAskCommentItem{
        get{
            return _item
        }
        set{
            _item = newValue
            iconBtn.imageView?.contentMode = .scaleAspectFill
            iconBtn.sd_setImage(with: URL.init(string: _item.iconURL), for: .normal, placeholderImage: DefaultUserIcon)
            nameLB.text = _item.nickname
            genderLB.attributedText = _item.gender
            timeLB.text = _item.createTime
            contentLB.attributedText = _item.content
            if isAskSolved{
                isAskSolvedLB.isHidden = false
            }else{
                isAskSolvedLB.isHidden = true
            }
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        
        iconBtn.setCornerRounded(radius: 4)
        addBottomSeparateLine()
    }
    @IBAction func iconBtnPressed(_ sender: Any) {
        iconBtnPressedBlock?()
    }
}
