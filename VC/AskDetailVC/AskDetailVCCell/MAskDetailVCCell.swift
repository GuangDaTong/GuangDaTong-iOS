

import UIKit

let MAskDetailVCCellIdentifier = "MAskDetailVCCell"

class MAskDetailVCCell: MBaseTVCell {

    static let MinHeight:CGFloat = 72
    // 评论内容的label的最小高度
    static let ContentMinCountHeight:CGFloat = 30
    
    let AcceptBtnDefaultWidth:CGFloat = 35
    var iconBtnPressedBlock:(() -> Void)?
    var acceptBtnPressedBlock:((_ isDelete:Bool) -> Void)?
    
    // MARK: - View
    @IBOutlet weak var iconBtn: UIButton!
    @IBOutlet weak var nameLB: UILabel!
    @IBOutlet weak var genderLB: UILabel!
    @IBOutlet weak var acceptBtn: UIButton!
    @IBOutlet weak var timeLB: UILabel!
    @IBOutlet weak var contentLB: MCopyLabel!
    @IBOutlet weak var acceptBtnWidth: NSLayoutConstraint!
    
    fileprivate var _item:MAskCommentItem! = nil
    var isAskSolved = false
    var acceptCommentID = ""
    var askPublisherID = ""
    var item:MAskCommentItem{
        get{
            return _item
        }
        set{
            _item = newValue
            iconBtn.imageView?.contentMode = .scaleAspectFill
            iconBtn.sd_setImage(with: URL.init(string: _item.iconURL), for: .normal, placeholderImage: DefaultUserIcon)
            nameLB.text = _item.nickname
            genderLB.attributedText = _item.gender
            timeLB.text = _item.createTime
            contentLB.attributedText = _item.content
            
            if isUserNotLogin{
                setAcceptBtn(show: false)
                return
            }
            let userID = uUserID
            
            
            //问问发布人本人
            if askPublisherID == userID{
                // 问问发布人自我评论
                if _item.publisherID == userID{
                    acceptBtn.isHidden = false
                    setAcceptBtn(show: true)
                    setupAcceptBtn(isDelete: true)
                    return
                }
                // 其他人评论问问
                
                // 问问已解决
                if isAskSolved{
                    setAcceptBtn(show: false)
                    return
                }
                //采纳他人
                setAcceptBtn(show: true)
                setupAcceptBtn(isDelete: false)
                return
                
            }else{
                // 非问问发布人本人
                
                // 评论人本人
                if userID == _item.publisherID {
                    // 评论被采纳，不允许删除
                    if _item.id == acceptCommentID{
                        setAcceptBtn(show: false)
                        return
                    }
                    setAcceptBtn(show: true)
                    setupAcceptBtn(isDelete: true)
                    return
                }
            }
            
            // 其他人的评论
            setAcceptBtn(show: false)
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        
        iconBtn.setCornerRounded(radius: 4)
        addBottomSeparateLine()
    }
    fileprivate func setupAcceptBtn(isDelete:Bool){
        
        if isDelete {
            acceptBtn.setTitle("删除", for: .normal)
            acceptBtn.setTitleColor(DefaultColorRed, for: .normal)
            acceptBtn.setBackgroundImage(UIImage.init(named: "ask_delete"), for: .normal)
            return
        }
        acceptBtn.setTitle("采纳", for: .normal)
        acceptBtn.setTitleColor(DefaultColorBlue, for: .normal)
        acceptBtn.setBackgroundImage(UIImage.init(named: "ask_accept"), for: .normal)
    }
    func setAcceptBtn(show:Bool){
        
        if show{
            acceptBtn.isHidden = false
            acceptBtnWidth.constant = AcceptBtnDefaultWidth
            return
        }
        acceptBtn.isHidden = true
        acceptBtnWidth.constant = 0
    }
    @IBAction func iconBtnPressed(_ sender: Any) {
        iconBtnPressedBlock?()
    }
    @IBAction func acceptBtnPressed(_ sender: UIButton) {
        
        if sender.titleLabel?.text == "采纳"{
            acceptBtnPressedBlock?(false)
        }else{
            acceptBtnPressedBlock?(true)
        }
    }
    
}
