
import Foundation
import SwiftyJSON

extension MAskDetailVC:MBaseTVDelegate{
    
    // MARK: - TV Datasoure & Delegate
    func nibsToRegister() -> [String]{
        
        return [MAskDetailVCCellIdentifier,MAskDetailVCAskAcceptCellIdentifier]
    }
    func loadData(pageIndex:Int){
        
        HttpController.shared.askCommentList(askItem.id, pageIndex: pageIndex, successBlock: { (json) in
            
            var dataArr = [MAskCommentItem]()
            
            for data in json["result"]["data"].arrayValue{
                
                let item:MAskCommentItem = self.parseItem(fromJSON: data)
                dataArr.append(item)
            }
            guard pageIndex != 0 else{
                if let acceptItem = self.acceptCommentItem{
                    self.isAskSolved = true
                    dataArr.insert(acceptItem, at: 0)
                }else{
                    // 加载已采纳的评论
                    self.loadAcceptComment()
                }
                self.detailTV.resetData(withDataArray: dataArr)
                return
            }
            self.detailTV.appendData(dataArr)
            
            }, failure: { (err) in
                
                self.detailTV.loadFailed()
                self.toastFail(err.localizedDescription)
        })
    }
    func parseItem(fromJSON data:JSON) -> MAskCommentItem{
        
        let id:String = data["id"].stringValue
        let commentReceiverID:String = data["commentReceiverID"].stringValue
        let commentReceiverNickname:String = data["commentReceiverNickname"].stringValue
        let createTime:NSString = data["createTime"].stringValue as NSString
        let publisherID:String = data["publisherID"].stringValue
        let icon:String = data["icon"].stringValue
        let nickname:String = data["nickname"].stringValue
        let gender:String = data["gender"].stringValue
        
        var content:String = data["content"].stringValue
        content = content.count > 0 ? content : "无"
        
        var contentAttrStr:NSMutableAttributedString! = nil
        if commentReceiverID == self.askItem.publisherID ||
            commentReceiverID == publisherID{
            contentAttrStr = NSMutableAttributedString.init(string: content)
        }else{
            contentAttrStr = NSMutableAttributedString.init(string: "@\(commentReceiverNickname)：\n\(content)")
            let contentRange = NSRange.init(location: 1, length: (commentReceiverNickname as NSString).length)
            contentAttrStr.addAttributes([NSAttributedStringKey.foregroundColor:DefaultColorBlue], range: contentRange)
        }
        
        let genderAttrStr:NSAttributedString = gender.attributedGenderString()
        let contentHeight:CGFloat = self.countContentHeight(content: contentAttrStr.string)
        let height:CGFloat = MAskDetailVCCell.MinHeight + contentHeight
        
        let time:String = createTime.timeShort() ?? "1970-12-31 11:11:11"
        
        let item:MAskCommentItem = MAskCommentItem.init(id: id, publisherID: publisherID, nickname: nickname, iconURL: icon, gender: genderAttrStr, content: contentAttrStr, commentReceiverID: commentReceiverID, commentReceiverName: commentReceiverNickname, createTime: time, contentHeight: height)
        return item
    }
    // 评论内容多出来的部分
    func countContentHeight(content:String) -> CGFloat{
        
        var height:CGFloat = 0
        let contentStr:NSString = content as NSString
        height = contentStr.getSizeWith(UIFont.systemFont(ofSize: 14), constrainedTo: CGSize.init(width: SCREEN_WIDTH - 80, height: CGFloat.greatestFiniteMagnitude)).height
        if height >= MAskDetailVCCell.ContentMinCountHeight{
            height -= MAskDetailVCCell.ContentMinCountHeight
        }else{
            height = 0
        }
        return height
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return detailTV.dataArr.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let row:Int = indexPath.row
        
        if acceptCommentItem != nil && row == 0{
            
            let cell:MAskDetailVCAskAcceptCell = tableView.dequeueReusableCell(withIdentifier: MAskDetailVCAskAcceptCellIdentifier) as! MAskDetailVCAskAcceptCell
            cell.selectionStyle = .none
            cell.isAskSolved = self.isAskSolved
            cell.item = acceptCommentItem
            cell.iconBtnPressedBlock = { [weak self] in
                self?.showUserInfoVC(withID: cell.item.publisherID,name: cell.item.nickname)
            }
            return cell
        }
        
        let cell:MAskDetailVCCell = tableView.dequeueReusableCell(withIdentifier: MAskDetailVCCellIdentifier) as! MAskDetailVCCell
        cell.askPublisherID = askItem.publisherID
        cell.acceptCommentID = askItem.acceptCommentID
        cell.selectionStyle = .none
        
        let item:MAskCommentItem = detailTV.dataArr[row] as! MAskCommentItem
        cell.isAskSolved = self.isAskSolved
        cell.item = item
        cell.acceptBtnPressedBlock = { [weak self] isDelete in
            self?.showOptionAlert(withCommentID: item.id,name:item.nickname, isDelete: isDelete)
        }
        cell.iconBtnPressedBlock = { [weak self] in
            self?.showUserInfoVC(withID: cell.item.publisherID,name: cell.item.nickname)
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        
        let item:MAskCommentItem = detailTV.dataArr[indexPath.row] as! MAskCommentItem
        return item.contentHeight
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
        let row:Int = indexPath.row
        // 点击已采纳的评论无响应
        if acceptCommentItem != nil && row == 0 {
            return
        }
        
        let item:MAskCommentItem = detailTV.dataArr[row] as! MAskCommentItem
        showMenu(withUserID: item.publisherID, objectID: item.id, name: item.nickname, isComment: true,row: row)
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        self.view.endEditing(true)
    }
}


extension MAskDetailVC{
    
    /// 加载已采纳的评论
    func loadAcceptComment(){
        
        //askDetail
        HttpController.shared.askDetail(askItem.id, successBlock: { (json) in
            
            self.parseAcceptComment(json)
            
            }, failure: { (err) in
                DLOG(err.localizedDescription)
        })
    }
    func parseAskItem(_ json:JSON) -> AskListItem{
        
        let font:UIFont = UIFont.systemFont(ofSize: 14)
        let userID:String = uUserID
        let currentDate:Date = DefaultDateFormatter.date(from: stringOfNow()) ?? Date()
        
        let item:AskListItem = MAskTV.parseAskItem(json, userID: userID, font: font, currentDate: currentDate)
        return item
    }
    func parseAcceptComment(_ json:JSON){
        
        let askItemJSON:JSON = json["result"]
        // 这里会重置已采纳状态acceptCommentID，所以setupAskPart需要分情况进行
        self.askItem = self.parseAskItem(askItemJSON)
        
        let acceptComment:JSON = json["result"]["adviseComment"]
        
        if acceptComment.type == .null{
            self.setupAskPart()
            return
        }
        
        let id:String = acceptComment["id"].stringValue
        self.askItem.acceptCommentID = id //补上已经被清空了的acceptCommentID
        self.setupAskPart()
        self.acceptBlock?(id) //列表显示已解决
        
        let publisherID:String = acceptComment["publisherID"].stringValue
        let nickname:String = acceptComment["nickname"].stringValue
        let icon:String = acceptComment["icon"].stringValue
        let gender:String = acceptComment["gender"].stringValue
        let content:String = acceptComment["content"].stringValue
        let createTime:NSString = acceptComment["createTime"].stringValue as NSString
        
        let time:String = createTime.timeShort() ?? "1970-12-31 11:11:11"
        
        let genderAttrStr:NSAttributedString = gender.attributedGenderString()
        let height:CGFloat = MAskDetailVCCell.MinHeight + self.countContentHeight(content: content)
        
        let acceptItem = MAskCommentItem.init(id: id, publisherID: publisherID, nickname: nickname, iconURL: icon, gender: genderAttrStr, content: NSMutableAttributedString.init(string: content), commentReceiverID: "", commentReceiverName: "", createTime: time, contentHeight: height)
        
        delay(second: 0.3, block: {
            
            self.detailTV.beginUpdates()
            self.acceptCommentItem = acceptItem
            self.detailTV.dataArr.insert(acceptItem, at: 0)
            self.detailTV.insertRows(at: [IndexPath.init(row: 0, section: 0)], with: .top)
            self.detailTV.endUpdates()
            self.detailTV.reloadData()
        })
    }
}
