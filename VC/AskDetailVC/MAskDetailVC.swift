

import UIKit
import SnapKit
import IQKeyboardManager

/// 问问评论项
struct MAskCommentItem {
    var id:String
    var publisherID:String
    var nickname:String
    var iconURL:String
    var gender:NSAttributedString
    var content:NSAttributedString
    var commentReceiverID:String
    var commentReceiverName:String
    var createTime:String
    var contentHeight:CGFloat
}


class MAskDetailVC: MBaseVC {

    // MARK: - Property
    var zanBlock:(() -> Void)?
    var collectBlock:(() -> Void)?
    var acceptBlock:((String) -> Void)? //列表显示已解决
    var commentSendedBlock:(() -> Void)? //评论后，评论数需要加1
    var dislikeAskBlock:(() -> Void)? //不想看到这条问问
    /// 用于从问问列表加载
    var askItem:AskListItem! = nil
    var isLoadingForCollection = false //加载我的收藏 - 问问，隐藏右上角的菜单和删除按钮
    var manageType:AskManageType = .none
    
    // MARK: - Views
    let detailTV = MBaseTV.init(frame: CGRect.zero, style: UITableViewStyle.plain)
    let askContainer = UIView.init(frame: CGRect.zero)
    let mainPart = MAskMainPartView.loadFromNib()
    var imagePart:MAskImagePartView! = nil
    let operationPart = MAskOperationPartView.loadFromNib()
    let commentView = MAskCommentView.loadFromNib()
    
    var isAskSolved = false
    var acceptCommentItem:MAskCommentItem! = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "帖子详情"
        setupTV()
        setupAskPart()
        setupCommentView()
        detailTV.refreshData()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        IQKeyboardManager.shared().isEnableAutoToolbar = false
        IQKeyboardManager.shared().isEnabled = false
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        view.endEditing(true)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        IQKeyboardManager.shared().isEnableAutoToolbar = true
        IQKeyboardManager.shared().isEnabled = true
    }
}

// MARK: - Setup View
extension MAskDetailVC{

    func setupNaviBar(){
        
        let menuBtn = UIButton.init()
        menuBtn.setImage(UIImage.init(named: "naviBar_menu"), for: .normal)
        menuBtn.sizeToFit()
        menuBtn.addTarget(self, action: #selector(showNaviMenu), for: .touchUpInside)
        let msgItem = UIBarButtonItem.init(customView: menuBtn)
        self.navigationItem.rightBarButtonItem = msgItem
    }
    func setupTV(){
        
        detailTV.isAccessibilityElement = true
        detailTV.showEmptyPropmt = false
        detailTV.setup(withDelegate: self)
        
        view.addSubview(detailTV)
        detailTV.snp.makeConstraints { (m) in
            m.leading.equalTo(view.snp.leading)
            m.trailing.equalTo(view.snp.trailing)
            m.top.equalTo(view.snp.top)
            m.bottom.equalTo(view.snp.bottom)
        }
        detailTV.separatorStyle = .none
        detailTV.backgroundColor = UIColor.groupTableViewBackground
    }
    
    func setupAskPart(){
        
        let item:AskListItem = self.askItem
        if item.acceptCommentID != ""{
            isAskSolved = true
        }
        if acceptCommentItem != nil{
            isAskSolved = true
        }
        
        manageType = (askItem.publisherID == uUserID ? AskManageType.delete : AskManageType.menu)
        if isLoadingForCollection{
            manageType = AskManageType.none
        }
        if manageType == AskManageType.menu{
            setupNaviBar()
        }
        
        askContainer.accessibilityIdentifier = "askContainer"
        mainPart.accessibilityIdentifier = "mainPart"
        operationPart.accessibilityIdentifier = "operationPart"
        
        let height:CGFloat = MAskMainPartViewHeight + item.detailHeight + item.imgHeight + MAskOperationPartViewHeight
        
        askContainer.backgroundColor = UIColor.white
        
        detailTV.tableHeaderView = askContainer
        
        askContainer.snp.remakeConstraints { (m) in
            m.top.equalTo(self.detailTV.snp.top)
            m.leading.equalTo(self.detailTV.snp.leading)
            m.width.equalTo(SCREEN_WIDTH)
            let heightCon:Constraint = m.height.equalTo(height).constraint
            heightCon.layoutConstraints.first?.priority = UILayoutPriority.defaultLow
            
            var frame:CGRect = self.askContainer.frame
            frame.size.height = height
            self.askContainer.frame = frame
            self.detailTV.tableHeaderView = self.askContainer
        }
        
        //问问主部件
        let mainPartHeight:CGFloat = MAskMainPartViewHeight + item.detailHeight
        askContainer.addSubview(mainPart)
        mainPart.snp.remakeConstraints { (make) in
            make.leading.equalTo(askContainer.snp.leading)
            make.trailing.equalTo(askContainer.snp.trailing)
            make.top.equalTo(askContainer.snp.top)
            make.height.equalTo(mainPartHeight)
        }
        
        mainPart.setup(manageType: AskManageType.none, title: item.title, iconURL: item.icon, name: item.nickname, genderText: item.genderText, solveStatus: item.acceptCommentID, time: item.createTime, rewardText: item.rewardText, detail: item.content, isUrgent: item.isUrgent, askTagText: item.askTagText)
        let titleTap:UITapGestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(titleOrDetailTapped))
        mainPart.titleLB.addGestureRecognizer(titleTap)
        let detailTap:UITapGestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(titleOrDetailTapped))
        mainPart.detailLB.addGestureRecognizer(detailTap)
        mainPart.viewBtnPressedBlock = {
            
            self.commentView.startComment(withUserID: self.askItem.publisherID,name: "发问人")
        }
        mainPart.iconBtnPressedBlock = {
            
            self.showUserInfoVC(withID: self.askItem.publisherID,name: self.askItem.nickname)
        }
        // 图片部分
        let imgCount:Int = item.imageURLs.count
        if imgCount > 1 {
            imagePart = MAskImagePartNineView.loadFromNib()
            imagePart.setupImages(with: item.thumbnailImageURLs)
            imagePart.setupHDImages(with: item.imageURLs)
            imagePart.isHidden = false
        }else if imgCount == 1{
            let singleImageView = MAskImagePartSingleView.loadFromNib()
            imagePart = singleImageView
            imagePart.setupImages(with: item.thumbnailImageURLs)
            imagePart.setupHDImages(with: item.imageURLs)
            imagePart.isHidden = false
            
        }else{
            imagePart = MAskImagePartSingleView.loadFromNib()
            imagePart.isHidden = true
        }
        askContainer.addSubview(imagePart)
        imagePart.snp.remakeConstraints { (make) in
            make.top.equalTo(mainPart.snp.bottom)
            make.leading.equalTo(askContainer.snp.leading).offset(MAskImagePartLeadingSpace)
            make.trailing.equalTo(askContainer.snp.trailing).offset(-MAskImagePartTrailingSpace)
            make.height.equalTo(item.imgHeight)
        }
        let touchBtn:UIButton = UIButton.init(frame: CGRect.zero)
        askContainer.addSubview(touchBtn)
        askContainer.bringSubview(toFront: imagePart) //图片按钮需要响应点击事件
        touchBtn.snp.remakeConstraints { (make) in
            make.top.equalTo(imagePart.snp.top)
            make.leading.equalTo(askContainer.snp.leading)
            make.trailing.equalTo(askContainer.snp.trailing)
            make.bottom.equalTo(imagePart.snp.bottom)
        }
        touchBtn.addTarget(self, action: #selector(imagePartBtnPressed), for: .touchUpInside)
        
        //底部操作条
        askContainer.addSubview(operationPart)
        operationPart.snp.remakeConstraints { (make) in
            make.top.equalTo(imagePart.snp.bottom)
            make.leading.equalTo(askContainer.snp.leading)
            make.trailing.equalTo(askContainer.snp.trailing)
            make.height.equalTo(MAskOperationPartViewHeight)
        }
        operationPart.setup(isZan:item.isZan, zanCount: item.zan, commentCount: askItem.commentCount)
        operationPart.shareBtnPressedBlock = {
            self.showShareMenu()
        }
        operationPart.zanBtnPressedBlock = {
            if isUserNotLogin{
                self.toastFail("您尚未登录")
                self.presentVCInMainStoryboard("MLoginNaviVC")
                return
            }
            self.makeZanRequest(ID: self.askItem.id)
            self.askItem.isZan = true
            self.zanBlock?()
        }
        operationPart.commentBtnPressedBlock = {
            
            self.commentView.startComment(withUserID: self.askItem.publisherID,name: "发问人")
        }
        
        let sepLine:UIView = UIView.init(frame: CGRect.zero)
        sepLine.backgroundColor = UIColor.lightGray
        askContainer.addSubview(sepLine)
        sepLine.snp.remakeConstraints { (make) in
            make.bottom.equalTo(askContainer.snp.bottom)
            make.leading.equalTo(askContainer.snp.leading)
            make.trailing.equalTo(askContainer.snp.trailing)
            make.height.equalTo(1 / UIScreen.main.scale)
        }
        self.detailTV.reloadData()
        self.detailTV.layoutIfNeeded()
    }
    @objc func titleOrDetailTapped(){
        self.commentView.startComment(withUserID: self.askItem.publisherID,name: "发问人")
    }
    @objc func imagePartBtnPressed(){
        
        self.commentView.startComment(withUserID: self.askItem.publisherID,name: "发问人")
    }
    func setupCommentView(){
        
        commentView.frame = CGRect.init(x: 0, y: SCREEN_HEIGHT * 2, width: SCREEN_WIDTH, height: MAskCommentView.DefaultHeight)
        view.addSubview(commentView)
        view.setNeedsLayout()
        
        commentView.sendBtnPressedBlock = {(id,text) in
            
            if isUserNotLogin{
                self.toastFail("您尚未登录")
                self.presentVCInMainStoryboard("MLoginNaviVC")
                return
            }
            self.sendComment(withUserID: id, text: text)
        }
    }
    func makeZanRequest(ID:String){
        
        HttpController.shared.zan(type: .ask, ID: ID, successBlock: { (json) in
            DLOG("赞 问问\(ID) 成功！")
        }, failure: { (err) in
            DLOG("赞 问问\(ID) 失败 ！")
        })
    }
}

// MARK: - Navigation
extension MAskDetailVC{
    
    func sendComment(withUserID id:String,text:String){
        
        view.endEditing(true)
        
        if text.count > DefaultMaxSendTextLength {
            self.toastFail("内容太长，请删减")
            return
        }
        
        if isUserNotLogin {
            self.toastFail("请您登录后进行评论")
            presentVCInMainStoryboard("MLoginNaviVC")
            return
        }
        view.makeToastActivity()
        
        HttpController.shared.sendComment(askItem.id, commentReceiverID: id, comment: text, successBlock: { (json) in
            
            self.view.hideToastActivity()
            self.commentView.clearText()
            self.toastSuccess("评论成功")
            self.detailTV.refreshData()
            self.askItem.commentCount += 1
            self.commentSendedBlock?()
            
        }, failure: { (err) in
            
            self.view.hideToastActivity()
            self.toastFail(err.localizedDescription)
        })
        
    }
    func showUserInfoVC(withID id:String,name:String){
        
        view.endEditing(true)
        
        let uiVC:MUserInfoVC = loadViewControllerInMainStoryboard("MUserInfoVC") as! MUserInfoVC
        uiVC.hidesBottomBarWhenPushed = true
        uiVC.userID = id
        uiVC.userName = name
        hideNaviBackBtnTitle()
        navigationController?.pushViewController(uiVC, animated: true)
    }
    func handleComment(withID ID:String,isDelete:Bool){
        
        view.makeToastActivity()
        
        HttpController.shared.acceptOrDeleteComment(askItem.id, commentID: ID, isDelete: isDelete, successBlock: { (json) in
            
            self.view.hideToastActivity()
            let msg:String = (isDelete ? "删除成功" : "采纳成功")
            if !isDelete{
                self.isAskSolved = true
                self.askItem.acceptCommentID = ID
                self.acceptBlock?(ID) //列表显示已解决
            }
            self.toastSuccess(msg)
            self.detailTV.refreshData()
            
        }, failure: { (err) in
            
            self.view.hideToastActivity()
            self.toastFail(err.localizedDescription)
        })
    }
}

// MARK: - Show Views
extension MAskDetailVC{
    
    @objc func showNaviMenu(){
        
        showMenu(withUserID: "", objectID: askItem.id, name: "", isComment: false)
    }
    
    /// 菜单
    ///
    /// - Parameters:
    ///   - ID: (回复、收藏、举报)对象的ID
    ///   - objID: 问问/评论ID
    ///   - name: 用户名字
    ///   - isComment: 点击事件是否为评论区域触发
    ///   - row: 要操作的评论所在的行，评论问问不需要提供值
    func showMenu(withUserID userID:String,objectID objID:String,name:String,isComment:Bool,row:Int = 0){
        
        view.endEditing(true)
        
        if isUserNotLogin{
            self.toastFail("您尚未登录")
            self.presentVCInMainStoryboard("MLoginNaviVC")
            return
        }
        
        DispatchQueue.global().async {
            
            let sheet = UIAlertController.init(title: "", message: "请选择", preferredStyle: .actionSheet)
            if isComment{
                
                let commentAction = UIAlertAction.init(title: "回复TA", style: .default) { (action) in
                    
                    var _name = name
                    if userID == self.askItem.publisherID{
                        _name = "发问人"
                    }
                    DispatchQueue.main.async{
                        
                        self.commentView.startComment(withUserID: userID,name: _name)
                    }
                }
                sheet.addAction(commentAction)
            }else{
                if uUserID == self.askItem.publisherID{
                    
                    DispatchQueue.main.async{
                        self.commentView.startComment(withUserID: userID,name: "自己")
                    }
                    return
                }
                let collectAction = UIAlertAction.init(title: "收藏帖子", style: .default) { (action) in
                    
                    DispatchQueue.main.async{
                        if self.askItem.isFavorite{
                            
                            self.toastFail("您已经收藏过了")
                            return
                        }
                        
                        self.view.makeToastActivity()
                        HttpController.shared.collect(type:.ask,ID:objID, successBlock: { (json) in
                            
                            self.view.hideToastActivity()
                            self.askItem.isFavorite = true
                            self.toastSuccess("收藏成功")
                            self.collectBlock?()
                        }, failure: { (err) in
                            
                            self.view.hideToastActivity()
                            self.toastFail(err.localizedDescription)
                        })
                    }
                }
                sheet.addAction(collectAction)
                let dislikeAction = UIAlertAction.init(title: "屏蔽此帖子", style: .default) { (action) in
                    self.dislikeAskBlock?()
                }
                sheet.addAction(dislikeAction)
            }
            let informMsg = (isComment ? "TA" : "帖子")
            let informID = (isComment ? objID : self.askItem.id)
            let wrongAction = UIAlertAction.init(title: "举报\(informMsg)", style: .destructive) { [weak self] (action) in

                DispatchQueue.main.async{

                    if isComment{
                        self?.showInformMenu(withObjID:informID,isComment: true,row: row)
                    }else{
                        self?.showInformMenu(withObjID:informID,isComment: false)
                    }
                }
            }
            sheet.addAction(wrongAction)
            let cancelAction = UIAlertAction.init(title: "取消", style: .cancel, handler: nil)
            sheet.addAction(cancelAction)
            
            if sheet.popoverPresentationController != nil{
                if isComment{//操作评论从cell弹出
                    let indexPath = IndexPath.init(row: row, section: 0)
                    let cell = self.detailTV.cellForRow(at: indexPath)!
                    sheet.popoverPresentationController?.sourceView = cell
                    sheet.popoverPresentationController?.sourceRect = cell.bounds
                }else{//操作问问从导航栏弹出
                    sheet.popoverPresentationController?.barButtonItem = self.navigationItem.rightBarButtonItem
                }
            }
            
            DispatchQueue.main.async{
                
                self.present(sheet, animated: true, completion: nil)
            }
        }
    }
    func showOptionAlert(withCommentID id:String,name:String,isDelete:Bool){
        
        var msg = ""
        if isDelete{
            msg = "删除评论？"
        }else{
            msg = "采纳\(name)的回复？"
        }
        
        let alert = UIAlertController(title: "提示", message: msg, preferredStyle: .alert)
        
        let okAction = UIAlertAction.init(title: "是的", style: .destructive) { (action) in
            self.handleComment(withID: id, isDelete: isDelete)
        }
        let cancelAction = UIAlertAction(title: "取消", style: .default, handler: nil)
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        
        present(alert, animated: true, completion: nil)
    }
    // 举报
    func showInformMenu(withObjID ID:String,isComment:Bool,row:Int = 0){
        
        let sheet = UIAlertController.init(title: "", message: "请选择", preferredStyle: .actionSheet)
        let cheat = "欺诈骗钱"
        let cheatAction = UIAlertAction.init(title: cheat, style: .default) { [weak self] (action) in
            self?.informWithReason(cheat,objID: ID,isComment: isComment)
        }
        sheet.addAction(cheatAction)
        let violence = "色情暴力"
        let violenceAction = UIAlertAction.init(title: violence, style: .default) { [weak self] (action) in
            self?.informWithReason(violence,objID: ID,isComment: isComment)
        }
        sheet.addAction(violenceAction)
        let ad = "广告骚扰"
        let adAction = UIAlertAction.init(title: "广告骚扰", style: .default) { [weak self] (action) in
            self?.informWithReason(ad,objID: ID,isComment: isComment)
        }
        sheet.addAction(adAction)
        let otherAction = UIAlertAction.init(title: "其他", style: .default) { [weak self] (action) in
            self?.informWithReason("其他",objID: ID,isComment: isComment)
        }
        sheet.addAction(otherAction)
        let cancelAction = UIAlertAction.init(title: "取消", style: .cancel, handler: nil)
        sheet.addAction(cancelAction)
        
        if sheet.popoverPresentationController != nil{
            if isComment{//操作评论从cell弹出
                let indexPath = IndexPath.init(row: row, section: 0)
                let cell = self.detailTV.cellForRow(at: indexPath)!
                sheet.popoverPresentationController?.sourceView = cell
                sheet.popoverPresentationController?.sourceRect = cell.bounds
            }else{//操作问问从导航栏弹出
                sheet.popoverPresentationController?.barButtonItem = self.navigationItem.rightBarButtonItem
            }
        }
        
        present(sheet, animated: true, completion: nil)
    }
    func informWithReason(_ reason:String,objID ID:String,isComment:Bool){
        
        if reason == "其他" {
            
            let tsVC = loadViewControllerInMainStoryboard("MTextSendVC") as! MTextSendVC
            tsVC.hidesBottomBarWhenPushed = true
            let informType = (isComment ? TextSendOperationType.informComment : TextSendOperationType.informAsk)
            tsVC.setupWith(operationType: informType, ID: ID)
            hideNaviBackBtnTitle()
            self.navigationController?.pushViewController(tsVC, animated: true)
            return
        }
        view.makeToastActivity()
        let informType = (isComment ? InformType.comment : InformType.ask)
        HttpController.shared.inform(type:informType,ID:ID, reason: reason, successBlock: {  (json) in
            
            self.view.hideToastActivity()
            self.toastSuccess("举报成功")
            
            }, failure: { (err) in
                
                self.view.hideToastActivity()
                self.toastFail(err.localizedDescription)
        })
    }
    // 分享问问
    func showShareMenu(){
        
        let shareView = MShareView.loadFromNib()
        shareView.show()
        shareView.shareSelectedBlock = { platform in
            
            self.shareWithPlatform(platform, askItem: self.askItem)
        }
    }
    func shareWithPlatform(_ platform:MSharePlatformType,askItem:AskListItem){
        
        let image:UIImage! = UIImage.init(named: "alert_mew")
        let titleStr = askItem.title
        let descriptionStr = askItem.content
        let urlStr = askItem.link + "?id=" + askItem.id
        
        let shareMaker = MShareMaker.init(ownerVC: self, type: .ask, platform: platform, objID: askItem.id, title: titleStr, description: descriptionStr, url: urlStr, image: image)
        shareMaker.startShare()
    }
}

