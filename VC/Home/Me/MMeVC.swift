

import UIKit
import SDWebImage

class MMeVC: MBaseVC{

    @IBOutlet weak var meTV: BaseTV!
    
    // MARK: - VC Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        setupNaviBar()
        setupTableView()
        
        NotiCenter.addObserver(self, selector: #selector(refreshUserInfo), name: UserDidModifyInfoNotification, object: nil)
        NotiCenter.addObserver(self, selector: #selector(refreshUserInfo), name: UserDidLoginNotification, object: nil)
        NotiCenter.addObserver(self, selector: #selector(refreshUserInfo), name: UserDidLogoutNotification, object: nil)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        getCoins()
        if APPDELEGATE.forceTouchLaunchType == .mine{
            
            APPDELEGATE.forceTouchLaunchType = .none
            self.showMineVC(animated:false)
        }else if APPDELEGATE.forceTouchLaunchType == .contact{
            self.showMineVC(animated:false)
        }
    }
    deinit {
        NotiCenter.removeObserver(self)
    }
    
    func setupNaviBar(){
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.barTintColor = DefaultColorBlack
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        
        let settingBtn:UIButton = UIButton.init()
        settingBtn.setImage(UIImage.init(named: "me_setting"), for: .normal)
        settingBtn.sizeToFit()
        settingBtn.addTarget(self, action: #selector(showSettingVC), for: .touchUpInside)
        let msgItem:UIBarButtonItem = UIBarButtonItem.init(customView: settingBtn)
        self.navigationItem.rightBarButtonItem = msgItem
    }
    func setupTableView(){
        
        meTV.separatorStyle = .none
        meTV.register(UINib.init(nibName: MMeVCCellIdentifier, bundle: nil), forCellReuseIdentifier: MMeVCCellIdentifier)
        meTV.register(UINib.init(nibName: MMeVCHeaderCellIdentifier, bundle: nil), forCellReuseIdentifier: MMeVCHeaderCellIdentifier)
        
    }
    func getCoins(){
        
        HttpController.shared.getCoins(successBlock: { (coins,ruleItem) in
            
            let coinStr:NSString = uUserCoins as NSString
            let oldCoin:Int = coinStr.integerValue
            if oldCoin == coins{
                return
            }
            uDefaults.set(coins, forKey: "UserCoins")
            uDefaults.synchronize()
            self.refreshUserInfo()
            
        }, failure: { (err) in
            DLOG(err.localizedDescription)
        })
    }
    @objc func refreshUserInfo(){
        
        self.meTV.reloadData()
    }
}

extension MMeVC{
    
    @objc func showSettingVC(){
        
        let sVC:MSettingVC = self.loadViewControllerInMainStoryboard("MSettingVC") as! MSettingVC
        sVC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(sVC, animated: true)
    }
    func showLevelVC(){
        
        let webVC:MWebVC = loadViewControllerInMainStoryboard("MWebVC") as! MWebVC
        //TODO: <FICOW> 完善链接
        webVC.setup(title: "等级信息", url: BaseURL)
        webVC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(webVC, animated: true)
    }
    func showHelpSheet(){
        
        DispatchQueue.global().async {
            
            let sheet:UIAlertController = UIAlertController.init(title: "", message: "请选择", preferredStyle: .actionSheet)
            let introAction:UIAlertAction = UIAlertAction.init(title: "查看引导页", style: .default) { (action) in
                
                let intro:MIntroView = MIntroView() //显示引导页
                intro.alpha = 0
                WIN.addSubview(intro)
                UIView.animate(withDuration: DefaultAnimationDuration, animations: {
                    intro.alpha = 1
                })
            }
            sheet.addAction(introAction)
            let webAction:UIAlertAction = UIAlertAction.init(title: "更多帮助内容", style: .default) {(action) in
                self.showWebVC(withIndex: 2) //显示网页帮助内容
            }
            sheet.addAction(webAction)
            let cancelAction:UIAlertAction = UIAlertAction.init(title: "取消", style: .cancel, handler: nil)
            sheet.addAction(cancelAction)
            
            if sheet.popoverPresentationController != nil{
                let cell:UITableViewCell = self.meTV.cellForRow(at: IndexPath.init(row: 2, section: 1))!
                sheet.popoverPresentationController?.sourceView = cell
                sheet.popoverPresentationController?.sourceRect = cell.bounds
            }
            
            DispatchQueue.main.async {
                self.present(sheet, animated: true, completion: nil)
            }
        }
    }
    func showWebVC(withIndex index:Int){
        
        let webVC:MWebVC = self.loadViewControllerInMainStoryboard("MWebVC") as! MWebVC
        var url:String = BaseURL
        var title:String = ""
        switch index {
        case 1:
            title = "商务合作"
            url += "/business" + "?token=\(uUserToken)"
        case 2:
            title = "使用帮助"
            url += "/help"
        case 3:
            title = "反馈和建议"
            url += "/feedback" + "?token=\(uUserToken)"
        default:
            title = ""
        }
        webVC.setup(title: title, url: url)
        webVC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(webVC, animated: true)
    }
    func showLoginVC(){
        
        self.toastFail("您尚未登录")
        presentVCInMainStoryboard("MLoginNaviVC")
    }
    func showMineVC(animated:Bool){
        
        let mineVC:MMineVC = loadViewControllerInMainStoryboard("MMineVC") as! MMineVC
        mineVC.hidesBottomBarWhenPushed = true
        hideNaviBackBtnTitle()
        navigationController?.pushViewController(mineVC, animated: true)
    }
    func showShareMenu(){
        
        let shareView:MShareView = MShareView.loadFromNib()
        shareView.show()
        shareView.shareSelectedBlock = { platform in
            
            self.shareWithPlatform(platform)
        }
    }
    func shareWithPlatform(_ platform:MSharePlatformType){
        
        let image:UIImage! = UIImage.init(named: "alert_mew")
        let titleStr:String = "广大通"
        let descriptionStr:String = "免费、有料、能救急的校园生活小助理~"
        
        let shareMaker:MShareMaker = MShareMaker.init(ownerVC: self, type: .app, platform: platform, objID: "", title: titleStr, description: descriptionStr, url: BaseURL, image: image)
        shareMaker.startShare()
    }
}

extension MMeVC:UITableViewDataSource,UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }
        return 3
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let headerCell:MMeVCHeaderCell = tableView.dequeueReusableCell(withIdentifier: MMeVCHeaderCellIdentifier) as! MMeVCHeaderCell
            headerCell.selectionStyle = .none
            headerCell.loadUserInfo()
            headerCell.iconPressedBlock = {
                self.showLoginVC()
            }
            headerCell.levelPressedBlock = { [weak self] in
                self?.showLevelVC()
            }
            return headerCell
        }
        let normalCell = tableView.dequeueReusableCell(withIdentifier: MMeVCCellIdentifier) as! MMeVCCell
        normalCell.selectionStyle = .none
        
        var title:String = ""
        var icon:UIImage! = nil
        switch indexPath.row {
        case 0:
            title = "我的"
            icon = UIImage.init(named: "me_mine")
//        case 1:
//            title = "商务合作"
//            icon = UIImage.init(named: "me_business")
//        case 2:
//            title = "使用帮助"
//            icon = UIImage.init(named: "me_help")
//        case 3:
//            title = "反馈和建议"
//            icon = UIImage.init(named: "me_feedback")
        case 1:
            title = "分享广大通App"
            icon = UIImage.init(named: "app_share")
        case 2:
            title = "关于"
            icon = UIImage.init(named: "me_about")
        default:
            break
        }
        normalCell.setup(title, image: icon)
        return normalCell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return MMeVCHeaderCellHeight
        }
        return MMeVCCellHeight
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
        if indexPath.section == 0 {
            if isUserNotLogin {
                showLoginVC()
                return
            }
            let uiVC:MUserInfoVC = self.loadViewControllerInMainStoryboard("MUserInfoVC") as! MUserInfoVC
            uiVC.hidesBottomBarWhenPushed = true
            uiVC.userID = uUserID
            uiVC.userName = uUserName
            self.hideNaviBackBtnTitle()
            self.navigationController?.pushViewController(uiVC, animated: true)
            return
        }
        let row:Int = indexPath.row
        if row == 0 {
            if isUserNotLogin {
                showLoginVC()
                return
            }
            self.showMineVC(animated:true)
            return
        }
        if row == 1{
            showShareMenu()
            return
        }
        if row == 2{
            let aboutVC:MAboutVC = loadViewControllerInMainStoryboard("MAboutVC") as! MAboutVC
            aboutVC.hidesBottomBarWhenPushed = true
            hideNaviBackBtnTitle()
            navigationController?.pushViewController(aboutVC, animated: true)
        }
    }
}
