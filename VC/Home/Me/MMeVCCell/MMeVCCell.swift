

import UIKit

let MMeVCCellIdentifier = "MMeVCCell"
let MMeVCCellHeight:CGFloat = 44

class MMeVCCell: MBaseTVCell {

    @IBOutlet weak var iconIV: UIImageView!
    @IBOutlet weak var titleLB: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        addBottomSeparateLine()
    }
    func setup(_ title:String,iconURL:String){
        titleLB.text = title
        iconIV.sd_setImage(with: URL.init(string: iconURL), placeholderImage: DefaultUserIcon)
    }
    func setup(_ title:String,image:UIImage){
        titleLB.text = title
        iconIV.image = image
    }
}
