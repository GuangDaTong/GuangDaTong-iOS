

import UIKit

let MMeVCHeaderCellIdentifier = "MMeVCHeaderCell"
let MMeVCHeaderCellHeight:CGFloat = 94

class MMeVCHeaderCell: MBaseTVCell {

    @IBOutlet weak var iconBtn: UIButton!
    @IBOutlet weak var nicknameLB: UILabel!
    @IBOutlet weak var levelBtn: UIButton!
    @IBOutlet weak var coinLB: UILabel!
    @IBOutlet weak var coinIV: UIImageView!
    
    var iconPressedBlock:(() -> Void)?
    var levelPressedBlock:(() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        loadUserInfo()
        addBottomSeparateLine()
        iconBtn.setCornerRounded(radius: 4)
        iconBtn.imageView?.contentMode = .scaleAspectFill
        iconBtn.imageView?.clipsToBounds = true
        levelBtn.setCornerRounded(radius: 8)
    }

    func loadUserInfo(){
        
        let coins:String = uUserCoins
        let coinStr:String = coins.count > 0 ? coins : "0"
        let attrStr:NSMutableAttributedString = NSMutableAttributedString.init(string: "金币: \(coinStr)")
        let range:NSRange = NSRange.init(location: 3, length: attrStr.length - 3)
        attrStr.addAttributes([NSAttributedStringKey.foregroundColor:UIColor.colorWithHex(value: 0xFE3824, alpha: 1)], range: range)
        coinLB.attributedText = attrStr
        
        if uUserID.count == 0 {
            iconBtn.setImage(DefaultUserIcon, for: .normal)
            nicknameLB.text = "未登录"
            levelBtn.isHidden = true
            return
        }
        iconBtn.sd_setImage(with: URL.init(string: uUserIcon), for: .normal, placeholderImage: DefaultUserIcon)
        nicknameLB.text = uUserName
        
//        levelBtn.isHidden = false
        let levelStr:String = uDefaults.string(forKey: "UserLevel") ?? "1"
        let level:String = "LV \(levelStr)"
        levelBtn.setTitle(level, for: .normal)
    }
    @IBAction func iconBtnPressed(_ sender: UIButton) {
        
        let url:String = uUserIcon
        if url != ""{
            showImageBrowser(withURLStr: url)
        }else{
            iconPressedBlock?()
        }
    }
    @IBAction func levelBtnPressed(_ sender: Any) {
        
        if let block = levelPressedBlock {
            block()
        }
    }
    
}
