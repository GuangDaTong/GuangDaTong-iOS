

import UIKit
import SDWebImage

class TabVC: UITabBarController,UITabBarControllerDelegate {
    
    let updater:MUpdateChecker = MUpdateChecker()
    let location:MLocationManager = MLocationManager()
    var isLaunchAnimationShowed:Bool = false //启动动画只显示一次
    var isIntroPageShowed:Bool = false //当次启动且引导页显示之后，不显示启动动画
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.delegate = self
        self.edgesForExtendedLayout = UIRectEdge.init(rawValue: 0)
        self.navigationController?.navigationBar.isHidden = true
        self.tabBar.tintColor = AppThemeColor
        
        // 登录即上传推送ID和位置信息
        NotiCenter.addObserver(self, selector: #selector(uploadUserInfo), name: UserDidLoginNotification, object: nil)
        // 获取到位置信息即上传
        NotiCenter.addObserver(self, selector: #selector(uploadUserInfo), name: UserLocationDidObtainNotification, object: nil)
        NotiCenter.addObserver(self, selector: #selector(showLoginVC), name: UserDidLogoutNotification, object: nil)
        
        location.beginLocate()
        delay(second: 1.3) {
            self.updater.beginCheck(isManualCheck: false)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        showLaunchImage()
    }
    deinit {
        NotiCenter.removeObserver(self)
    }
    func showLaunchImage(){
        
        showLaunchAnimation()
    }
    func showLaunchAnimation(){
        
        if isLaunchAnimationShowed{
            return
        }
        if isIntroPageShowed{
            return
        }
        isLaunchAnimationShowed = true
        APP.setStatusBarHidden(false, with: .fade)
    }
    
    @objc func showLoginVC(){
        
        delay(second: 0.5) { 
            self.presentVCInMainStoryboard("MLoginNaviVC")
        }
    }
    
    // MARK: - Location & RegistrationID
    @objc func uploadUserInfo(){
        
        let country:String = uDefaults.string(forKey: "UserLocation_country") ?? ""
        let province:String = uDefaults.string(forKey: "UserLocation_province") ?? ""
        let city:String = uDefaults.string(forKey: "UserLocation_city") ?? ""
        let district:String = uDefaults.string(forKey: "UserLocation_district") ?? ""
        let county:String = uDefaults.string(forKey: "UserLocation_county") ?? ""
        let street:String = uDefaults.string(forKey: "UserLocation_street") ?? ""
        let name:String = uDefaults.string(forKey: "UserLocation_name") ?? ""
        
        let regID:String = uDefaults.string(forKey: "registrationID") ?? ""
        // 没有registrationID 就不上传
        guard regID != "" else {
            return
        }
        
        HttpController.shared.uploadUserInfo(regID, locationDict: ["country":country,"province":province,"city":city,"district":district,"county":county,"street":street,"siteName":name], success: { (json) in
        }) { (err) in
        }
    }
    // MARK: - UITabBarControllerDelegate
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        
        if tabBarController.selectedIndex == 3 {
        }
    }
}
