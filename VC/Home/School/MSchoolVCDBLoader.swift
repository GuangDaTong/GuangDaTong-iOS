import Foundation

extension MSchoolVC{
    
    func loadDB(){
        
        let q:FMDatabaseQueue = FMDatabaseQueue(url: DefaultURLForDB)
        q.inDatabase { (db) in
            
            if !self.createTable(db: db){
                return
            }
            db.select(from: DBTableNameForSchool, keys: dbKeys, condition: " WHERE listType = 'guidance'", { (res) in
                
                guard let r = res else{ return }
                self.guidanceTV.dataArr = self.parseResult(r)
            })
            db.select(from: DBTableNameForSchool, keys: dbKeys, condition: " WHERE listType = 'summary'", { (res) in
                
                guard let r = res else{ return }
                self.summaryTV.dataArr = self.parseResult(r)
            })
            db.select(from: DBTableNameForSchool, keys: dbKeys, condition: " WHERE listType = 'club'", { (res) in
                
                guard let r = res else{ return }
                self.clubTV.dataArr = self.parseResult(r)
            })
            db.select(from: DBTableNameForSchool, keys: dbKeys, condition: " WHERE listType = 'look'", { (res) in
                
                guard let r = res else{ return }
                self.lookTV.dataArr = self.parseResult(r)
            })
        }
    }
    func parseResult(_ r:FMResultSet) -> [SchoolListItem]{
        
        var list:[SchoolListItem] = [SchoolListItem]()
        while r.next(){
            let id:String = r.string(forCol: 0)
            let link:String = r.string(forCol: 1)
            let title:String = r.string(forCol: 2)
            let type:String = r.string(forCol: 3)
            let isCollect:Bool = r.bool(forCol: 4)
            let isZan:Bool = r.bool(forCol: 5)
            let zanCount:Int = r.int(forCol: 6)
            let readCount:Int = r.int(forCol: 7)
            let item:SchoolListItem = SchoolListItem.init(id: id, link: link, title: title, type: type, isCollect: isCollect, isZan: isZan, zanCount: zanCount, readCount: readCount, favorID: "")
            list.append(item)
        }
        return list
    }
    func createTable(db:FMDatabase) -> Bool{
        
        guard db.create(DBTableNameForSchool,
                        colDict:["id":.text,
                                 "link":.text,
                                 "title":.text,
                                 "type":.text,
                                 "isCollect":.bool,
                                 "isZan":.bool,
                                 "zanCount":.int,
                                 "readCount":.int,
                                 "listType":.text]) else{
                                    DLOG("create table failed")
                                    return false
        }
        return true
    }
    func saveToDB(tv:MBaseTV){
        
        let q:FMDatabaseQueue = FMDatabaseQueue(url: DefaultURLForDB)
        q.inDatabase { (db) in
            
            var type:String = ""
            if tv == self.guidanceTV{
                type = "guidance"
            }
            if tv == self.summaryTV{
                type = "summary"
            }
            if tv == self.clubTV{
                type = "club"
            }
            if tv == self.lookTV{
                type = "look"
            }
            if !db.delete(from: DBTableNameForSchool, condition: "WHERE listType = '\(type)'"){
                DLOG("删除校园\(type)旧数据，失败！！！")
                return
            }
            for item in tv.dataArr{
                let i:SchoolListItem = item as! SchoolListItem
                _ = db.insert(into: DBTableNameForSchool, dict: ["id":i.id,
                                                                 "link":i.link,
                                                                 "title":i.title,
                                                                 "type":i.type,
                                                                 "isCollect":i.isCollect,
                                                                 "isZan":i.isZan,
                                                                 "zanCount":i.zanCount,
                                                                 "readCount":i.readCount,
                                                                 "listType":type])
            }
        }
    }
}
