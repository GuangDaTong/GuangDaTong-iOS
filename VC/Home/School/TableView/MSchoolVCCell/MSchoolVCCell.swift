import UIKit

enum SchoolVCCellType {
    case normal
    case hot
    case new
}

let MSchoolVCCellIdentifier = "MSchoolVCCell"
class MSchoolVCCell: MBaseTVCell {

    @IBOutlet weak var hotIV: UIImageView!
    @IBOutlet weak var hotIVWidth: NSLayoutConstraint!
    @IBOutlet weak var titleLB: UILabel!
    @IBOutlet weak var titleLBLeading: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        titleLB.text = ""
        addBottomSeparateLine()
    }
    func setup(withTitle title:String,type:SchoolVCCellType){
        titleLB.text = title
        switch type {
        case .normal:
            titleLBLeading.constant = 0
            hotIVWidth.constant = 0
            hotIV.isHidden = true
        case .new:
            titleLBLeading.constant = 8
            hotIVWidth.constant = 30
            hotIV.isHidden = false
            hotIV.image = UIImage.init(named: "school_new")
        case .hot:
            titleLBLeading.constant = 8
            hotIVWidth.constant = 30
            hotIV.isHidden = false
            hotIV.image = UIImage.init(named: "school_hot")
        }

    }
    
}
