import UIKit
import SwiftyJSON

class MSchoolTV: MBaseTV{
    
    weak var ownerVC:MBaseVC?
    var isLoadForCollectList = false
    var listType:SchoolListType = .guidance
    var cellPressedBlock:((_ index:Int) -> Void)?
    
    override init(frame: CGRect, style: UITableViewStyle) {
        super.init(frame: frame, style: style)
        
        setup()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    func setup(){
        
        setup(withDelegate: self)
    }
}

extension MSchoolTV:MBaseTVDelegate{
    
    func loadData(pageIndex: Int) {
        
        if isLoadForCollectList{
            loadForCollect(pageIndex: pageIndex)
        }else{
            loadForSchoolVC(pageIndex: pageIndex)
        }
    }
    func loadForSchoolVC(pageIndex: Int){
        
        HttpController.shared.school(type: self.listType, pageIndex: pageIndex, successBlock: { (json) in
            
            self.parseJSON(json)
            
            }, failure: { (err) in
                
                self.loadFailed()
                DLOG(err.localizedDescription)
                self.ownerVC?.toastFail(err.localizedDescription)
        })
    }
    func loadForCollect(pageIndex: Int){
        
        HttpController.shared.schoolCollectList(type: self.listType, pageIndex: pageIndex, successBlock: { (json) in
            
            self.parseJSON(json)
            
        }, failure: {(err) in
            
            self.loadFailed()
            DLOG(err.localizedDescription)
            self.ownerVC?.toastFail(err.localizedDescription)
        })
    }
    func parseJSON(_ json:JSON){
        
        var dataArr = [SchoolListItem]()
        for data in json["result"]["data"].arrayValue{
            let id = data["id"].stringValue
            let title = data["title"].stringValue
            let type = data["type"].stringValue
            let link = data["link"].stringValue
            let isCollect = (data["isFavorite"].stringValue == "1" ? true : false)
            let isZan = (data["isZan"].stringValue == "1" ? true : false)
            let zanCount = (data["zan"].stringValue as NSString).integerValue
            let readCount = (data["readCount"].stringValue as NSString).integerValue
            let favorID = data["favorID"].stringValue
            
            let item = SchoolListItem.init(id: id, link: link, title: title, type: type, isCollect: isCollect, isZan: isZan, zanCount: zanCount, readCount: readCount, favorID: favorID)
            dataArr.append(item)
        }
        self.appendData(dataArr)
    }
    func nibsToRegister() -> [String] {
        return [MSchoolVCCellIdentifier]
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return dataArr.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: MSchoolVCCellIdentifier) as! MSchoolVCCell
        let row = indexPath.row
        var type = SchoolVCCellType.normal
        let item = dataArr[row] as! SchoolListItem
        switch item.type {
        case "HOT":
            type = .hot
        case "NEW":
            type = .new
        default:
            type = .normal
        }
        cell.setup(withTitle: item.title, type: type)
        
        cell.tintColor = DefaultColorRed
        if tableView.isEditing{
            cell.selectionStyle = .default
        }else{
            cell.selectionStyle = .none
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        
        let value = UITableViewCellEditingStyle.delete.rawValue |
            UITableViewCellEditingStyle.insert.rawValue
        return UITableViewCellEditingStyle.init(rawValue: value)!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView.isEditing{
            return
        }
        tableView.deselectRow(at: indexPath, animated: true)
        cellPressedBlock?(indexPath.row)
    }
}

