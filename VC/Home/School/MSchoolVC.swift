import UIKit
import SDCycleScrollView

class AdItem:NSObject,NSCoding {
    var imageURL:String = ""
    var url:String = ""
    var title:String = ""
    init(imageURL:String,url:String,title:String) {
        super.init()
        self.imageURL = imageURL
        self.url = url
        self.title = title
    }
    func encode(with aCoder: NSCoder){
        aCoder.encode(imageURL, forKey: "imageURL")
        aCoder.encode(url, forKey: "url")
        aCoder.encode(title, forKey: "title")
    }
    required init?(coder aDecoder: NSCoder){
        self.imageURL = aDecoder.decodeObject(forKey: "imageURL") as? String  ?? ""
        self.url = aDecoder.decodeObject(forKey: "url") as? String ?? ""
        self.title = aDecoder.decodeObject(forKey: "title") as? String  ?? ""
    }
}

struct SchoolListItem {
    var id:String
    var link:String
    var title:String
    var type:String
    var isCollect:Bool
    var isZan:Bool
    var zanCount:Int
    var readCount:Int
    var favorID:String //用于收藏列表
}

class MSchoolVC: MBaseVC,UIScrollViewDelegate,SDCycleScrollViewDelegate,MHorizontalTabViewDelegate {

    // MARK: - Views
    var adView:SDCycleScrollView! = nil
    var horizontalTab:MHorizontalTabView! = nil
    var tabView:MHorizontalTabView{
        get{
            if horizontalTab != nil {
                return horizontalTab
            }
            horizontalTab = MHorizontalTabView()
            horizontalTab.delegate = self
            horizontalTab.setup(withTitles: ["特别推荐","图文咨询","最近更新","本月热点"])
            return horizontalTab
        }
        set{
            horizontalTab = newValue
        }
    }
    var scrollView:UIScrollView = UIScrollView.init(frame: CGRect.zero)
    var guidanceTV:MSchoolTV = MSchoolTV.init(frame: CGRect.zero, style: UITableViewStyle.plain)
    var summaryTV:MSchoolTV = MSchoolTV.init(frame: CGRect.zero, style: UITableViewStyle.plain)
    var clubTV:MSchoolTV = MSchoolTV.init(frame: CGRect.zero, style: UITableViewStyle.plain)
    var lookTV:MSchoolTV = MSchoolTV.init(frame: CGRect.zero, style: UITableViewStyle.plain)
    let msgBtn:UIButton = UIButton.init()
    var badgeView:UIView?
    
    // MARK: - Property
    var AdArr:[AdItem] = [AdItem]()
    let AdArrFilePath:String = NSHomeDirectory() + "/Documents/AdArr.plist"
    let ADPlaceholderIV:UIImageView = UIImageView.init(image: UIImage.init(named: "ADImage"))
    let dbKeys:[String] = ["id","link","title","type","isCollect","isZan","zanCount","readCount","listType"]
    /// 启动后第一次加载时，自动刷新
    var isFirstLoad:[Bool] = [Bool].init(repeating: true, count: 4)
    
    // MARK: - VC Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotiCenter.addObserver(self, selector: #selector(checkServerMsg), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
        NotiCenter.addObserver(self, selector: #selector(checkServerMsg), name: UserDidReceiverRemoteNotification, object: nil)
        setupUI()
        loadLocalAd()
        loadRemoteAd()
        horizontalTab.selectIndex(0)
        DispatchQueue.global().async {
            self.loadDB()
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // 调整广告的位置
        if self.adView != nil {
            if adView.imageURLStringsGroup != nil{
                adView.adjustWhenControllerViewWillAppera()
            }
        }
        checkServerMsg()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if APPDELEGATE.forceTouchLaunchType == .msgCenter{
            
            APPDELEGATE.forceTouchLaunchType = .none
            self.showMsgCenter()
        }
    }
    
    // MARK: - Setup
    func setupUI(){
        
        setupNaviBar()
        setupADView()
        setupTabView()
        setupScrollView()
        setupTableView()
    }
    func setupNaviBar(){
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.barTintColor = DefaultColorBlack
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        
        
//        msgBtn.setImage(UIImage.init(named: "navi_msg"), for: .normal)
//        msgBtn.sizeToFit()
//        msgBtn.addTarget(self, action: #selector(showMsgCenter), for: .touchUpInside)
//        let msgItem = UIBarButtonItem.init(customView: msgBtn)
//        self.navigationItem.rightBarButtonItem = msgItem
    }
    /// 检查未读消息
    @objc func checkServerMsg(){
        
        if isUserNotLogin{
            return
        }
        MMsgLoader.loadServerMsg() //消息中心数据
        checkMsgBadge()
    }
    /// 未读消息Badge
    func checkMsgBadge(){
        
        MMsgLoader.countUnreadMsgFor(.leaveMsg) { (unreadCount) in
            
            if unreadCount > 0{
                self.showBadgeView()
                return
            }
            MMsgLoader.countUnreadMsgFor(.askMsg) { (unreadCount) in
                
                if unreadCount > 0{
                    self.showBadgeView()
                    return
                }
                MMsgLoader.countUnreadMsgFor(.notice) { (unreadCount) in
                    
                    if unreadCount > 0{
                        self.showBadgeView()
                        return
                    }
                    DispatchQueue.main.async{
                        self.badgeView?.removeFromSuperview()
                        self.badgeView = nil
                    }
                }
            }
        }
    }
    func showBadgeView(){
        
        DispatchQueue.main.async{
            
            if let _ = self.badgeView?.superview{
                return
            }
            let badge = UIView()
            self.badgeView = badge
            badge.backgroundColor = DefaultColorRed
            let halfWidth:CGFloat = 5
            badge.setCornerRounded(radius: halfWidth)
            self.msgBtn.addSubview(badge)
            badge.snp.makeConstraints { (m) in
                let width = 2 * halfWidth
                m.size.equalTo(CGSize.init(width: width, height: width))
                m.top.equalTo(self.msgBtn.snp.top).offset(-halfWidth + 2)
                m.trailing.equalTo(self.msgBtn.snp.trailing).offset(halfWidth - 2)
            }
        }
    }
    func setupADView(){
        
        adView = SDCycleScrollView.init(frame: CGRect.zero, delegate: self, placeholderImage: nil) // 初始化轮播图
        adView.autoScrollTimeInterval = 6 // 滚动时间间隔
        adView.pageControlBottomOffset = -6 //页码标识器距离轮播图底部的距离
        adView.clipsToBounds = true // 切割不规范的图片超出的部分
        adView.accessibilityIdentifier = "横幅广告"
        self.view.addSubview(adView)
        adView.snp.makeConstraints { (make) in
            make.leading.equalTo(view.snp.leading)
            make.trailing.equalTo(view.snp.trailing)
            make.top.equalTo(view.snp.top)
            make.height.equalTo(180 / 667 * SCREEN_HEIGHT)
        }
        self.view.addSubview(ADPlaceholderIV)
        ADPlaceholderIV.snp.makeConstraints { (make) in
            make.leading.equalTo(adView.snp.leading)
            make.trailing.equalTo(adView.snp.trailing)
            make.top.equalTo(adView.snp.top)
            make.bottom.equalTo(adView.snp.bottom)
        }
        
        let ADmask = UIImageView.init(image: UIImage.init(named: "ADmask")) // 一层浅黑色的图片，用于增强显示效果
        ADmask.alpha = 0.6
        self.view.addSubview(ADmask)
        ADmask.snp.makeConstraints { (make) in
            make.leading.equalTo(view.snp.leading)
            make.trailing.equalTo(view.snp.trailing)
            make.bottom.equalTo(adView.snp.bottom)
            make.height.equalTo(44 / 667 * SCREEN_HEIGHT)
        }
    }
    func setupTabView(){
        
        self.view.addSubview(tabView)
        horizontalTab.snp.makeConstraints { (make) in
            make.leading.equalTo(view.snp.leading)
            make.trailing.equalTo(view.snp.trailing)
            make.top.equalTo(adView.snp.bottom)
            make.height.equalTo(tabView.defaultHeight)
        }
    }
    func setupScrollView(){
        
        scrollView.contentSize = CGSize.init(width: GetView_Width(view: self.view) * 4, height: 0)
        scrollView.delegate = self
        scrollView.isPagingEnabled = true
        
        self.view.addSubview(scrollView)
        scrollView.snp.makeConstraints { (make) in
            make.leading.equalTo(view.snp.leading)
            make.trailing.equalTo(view.snp.trailing)
            make.top.equalTo(horizontalTab.snp.bottom).offset(4)
            make.bottom.equalTo(view.snp.bottom)
        }
        
        scrollView.addSubview(guidanceTV)
        scrollView.addSubview(summaryTV)
        scrollView.addSubview(clubTV)
        scrollView.addSubview(lookTV)
        
        guidanceTV.ownerVC = self
        summaryTV.ownerVC = self
        clubTV.ownerVC = self
        lookTV.ownerVC = self
    }
    func setupTableView(){
        
        let tvLoadFinishBlock:((MBaseTV,Int) -> Void) = { tv,pageIndex in
            if pageIndex != 0 { return }
            self.saveToDB(tv: tv)
            if tv == self.guidanceTV && self.adView.imageURLStringsGroup == nil{
                self.loadRemoteAd() //重新加载广告，应对首次安装请求网络权限时加载广告失败的情况
            }
        }
        guidanceTV.listType = .guidance
        guidanceTV.loadFinishBlock = tvLoadFinishBlock
        guidanceTV.cellPressedBlock = { [weak self] (index) in
            
            self?.showWebVC(listType: .guidance, index: index, bottomBarType: .zan,
                            navigationItemType: .menuWithFeedback, zanType: .guidance)
        }
        guidanceTV.separatorStyle = .none
        guidanceTV.snp.makeConstraints { (make) in
            make.leading.equalTo(scrollView.snp.leading)
            make.top.equalTo(scrollView.snp.top)
            make.width.equalTo(view.snp.width)
            make.height.equalTo(scrollView.snp.height)
        }
        
        summaryTV.listType = .summary
        summaryTV.loadFinishBlock = tvLoadFinishBlock
        summaryTV.cellPressedBlock = { [weak self] (index) in
            
            self?.showWebVC(listType: .summary, index: index, bottomBarType: .zan,
                            navigationItemType: .menuWithFeedback, zanType: .summary)
        }
        summaryTV.separatorStyle = .none
        summaryTV.snp.makeConstraints { (make) in
            make.leading.equalTo(guidanceTV.snp.trailing)
            make.top.equalTo(scrollView.snp.top)
            make.width.equalTo(view.snp.width)
            make.height.equalTo(scrollView.snp.height)
        }
        
        clubTV.listType = .club
        clubTV.loadFinishBlock = tvLoadFinishBlock
        clubTV.cellPressedBlock = { [weak self] (index) in
            
            self?.showWebVC(listType: .club, index: index, bottomBarType: .zan,
                            navigationItemType: .menuWithFeedback, zanType: .club)
        }
        clubTV.separatorStyle = .none
        clubTV.snp.makeConstraints { (make) in
            make.leading.equalTo(summaryTV.snp.trailing)
            make.top.equalTo(scrollView.snp.top)
            make.width.equalTo(view.snp.width)
            make.height.equalTo(scrollView.snp.height)
        }
        
        lookTV.listType = .look
        lookTV.loadFinishBlock = tvLoadFinishBlock
        lookTV.cellPressedBlock = { [weak self] (index) in
            
            self?.showWebVC(listType: .look, index: index, bottomBarType: .zan,
                            navigationItemType: .menuWithFeedback, zanType: .look)
        }
        lookTV.separatorStyle = .none
        lookTV.snp.makeConstraints { (make) in
            make.leading.equalTo(clubTV.snp.trailing)
            make.top.equalTo(scrollView.snp.top)
            make.width.equalTo(view.snp.width)
            make.height.equalTo(scrollView.snp.height)
        }
    }
    
    // MARK: - Setup Data
    func loadLocalAd(){
        
        let urlArr = NSKeyedUnarchiver.unarchiveObject(withFile: AdArrFilePath) as? [AdItem]
        if urlArr != nil{
            self.AdArr = urlArr!
            if self.AdArr.count == 0{ //广告图为0张
                return
            }
            ADPlaceholderIV.isHidden = true
            var imageArr = [String]()
            for item in AdArr{
                let imageURL = item.imageURL
                imageArr.append(imageURL)
            }
            adView.imageURLStringsGroup = imageArr
        }
    }
    func loadRemoteAd(){
        
        HttpController.shared.advertisement(successBlock: { (json) in
            
            let dataArray = json["result"]["data"].arrayValue
            if dataArray.count == 0{  //广告图为0张
                return
            }
            var imageArr = [String]()
            self.AdArr.removeAll()
            for json in dataArray{
                let imageURL = json["image"].stringValue
                let url = json["link"].stringValue
                let title = json["title"].stringValue
                imageArr.append(imageURL)
                let item = AdItem.init(imageURL: imageURL, url: url,title:title)
                self.AdArr.append(item)
            }
            
            self.adView.autoScroll = false
            self.adView.imageURLStringsGroup = imageArr
            self.adView.autoScroll = true
            self.saveURLs()
            delay(second: 0.8, block: {
                self.ADPlaceholderIV.isHidden = true
            })
        }, failure: { (err) in
            DLOG(err.localizedDescription)
        })
    }
    
    func saveURLs(){
        let urls = AdArr
        let manager = FileManager.default
        if !manager.fileExists(atPath: AdArrFilePath){
            let created = manager.createFile(atPath: AdArrFilePath, contents: nil, attributes: nil)
            if created{
                NSKeyedArchiver.archiveRootObject(urls, toFile: AdArrFilePath)
                return
            }else{
                DLOG("*** 创建广告文件目录失败！***")
                return
            }
        }
        let success = NSKeyedArchiver.archiveRootObject(urls, toFile: AdArrFilePath)
        if !success{
            DLOG("*** 保存广告文件失败！***")
        }
    }
    
    // MARK: - Navigation
    func showWebVC(listType:SchoolListType,index:Int,bottomBarType:WebVCBottomBarType,navigationItemType:WebVCRightNavigationItemType,zanType:ZanType){
        
        var tableView:MSchoolTV! = nil
        switch listType {
        case .guidance:
            tableView = guidanceTV
        case .summary:
            tableView = summaryTV
        case .club:
            tableView = clubTV
        case .look:
            tableView = lookTV
        }
        var item:SchoolListItem = tableView.dataArr[index] as! SchoolListItem
        let webVC:MWebVC = self.loadViewControllerInMainStoryboard("MWebVC") as! MWebVC
        let url:String = item.link + "?id=\(item.id)"
        webVC.setup(title: item.title, url: url, bottomBarType: bottomBarType, rightNavigationItemType: navigationItemType, zanType: zanType,itemID: item.id,isCollect: item.isCollect,isZan: item.isZan,readCount: item.readCount,zanCount: item.zanCount)
        webVC.zanBlock = {
            item.zanCount += 1
            item.isZan = true
            tableView.dataArr[index] = item
        }
        webVC.collectBlock = {
            item.isCollect = true
            tableView.dataArr[index] = item
        }
        webVC.readBlock = {
            item.readCount += 1
            tableView.dataArr[index] = item
        }
        webVC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(webVC, animated: true)
    }
    @objc func showMsgCenter(){
        
        showMsgVC(animated: true, msgTab: -1)
    }
    func showMsgVC(animated:Bool = true,msgTab:Int){
        
        if isUserNotLogin {
            self.toastFail("您尚未登录")
            self.presentVCInMainStoryboard("MLoginNaviVC")
            return
        }
        let msgVC = loadViewControllerInMainStoryboard("MMsgCenterVC") as! MMsgCenterVC
        msgVC.selectedTabForPush = msgTab
        msgVC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(msgVC, animated: animated)
    }

    
    // MARK: - SDCycleScrollView Delegate
    func cycleScrollView(_ cycleScrollView: SDCycleScrollView!, didSelectItemAt index: Int) {
        
        DLOG("index\(index)")
        let webVC = loadViewControllerInMainStoryboard("MWebVC") as! MWebVC
        let item = AdArr[index]
        webVC.setup(title: item.title, url: item.url, bottomBarType: .none, rightNavigationItemType: .none, zanType: .none)
        webVC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(webVC, animated: true)
    }
    // MARK: - HorizontalTabView Delegate
    func horizontalTabViewDidSelect(_ index:Int){
        
        let point = CGPoint.init(x: CGFloat(index) * GetView_Width(view: self.view), y: 0)
        scrollView.setContentOffset(point, animated: true)
        scrollView.flashScrollIndicators()
        
        switch index {
        case 0:
            if isFirstLoad[0]{
                isFirstLoad[0] = false
                guidanceTV.refreshData()
                return
            }
            if guidanceTV.dataArr.count == 0{
                guidanceTV.refreshData()
            }
        case 1:
            if isFirstLoad[1]{
                isFirstLoad[1] = false
                summaryTV.refreshData()
                return
            }
            if summaryTV.dataArr.count == 0{
                summaryTV.refreshData()
            }
        case 2:
            if isFirstLoad[2]{
                isFirstLoad[2] = false
                clubTV.refreshData()
                return
            }
            if clubTV.dataArr.count == 0{
                clubTV.refreshData()
            }
        case 3:
            if isFirstLoad[3]{
                isFirstLoad[3] = false
                lookTV.refreshData()
                return
            }
            if lookTV.dataArr.count == 0{
                lookTV.refreshData()
            }
        default:
            break
        }
    }
    // MARK: - UIScrollView Delegate
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        if scrollView == self.scrollView {
            let index = Int(targetContentOffset.pointee.x / GetView_Width(view: self.view) + 0.1)
            if horizontalTab.selectedIndex == index{
                return
            }
            horizontalTab.selectIndex(index)
        }
    }
}
