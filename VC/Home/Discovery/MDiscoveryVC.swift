import UIKit


/// 发现列表类型
///
/// - partime: 兼职
/// - training: 培训
/// - activity: 团体活动
enum DiscoveryListType:Int {
    case partTime = 0
    case training
    case activity
}


/// 发现列表item
struct DiscoveryListItem {
    var id:String
    var icon:String
    var title:String
    var createTime:String
    var position:String //地址
    var fee:String
    var description:String
    var link:String
    var isCollect:Bool
    var isZan:Bool
    var zanCount:Int
    var readCount:Int
    var cellHeight:CGFloat
    var favorID:String //用于收藏列表
}

class MDiscoveryVC: MBaseVC,UIScrollViewDelegate,MHorizontalTabViewDelegate {

    // MARK: - Views
    var horizontalTab:MHorizontalTabView! = nil
    var tabView:MHorizontalTabView{
        get{
            if horizontalTab != nil {
                return horizontalTab
            }
            horizontalTab = MHorizontalTabView()
            horizontalTab.delegate = self
            horizontalTab.setup(withTitles: ["面试","学习","职场"])
            return horizontalTab
        }
        set{
            horizontalTab = newValue
        }
    }
    var scrollView:UIScrollView = UIScrollView.init(frame: CGRect.zero)
    var partTimeTV:MDiscoveryPartTimeTV = MDiscoveryPartTimeTV.init(frame: CGRect.zero, style: UITableViewStyle.plain)
    var trainingTV:MDiscoveryTrainingTV = MDiscoveryTrainingTV.init(frame: CGRect.zero, style: UITableViewStyle.plain)
    var activityTV:MDiscoveryActivityTV = MDiscoveryActivityTV.init(frame: CGRect.zero, style: UITableViewStyle.plain)
    let dbKeys:[String] = ["id","icon","title","createTime","position","fee","description","link","isCollect","isZan","zanCount","readCount","cellHeight","listType"]
    /// 启动后第一次加载时，自动刷新
    var isFirstLoad:[Bool] = [Bool].init(repeating: true, count: 3)
    
    // MARK: - VC Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        setupNaviBar()
        setupTabView()
        setupScrollView()
        setupTableView()
        horizontalTab.selectIndex(1)
        DispatchQueue.global().async {
            self.loadDB()
        }
    }
    
    // MARK: - Setup UI
    func setupNaviBar(){
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.barTintColor = DefaultColorBlack
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        
    }
    func setupTabView(){
        
        self.view.addSubview(tabView)
        tabView.snp.makeConstraints { (make) in
            make.leading.equalTo(view.snp.leading)
            make.trailing.equalTo(view.snp.trailing)
            make.top.equalTo(view.snp.top)
            make.height.equalTo(tabView.defaultHeight)
        }
    }
    func setupScrollView(){
        
        scrollView.contentSize = CGSize.init(width: GetView_Width(view: self.view) * 3, height: 0)
        scrollView.delegate = self
        scrollView.isPagingEnabled = true
        
        self.view.addSubview(scrollView)
        scrollView.snp.makeConstraints { (make) in
            make.leading.equalTo(view.snp.leading)
            make.trailing.equalTo(view.snp.trailing)
            make.top.equalTo(horizontalTab.snp.bottom).offset(4)
            make.bottom.equalTo(view.snp.bottom)
        }
        scrollView.addSubview(partTimeTV)
        scrollView.addSubview(trainingTV)
        scrollView.addSubview(activityTV)
        
        partTimeTV.ownerVC = self
        trainingTV.ownerVC = self
        activityTV.ownerVC = self
    }
    func setupTableView(){
        
        let tvLoadFinishBlock:((MBaseTV,Int) -> Void) = { tv,pageIndex in
            if pageIndex != 0 { return }
            self.saveToDB(tv: tv)
        }
        partTimeTV.loadFinishBlock = tvLoadFinishBlock
        partTimeTV.cellPressedBlock = { [weak self] (index) in
            
            self?.showWebVC(listType: .partTime, index: index, bottomBarType: .signUp,
                            navigationItemType: .menu, zanType: .partTime)
        }
        partTimeTV.separatorStyle = .none
        partTimeTV.snp.makeConstraints { (make) in
            make.leading.equalTo(scrollView.snp.leading)
            make.top.equalTo(scrollView.snp.top)
            make.width.equalTo(view.snp.width)
            make.height.equalTo(scrollView.snp.height)
        }
        
        trainingTV.loadFinishBlock = tvLoadFinishBlock
        trainingTV.cellPressedBlock = { [weak self] (index) in
            
            self?.showWebVC(listType: .training, index: index, bottomBarType: .signUp,
                            navigationItemType: .menu, zanType: .training)
        }
        trainingTV.separatorStyle = .none
        trainingTV.snp.makeConstraints { (make) in
            make.leading.equalTo(partTimeTV.snp.trailing)
            make.top.equalTo(scrollView.snp.top)
            make.width.equalTo(view.snp.width)
            make.height.equalTo(scrollView.snp.height)
        }
        
        activityTV.loadFinishBlock = tvLoadFinishBlock
        activityTV.cellPressedBlock = { [weak self] (index) in
            
            self?.showWebVC(listType: .activity, index: index, bottomBarType: .signUp,
                            navigationItemType: .menu, zanType: .activity)
        }
        activityTV.separatorStyle = .none
        activityTV.snp.makeConstraints { (make) in
            make.leading.equalTo(trainingTV.snp.trailing)
            make.top.equalTo(scrollView.snp.top)
            make.width.equalTo(view.snp.width)
            make.height.equalTo(scrollView.snp.height)
        }
    }
    func showWebVC(listType:DiscoveryListType,index:Int,bottomBarType:WebVCBottomBarType,navigationItemType:WebVCRightNavigationItemType,zanType:ZanType){
        
        var tableView:MDiscoveryTV! = nil
        switch listType {
        case .partTime:
            tableView = partTimeTV
        case .training:
            tableView = trainingTV
        case .activity:
            tableView = activityTV
        }
        var item = tableView.dataArr[index] as! DiscoveryListItem
        let webVC = self.loadViewControllerInMainStoryboard("MWebVC") as! MWebVC
        let url = item.link + "?id=\(item.id)"
        webVC.setup(title: item.title, url: url, bottomBarType: bottomBarType, rightNavigationItemType: navigationItemType, zanType: zanType,itemID: item.id,isCollect: item.isCollect,isZan: item.isZan,readCount: item.readCount,zanCount: item.zanCount)
        webVC.zanBlock = {
            item.zanCount += 1
            item.isZan = true
            tableView.dataArr[index] = item
        }
        webVC.collectBlock = {
            item.isCollect = true
            tableView.dataArr[index] = item
        }
        webVC.readBlock = {
            item.readCount += 1
            tableView.dataArr[index] = item
        }
        webVC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(webVC, animated: true)
    }

    // MARK: - HorizontalTabView Delegate
    func horizontalTabViewDidSelect(_ index:Int){
        
        let point = CGPoint.init(x: CGFloat(index) * GetView_Width(view: self.view), y: 0)
        scrollView.setContentOffset(point, animated: true)
        scrollView.flashScrollIndicators()
        
        switch index {
        case 0:
            if isFirstLoad[0]{
                isFirstLoad[0] = false
                partTimeTV.refreshData()
                return
            }
            if partTimeTV.dataArr.count == 0{
                partTimeTV.refreshData()
            }
        case 1:
            if isFirstLoad[1]{
                isFirstLoad[1] = false
                trainingTV.refreshData()
                return
            }
            if trainingTV.dataArr.count == 0{
                trainingTV.refreshData()
            }
        case 2:
            if isFirstLoad[2]{
                isFirstLoad[2] = false
                activityTV.refreshData()
                return
            }
            if activityTV.dataArr.count == 0{
                activityTV.refreshData()
            }
        default:
            break
        }
    }
    // MARK: - UIScrollView Delegate
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        if scrollView == self.scrollView {
            let index = Int(targetContentOffset.pointee.x / GetView_Width(view: self.view) + 0.1)
            if horizontalTab.selectedIndex == index{
                return
            }
            horizontalTab.selectIndex(index)
        }
    }
}
