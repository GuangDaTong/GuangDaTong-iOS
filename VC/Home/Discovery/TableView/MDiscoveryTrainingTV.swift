

import UIKit

class MDiscoveryTrainingTV: MDiscoveryTV {

    override var listType:DiscoveryListType{
        return .training
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let trainingCell:MDiscoveryTrainingCell = tableView.dequeueReusableCell(withIdentifier: MDiscoveryTrainingCellIdentifier) as! MDiscoveryTrainingCell
        let item:DiscoveryListItem = dataArr[indexPath.row] as! DiscoveryListItem
        trainingCell.item = item
        
        trainingCell.tintColor = DefaultColorRed
        if tableView.isEditing{
            trainingCell.selectionStyle = .default
        }else{
            trainingCell.selectionStyle = .none
        }
        return trainingCell
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let item:DiscoveryListItem = dataArr[indexPath.row] as! DiscoveryListItem
        return MDiscoveryTrainingCellHeight + item.cellHeight
    }

}
