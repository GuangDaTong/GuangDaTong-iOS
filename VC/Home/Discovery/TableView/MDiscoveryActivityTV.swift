import UIKit

class MDiscoveryActivityTV: MDiscoveryTV {

    override var listType:DiscoveryListType{
        return .activity
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let travelCell:MDiscoveryActivityCell = tableView.dequeueReusableCell(withIdentifier: MDiscoveryActivityCellIdentifier) as! MDiscoveryActivityCell
        let item:DiscoveryListItem = dataArr[indexPath.row] as! DiscoveryListItem
        travelCell.item = item
        
        travelCell.tintColor = DefaultColorRed
        if tableView.isEditing{
            travelCell.selectionStyle = UITableViewCellSelectionStyle.default
        }else{
            travelCell.selectionStyle = UITableViewCellSelectionStyle.none
        }
        return travelCell
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let item = dataArr[indexPath.row] as! DiscoveryListItem
        return MDiscoveryActivityCellHeight + item.cellHeight
    }

}
