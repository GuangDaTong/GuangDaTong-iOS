

import UIKit
import SwiftyJSON

class MDiscoveryTV: MBaseTV {

    weak var ownerVC:MBaseVC?
    var isLoadForCollectList = false
    var listType:DiscoveryListType{
        return .partTime
    }
    var cellPressedBlock:((_ index:Int) -> Void)?
    
    override init(frame: CGRect, style: UITableViewStyle) {
        super.init(frame: frame, style: style)
        
        setup()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    func setup(){
        
        setup(withDelegate: self)
    }
}

extension MDiscoveryTV:MBaseTVDelegate{
    
    func loadData(pageIndex: Int) {
        
        if isLoadForCollectList{
            loadForCollect(pageIndex: pageIndex)
        }else{
            loadForDiscoveryVC(pageIndex: pageIndex)
        }
    }
    func loadForDiscoveryVC(pageIndex:Int){
        
        let type = self.listType
        
        HttpController.shared.discovery(type: type, pageIndex: pageIndex, successBlock: { (json) in
            
            self.parseJSON(json,listType: type)
            
            }, failure: { (err) in
                
                self.loadFailed()
                DLOG(err.localizedDescription)
                self.ownerVC?.toastFail(err.localizedDescription)
        })
    }
    func loadForCollect(pageIndex: Int){
        
        let type = self.listType
        
        HttpController.shared.discoveryCollectList(type: type, pageIndex: pageIndex, successBlock: { (json) in
            
            self.parseJSON(json,listType: type)
            
        }, failure: { (err) in
            
            self.loadFailed()
            DLOG(err.localizedDescription)
            self.ownerVC?.toastFail(err.localizedDescription)
        })
    }
    func parseJSON(_ json:JSON,listType:DiscoveryListType){
        
        var dataArr = [DiscoveryListItem]()
        for data in json["result"]["data"].arrayValue{
            let id:String = data["id"].stringValue
            let icon:String = data["icon"].stringValue
            let title:String = data["title"].stringValue
            let createTime:NSString = data["createTime"].stringValue as NSString
            let position:String = data["position"].stringValue
            let fee:String = data["fee"].stringValue
            var description:String = data["description"].stringValue
            let link:String = data["link"].stringValue
            let isCollect:Bool = (data["isFavorite"].stringValue == "1" ? true : false)
            let isZan:Bool = (data["isZan"].stringValue == "1" ? true : false)
            let zanCount:Int = (data["zan"].stringValue as NSString).integerValue
            let readCount:Int = (data["readCount"].stringValue as NSString).integerValue
            let favorID:String = data["favorID"].stringValue
            
            var height:CGFloat = 0
            if listType != .partTime{
                description = description.count > 0 ? description : "无"
                let desStr = description as NSString
                height = desStr.getSizeWith(UIFont.systemFont(ofSize: 13), constrainedTo: CGSize.init(width: SCREEN_WIDTH - 100, height: CGFloat.greatestFiniteMagnitude)).height
            }
            
            var time:String? = createTime.timeShort()
            if time == nil{
                time = ""
            }
            
            let item:DiscoveryListItem = DiscoveryListItem.init(id: id, icon: icon, title: title, createTime: time!, position: position, fee: fee, description: description, link: link, isCollect: isCollect, isZan: isZan, zanCount: zanCount, readCount: readCount,cellHeight: height, favorID: favorID)
            dataArr.append(item)
        }
        
        self.appendData(dataArr)
    }
    func nibsToRegister() -> [String] {
        switch listType {
        case .partTime:
            return [MDiscoveryPartTimeCellIdentifier]
        case .training:
            return [MDiscoveryTrainingCellIdentifier]
        case .activity:
            return [MDiscoveryActivityCellIdentifier]
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return dataArr.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        assertionFailure("子类必须实现此方法！")
        let cell = UITableViewCell()
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        assertionFailure("子类必须实现此方法！")
        return 44
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        
        let value = UITableViewCellEditingStyle.delete.rawValue |
            UITableViewCellEditingStyle.insert.rawValue
        return UITableViewCellEditingStyle.init(rawValue: value)!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView.isEditing{
            return
        }
        tableView.deselectRow(at: indexPath, animated: true)
        cellPressedBlock?(indexPath.row)
    }
}
