import UIKit

class MDiscoveryPartTimeTV: MDiscoveryTV {

    override var listType:DiscoveryListType{
        return .partTime
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let partTimeCell:MDiscoveryPartTimeCell = tableView.dequeueReusableCell(withIdentifier: MDiscoveryPartTimeCellIdentifier) as! MDiscoveryPartTimeCell
        let item:DiscoveryListItem = dataArr[indexPath.row] as! DiscoveryListItem
        partTimeCell.item = item
        
        partTimeCell.tintColor = DefaultColorRed
        if tableView.isEditing{
            partTimeCell.selectionStyle = .default
        }else{
            partTimeCell.selectionStyle = .none
        }
        return partTimeCell
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return MDiscoveryPartTimeCellHeight
    }

}
