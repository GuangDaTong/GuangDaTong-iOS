

import UIKit
import SDWebImage

let MDiscoveryPartTimeCellIdentifier = "MDiscoveryPartTimeCell"
let MDiscoveryPartTimeCellHeight:CGFloat = 107

class MDiscoveryPartTimeCell: MBaseTVCell {

    // MARK: - View
    @IBOutlet weak var iconBtn: UIButton!
    @IBOutlet weak var titleLB: UILabel!
    @IBOutlet weak var timeLB: UILabel!
    @IBOutlet weak var siteLB: UILabel!
    @IBOutlet weak var salaryLB: UILabel!
    
    var _item:DiscoveryListItem! = nil
    var item:DiscoveryListItem{
        get{
            return _item
        }
        set{
            _item = newValue
            iconBtn.sd_setImage(with: URL.init(string: _item.icon), for: .normal, placeholderImage: DefaultImagePlaceholder)
            titleLB.text = _item.title
            timeLB.text = "发布时间:\(_item.createTime)"
            siteLB.text = "\(_item.position)"
//            salaryLB.text = "薪酬:\(_item.fee)"
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        iconBtn.imageView?.contentMode = .scaleAspectFill
        iconBtn.imageView?.clipsToBounds = true
        
        iconBtn.setCornerRounded(radius: 4)
        addBottomSeparateLine()
    }
    @IBAction func iconBtnPressed(_ sender: Any) {
        
        showImageBrowser(withURLStr: _item.icon)
    }
}
