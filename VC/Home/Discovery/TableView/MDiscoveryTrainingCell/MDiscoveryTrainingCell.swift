

import UIKit

let MDiscoveryTrainingCellIdentifier = "MDiscoveryTrainingCell"
let MDiscoveryTrainingCellHeight:CGFloat = 127

class MDiscoveryTrainingCell: MBaseTVCell {

    // MARK: - View
    @IBOutlet weak var iconBtn: UIButton!
    @IBOutlet weak var titleLB: UILabel!
    @IBOutlet weak var timeLB: UILabel!
    @IBOutlet weak var siteLB: MCopyLabel!
    @IBOutlet weak var feeLB: UILabel!
    @IBOutlet weak var discriptionLB: MCopyLabel!
    
    var _item:DiscoveryListItem! = nil
    var item:DiscoveryListItem{
        get{
            return _item
        }
        set{
            _item = newValue
            iconBtn.sd_setImage(with: URL.init(string: _item.icon), for: .normal, placeholderImage: DefaultImagePlaceholder)
            titleLB.text = _item.title
            timeLB.text = "发布时间:\(_item.createTime)"
            siteLB.text = "\(_item.position)"
//            feeLB.text = "学费:\(_item.fee)"
            discriptionLB.text = "摘要:\(_item.description)"
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        iconBtn.imageView?.contentMode = .scaleAspectFill
        iconBtn.imageView?.clipsToBounds = true
        
        iconBtn.setCornerRounded(radius: 4)
        addBottomSeparateLine()
    }
    @IBAction func iconBtnPressed(_ sender: Any) {
        
        showImageBrowser(withURLStr: _item.icon)
    }
}
