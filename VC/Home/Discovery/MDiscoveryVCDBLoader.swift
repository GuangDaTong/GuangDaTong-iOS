import Foundation

extension MDiscoveryVC{
    
    func loadDB(){
        
        let q:FMDatabaseQueue = FMDatabaseQueue(url: DefaultURLForDB)
        q.inDatabase { (db) in
            
            if !self.createTable(db: db){
                return
            }
            db.select(from: DBTableNameForDiscovery, keys: dbKeys, condition: " WHERE listType = 'partTime'", { (res) in
                
                guard let r = res else{ return }
                self.partTimeTV.dataArr = self.parseResult(r)
            })
            db.select(from: DBTableNameForDiscovery, keys: dbKeys, condition: " WHERE listType = 'training'", { (res) in
                
                guard let r = res else{ return }
                self.trainingTV.dataArr = self.parseResult(r)
            })
            db.select(from: DBTableNameForDiscovery, keys: dbKeys, condition: " WHERE listType = 'activity'", { (res) in
                
                guard let r = res else{ return }
                self.activityTV.dataArr = self.parseResult(r)
            })
        }
    }
    func parseResult(_ r:FMResultSet) -> [DiscoveryListItem]{
        
        var list:[DiscoveryListItem] = [DiscoveryListItem]()
        while r.next(){
            let id:String = r.string(forCol: 0)
            let icon:String = r.string(forCol: 1)
            let title:String = r.string(forCol: 2)
            let time:String = r.string(forCol: 3)
            let position:String = r.string(forCol: 4)
            let fee:String = r.string(forCol: 5)
            let description:String = r.string(forCol:6)
            let link:String = r.string(forCol: 7)
            let isCollect:Bool = r.bool(forCol: 8)
            let isZan:Bool = r.bool(forCol: 9)
            let zanCount:Int = r.int(forCol: 10)
            let readCount:Int = r.int(forCol: 11)
            let height:CGFloat = r.float(forCol:12)
//            let listType = r.string(forCol: 13)
            let item:DiscoveryListItem = DiscoveryListItem.init(id: id, icon: icon, title: title, createTime: time, position: position, fee: fee, description: description, link: link, isCollect: isCollect, isZan: isZan, zanCount: zanCount, readCount: readCount, cellHeight: height, favorID: "")
            
            list.append(item)
        }
        return list
    }
    func createTable(db:FMDatabase) -> Bool{
        
        guard db.create(DBTableNameForDiscovery,
                        colDict:["id":.text,
                                 "icon":.text,
                                 "title":.text,
                                 "createTime":.text,
                                 "position":.text,
                                 "fee":.text,
                                 "description":.text,
                                 "link":.text,
                                 "isCollect":.bool,
                                 "isZan":.bool,
                                 "zanCount":.int,
                                 "readCount":.int,
                                 "cellHeight":.real,
                                 "listType":.text]) else{
                                    DLOG("create table failed")
                                    return false
        }
        return true
    }
    func saveToDB(tv:MBaseTV){
        
        let q:FMDatabaseQueue = FMDatabaseQueue(url: DefaultURLForDB)
        q.inDatabase { (db) in
            
            var type:String = ""
            if tv == self.partTimeTV{
                type = "partTime"
            }
            if tv == self.trainingTV{
                type = "training"
            }
            if tv == self.activityTV{
                type = "activity"
            }
            if !db.delete(from: DBTableNameForDiscovery, condition: "WHERE listType = '\(type)'"){
                DLOG("删除发现\(type)旧数据，失败！！！")
                return
            }
            for item in tv.dataArr{
                let i:DiscoveryListItem = item as! DiscoveryListItem
                let inserted:Bool = db.insert(into: DBTableNameForDiscovery, dict: ["id":i.id,
                                                                    "icon":i.icon,
                                                                    "title":i.title,
                                                                    "createTime":i.createTime,
                                                                    "position":i.position,
                                                                    "fee":i.fee,
                                                                    "description":i.description,
                                                                    "link":i.link,
                                                                    "isCollect":i.isCollect,
                                                                    "isZan":i.isZan,
                                                                    "zanCount":i.zanCount,
                                                                    "readCount":i.readCount,
                                                                    "cellHeight":i.cellHeight,
                                                                    "listType":type])
                if !inserted{
                    DLOG("insert discovery item failed...")
                }
            }
        }
    }
}
