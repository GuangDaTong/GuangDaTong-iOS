

import UIKit

let MAskFilterDictKey = "MAskFilterDict"
let MBlackListKey = "MBlackListKey"

class MAskFilter: NSObject {

    var filterDict = [String:[String]]()
    var blacklist = [String]()
    static let shared = MAskFilter()
    
    /// 配置过滤列表
    func configFilter(){
        
        DispatchQueue.global().async {
            if let dict = uDefaults.object(forKey: MAskFilterDictKey) as? [String:[String]]{
                if !isUserNotLogin{
                    self.filterDict = dict
                }
            }else{
                uDefaults.set(self.filterDict, forKey: MAskFilterDictKey)
                uDefaults.synchronize()
            }
            if let list = uDefaults.object(forKey: MBlackListKey) as? [String]{
                self.blacklist = list
            }else{
                uDefaults.set(self.blacklist, forKey: MBlackListKey)
                uDefaults.synchronize()
            }
        }
    }
    func filteredAskForCurrentUser() -> [String]{
        
        if isUserNotLogin{
            return [String]()
        }
        
        let list = filterDict[uUserID] ?? [String]()
        return list
    }
    func filteredUserForCurrentUser() -> [String]{
        
        if isUserNotLogin{
            return [String]()
        }
        return blacklist
    }
    func addDislikeAsk(askID:String){
        
        if isUserNotLogin{
            return
        }
        if let _ = self.filterDict[uUserID]{
            if self.filterDict[uUserID]!.contains(askID){
                return
            }
            self.filterDict[uUserID]!.append(askID)
        }else{
            self.filterDict[uUserID] = [String]()
            self.filterDict[uUserID]!.append(askID)
        }
        
        DispatchQueue.global().async {
            uDefaults.set(self.filterDict, forKey: MAskFilterDictKey)
            uDefaults.synchronize()
        }
    }
    /// 重新加载黑名单
    func resetBlackList(list:[String]){
        if isUserNotLogin{
            return
        }
        self.blacklist = list
        saveBlacklist()
    }
    func appendBlacklist(userID:String){
        
        if isUserNotLogin{
            return
        }
        if self.blacklist.contains(userID){
            return
        }
        self.blacklist.append(userID)
        saveBlacklist()
    }
    func removeFromBlacklist(userID:String){
        
        var index = 0
        var removeIndex = -1
        for ID in blacklist{
            if ID == userID{
                removeIndex = index
                break
            }
            index += 1
        }
        if removeIndex != -1{
            self.blacklist.remove(at: removeIndex)
        }
        saveBlacklist()
    }
    func saveBlacklist(){
        
        DispatchQueue.global().async {
            uDefaults.set(self.blacklist, forKey: MBlackListKey)
            uDefaults.synchronize()
        }
    }
}
