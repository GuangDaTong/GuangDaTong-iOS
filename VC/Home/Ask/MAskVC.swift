

import UIKit

class MAskVC: MBaseVC,UISearchBarDelegate {

    /// 急问推送携带的问问范围信息（学院、学校）
    var pushAskRangeType:AskRangeType! = nil
    /// 急问推送携带的问问ID
    var pushAskID:String = ""
    
    var askTV:MAskTV! = MAskTV.init(frame: CGRect.zero, style: .grouped)
    var manager:MAskManager! = nil
    let dbKeys:[String] = ["id","title","createTime","publisherID","nickname",
                  "icon","gender","reward","zan","acceptCommentID",
                  "type","label","content","link","thumbnailImageURLs",
                  "imageURLs","commentCount","isFavorite","isZan",
                  "isOwner","isUrgent","detailHeight","imgHeight",
                  "genderText","rewardText","askTagText"]
    
    var selectionView:MAskTypeSelectionView = MAskTypeSelectionView.loadFromNib()
    let titleView:MAskNaviTitleView = MAskNaviTitleView.loadFromNib()
    let shareView:MShareView = MShareView.loadFromNib()
    
    private lazy var searchBar: UISearchBar = self.loadSearchBar()
    
    // MARK: - VC Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        setupNaviBar()
        setupTableView()
        self.view.makeToastActivity()
        DispatchQueue.global().async {
            self.loadDB()
        }
        if uUserID.count == 0{
            self.titleView.isEnable = false
        }
        NotiCenter.addObserver(self, selector: #selector(refreshAskTV), name: UserDidModifyInfoNotification, object: nil)
        NotiCenter.addObserver(self, selector: #selector(refreshAskTV), name: UserDidLoginNotification, object: nil)
        NotiCenter.addObserver(self, selector: #selector(refreshAskTV), name: UserDidLogoutNotification, object: nil)
        NotiCenter.addObserver(self, selector: #selector(refreshAskTV), name: UserAlteredBlacklistNotification, object: nil)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if pushAskRangeType != nil{
            self.askTV.listRangeType = pushAskRangeType
            if pushAskRangeType == .college{ // 勾选为学院
                selectionView.isSchool = false
            }else{ // 勾选为学校
                selectionView.isSchool = true
            }
            pushAskRangeType = nil
            self.showPushAsk()
            self.askTV.refreshData()
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if APPDELEGATE.forceTouchLaunchType == .writeAsk{
            
            APPDELEGATE.forceTouchLaunchType = .none
            self.showWriteVC()
        }
    }
    // MARK: - Setup
    func setupNaviBar(){
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.barTintColor = DefaultColorBlack
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        
        self.titleView.titlePressedBlock = {
            self.selectionView.show()
        }
        
        self.navigationItem.titleView = titleView
        if isAfter_IOS10{ //iOS11需要设置约束
            self.titleView.snp.makeConstraints { (m) in
                m.centerX.equalTo(self.navigationController!.navigationBar.snp.centerX)
                m.width.equalTo(80)
            }
        }
        
        let writeBtn = UIButton.init()
        writeBtn.setImage(UIImage.init(named: "ask_write"), for: .normal)
        writeBtn.sizeToFit()
        writeBtn.addTarget(self, action: #selector(showWriteVC), for: .touchUpInside)
        let msgItem = UIBarButtonItem.init(customView: writeBtn)
        self.navigationItem.rightBarButtonItem = msgItem
        
        self.selectionView.isSchool = true
        self.selectionView.selectBlock = { (isSchool) in
            
            self.titleView.isArrowPointUp = false
            if isSchool && self.askTV.listRangeType == .school{
                return
            }
            if !isSchool && self.askTV.listRangeType == .college{
                return
            }
            if isSchool{
                self.askTV.listRangeType = .school
            }else{
                self.askTV.listRangeType = .college
            }
            self.askTV.refreshData()
        }
    }
    func setupTableView(){
        
        askTV.backgroundColor = UIColor.white
        askTV.separatorStyle = .none
        askTV.ownerVC = self
        manager = MAskManager.init(withOwnerVC: self, tableView: askTV)
        
        askTV.setup(withLoadType: .mainList) { (cellIndex,touchType) in
            DLOG("cellIndex:\(cellIndex),touchType:\(touchType)")
            self.manager.processCellTouch(withIndex: cellIndex, touchType: touchType)
        }
        askTV.loadFinishBlock = { tv,pageIndex in
            
            if self.askTV.listRangeType != .school{ return } //只缓存学校的内容
            if pageIndex == 0{
                self.saveToDB()
            }
        }
        
        self.view.addSubview(askTV)
        askTV.snp.makeConstraints { (m) in
            m.top.equalTo(self.view.snp.top)
            m.leading.equalTo(self.view.snp.leading)
            m.bottom.equalTo(self.view.snp.bottom)
            m.trailing.equalTo(self.view.snp.trailing)
        }
        // searchBar
        let searchContainerFrame:CGRect = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: kSearchBarHeight)
        let searchBarContainer:UIView = UIView.init(frame: searchContainerFrame)
        searchBarContainer.addSubview(self.searchBar)
        self.searchBar.center = searchBarContainer.center
        askTV.tableHeaderView = searchBarContainer
    }
    func loadSearchBar() -> UISearchBar{
        
        let searchBar:UISearchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 32))
        searchBar.placeholder = "搜索你感兴趣的问题吧"
        searchBar.barTintColor = UIColor.white
        searchBar.searchBarStyle = .minimal
        searchBar.delegate = self
        
        return searchBar
    }
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        
        manager.showAskSearchVC(range: askTV.listRangeType)
        return false
    }
    func setListLoadType(_ type:AskRangeType){
        
        switch type {
        case .college:
            self.askTV.listRangeType = .college
            self.selectionView.isSchool = false
            break
        case .school:
            self.askTV.listRangeType = .school
            self.selectionView.isSchool = true
            break
        case .allCollege:
            break
        case .allSchool:
            break
        }
        self.askTV.mj_header.beginRefreshing()
    }
    @objc func refreshAskTV(){
        
        if uUserID.count > 0{
            self.titleView.isEnable = true
            self.askTV.mj_header.beginRefreshing()
        }else{
            self.titleView.isEnable = false
            setListLoadType(.school)
        }
    }
    /// 显示推送过来的问问
    func showPushAsk(){
        
        view.makeToastActivity()
        let pushID = pushAskID
        self.pushAskID = ""
        
        HttpController.shared.askDetail(pushID, successBlock: { (json) in
            
            let askItemJSON = json["result"]
            let font:UIFont = UIFont.systemFont(ofSize: 14)
            let userID:String = uUserID
            let currentDate:Date = DefaultDateFormatter.date(from: stringOfNow()) ?? Date()
            let item:AskListItem = MAskTV.parseAskItem(askItemJSON, userID: userID, font: font, currentDate: currentDate)
            
            self.view.hideToastActivity()
            
            let askDetailVC:MAskDetailVC = self.loadViewControllerInMainStoryboard("MAskDetailVC") as! MAskDetailVC
            askDetailVC.askItem = item
            askDetailVC.hidesBottomBarWhenPushed = true
            self.hideNaviBackBtnTitle()
            self.navigationController?.pushViewController(askDetailVC, animated: true)
            
        }, failure: { (err) in
            
            self.view.hideToastActivity()
            DLOG(err.localizedDescription)
        })
    }
    // MARK: - 发问问
    @objc func showWriteVC(){

        if isUserNotLogin {
            self.toastFail("发表问问前请您登录")
            self.presentVCInMainStoryboard("MLoginNaviVC")
            return
        }
        self.navigationItem.rightBarButtonItem?.isEnabled = false
        getCoins()
    }
    func getCoins(){
        
        view.makeToastActivity()
        
        HttpController.shared.getCoins(successBlock: { (coins,ruleItem) in
            
            self.view.hideToastActivity()
            self.showWriteVCWith(coins: coins, rule: ruleItem)
            self.navigationItem.rightBarButtonItem?.isEnabled = true
            
        }, failure: { (err) in
            
            self.navigationItem.rightBarButtonItem?.isEnabled = true
            self.view.hideToastActivity()
            self.toastFail(err.localizedDescription)
            DLOG(err.localizedDescription)
        })
    }
    func showWriteVCWith(coins:Int,rule:MAskRuleItem){
        
        let awVC = loadViewControllerInMainStoryboard("MAskWriteVC") as! MAskWriteVC
        awVC.ruleItem = rule
        awVC.coins = coins
        
        awVC.hidesBottomBarWhenPushed = true
        awVC.askWriteFinishBlock = { range in
            
            DispatchQueue.main.async{
                self.setListLoadType(range)
            }
        }
        hideNaviBackBtnTitle()
        self.navigationController?.pushViewController(awVC, animated: true)
    }
}
