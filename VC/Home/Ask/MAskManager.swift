

import UIKit

class MAskManager: NSObject {

    weak var ownerVC:MBaseVC! = nil
    var askTV:MAskTV! = nil
    
    init(withOwnerVC vc:MBaseVC,tableView:MAskTV) {
        super.init()
        
        self.ownerVC = vc
        self.askTV = tableView
    }
    
    func showAskSearchVC(range:AskRangeType){
        
        let asVC = ownerVC.loadViewControllerInMainStoryboard("MAskSearchVC") as! MAskSearchVC
        asVC.searchRange = range
        asVC.hidesBottomBarWhenPushed = true
        ownerVC.hideNaviBackBtnTitle()
        ownerVC.navigationController?.pushViewController(asVC, animated: true)
    }
    func showUserInfoVC(withID id:String,name:String){
        
        let uiVC:MUserInfoVC = ownerVC.loadViewControllerInMainStoryboard("MUserInfoVC") as! MUserInfoVC
        uiVC.hidesBottomBarWhenPushed = true
        uiVC.userID = id
        uiVC.userName = name
        ownerVC.hideNaviBackBtnTitle()
        ownerVC.navigationController?.pushViewController(uiVC, animated: true)
    }
    func showAskDetailVC(withAskItem item:AskListItem,index: Int){
        
        let askDetailVC:MAskDetailVC = ownerVC.loadViewControllerInMainStoryboard("MAskDetailVC") as! MAskDetailVC
        askDetailVC.hidesBottomBarWhenPushed = true
        if askTV.loadType == MAskTVLoadType.collectList{
            askDetailVC.isLoadingForCollection = true
        }
        
        askDetailVC.askItem = item
        
        askDetailVC.zanBlock = {
            self.zanAsk(index: index)
            self.askTV.reloadData()
        }
        askDetailVC.collectBlock = {
            self.collectAsk(index: index)
            self.askTV.reloadData()
        }
        askDetailVC.acceptBlock = { ID in
            self.acceptAsk(index: index,commentID: ID)
            self.askTV.reloadData()
        }
        askDetailVC.commentSendedBlock = {
            var item:AskListItem = self.askTV.dataArr[index] as! AskListItem
            item.commentCount += 1
            self.askTV.dataArr[index] = item
            self.askTV.reloadData()
        }
        askDetailVC.dislikeAskBlock = {
            self.dislikeAskItem(item, index: index)
        }
        
        self.ownerVC.hideNaviBackBtnTitle()
        self.ownerVC.navigationController?.pushViewController(askDetailVC, animated: true)
    }
    func dislikeAskItem(_ item:AskListItem,index:Int){
        
        self.ownerVC.showSystemStyleAlert(title: "提示", msg: "屏蔽此问问？") {
            
            let item:AskListItem = self.askTV.dataArr[index] as! AskListItem
            self.askTV.dataArr.remove(at: index)
            self.askTV.reloadData()
            TopToastView.shared.makeSuccessToast("已屏蔽")
            MAskFilter.shared.addDislikeAsk(askID: item.id)
            self.ownerVC.popViewController()
        }
    }
    // MARK: - Cell内点击事件处理
    func processCellTouch(withIndex index:Int,touchType type:AskCellTouchEventType){
        
        let item = askTV.dataArr[index] as! AskListItem
        
        if item.id == "" && type != .iconPressed{
            ownerVC.toastFail("操作失败，该问问已被删除")
            return
        }
        
        switch type {
        case .commentBtnPressed:
            fallthrough
        case .cellPressed:
            showAskDetailVC(withAskItem: item,index: index)
        case .iconPressed:
            showUserInfoVC(withID: item.publisherID,name: item.nickname)
        case .deleteBtnPressed:
            showAskDeleteAlert(withAskID: item.id,index: index)
        case .optionBtnPressed:
            showMenu(withAskID: item.id,index: index)
        case .askTagBtnPressed:
            self.showSameTagAsks(withAsk: item)
        case .shareBtnPressed:
            showShareMenu(askItem: item)
        case .zanBtnPressed:
            if isUserNotLogin{
                ownerVC.toastFail("您尚未登录")
                ownerVC.presentVCInMainStoryboard("MLoginNaviVC")
                return
            }
            zanAsk(index: index)
            makeZanRequest(withIndex: index)
        }
    }
    func zanAsk(index:Int){
        var item:AskListItem = askTV.dataArr[index] as! AskListItem
        item.zan += 1
        item.isZan = true
        askTV.dataArr[index] = item
    }
    func makeZanRequest(withIndex index:Int){
        
        let ID:String! = (askTV.dataArr[index] as! AskListItem).id
        
        HttpController.shared.zan(type: .ask, ID: ID, successBlock: { (json) in
            DLOG("赞 问问\(ID) 成功！")
        }, failure: { (err) in
            DLOG("赞 问问\(ID) 失败 ！")
        })
    }
    func collectAsk(index:Int){
        if isUserNotLogin{
            ownerVC.toastFail("您尚未登录")
            ownerVC.presentVCInMainStoryboard("MLoginNaviVC")
            return
        }
        var item:AskListItem = askTV.dataArr[index] as! AskListItem
        item.isFavorite = true
        askTV.dataArr[index] = item
    }
    func acceptAsk(index:Int,commentID:String){
        var item:AskListItem = askTV.dataArr[index] as! AskListItem
        item.acceptCommentID = commentID
        askTV.dataArr[index] = item
    }
    func showSameTagAsks(withAsk item:AskListItem){
        
        let slVC = self.ownerVC.loadViewControllerInMainStoryboard("MAskSearchListVC") as! MAskSearchListVC
        slVC.hidesBottomBarWhenPushed = true
        let tag:AskTag = AskTag(rawValue: item.label) ?? AskTag.other
        slVC.setup(withRange:self.askTV.listRangeType,text: "", type: nil, tag: tag)
        self.ownerVC.hideNaviBackBtnTitle()
        self.ownerVC.navigationController?.pushViewController(slVC, animated: true)
    }
}

extension MAskManager{
    
    // 问问菜单
    func showMenu(withAskID id:String,index:Int){
        
        if isUserNotLogin{
            ownerVC.toastFail("您尚未登录")
            ownerVC.presentVCInMainStoryboard("MLoginNaviVC")
            return
        }
        let sheet:UIAlertController = UIAlertController.init(title: "", message: "请选择", preferredStyle: .actionSheet)
        let collectAction:UIAlertAction = UIAlertAction.init(title: "收藏", style: .default) { (action) in
            
            let item = self.askTV.dataArr[index] as! AskListItem
            if item.isFavorite{
                self.ownerVC.toastFail("您已经收藏过了")
                return
            }
            
            self.ownerVC.view.makeToastActivity()
            HttpController.shared.collect(type:.ask,ID:id, successBlock: { (json) in
                
                self.ownerVC.view.hideToastActivity()
                self.ownerVC.toastSuccess("收藏成功")
                self.collectAsk(index: index)
            }, failure: { (err) in
                
                self.ownerVC.view.hideToastActivity()
                self.ownerVC.toastFail(err.localizedDescription)
            })
        }
        sheet.addAction(collectAction)
        let informAction:UIAlertAction = UIAlertAction.init(title: "举报", style: .destructive) { [weak self] (action) in
            self?.showInformMenu(id,index:index)
        }
        sheet.addAction(informAction)
        let dislikeAction:UIAlertAction = UIAlertAction.init(title: "屏蔽此问问", style: .default) { (action) in
            self.dislikeAsk(askID: id, index: index)
        }
        sheet.addAction(dislikeAction)
        let cancelAction:UIAlertAction = UIAlertAction.init(title: "取消", style: .cancel, handler: nil)
        sheet.addAction(cancelAction)
        
        if sheet.popoverPresentationController != nil{
            let cell:UITableViewCell = self.askTV.cellForRow(at: IndexPath.init(row: 0, section: index))!
            sheet.popoverPresentationController?.sourceView = cell
            sheet.popoverPresentationController?.sourceRect = self.sourceRectForPopver(cell.bounds)
        }
        
        self.ownerVC.present(sheet, animated: true, completion: nil)
    }
    func dislikeAsk(askID:String,index:Int){
        
        self.ownerVC.showSystemStyleAlert(title: "提示", msg: "屏蔽此问问？") {
            self.askTV.dataArr.remove(at: index)
            self.askTV.reloadData()
            MAskFilter.shared.addDislikeAsk(askID: askID)
            self.ownerVC.toastSuccess("已屏蔽")
        }
    }
    // 举报问问
    func showInformMenu(_ askID:String,index:Int){
        
        let sheet:UIAlertController = UIAlertController.init(title: "", message: "请选择", preferredStyle: .actionSheet)
        let cheat:String = "欺诈骗钱"
        let cheatAction:UIAlertAction = UIAlertAction.init(title: cheat, style: .default) { [weak self] (action) in
            self?.informWithReason(cheat,askID: askID)
        }
        sheet.addAction(cheatAction)
        let violence:String = "色情暴力"
        let violenceAction:UIAlertAction = UIAlertAction.init(title: violence, style: .default) { [weak self] (action) in
            self?.informWithReason(violence,askID: askID)
        }
        sheet.addAction(violenceAction)
        let ad:String = "广告骚扰"
        let adAction = UIAlertAction.init(title: "广告骚扰", style: .default) { [weak self] (action) in
            self?.informWithReason(ad,askID: askID)
        }
        sheet.addAction(adAction)
        let otherAction:UIAlertAction = UIAlertAction.init(title: "其他", style: .default) { [weak self] (action) in
            self?.informWithReason("其他",askID: askID)
        }
        sheet.addAction(otherAction)
        let cancelAction:UIAlertAction = UIAlertAction.init(title: "取消", style: .cancel, handler: nil)
        sheet.addAction(cancelAction)
        
        if sheet.popoverPresentationController != nil{
            let cell:UITableViewCell = self.askTV.cellForRow(at: IndexPath.init(row: 0, section: index))!
            sheet.popoverPresentationController?.sourceView = cell
            sheet.popoverPresentationController?.sourceRect = self.sourceRectForPopver(cell.bounds)
        }
        
        self.ownerVC.present(sheet, animated: true, completion: nil)
    }
    // 计算iPad Popver弹出位置
    func sourceRectForPopver(_ bound:CGRect) -> CGRect{
        
        var bounds:CGRect = bound
        bounds.origin.x = bounds.size.width / 7.0 * 6.5
        bounds.size.width = bounds.size.width / 7.0
        bounds.size.height /= 3.0
        return bounds
    }
    func informWithReason(_ reason:String,askID:String){
        
        if reason == "其他" {
            
            let tsVC:MTextSendVC = ownerVC.loadViewControllerInMainStoryboard("MTextSendVC") as! MTextSendVC
            tsVC.hidesBottomBarWhenPushed = true
            tsVC.setupWith(operationType: TextSendOperationType.informAsk, ID: askID)
            self.ownerVC.hideNaviBackBtnTitle()
            self.ownerVC.navigationController?.pushViewController(tsVC, animated: true)
            return
        }
        self.ownerVC.view.makeToastActivity()
        HttpController.shared.inform(type: .ask,ID: askID, reason: reason, successBlock: { (json) in
            self.ownerVC.view.hideToastActivity()
            self.ownerVC.toastSuccess("举报成功")
        }, failure: { (err) in
            self.ownerVC.view.hideToastActivity()
            self.ownerVC.toastFail(err.localizedDescription)
        })
    }
    // 删除问问
    func showAskDeleteAlert(withAskID id:String,index:Int){
        
        let alert:UIAlertController = UIAlertController(title: "提示", message: "删除这条问问？", preferredStyle: .alert)
        let YESaction:UIAlertAction = UIAlertAction.init(title: "确定", style: .destructive) { (action) in
            self.deleteAsk(withAskID: id, index: index)
        }
        let NOaction:UIAlertAction = UIAlertAction(title: "取消", style: .default, handler: nil)
        alert.addAction(YESaction)
        alert.addAction(NOaction)
        
        self.ownerVC.present(alert, animated: true, completion: nil)
    }
    func deleteAsk(withAskID id:String,index:Int){
        
        ownerVC.view.makeToastActivity()
        HttpController.shared.deleteAsk([id], successBlock: { (json) in
            self.ownerVC.view.hideToastActivity()
            self.ownerVC.toastSuccess("删除成功")
            self.askTV.dataArr.remove(at: index)
            self.askTV.reloadData()
        }, failure: { (err) in
            self.ownerVC.view.hideToastActivity()
            self.ownerVC.toastFail(err.localizedDescription)
        })
    }
    // 分享问问
    func showShareMenu(askItem:AskListItem){
        
        let shareView:MShareView = MShareView.loadFromNib()
        shareView.show()
        shareView.shareSelectedBlock = { platform in
            
            self.shareWithPlatform(platform, askItem: askItem)
        }
    }
    func shareWithPlatform(_ platform:MSharePlatformType,askItem:AskListItem){
        
        let image:UIImage! = UIImage.init(named: "alert_mew")
        let titleStr:String = askItem.title
        let descriptionStr:String = askItem.content
        let urlStr:String = askItem.link + "?id=" + askItem.id
        let shareMaker:MShareMaker = MShareMaker.init(ownerVC: self.ownerVC, type: .ask, platform: platform, objID: askItem.id, title: titleStr, description: descriptionStr, url: urlStr, image: image)
        shareMaker.startShare()
    }
}
