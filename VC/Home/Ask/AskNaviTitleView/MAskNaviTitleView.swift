

import UIKit

class MAskNaviTitleView: UIView {

    @IBOutlet weak var indicatorIV: UIImageView!
    @IBOutlet weak var coverBtn: UIButton!
    
    fileprivate var _isEnable = false
    var isEnable:Bool{
        set{
            _isEnable = newValue
            if _isEnable {
                indicatorIV.isHidden = false
                coverBtn.isUserInteractionEnabled = true
            }else{
                indicatorIV.isHidden = true
                coverBtn.isUserInteractionEnabled = false
            }
        }
        get{
            return _isEnable
        }
    }
    var titlePressedBlock:(() -> Void)?
    fileprivate var _isArrowPointUp = false
    var isArrowPointUp:Bool{
        get{
            return _isArrowPointUp
        }
        set{
            _isArrowPointUp = newValue
            if _isArrowPointUp{
                UIView.animate(withDuration: DefaultAnimationDuration, animations: {
                    self.indicatorIV.transform = CGAffineTransform.init(rotationAngle: CGFloat(-Double.pi))
                })
            }else{
                UIView.animate(withDuration: DefaultAnimationDuration, animations: {
                    self.indicatorIV.transform = CGAffineTransform.identity
                })
            }
        }
    }
    override var intrinsicContentSize: CGSize{
        return CGSize.init(width: 80, height: 32)
    }
    class func loadFromNib() -> MAskNaviTitleView{
        
        let view = Bundle.main.loadNibNamed("MAskNaviTitleView", owner: nil, options: nil)?.last as! MAskNaviTitleView
        return view
    }
    
    @IBAction func titleBtnPressed(_ sender: Any) {
        
        isArrowPointUp = true
        titlePressedBlock?()
    }

    

}
