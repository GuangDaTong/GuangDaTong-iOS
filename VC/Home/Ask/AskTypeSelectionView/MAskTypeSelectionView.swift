

import UIKit

class MAskTypeSelectionView: UIView {

    // MARK: - View
    @IBOutlet weak var grayBackgroundV: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var instituteSelectLB: UILabel!
    @IBOutlet weak var schoolSelectLB: UILabel!
    
    // MARK: - Constraint
    @IBOutlet weak var upperSepLineHeight: NSLayoutConstraint!
    @IBOutlet weak var lowerSepLineHeight: NSLayoutConstraint!
    @IBOutlet weak var spaceToTop: NSLayoutConstraint!
    
    // MARK: - Property
    fileprivate var _isSchool = false
    
    /// 问问列表显示的是否为学校范围的问问
    var isSchool:Bool{
        get{
            return _isSchool
        }
        set{
            _isSchool = newValue
            if _isSchool{
                instituteSelectLB.isHidden = true
                schoolSelectLB.isHidden = false
            }else{
                instituteSelectLB.isHidden = false
                schoolSelectLB.isHidden = true
            }
        }
    }
    var selectBlock:((_ isSchool:Bool) -> Void)?
    
    
    // MARK: - Load
    class func loadFromNib() -> MAskTypeSelectionView{
        
        let view = Bundle.main.loadNibNamed("MAskTypeSelectionView", owner: nil, options: nil)?.last as! MAskTypeSelectionView
        return view
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(hide))
        let tap2 = UITapGestureRecognizer.init(target: self, action: #selector(hide))
        self.addGestureRecognizer(tap)
        grayBackgroundV.addGestureRecognizer(tap2)
        
        upperSepLineHeight.setToOnePixel()
        lowerSepLineHeight.setToOnePixel()
        containerView.setCornerRounded(radius: 4)
        if is_iPhone_X{
            spaceToTop.constant = 88
        }
    }
    func show(){
        
        setHide()
        WIN.addSubview(self)
        self.snp.makeConstraints { (m) in
            m.top.equalTo(WIN.snp.top)
            m.leading.equalTo(WIN.snp.leading)
            m.bottom.equalTo(WIN.snp.bottom)
            m.trailing.equalTo(WIN.snp.trailing)
        }
//        containerView.layer.anchorPoint = CGPoint.init(x: 0.5, y: 0)
        UIView.animate(withDuration: DefaultAnimationDuration) {
            self.grayBackgroundV.alpha = 0.8
            self.containerView.alpha = 1
            self.containerView.transform = CGAffineTransform.identity
        }
    }
    fileprivate func setHide(){
        
        self.containerView.alpha = 0
        grayBackgroundV.alpha = 0
        containerView.transform = CGAffineTransform.init(scaleX: 0.1, y: 0.1)
    }
    @objc func hide(){
        UIView.animate(withDuration: DefaultAnimationDuration, animations: {
            self.setHide()
        }, completion: { (finish) in
            
            self.selectBlock?(self.isSchool)
            self.removeFromSuperview()
        })
    }
    
    // MARK: - Btn Event
    @IBAction func instituteBtnPressed(_ sender: Any) {
        
        isSchool = false
        hide()
    }
    @IBAction func schoolBtnPressed(_ sender: Any) {
        
        isSchool = true
        hide()
    }
    
    

}
