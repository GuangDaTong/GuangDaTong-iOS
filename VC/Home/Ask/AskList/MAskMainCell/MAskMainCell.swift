

import UIKit

let MAskMainCellIdentifier = "MAskMainCell"

class MAskMainCell: MAskVCCell {

    var isLoadingForCollection = false //加载我的收藏 - 问问，隐藏右上角的菜单和删除按钮
    var isSepLineAdded = false
    fileprivate let mainPart = MAskMainPartView.loadFromNib()
    fileprivate let ContentMaxHeight = UIFont.systemFont(ofSize: 14).lineHeight * DefaultAskContentMaxLine
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        contentView.addSubview(mainPart)
        mainPart.viewBtn.isHidden = true
        mainPart.iconBtnPressedBlock = {
            self.cellTouchBlock?(.iconPressed)
        }
        mainPart.deleteBtnPressedBlock = {
            self.cellTouchBlock?(.deleteBtnPressed)
        }
        mainPart.optionBtnPressedBlock = {
            self.cellTouchBlock?(.optionBtnPressed)
        }
        mainPart.askTagBtnPressedBlock = {
            self.cellTouchBlock?(.askTagBtnPressed)
        }
        mainPart.snp.remakeConstraints { (make) in
            make.leading.equalTo(contentView.snp.leading)
            make.width.equalTo(contentView.snp.width)
            make.top.equalTo(contentView.snp.top)
            make.height.equalTo(MAskMainPartViewHeight)
        }
    }
    override func addBottomSeparateLine(){
        
        if isSepLineAdded{
            return
        }
        isSepLineAdded = true
        
        let line = UIView.init(frame: CGRect.zero)
        line.backgroundColor = DefaultSeparateLineColor
        self.addSubview(line)
        line.snp.makeConstraints { (m) in
            m.leading.equalTo(self.snp.leading)
            m.trailing.equalTo(self.snp.trailing)
            m.bottom.equalTo(self.snp.bottom)
            m.height.equalTo(1/UIScreen.main.scale)
        }
    }
    override func addSubview(_ view: UIView) {
        
        // 删除分割线，以防止编辑状态遮挡
        if view.isKind(of: NSClassFromString("_UITableViewCellSeparatorView").self!) {
            return
        }
        super.addSubview(view)
    }
    override func setup(){
        
        let item = self.cellItem!
        let totalHeight = item.detailHeight
        let contentHeight = totalHeight > self.ContentMaxHeight ? self.ContentMaxHeight : totalHeight
        let mainPartHeight = MAskMainPartViewHeight + contentHeight
        var type = (item.isOwner ? AskManageType.delete : AskManageType.menu)
        if isLoadingForCollection{
            type = .none
            addBottomSeparateLine()
        }
        let mainP = self.mainPart
        
        mainP.snp.updateConstraints { (make) in
            make.height.equalTo(mainPartHeight)
        }
        mainP.setup(manageType: type,title: item.title, iconURL: item.icon, name: item.nickname, genderText: item.genderText, solveStatus: item.acceptCommentID, time: item.createTime, rewardText: item.rewardText, detail: item.content, isUrgent: item.isUrgent, askTagText: item.askTagText)
    }
}
