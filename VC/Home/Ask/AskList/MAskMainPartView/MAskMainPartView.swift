

import UIKit
import SDWebImage

enum AskManageType{
    case none
    case delete
    case menu
}

/// 排除文本内容部分之后的高度
let MAskMainPartViewHeight:CGFloat = 110
/// 问问文本内容宽度
let MAskMainPartTextContentWidth:CGFloat = SCREEN_WIDTH - 92

/// 问问列表cell的用户信息和问问文本部分
class MAskMainPartView: UIView {
    
    @IBOutlet weak var viewBtn: UIButton!
    @IBOutlet weak var iconBtn: UIButton!
    @IBOutlet weak var titleLB: MCopyLabel!
    @IBOutlet weak var nameLB: UILabel!
    @IBOutlet weak var sexLB: UILabel!
    @IBOutlet weak var solveStatusLB: UILabel!
    @IBOutlet weak var timeLB: UILabel!
    @IBOutlet weak var awardLB: UILabel!
    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var optionBtn: UIButton!
    @IBOutlet weak var detailLB: MCopyLabel!
    @IBOutlet weak var askTypeBtn: UIButton!
    @IBOutlet weak var askTagBtn: UIButton!
    
    var viewBtnPressedBlock:(() -> Void)?
    var iconBtnPressedBlock:(() -> Void)?
    var deleteBtnPressedBlock:(() -> Void)?
    var optionBtnPressedBlock:(() -> Void)?
    var askTagBtnPressedBlock:(() -> Void)?
    
    let UrgentBtnDefaultWidth:CGFloat = 38
    @IBOutlet weak var urgentBtnWidth: NSLayoutConstraint!
    
    
    class func loadFromNib() -> MAskMainPartView{
        
        let view = Bundle.main.loadNibNamed("MAskMainPartView", owner: nil, options: nil)?.last as! MAskMainPartView
        return view
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        
        iconBtn.imageView?.contentMode = .scaleAspectFill
        iconBtn.imageView?.clipsToBounds = true
        
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(userNameTapped))
        nameLB.addGestureRecognizer(tap)
        iconBtn.setCornerRounded(radius: 4)
    }
    func setup(manageType:AskManageType,title:String,iconURL:String,name:String,genderText:NSAttributedString,solveStatus:String,time:String,rewardText:NSAttributedString,detail:String,isUrgent:Bool,askTagText:NSAttributedString){
        
        titleLB.text = "“\(title)”"
        iconBtn.sd_setImage(with: URL.init(string: iconURL), for: .normal, placeholderImage: DefaultImagePlaceholder)
        nameLB.text = name
        sexLB.attributedText = genderText
        
        timeLB.text = time
        awardLB.attributedText = rewardText
        detailLB.text = detail
        askTagBtn.setAttributedTitle(askTagText, for: .normal)
        
        DispatchQueue.global().async {
            
            var isDeleteHidden = true
            var isOptionHidden = true
            switch manageType {
            case .none:
                break
            case .delete:
                isDeleteHidden = false
                isOptionHidden = true
            case .menu:
                isDeleteHidden = true
                isOptionHidden = false
            }
            
            var isAskTypeHidden = true
            var urgentWidth:CGFloat = 0
            if isUrgent{
                isAskTypeHidden = false
                urgentWidth = self.UrgentBtnDefaultWidth
            }
            
            var isSolveStatusHidden = true
            var solveText = ""
            if solveStatus.count > 0{
                isSolveStatusHidden = false
                solveText = "[已解决]"
            }
            
            DispatchQueue.main.async{
                
                self.deleteBtn.isHidden = isDeleteHidden
                self.optionBtn.isHidden = isOptionHidden
                
                self.solveStatusLB.text = solveText
                self.solveStatusLB.isHidden = isSolveStatusHidden
                
                self.askTypeBtn.isHidden = isAskTypeHidden
                self.urgentBtnWidth.constant = urgentWidth
            }
        }
    }
    @objc func userNameTapped(){
        iconBtnPressedBlock?()
    }
    @IBAction func viewBtnPressed(_ sender: Any) {
        DLOG("")
        viewBtnPressedBlock?()
    }
    @IBAction func iconBtnPressed(_ sender: Any) {
        DLOG("")
        iconBtnPressedBlock?()
    }
    @IBAction func deleteBtnPressed(_ sender: Any) {
        DLOG("")
        deleteBtnPressedBlock?()
    }
    @IBAction func optionBtnPressed(_ sender: Any) {
        DLOG("")
        optionBtnPressedBlock?()
    }
    @IBAction func askTagBtnPressed(_ sender: Any) {
        DLOG("")
        askTagBtnPressedBlock?()
    }
}
