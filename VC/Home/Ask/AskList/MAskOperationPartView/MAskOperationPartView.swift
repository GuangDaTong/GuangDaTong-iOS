

import UIKit

let MAskOperationPartViewHeight:CGFloat = 32

/// 问问列表cell的底部操作部分
class MAskOperationPartView: UIView {

    @IBOutlet weak var shareBtn: UIButton!
    @IBOutlet weak var zanBtn: UIButton!
    @IBOutlet weak var commentBtn: UIButton!
    
    var isZan = false
    var zanCount = 0
    
    var shareBtnPressedBlock:(() -> Void)?
    var zanBtnPressedBlock:(() -> Void)?
    var commentBtnPressedBlock:(() -> Void)?
    
    let zanBlueImage = UIImage.init(named: "zan_blue")
    let zanGrayImage = UIImage.init(named: "zan_gray")
    
    class func loadFromNib() -> MAskOperationPartView{
        
        let view = Bundle.main.loadNibNamed("MAskOperationPartView", owner: nil, options: nil)?.last as! MAskOperationPartView
        view.setup()
        return view
    }
    fileprivate func setup(){
        
        commentBtn.imageEdgeInsets = UIEdgeInsetsMake(4, 0, 0, 0)
    }
    
    func setup(isZan:Bool,zanCount:Int,commentCount:Int){
        
        self.isZan = isZan
        self.zanCount = zanCount
        setupZanBtn()
        zanBtn.setTitle(" \(zanCount)", for: .normal)
        commentBtn.setTitle(" \(commentCount)", for: .normal)
    }
    
    @IBAction func shareBtnPressed(_ sender: Any) {
        DLOG("")
        if let block = shareBtnPressedBlock {
            block()
        }
    }
    @IBAction func zanBtnPressed(_ sender: Any) {
        
        // 已经赞了就不能取消赞
        if self.isZan {
            return
        }
        // 未登录
        if isUserNotLogin{
            zanBtnPressedBlock?()
            return
        }
        self.isZan = true
        setupZanBtn()
        UIView.animate(withDuration: 0.15, animations: {
            self.zanBtn.imageView?.layer.transform = CATransform3DMakeScale(1.1, 1.1, 0)
        }, completion: { (finish) in
            UIView.animate(withDuration: 0.15, animations: {
                self.zanBtn.imageView?.layer.transform = CATransform3DIdentity
            })
        })
        
        zanBtn.setTitle(" \(zanCount + 1)", for: .normal)
        zanBtnPressedBlock?()
    }
    fileprivate func setupZanBtn(){
        
        if self.isZan {
            zanBtn.setImage(zanBlueImage, for: .normal)
        }else{
            zanBtn.setImage(zanGrayImage, for: .normal)
        }
    }
    @IBAction func commentBtnPressed(_ sender: Any) {
        DLOG("")
        if let block = commentBtnPressedBlock {
            block()
        }
    }
    

}
