

import UIKit

enum AskCellTouchEventType {
    case cellPressed
    case iconPressed
    case deleteBtnPressed
    case optionBtnPressed
    case askTagBtnPressed
    case shareBtnPressed
    case zanBtnPressed
    case commentBtnPressed
}

let MAskVCCellIdentifier = "MAskVCCell"

class MAskVCCell: MBaseTVCell {
    
    var cellTouchBlock:((_ eventType:AskCellTouchEventType) -> Void)?
    var cellItem:AskListItem! = nil
    
    var item:AskListItem?{
        get{
            return cellItem
        }
        set{
            cellItem = newValue
            setup()
        }
    }
    override func addSubview(_ view: UIView) {
        
        // 删除分割线，以防止编辑状态遮挡
        if view.isKind(of: NSClassFromString("_UITableViewCellSeparatorView").self!) {
            return
        }
        super.addSubview(view)
    }
    func setup(){
        
    }
}
