

import UIKit
import SDWebImage

/// 问问列表cell的图片部分
class MAskImagePartView: UIView {
    
    fileprivate var urlArr:[String] = [String]()
    fileprivate var urlForHDArr:[String] = [String]()
    
    /// 问问图片按钮
    internal var imageBtnArr:[UIButton]!{
        get{
            assertionFailure("子类必须重写此属性")
            return [UIButton]()
        }
    }
    /// 从.xib加载当前类实例
    ///
    /// - Returns: 类实例
    class func loadFromNib() -> MAskImagePartView{
        
        assertionFailure("子类必须重写此方法")
        let view = MAskImagePartView.init(frame: CGRect.zero)
        return view
    }
    
    /// 设置图片按钮的图片
    ///
    /// - Parameter urlArr: 图片链接数组
    func setupImages(with urlArr:[String]?){
        
        let btnCount:Int = self.imageBtnArr.count
        guard let urls = urlArr else {
            ERRLOG("问问图片数组为空！")
            for idx in 0..<btnCount {
                let btn:UIButton = self.imageBtnArr[idx]
                btn.setImage(DefaultImagePlaceholder, for: UIControlState.normal)
                btn.isHidden = false
            }
            return
        }
        self.urlArr = urls
        guard urls.count <= btnCount  else {
            ERRLOG("问问图片链接数组设置有误，图片数量超过当前类可显示数量！")
            var index:Int = 0
            for url in urls{
                let btn:UIButton = self.imageBtnArr[index]
                let url:URL? = URL.init(string: url)
                btn.sd_setImage(with: url, for: UIControlState.normal, placeholderImage: DefaultImagePlaceholder)
                btn.isHidden = false
                index += 1
                if index == btnCount {
                    break
                }
            }
            return
        }
        
        var index:Int = 0
        for url in urls{
            let btn:UIButton = self.imageBtnArr[index]
            let url:URL? = URL.init(string: url)
            btn.sd_setImage(with: url, for: UIControlState.normal, placeholderImage: DefaultImagePlaceholder)
            btn.isHidden = false
            index += 1
        }
        for idx in index..<btnCount {
            let btn:UIButton = imageBtnArr[idx]
            btn.isHidden = true
        }
    }
    
    /// 高清图链接数组
    ///
    /// - Parameter urlArr: 链接数组
    func setupHDImages(with urlArr:[String]?){
        guard let urls = urlArr else { return }
        self.urlForHDArr = urls
    }
    
    /// 调用图片点击回调函数
    ///
    /// - Parameter sender: 响应当前点击事件的按钮
    func invokeBlock(with sender:UIButton){
        
        var index:Int = -1
        for btn in self.imageBtnArr{
            
            index += 1
            if sender != btn{
                continue
            }
            DLOG("pressed imageBtn index:\(index)")
            self.showImage(index,urls:self.urlForHDArr)
            return
        }
        ERRLOG("问问图片点击回调失败，配置有误！")
    }
    func showImage(_ index:Int,urls:[String]){
        
        showImageBrowser(currentIndex: index, urls: urls)
    }
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        
        let hitView = super.hitTest(point, with: event)
        if hitView == self{
            return nil
        }
        return hitView
    }
}
