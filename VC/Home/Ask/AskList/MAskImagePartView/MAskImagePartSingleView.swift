

import UIKit
import SDWebImage

let MAskImagePartSingleViewHeight:CGFloat =  176.0 / 667.0 * SCREEN_HEIGHT

let MAskImagePartSingleViewMaxHeight:CGFloat =  MAskImagePartSingleViewHeight - 16
let MAskImagePartSingleViewMaxWidth:CGFloat =  SCREEN_WIDTH - 125

/// 1张图片的View
class MAskImagePartSingleView: MAskImagePartView {
    
    @IBOutlet weak var imgBtn: UIButton!
    @IBOutlet weak var imgBtnWidth: NSLayoutConstraint!
    @IBOutlet weak var imgBtnHeight: NSLayoutConstraint!
    var imgCacheIV = UIImageView.init()
    
    override var imageBtnArr:[UIButton]!{
        get{
            return [imgBtn]
        }
    }
    
    override class func loadFromNib() -> MAskImagePartSingleView{
        
        let view = Bundle.main.loadNibNamed("MAskImagePartSingleView", owner: nil, options: nil)?.last as! MAskImagePartSingleView
        return view
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        
        for btn in self.imageBtnArr{
            btn.imageView?.contentMode = .scaleAspectFill
        }
    }
    override func setupImages(with urlArr:[String]?){
        
        let urlStr = urlArr?.first
        imgBtn.setImage(DefaultImagePlaceholder, for: .normal)
        guard urlStr != nil else {
            ERRLOG("单张的问问图片为空！")
            imgBtn.isHidden = true
            return
        }
        imgBtn.isHidden = false
        imgCacheIV.sd_setImage(with: URL.init(string: urlStr!)) { [weak self] (img, err, cacheType, url) in
            self?.imgBtn.setImage(img, for: .normal)
            self?.adjustImageFrame()
        }
    }
    func adjustImageFrame(){
        
        let image = imgBtn.image(for: .normal)
        if image == nil{
            imgBtnWidth.constant = 0
            imgBtnHeight.constant = 0
            return
        }
        let img = image!
        var width = img.size.width
        var height = img.size.height
        guard width != 0 && height != 0 else{
            return
        }
        if height >= width {
            let scale = width / height
            height = MAskImagePartSingleViewMaxHeight
            width = height * scale
        }else{
            let scale = height / width
            width = MAskImagePartSingleViewMaxWidth
            height = width * scale
            if height > MAskImagePartSingleViewMaxHeight{
                height = MAskImagePartSingleViewMaxHeight
                width = height / scale
            }
        }
        self.imgBtnWidth.constant = width
        self.imgBtnHeight.constant = height
    }
    
    @IBAction func imgBtnPressed(_ sender: UIButton) {
        
        invokeBlock(with: sender)
    }

}
