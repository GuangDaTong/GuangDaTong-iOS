

import UIKit

let MAskImagePartLeadingSpace:CGFloat = 80
let MAskImagePartTrailingSpace:CGFloat = 48
/// 图片间的空隙宽度
let MAskImageDefaultSpaceHeight:CGFloat = 8

/// 图片默认宽度
let MAskImageDefaultWidth:CGFloat = (SCREEN_WIDTH - MAskImagePartLeadingSpace - MAskImagePartTrailingSpace - 2.0 * MAskImageDefaultSpaceHeight) / 3.0

let MAskImagePartThreeViewHeight:CGFloat = MAskImageDefaultWidth + MAskImageDefaultSpaceHeight * 2
let MAskImagePartSixViewHeight:CGFloat = MAskImageDefaultWidth * 2 + MAskImageDefaultSpaceHeight * 3
let MAskImagePartNineViewHeight:CGFloat = MAskImageDefaultWidth * 3 + MAskImageDefaultSpaceHeight * 4

/// 7-9张图片的View
class MAskImagePartNineView: MAskImagePartView {
    
    @IBOutlet weak var imgBtn1: UIButton!
    @IBOutlet weak var imgBtn2: UIButton!
    @IBOutlet weak var imgBtn3: UIButton!
    @IBOutlet weak var imgBtn4: UIButton!
    @IBOutlet weak var imgBtn5: UIButton!
    @IBOutlet weak var imgBtn6: UIButton!
    @IBOutlet weak var imgBtn7: UIButton!
    @IBOutlet weak var imgBtn8: UIButton!
    @IBOutlet weak var imgBtn9: UIButton!
    
    @IBOutlet weak var imgBtnWidth: NSLayoutConstraint!
    
    override var imageBtnArr:[UIButton]!{
        get{
            return [imgBtn1,imgBtn2,imgBtn3,imgBtn4,imgBtn5,
                    imgBtn6,imgBtn7,imgBtn8,imgBtn9]
        }
    }
    
    override class func loadFromNib() -> MAskImagePartNineView{
        
        let view = Bundle.main.loadNibNamed("MAskImagePartNineView", owner: nil, options: nil)?.last as! MAskImagePartNineView
        return view
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        
        imgBtnWidth.constant = MAskImageDefaultWidth
        for btn in self.imageBtnArr{
            btn.imageView?.contentMode = .scaleAspectFill
        }
    }
    @IBAction func imgBtnPressed(_ sender: UIButton) {
        
        invokeBlock(with: sender)
    }

}
