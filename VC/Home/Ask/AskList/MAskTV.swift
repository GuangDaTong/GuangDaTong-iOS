

import UIKit
import SwiftyJSON

/// 问问列表item
struct AskListItem {
    var id:String
    var title:String
    var createTime:String
    var publisherID:String
    var nickname:String
    var icon:String
    var gender:String
    var reward:String //悬赏
    var zan:Int
    var acceptCommentID:String //已采纳的评论的ID
    var type:String //问问类型：普问、急问、失物招领、寻物启事等等
    var label:String //标签：学习、旅游、交友等等
    var content:String //问问内容
    var link:String //分享到APP外部的链接
    var thumbnailImageURLs:[String] //问问缩略图
    var imageURLs:[String] //问问里的图片内容
    var commentCount:Int
    var isFavorite:Bool
    var isZan:Bool
    var isOwner:Bool //当前用户是否为问问发布人
    var isUrgent:Bool //是否急问
    var detailHeight:CGFloat
    var imgHeight:CGFloat
    var genderText:NSAttributedString
    var rewardText:NSAttributedString
    var askTagText:NSAttributedString
    var favorID:String //用于收藏列表
}

enum MAskTVLoadType:String {
    case mainList = "问问主列表"
    case searchList = "问问搜索结果"
    case collectList = "收藏列表"
    case mineAskList = "我的问问"
}

class MAskTV: MBaseTV {

    weak var ownerVC:MBaseVC?
    var loadType:MAskTVLoadType = .mainList
    var cellTouchBlock:((_ index:Int,_ eventType:AskCellTouchEventType) -> Void)?
    
    var searchRange:AskRangeType! = nil
    var searchText = ""
    var searchAskType:AskType! = nil
    var searchAskTag:AskTag! = nil
    
    var pageIndex = 0
    var listRangeType = AskRangeType.school
    
    fileprivate let ContentMaxHeight = UIFont.systemFont(ofSize: 14).lineHeight * DefaultAskContentMaxLine
    
    private func setup(){
        
        setup(withDelegate: self)
        self.separatorStyle = .none
        self.refreshData()
    }
    
    func setup(withLoadType type:MAskTVLoadType,touchEventBlock:((_ index:Int,_ eventType:AskCellTouchEventType) -> Void)?){
        
        self.loadType = type
        self.cellTouchBlock = touchEventBlock
        setup()
    }
    func setupAskSearch(range:AskRangeType,text:String,type:AskType?,tag:AskTag?){
        
        self.searchRange = range
        self.searchText = text
        self.searchAskType = type
        self.searchAskTag = tag
        setup()
    }
}

extension MAskTV:MBaseTVDelegate{
    
    func loadData(pageIndex: Int) {
        
        switch loadType {
        case .mainList:
            loadMainList(page: pageIndex)
        case .searchList:
            loadSearchList(page: pageIndex)
        case .collectList:
            loadCollectList(page: pageIndex)
        case .mineAskList:
            loadMineList(page: pageIndex)
        }
    }
    func nibsToRegister() -> [String] {
        return [MAskMainCellIdentifier,MAskImageCellIdentifier,MAskOperationCellIdentifier,MAskSingleImageCellIdentifier]
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        
        let count = dataArr.count
        if count > 0 {
            tableHeaderView?.isHidden = false
        }else{
            tableHeaderView?.isHidden = true
        }
        
        return count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if loadType == .collectList{
            return 1 // 收藏列表不显示图片和点赞
        }
        return 3
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var askCell:MAskVCCell! = nil
        
        let row = indexPath.row
        switch row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: MAskMainCellIdentifier) as! MAskMainCell
            cell.tintColor = DefaultColorRed
            askCell = cell
        case 1:
            let item = dataArr[indexPath.section] as! AskListItem
            if item.imageURLs.count == 1{
                askCell = tableView.dequeueReusableCell(withIdentifier: MAskSingleImageCellIdentifier) as! MAskSingleImageCell
            }else{
                askCell = tableView.dequeueReusableCell(withIdentifier: MAskImageCellIdentifier) as! MAskImageCell
            }
        case 2:
            askCell = tableView.dequeueReusableCell(withIdentifier: MAskOperationCellIdentifier) as! MAskOperationCell
        default:
            assertionFailure()
        }
        
        return askCell
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let askCell = cell as! MAskVCCell
        
        let section = indexPath.section
        let row = indexPath.row
        
        switch row {
        case 0:
            let cell = askCell as! MAskMainCell
            cell.tintColor = DefaultColorRed
            if loadType == .collectList{
                cell.isLoadingForCollection = true
            }
        default:
            break
        }
        
        let item = dataArr[section] as! AskListItem
        askCell.item = item
        askCell.cellTouchBlock = { [weak self] (touchType) in
            self?.cellTouchBlock?(indexPath.section,touchType)
        }
        
        if tableView.isEditing{
            askCell.selectionStyle = .default
        }else{
            askCell.selectionStyle = .none
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let section = indexPath.section
        let row = indexPath.row
        
        let item = dataArr[section]  as! AskListItem
        switch row {
        case 0:
            let totalHeight = item.detailHeight
            let contentHeight = totalHeight > ContentMaxHeight ? ContentMaxHeight : totalHeight
            return MAskMainPartViewHeight + contentHeight
        case 1:
            return item.imgHeight
        case 2:
            return MAskOperationPartViewHeight
        default:
            break
        }
        return  0
    }
    // iOS 11 如果不实现viewFor...InSection，就不会请求Header/Footer高度
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return nil
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return nil
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        
        let value = UITableViewCellEditingStyle.delete.rawValue |
            UITableViewCellEditingStyle.insert.rawValue
        return UITableViewCellEditingStyle.init(rawValue: value)!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView.isEditing{
            return
        }
        tableView.deselectRow(at: indexPath, animated: true)
        cellTouchBlock?(indexPath.section,.cellPressed)
    }
}
