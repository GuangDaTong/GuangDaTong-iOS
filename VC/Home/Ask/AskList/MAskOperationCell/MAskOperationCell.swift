

import UIKit

let MAskOperationCellIdentifier = "MAskOperationCell"

class MAskOperationCell: MAskVCCell {

    fileprivate let operationPart = MAskOperationPartView.loadFromNib()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        addBottomSeparateLine()
        
        contentView.addSubview(operationPart)
        operationPart.shareBtnPressedBlock = {
            self.cellTouchBlock?(.shareBtnPressed)
        }
        operationPart.zanBtnPressedBlock = {
            self.cellTouchBlock?(.zanBtnPressed)
        }
        operationPart.commentBtnPressedBlock = {
            self.cellTouchBlock?(.commentBtnPressed)
        }
        
        operationPart.snp.remakeConstraints { (make) in
            make.top.equalTo(contentView.snp.top)
            make.leading.equalTo(contentView.snp.leading)
            make.width.equalTo(contentView.snp.width)
            make.height.equalTo(MAskOperationPartViewHeight)
        }
    }
    
    override func setup(){
        
        let item = self.cellItem!
        let opreateP = self.operationPart
        
        opreateP.setup(isZan:item.isZan, zanCount: item.zan, commentCount: item.commentCount)
    }
    
}
