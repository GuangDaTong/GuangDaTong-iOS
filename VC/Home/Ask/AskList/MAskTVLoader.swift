

import Foundation
import SwiftyJSON

extension MAskTV{
    
    func loadMainList(page:Int){
        
        HttpController.shared.askList(range: listRangeType, pageIndex: page, successBlock: { (json) in
            self.processLoadSuccess(json: json)
            }, failure: { (err) in
                self.processLoadFail(err: err)
        })
    }
    func loadSearchList(page:Int){
        
        HttpController.shared.askSearchList(text: searchText, type: searchAskType, tag: searchAskTag, range: searchRange, pageIndex: page, successBlock: { (json) in
            self.processLoadSuccess(json: json)
        }, failure: { (err) in
            self.processLoadFail(err: err)
        })
    }
    func loadCollectList(page:Int){
        
        HttpController.shared.askCollectList(pageIndex: page, successBlock: { (json) in
            self.processLoadSuccess(json: json)
        }, failure: { (err) in
            self.processLoadFail(err: err)
        })
    }
    func loadMineList(page:Int){
        
        HttpController.shared.myAskList(pageIndex: page, successBlock: { (json) in
            self.processLoadSuccess(json: json)
        }, failure: { (err) in
            self.processLoadFail(err: err)
        })
    }
    
    func processLoadSuccess(json:JSON){
        
        let dataArr = self.parseJSON(json)
        self.appendData(dataArr)
    }
    func processLoadFail(err:Error){
        
        self.loadFailed()
        DLOG(err.localizedDescription)
        self.ownerVC?.toastFail(err.localizedDescription)
    }
    
    func parseJSON(_ json:JSON) -> [AskListItem]{
        
        let font = UIFont.systemFont(ofSize: 14)
        let userID = uUserID
        let currentDate = DefaultDateFormatter.date(from: stringOfNow())!
        
        let filterAsks = MAskFilter.shared.filteredAskForCurrentUser()
        let filterUsers = MAskFilter.shared.filteredUserForCurrentUser()
        var dataArr = [AskListItem]()
        for data in json["result"]["data"].arrayValue{
            
            let item = MAskTV.parseAskItem(data, userID: userID, font: font, currentDate: currentDate)
            // 过滤屏蔽的问问
            if filterAsks.contains(item.id){
                continue
            }
            // 过滤黑名单用户
            if filterUsers.contains(item.publisherID){
                continue
            }
            dataArr.append(item)
        }
        return dataArr
    }
    class func parseAskItem(_ data:JSON,userID:String,font:UIFont,currentDate:Date) -> AskListItem{
        
        let id:String = data["id"].stringValue
        let title:String = data["title"].stringValue
        let createTime:NSString = data["createTime"].stringValue as NSString
        let publisherID:String = data["publisherID"].stringValue
        let nickname:String = data["nickname"].stringValue
        let icon:String = data["icon"].stringValue
        let gender:String = data["gender"].stringValue
        let reward:String = data["reward"].stringValue
        let zanStr:NSString = data["zan"].stringValue as NSString
        let zan:Int = zanStr.integerValue
        let commentID:String = data["commentID"].stringValue
        let askType:String = data["type"].stringValue
        let askTag:String = data["label"].stringValue
        let content:String = data["content"].stringValue
        let link:String = data["link"].stringValue
        let commentCountStr:NSString = data["commentCount"].stringValue as NSString
        let commentCount:Int = commentCountStr.integerValue
        let isFavoriteStr:String = data["isFavorite"].stringValue
        let isFavorite:Bool = (isFavoriteStr == "1" ? true : false)
        let isZanStr:String = data["isZan"].stringValue
        let isZan:Bool = (isZanStr == "1" ? true : false)
        let favorID:String = data["favorID"].stringValue
        let imageArr:[JSON] = data["image"].arrayValue
        let thumbnailImageArr:[JSON] = data["thumbnailImage"].arrayValue
        
        var imageURLArr:[String] = [String]()
        for json in imageArr{
            let url:String = json.stringValue
            imageURLArr.append(url)
        }
        var thumbnailURLArr:[String] = [String]()
        for json in thumbnailImageArr{
            let url:String = json.stringValue
            thumbnailURLArr.append(url)
        }
        while imageURLArr.count > 9{
            imageURLArr.removeLast()
            DLOG("imageURLArr 问问图片数量异常！")
        }
        // 内容高度
        let contentStr:NSString = content as NSString
        let size:CGSize = CGSize.init(width: MAskMainPartTextContentWidth, height: CGFloat.greatestFiniteMagnitude)
        let resultSize:CGSize = contentStr.getSizeWith(font, constrainedTo: size)
        let detailHeight:CGFloat = resultSize.height
        //图片高度
        var imageHeight:CGFloat = 0
        switch imageURLArr.count {
        case 1:
            imageHeight = MAskImagePartSingleViewHeight
        case 2...3:
            imageHeight = MAskImagePartThreeViewHeight
        case 4...6:
            imageHeight = MAskImagePartSixViewHeight
        case 7...9:
            imageHeight = MAskImagePartNineViewHeight
        default:
            imageHeight = 0
        }
        // 时间
        var time:String? = createTime.shortenTime(for: currentDate)
        if time == nil{
            time = ""
        }
        // 性别
        let genderAttrStr:NSAttributedString = gender.attributedGenderString()
        // 悬赏
        var rewardAttrStr:NSMutableAttributedString! = nil
        if reward != "0"{
            rewardAttrStr = NSMutableAttributedString.init(string: "悬赏: \(reward)金币")
            let rewardRange:NSRange = NSRange.init(location: 3, length: rewardAttrStr.length - 5)
            rewardAttrStr.addAttributes([NSAttributedStringKey.foregroundColor:DefaultColorRed], range: rewardRange)
        }else{
            rewardAttrStr = NSMutableAttributedString.init(string: "")
        }
        // 问问标签
        let askTagAttrStr:NSMutableAttributedString = NSMutableAttributedString.init(string: "#\(askTag)")
        let typeRange:NSRange = NSRange.init(location: 0, length: askTagAttrStr.length)
        switch askTagAttrStr.string {
        case "#寻物启事":
            askTagAttrStr.setAttributes([NSAttributedStringKey.foregroundColor:DefaultColorRed], range: typeRange)
        case "#失物招领":
            askTagAttrStr.setAttributes([NSAttributedStringKey.foregroundColor:DefaultColorGreen], range: typeRange)
        default:
            askTagAttrStr.setAttributes([NSAttributedStringKey.foregroundColor:DefaultColorYellow], range: typeRange)
        }
        let isUrgent:Bool = (askType == "急问")
        let isOwner:Bool = (publisherID == userID)
        let item:AskListItem = AskListItem.init(id: id,title: title, createTime: time!, publisherID: publisherID, nickname: nickname, icon: icon, gender: gender, reward: reward, zan: zan, acceptCommentID: commentID, type: askType, label: askTag, content: content, link: link,thumbnailImageURLs:thumbnailURLArr, imageURLs: imageURLArr, commentCount: commentCount,isFavorite: isFavorite,isZan: isZan, isOwner: isOwner,isUrgent:isUrgent,detailHeight: detailHeight, imgHeight: imageHeight, genderText: genderAttrStr, rewardText: rewardAttrStr, askTagText: askTagAttrStr,favorID:favorID)
        return item
    }
}
