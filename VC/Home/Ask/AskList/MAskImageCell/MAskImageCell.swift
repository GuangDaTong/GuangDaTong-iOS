

import UIKit

let MAskImageCellIdentifier = "MAskImageCell"

class MAskImageCell: MAskVCCell {
    
    fileprivate let nineImgView = MAskImagePartNineView.loadFromNib()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        contentView.addSubview(nineImgView)
        
        let ImageIVWidth = SCREEN_WIDTH - MAskImagePartLeadingSpace - MAskImagePartTrailingSpace
        
        self.nineImgView.snp.remakeConstraints { (make) in
            make.top.equalTo(contentView.snp.top)
            make.leading.equalTo(contentView.snp.leading).offset(MAskImagePartLeadingSpace)
            make.width.equalTo(ImageIVWidth)
            make.height.equalTo(0)
        }
    }
    override func setup(){
        
        let item = self.cellItem!
        
        let nineP = self.nineImgView
        self.nineImgView.snp.updateConstraints { (make) in
            make.height.equalTo(item.imgHeight)
        }
        nineP.setupImages(with: item.thumbnailImageURLs)
        nineP.setupHDImages(with: item.imageURLs)
    }
    
}
