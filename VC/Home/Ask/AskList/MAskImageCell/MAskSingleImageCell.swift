

import UIKit

let MAskSingleImageCellIdentifier = "MAskSingleImageCell"

class MAskSingleImageCell: MAskVCCell {
    
    fileprivate let singleImageView = MAskImagePartSingleView.loadFromNib()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        contentView.addSubview(singleImageView)
        
        let ImageIVWidth = SCREEN_WIDTH - MAskImagePartLeadingSpace * 2
        
        self.singleImageView.snp.remakeConstraints { (make) in
            make.top.equalTo(contentView.snp.top)
            make.leading.equalTo(contentView.snp.leading).offset(MAskImagePartLeadingSpace)
            make.width.equalTo(ImageIVWidth)
            make.height.equalTo(0)
        }
    }
    override func setup(){
        
        let item = self.cellItem!
        
        singleImageView.snp.updateConstraints { (make) in
            make.height.equalTo(item.imgHeight)
        }
        singleImageView.setupImages(with: item.thumbnailImageURLs)
        singleImageView.setupHDImages(with: item.imageURLs)
    }
    
}

