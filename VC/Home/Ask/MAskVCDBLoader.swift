

import Foundation

extension MAskVC{
    
    func loadDB(){
        
        let q:FMDatabaseQueue = FMDatabaseQueue(url: DefaultURLForDB)
        q.inDatabase { (db) in
            
            if !self.createTable(db: db){
                return
            }
            db.select(from: DBTableNameForAsk, keys: dbKeys, { (res) in
                
                guard let r:FMResultSet = res else{ return }
                self.askTV.dataArr = self.parseResult(r)
                DispatchQueue.main.async{
                    self.view.hideToastActivity()
                }
            })
        }
    }
    func parseResult(_ r:FMResultSet) -> [AskListItem]{
        
        var list:[AskListItem] = [AskListItem]()
        while r.next(){
            let id:String = r.string(forCol: 0)
            let title:String = r.string(forCol: 1)
            let time:String = r.string(forCol: 2)
            let pubID:String = r.string(forCol: 3)
            let name:String = r.string(forCol: 4)
            let icon:String = r.string(forCol: 5)
            let gender:String = r.string(forCol: 6)
            let reward:String = r.string(forCol: 7)
            let zan:Int = r.int(forCol: 8)
            let acceptCommentID:String = r.string(forCol: 9)
            let type:String = r.string(forCol: 10)
            let label:String = r.string(forCol: 11)
            let content:String = r.string(forCol: 12)
            let link:String = r.string(forCol: 13)
            var nailImages:[String]? = r.object(forCol: 14) as? [String]
            if nailImages == nil{
                nailImages = [String]()
            }
            let hdImages:[String] = (r.object(forCol: 15) as? [String]) ?? [String]()
            let cCount:Int = r.int(forCol: 16)
            let isCollect:Bool = r.bool(forCol: 17)
            let isZan:Bool = r.bool(forCol: 18)
            let isOwner:Bool = r.bool(forCol: 19)
            let isUrgent:Bool = r.bool(forCol: 20)
            let dHeight:CGFloat = r.float(forCol: 21)
            let iHeight:CGFloat = r.float(forCol: 22)
            let gText:NSAttributedString = (r.object(forCol: 23) as? NSAttributedString) ?? NSAttributedString.init(string: "")
            let rText:NSAttributedString = (r.object(forCol: 24) as? NSAttributedString) ?? NSAttributedString.init(string: "")
            let tText:NSAttributedString = (r.object(forCol: 25) as? NSAttributedString) ?? NSAttributedString.init(string: "")
            
            let item = AskListItem.init(id: id, title: title, createTime: time, publisherID: pubID, nickname: name, icon: icon, gender: gender, reward: reward, zan: zan, acceptCommentID: acceptCommentID, type: type, label: label, content: content, link: link, thumbnailImageURLs: nailImages!, imageURLs: hdImages, commentCount: cCount, isFavorite: isCollect, isZan: isZan, isOwner: isOwner, isUrgent: isUrgent, detailHeight: dHeight, imgHeight: iHeight, genderText: gText, rewardText: rText, askTagText: tText, favorID: "")
            list.append(item)
        }
        return list
    }
    func createTable(db:FMDatabase) -> Bool{
        guard db.create(DBTableNameForAsk,
                        colDict:["id":.text,"title":.text,"createTime":.text,
                                 "publisherID":.text,"nickname":.text,
                                 "icon":.text,"gender":.text,"reward":.text,
                                 "zan":.int,"acceptCommentID":.text,"type":.text,
                                 "label":.text,"content":.text,"link":.text,
                                 "thumbnailImageURLs":.text,"imageURLs":.text,
                                 "commentCount":.int,"isFavorite":.bool,
                                 "isZan":.bool,"isOwner":.bool,
                                 "isUrgent":.bool,"detailHeight":.real,
                                 "imgHeight":.real,"genderText":.blob,
                                 "rewardText":.blob,"askTagText":.blob]) else{
                                    DLOG("create table failed")
                                    return false
        }
        return true
    }
    func saveToDB(){
        
        let q:FMDatabaseQueue = FMDatabaseQueue(url: DefaultURLForDB)
        q.inDatabase { (db) in
            if !db.delete(from: DBTableNameForAsk){
                DLOG("删除问问主列表旧数据，失败！！！")
            }
            for item in self.askTV.dataArr{
                let i = item as! AskListItem
                let nailArr = i.thumbnailImageURLs
                let hdArr = i.imageURLs as NSArray
                let nailData = db.data(forObject: nailArr)
                let hdData = db.data(forObject: hdArr)
                let gData = db.data(forObject: i.genderText)
                let rText = db.data(forObject: i.rewardText)
                let aText = db.data(forObject: i.askTagText)
                _ = db.insert(into: DBTableNameForAsk, dict: ["id":i.id,"title":i.title,"createTime":i.createTime,
                                                              "publisherID":i.publisherID,"nickname":i.nickname,
                                                              "icon":i.icon,"gender":i.gender,"reward":i.reward,
                                                              "zan":i.zan,"acceptCommentID":i.acceptCommentID,"type":i.type,
                                                              "label":i.label,"content":i.content,"link":i.link,
                                                              "thumbnailImageURLs":nailData,"imageURLs":hdData,
                                                              "commentCount":i.commentCount,"isFavorite":i.isFavorite,
                                                              "isZan":i.isZan,"isOwner":i.isOwner,
                                                              "isUrgent":i.isUrgent,"detailHeight":i.detailHeight,
                                                              "imgHeight":i.imgHeight,"genderText":gData,
                                                              "rewardText":rText,"askTagText":aText])
            }
        }
    }

}
