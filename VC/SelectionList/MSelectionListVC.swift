

import UIKit

class MSelectionListVC: MBaseVC,UITableViewDelegate,UITableViewDataSource {

    var selectedStr:String! = nil
    var selectedBlock:((String) -> Void)! = nil
    var list:[String] = [String]()
    @IBOutlet weak var selectionTV: BaseTV!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        selectionTV.separatorStyle = .none
        selectionTV.register(UINib.init(nibName: "MSelectionListVCCell", bundle: nil), forCellReuseIdentifier: MSelectionListVCCellIdentifier)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.selectionTV.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: MSelectionListVCCellIdentifier) as! MSelectionListVCCell
        let str = list[indexPath.row]
        cell.titleLB.text = str
        if selectedStr != nil && str == selectedStr {
            cell.selectIV.isHidden = false
        }else{
            cell.selectIV.isHidden = true
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        selectedStr = list[indexPath.row]
        if selectedBlock != nil {
            selectedBlock(list[indexPath.row])
        }
        tableView.deselectRow(at: indexPath, animated: true)
        self.navigationController?.popViewController(animated: true)
    }
}
