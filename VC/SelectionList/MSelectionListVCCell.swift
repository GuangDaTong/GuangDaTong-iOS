

import UIKit

let MSelectionListVCCellIdentifier = "MSelectionListVCCell"

class MSelectionListVCCell: MBaseTVCell {

    @IBOutlet weak var titleLB: UILabel!
    @IBOutlet weak var selectIV: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        addBottomSeparateLine()
    }
}
