

import Foundation
import SwiftyJSON

struct MMsgCenterNoticeItem {
    var id:String
    var nickname:String
    var iconURL:String
    var gender:NSAttributedString
    var senderID:String
    var createTime:String
    var type:MMsgCenterNoticeType
    var detailStr:String
    var detailAttrStr:NSAttributedString
    var contentHeight:CGFloat
    var isUnread:Bool
}

/// 系统通知类型
enum MMsgCenterNoticeType:String {
    /// 用户被举报
    case zanUser
    /// 赠送喵币
    case giveCoins
    /// 索要喵币
    case wantCoins
    /// 帖子被举报
    case askInformed
    /// 反馈被采纳
    case reportAdopted
    /// 建议被采纳
    case adviseAdopted
    /// 帖子被删除
    case askDeleted
    /// 帖子评论被删除
    case commentAskDeleted
    /// 被添加联系人
    case contactAdded
    /// @objc 用户被举报
    case userInformed
    /// 禁用聊天功能
    case chatForbidden
    /// 举报被@objc 奖赏
    case rewardForInform
    /// 帖子评论被举报
    case commentAskInformed
    /// 反馈未被采纳
    case reportRejected
    /// 建议未被采纳
    case adviseRejected
    /// 举报无效
    case informRejected
    /// 举报功能被禁用
    case informForbidden
}

class MMsgCenterNoticeTV: MMsgCenterTV {
    
    override var listType:MsgCenterListType{
        return .notice
    }
}

extension MMsgCenterNoticeTV{
    
    @objc override func loadData(pageIndex:Int) -> Void{
        
        let keys = DBKeysForNotice.keys.sorted()
        MMsgLoader.loadDBMsgFor(.notice, keys: keys, pageIndex: pageIndex) { (objArray) in
            
            let msgArray = objArray as! [MMsgCenterNoticeItem]
            self.appendData(msgArray)
            self.loadFinishedBlock?(.notice)
        }
    }
    @objc override func nibsToRegister() -> [String]{
        return [MMsgCenterVCCellIdentifier]
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell = tableView.dequeueReusableCell(withIdentifier: MMsgCenterVCCellIdentifier) as! MMsgCenterVCCell
        cell.selectionStyle = .none
        
        let item = dataArr[indexPath.row] as! MMsgCenterNoticeItem
        let d = ownerVC?.userDict
        let sender = item.senderID
        if let info = d?[sender]{ //获取内存缓存
            cell.setup(name: info.name, icon: info.icon, gender: info.gender, time: item.createTime, content: item.detailAttrStr,unread: item.isUnread)
        }else{
            cell.setup(name: item.nickname, icon: item.iconURL, gender: item.gender, time: item.createTime, content: item.detailAttrStr,unread: item.isUnread)
        }
        
        cell.iconBtnPressedBlock = {
            self.iconBtnPressedBlock?(indexPath.row)
        }
        
        return cell
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let item = dataArr[indexPath.row] as! MMsgCenterNoticeItem
        return item.contentHeight
    }
}
