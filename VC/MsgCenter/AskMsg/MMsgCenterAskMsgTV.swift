

import Foundation
import SwiftyJSON

struct MMsgCenterAskMsgItem {
    var id:String
    var askID:String
    var nickname:String
    var iconURL:String
    var gender:NSAttributedString
    var senderID:String
    var createTime:String
    var type:MMsgCenterAskMsgType
    var detail:NSAttributedString
    var contentHeight:CGFloat
    var isUnread:Bool
}

enum MMsgCenterAskMsgType:String {
    case zanAsk
    case adoptComment
    case favoriteAsk
    case commentAsk
    case commentUser
}

class MMsgCenterAskMsgTV: MMsgCenterTV {
    
    override var listType: MsgCenterListType{
        return .askMsg
    }
    
}

extension MMsgCenterAskMsgTV{
    
    @objc override func loadData(pageIndex:Int) -> Void{
        
        let keys = DBKeysForAskMsg.keys.sorted()
        MMsgLoader.loadDBMsgFor(.askMsg, keys: keys, pageIndex: pageIndex) { (objArray) in
            
            let msgArray = objArray as! [MMsgCenterAskMsgItem]
            self.appendData(msgArray)
            self.loadFinishedBlock?(.askMsg)
        }
    }
    @objc override func nibsToRegister() -> [String]{
        return [MMsgCenterVCCellIdentifier]
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell = tableView.dequeueReusableCell(withIdentifier: MMsgCenterVCCellIdentifier) as! MMsgCenterVCCell
        cell.selectionStyle = .none
        
        let item = dataArr[indexPath.row] as! MMsgCenterAskMsgItem
        let d = ownerVC?.userDict
        let sender = item.senderID
        if let info = d?[sender]{ //获取内存缓存
            cell.setup(name: info.name, icon: info.icon, gender: info.gender, time: item.createTime, content: item.detail,unread: item.isUnread)
        }else{
            cell.setup(name: item.nickname, icon: item.iconURL, gender: item.gender, time: item.createTime, content: item.detail,unread: item.isUnread)
        }
        
        cell.iconBtnPressedBlock = {
            self.iconBtnPressedBlock?(indexPath.row)
        }
        return cell
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let item = dataArr[indexPath.row] as! MMsgCenterAskMsgItem
        return item.contentHeight
    }
}
