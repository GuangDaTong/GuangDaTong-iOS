

import Foundation
import SwiftyJSON

struct MMsgCenterLeaveMsgItem {
    var id:String
    var nickname:String
    var iconURL:String
    var gender:NSAttributedString
    var senderID:String
    var createTime:String
    var content:NSAttributedString
    var contentHeight:CGFloat
    var isUnread:Bool
}

class MMsgCenterLeaveMsgTV: MMsgCenterTV {
    
    override var listType:MsgCenterListType{
        return .leaveMsg
    }
}


extension MMsgCenterLeaveMsgTV{
    
    func checkUserInfo(for msgArray:inout [MMsgCenterLeaveMsgItem] ){
        
        var IDs:[String] = [String]() //需要更新信息的用户的ID
        let count:Int = msgArray.count
        let dict:[String:MLatestUserInfo]? = self.ownerVC?.userDict
        for index in 0..<count{
            let id:String = msgArray[index].senderID
            if let info:MLatestUserInfo = dict?[id]{ // 没有对应的内存缓存
                msgArray[index].iconURL = info.icon
                msgArray[index].nickname = info.name
                msgArray[index].gender = info.gender
            }else if !IDs.contains(id){ // 没有需要更新的ID
                IDs.append(id)
            }
        }
        if IDs.count > 0{
            self.ownerVC?.checkUserInfo(IDs)
        }
    }
    @objc override func loadData(pageIndex:Int) -> Void{
        
        let keys:[String] = DBKeysForLeaveMsg.keys.sorted()
        MMsgLoader.loadDBMsgFor(.leaveMsg, keys: keys, pageIndex: pageIndex) { (objArray) in
            
            var msgArray = objArray as! [MMsgCenterLeaveMsgItem]
            self.checkUserInfo(for: &msgArray)
            
            self.appendData(msgArray)
            self.loadFinishedBlock?(.leaveMsg)
        }
    }
    @objc override func nibsToRegister() -> [String]{
        return [MMsgCenterVCCellIdentifier]
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell = tableView.dequeueReusableCell(withIdentifier: MMsgCenterVCCellIdentifier) as! MMsgCenterVCCell
        cell.selectionStyle = .none
        
        let item = dataArr[indexPath.row] as! MMsgCenterLeaveMsgItem
        
        let d = ownerVC?.userDict
        let sender = item.senderID
        if let info = d?[sender]{ //获取内存缓存
            cell.setup(name: info.name, icon: info.icon, gender: info.gender, time: item.createTime, content: item.content,unread: item.isUnread)
        }else{
            cell.setup(name: item.nickname, icon: item.iconURL, gender: item.gender, time: item.createTime, content: item.content,unread: item.isUnread)
        }
        cell.iconBtnPressedBlock = {
            self.iconBtnPressedBlock?(indexPath.row)
        }
        
        return cell
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let item = dataArr[indexPath.row] as! MMsgCenterLeaveMsgItem
        return item.contentHeight
    }
}
