

import UIKit

let MMsgCenterVCCellIdentifier = "MMsgCenterVCCell"

class MMsgCenterVCCell: MBaseTVCell {

    static let MinHeight:CGFloat = 72
    // 消息内容的label的最小高度
    static let ContentMinCountHeight:CGFloat = 30
    
    var iconBtnPressedBlock:(() -> Void)?
    @IBOutlet weak var iconBtn: UIButton!
    @IBOutlet weak var nameLB: UILabel!
    @IBOutlet weak var genderLB: UILabel!
    @IBOutlet weak var timeLB: UILabel!
    @IBOutlet weak var contentLB: MCopyLabel!
    @IBOutlet weak var badgeView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        iconBtn.imageView?.contentMode = .scaleAspectFill
        iconBtn.imageView?.clipsToBounds = true
        iconBtn.setCornerRounded(radius: 4)
        badgeView.setCornerRounded(radius: 4)
        addBottomSeparateLine()
    }
    func setup(name:String,icon:String,gender:NSAttributedString,time:String,content:NSAttributedString,unread:Bool){
        
        nameLB.text = name
        iconBtn.sd_setImage(with: URL.init(string: icon), for: .normal, placeholderImage: DefaultUserIcon)
        genderLB.attributedText = gender
        timeLB.text = time
        contentLB.attributedText = content
        badgeView.isHidden = !unread
    }
    @IBAction func iconBtnPressed(_ sender: Any) {
        
        iconBtnPressedBlock?()
    }
}
