

import Foundation



class MMsgCenterTV: MBaseTV {
    
    weak var ownerVC:MMsgCenterVC?
    var listType:MsgCenterListType{
        return .askMsg
    }
    /// 后端数据加载完成后的回调，用于决定是否显示清空按钮
    let dateFormatter = DefaultDateFormatter
    
    var loadFinishedBlock:((_ type:MsgCenterListType) -> Void)?
    var cellPressedBlock:((_ index:Int) -> Void)?
    var iconBtnPressedBlock:((_ index:Int) -> Void)?
    
    override init(frame: CGRect, style: UITableViewStyle) {
        super.init(frame: frame, style: style)
        
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    func setup(){
        setup(withDelegate: self)
    }
    
}

extension MMsgCenterTV:MBaseTVDelegate{
    
    func loadData(pageIndex:Int) -> Void{
        assertionFailure("子类必需实现此方法")
    }
    func nibsToRegister() -> [String]{
        assertionFailure("子类必需实现此方法")
        return [""]
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return dataArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        assertionFailure("子类必需实现此方法")
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        assertionFailure("子类必需实现此方法")
        return 0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        cellPressedBlock?(indexPath.row)
    }
    
}
