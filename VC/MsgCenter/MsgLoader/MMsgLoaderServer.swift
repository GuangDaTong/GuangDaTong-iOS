

import Foundation
import SwiftyJSON

extension MMsgLoader{
    
    /// 从后端读取消息中心数据
    class func loadServerMsg() -> Void{
        
        HttpController.shared.msgCenter(type: MsgCenterListType.leaveMsg, pageIndex: 0, successBlock: { (json) in
            
            let jsonArr:[JSON] = json["result"]["data"].arrayValue
            if jsonArr.count == 0{
                return
            }
            var msgArr:[MMsgCenterLeaveMsgItem] = [MMsgCenterLeaveMsgItem]()
            
            for itemJSON in jsonArr{
                let item = MMsgLoader.parseLeaveMsgItem(fromJSON: itemJSON)
                msgArr.append(item)
            }
            MMsgLoader.saveLeaveMsg(msgArr)
        }, failure: { (err) in
            loadMsgFromServerFailed(err)
        })
        
        if HttpController.shared.isNetworkAvailable {
            MMsgLoader.loadAskMsgFromServer()
            MMsgLoader.loadNoticeMsgFromServer()
        }
    }
    class func loadAskMsgFromServer(){
        HttpController.shared.msgCenter(type: MsgCenterListType.askMsg, pageIndex: 0, successBlock: { (json) in
            
            let jsonArr:[JSON] = json["result"]["data"].arrayValue
            if jsonArr.count == 0{
                return
            }
            var askMsgArr:[MMsgCenterAskMsgItem] = [MMsgCenterAskMsgItem]()
            for itemJSON in jsonArr{
                let item = MMsgLoader.parseAskMsgItem(fromJSON: itemJSON)
                askMsgArr.append(item)
            }
            MMsgLoader.saveAskMsg(askMsgArr)
        }, failure: { (err) in
            loadMsgFromServerFailed(err)
        })
    }
    class func loadNoticeMsgFromServer(){
        HttpController.shared.msgCenter(type: MsgCenterListType.notice, pageIndex: 0, successBlock: { (json) in
            
            let jsonArr:[JSON] = json["result"]["data"].arrayValue
            if jsonArr.count == 0{
                return
            }
            var noticeArr:[MMsgCenterNoticeItem] = [MMsgCenterNoticeItem]()
            for itemJSON in jsonArr{
                let item = MMsgLoader.parseNoticeItem(fromJSON: itemJSON)
                noticeArr.append(item)
            }
            MMsgLoader.saveNoticeMsg(noticeArr)
        }, failure: { (err) in
            loadMsgFromServerFailed(err)
        })
    }
    class func loadMsgFromServerFailed(_ err:Error){
        
        DLOG(err.localizedDescription)
    }
    
    class func parseLeaveMsgItem(fromJSON data:JSON) -> MMsgCenterLeaveMsgItem{
        
        let id:String = data["id"].stringValue
        let nickname:String = data["nickname"].stringValue
        let iconURL:String = data["icon"].stringValue
        let gender:String = data["gender"].stringValue
        let senderID:String = data["senderID"].stringValue
        let createTime:NSString = data["createTime"].stringValue as NSString
        var content:String = data["words"].stringValue
        
        content = content.count > 0 ? content : "无"
        
        let genderAttrStr:NSAttributedString = gender.attributedGenderString()
        let height:CGFloat = MMsgLoader.countContentHeight(content: content)
        
        let time:String = createTime.timeShort() ?? "1970-12-31 11:11:11"
        let detailAttrStr:NSAttributedString = NSAttributedString.init(string: content)
        
        let item:MMsgCenterLeaveMsgItem = MMsgCenterLeaveMsgItem.init(id: id, nickname: nickname, iconURL: iconURL, gender: genderAttrStr, senderID: senderID, createTime: time, content: detailAttrStr, contentHeight: height,isUnread: true)
        
        return item
    }
    class func parseAskMsgItem(fromJSON data:JSON) -> MMsgCenterAskMsgItem{
        
        let id:String = data["id"].stringValue
        let askID:String = data["askID"].stringValue
        let nickname:String = data["nickname"].stringValue
        let iconURL:String = data["icon"].stringValue
        let gender:String = data["gender"].stringValue
        let senderID:String = data["senderID"].stringValue
        let createTime:NSString = data["createTime"].stringValue as NSString
        let typeStr:String = data["type"].stringValue
        var detailStr:String = data["detail"].stringValue
        
        detailStr = detailStr.count > 0 ? detailStr : "{}"
        
        let genderAttrStr:NSAttributedString = gender.attributedGenderString()
        let time:String = createTime.timeShort() ?? "1970-12-31 11:11:11"
        
        let detailJSON:JSON = JSON.init(parseJSON: detailStr)
        let title:String = detailJSON["title"].stringValue
        let titleStr:NSString = title as NSString
        let titleLen:Int = titleStr.length
        
        let type:MMsgCenterAskMsgType = MMsgCenterAskMsgType(rawValue: typeStr) ?? .zanAsk
        var detailAttrStr:NSMutableAttributedString! = nil
        let attribute:[NSAttributedStringKey:Any] = [NSAttributedStringKey.foregroundColor:AppThemeColor,
                                      NSAttributedStringKey.underlineStyle:NSUnderlineStyle.styleSingle.rawValue]
        switch type {
        case .zanAsk:
            detailAttrStr = NSMutableAttributedString.init(string: "TA赞了你的帖子\(title)")
            let detailRange:NSRange = NSRange.init(location: 8, length: titleLen)
            detailAttrStr.addAttributes(attribute, range: detailRange)
        case .adoptComment:
            let coins:String = detailJSON["coins"].stringValue
            // title
            detailAttrStr = NSMutableAttributedString.init(string: "我在帖子\(title)采纳了你的评论，\(coins)个金币归你啦！")
            var detailRange:NSRange = NSRange.init(location: 4, length: titleLen)
            detailAttrStr.addAttributes(attribute, range: detailRange)
            // coin
            detailRange = NSRange.init(location: 4 + titleLen + 8, length: (coins as NSString).length)
            let coinAttr:[NSAttributedStringKey:Any] = [NSAttributedStringKey.foregroundColor:DefaultColorRed]
            detailAttrStr.addAttributes(coinAttr, range: detailRange)
        case .favoriteAsk:
            detailAttrStr = NSMutableAttributedString.init(string: "我收藏了你的帖子\(title)")
            let detailRange:NSRange = NSRange.init(location: 8, length: titleLen)
            detailAttrStr.addAttributes(attribute, range: detailRange)
        case .commentAsk:
            let comment:String = detailJSON["comment"].stringValue
            // title
            detailAttrStr = NSMutableAttributedString.init(string: "我回复了你的帖子\(title)：\(comment)")
            let detailRange:NSRange = NSRange.init(location: 8, length: titleLen)
            detailAttrStr.addAttributes(attribute, range: detailRange)
        case .commentUser:
            let comment:String = detailJSON["comment"].stringValue
            detailAttrStr = NSMutableAttributedString.init(string: "我在帖子\(title)@你：\(comment)")
            let detailRange:NSRange = NSRange.init(location: 4, length: titleLen)
            detailAttrStr.addAttributes(attribute, range: detailRange)
        }
        let height:CGFloat = self.countContentHeight(content: detailAttrStr.string)
        
        let item:MMsgCenterAskMsgItem = MMsgCenterAskMsgItem.init(id: id,askID: askID,nickname: nickname, iconURL: iconURL, gender: genderAttrStr, senderID: senderID, createTime: time, type: type, detail: detailAttrStr, contentHeight: height,isUnread: true)
        
        return item
    }
    class func parseNoticeItem(fromJSON data:JSON) -> MMsgCenterNoticeItem{
        
        let id:String = data["id"].stringValue
        let nickname:String = data["nickname"].stringValue
        let iconURL:String = data["icon"].stringValue
        let gender:String = data["gender"].stringValue
        let senderID:String = data["senderID"].stringValue
        let createTime:NSString = data["createTime"].stringValue as NSString
        let typeStr:String = data["type"].stringValue
        var detailStr:String = data["detail"].stringValue
        
        detailStr = detailStr.count > 0 ? detailStr : "{}"
        
        let genderAttrStr:NSAttributedString = gender.attributedGenderString()
        let time:String = createTime.timeShort() ?? "1970-12-31 11:11:11"
        
        let detailJSON:JSON = JSON.init(parseJSON: detailStr)
        
        let type:MMsgCenterNoticeType = MMsgCenterNoticeType(rawValue: typeStr) ?? .zanUser
        var detailAttrStr:NSMutableAttributedString! = nil
        
        switch type {
        case .zanUser:
            detailAttrStr = NSMutableAttributedString.init(string: "我赞了你的主页!")
        case .giveCoins:
            let coins:String = detailJSON["coins"].stringValue
            let words:String = detailJSON["words"].stringValue
            if words.count > 0 {
                detailAttrStr = NSMutableAttributedString.init(string: "我赠送了你\(coins)金币。\n\n留言：\(words)")
            }else{
                detailAttrStr = NSMutableAttributedString.init(string: "我赠送了你\(coins)金币。")
            }
            let detailRange:NSRange = NSRange.init(location: 5, length: (coins as NSString).length)
            let coinAttr:[NSAttributedStringKey:Any] = [NSAttributedStringKey.foregroundColor:DefaultColorRed]
            detailAttrStr.addAttributes(coinAttr, range: detailRange)
        case .wantCoins:
            let coins:String = detailJSON["coins"].stringValue
            let words:String = detailJSON["words"].stringValue
            if words.count > 0 {
                detailAttrStr = NSMutableAttributedString.init(string: "我向你索要\(coins)金币。\n\n留言：\(words)")
            }else{
                detailAttrStr = NSMutableAttributedString.init(string: "我向你索要\(coins)金币。")
            }
            let detailRange:NSRange = NSRange.init(location: 5, length: (coins as NSString).length)
            let coinAttr:[NSAttributedStringKey:Any] = [NSAttributedStringKey.foregroundColor:DefaultColorRed]
            detailAttrStr.addAttributes(coinAttr, range: detailRange)
        case .askInformed:
            let title:String = detailJSON["title"].stringValue
            let titleLen:Int = (title as NSString).length
            let reason:String = detailJSON["reason"].stringValue
            detailAttrStr = NSMutableAttributedString.init(string: "亲，有人举报您的帖子：\(title)\n举报理由：\(reason)\n若举报属实，请您尽快自行删除！")
            // title
            let attribute:[NSAttributedStringKey:Any] = [NSAttributedStringKey.foregroundColor:AppThemeColor,
                                          NSAttributedStringKey.underlineStyle:NSUnderlineStyle.styleSingle.rawValue]
            var detailRange:NSRange = NSRange.init(location: 11, length: titleLen)
            detailAttrStr.addAttributes(attribute, range: detailRange)
            // reason
            let reasonBegin:Int = 11 + titleLen + 6
            detailRange = NSRange.init(location: reasonBegin, length: detailAttrStr.length - reasonBegin)
            detailAttrStr.addAttributes([NSAttributedStringKey.foregroundColor:DefaultColorRed], range: detailRange)
        case .userInformed: //reason
            let reason:String = detailJSON["reason"].stringValue
            detailAttrStr = NSMutableAttributedString.init(string: "亲，有人举报您！\n举报理由：\(reason)\n请您立刻停止这种行为，否则您的留言功能可能会被禁用！")
            let attribute:[NSAttributedStringKey:Any] = [NSAttributedStringKey.foregroundColor:DefaultColorRed]
            let detailRange:NSRange = NSRange.init(location: 14, length: detailAttrStr.length - 14)
            detailAttrStr.addAttributes(attribute, range: detailRange)
        case .commentAskInformed:
            let title:String = detailJSON["title"].stringValue
            let titleLen:Int = (title as NSString).length
            let comment:String = detailJSON["comment"].stringValue
            let commentLen:Int = (comment as NSString).length
            let reason:String = detailJSON["reason"].stringValue
            
            detailAttrStr = NSMutableAttributedString.init(string: "亲，您在帖子\(title)里发表的评论\(comment)被人举报了！\n举报理由：\(reason)\n若举报属实，请您尽快自行删除！")
            // title
            let attribute:[NSAttributedStringKey:Any] = [NSAttributedStringKey.foregroundColor:AppThemeColor,
                                          NSAttributedStringKey.underlineStyle:NSUnderlineStyle.styleSingle]
            var detailRange:NSRange = NSRange.init(location: 6, length: titleLen)
            detailAttrStr.addAttributes(attribute, range: detailRange)
            // comment
            let commentBegin:Int = 6 + titleLen + 6
            detailRange = NSRange.init(location: commentBegin, length: commentLen)
            detailAttrStr.addAttributes(attribute, range: detailRange)
            // reason
            let reasonBegin:Int = commentBegin + commentLen + 12
            detailRange = NSRange.init(location: reasonBegin, length: detailAttrStr.length - reasonBegin)
            detailAttrStr.addAttributes([NSAttributedStringKey.foregroundColor:DefaultColorRed], range: detailRange)
        case .contactAdded:
            detailAttrStr = NSMutableAttributedString.init(string: "我添加你为联系人啦！")
        case .rewardForInform:
            let coins:String = detailJSON["coins"].stringValue
            detailAttrStr = NSMutableAttributedString.init(string: "亲，您的举报已被系统查实有效，感谢您！\n系统赠送了您\(coins)金币！")
            let detailRange:NSRange = NSRange.init(location: 26, length: (coins as NSString).length)
            let coinAttr:[NSAttributedStringKey:Any] = [NSAttributedStringKey.foregroundColor:DefaultColorRed]
            detailAttrStr.addAttributes(coinAttr, range: detailRange)
        case .reportAdopted:
            let coins:String = detailJSON["coins"].stringValue
            detailAttrStr = NSMutableAttributedString.init(string: "亲，您的反馈已被系统采纳，感谢您！\n系统赠送了您\(coins)金币！")
            let detailRange:NSRange = NSRange.init(location: 24, length: (coins as NSString).length)
            let coinAttr:[NSAttributedStringKey:Any] = [NSAttributedStringKey.foregroundColor:DefaultColorRed]
            detailAttrStr.addAttributes(coinAttr, range: detailRange)
        case .adviseAdopted:
            let coins:String = detailJSON["coins"].stringValue
            detailAttrStr = NSMutableAttributedString.init(string: "亲，您的建议已被系统采纳，感谢您！\n系统赠送了您\(coins)金币！")
            let detailRange:NSRange = NSRange.init(location: 24, length: (coins as NSString).length)
            let coinAttr:[NSAttributedStringKey:Any] = [NSAttributedStringKey.foregroundColor:DefaultColorRed]
            detailAttrStr.addAttributes(coinAttr, range: detailRange)
        case .askDeleted:
            let title:String = detailJSON["title"].stringValue
            let titleLen:Int = (title as NSString).length
            detailAttrStr = NSMutableAttributedString.init(string: "亲，您的帖子：\(title) 已被系统删除。\n在未来的72小时内，您将不能再发送帖子！")
            // title
            let attribute:[NSAttributedStringKey:Any] = [NSAttributedStringKey.foregroundColor:AppThemeColor,
                                          NSAttributedStringKey.underlineStyle:NSUnderlineStyle.styleSingle]
            var detailRange:NSRange = NSRange.init(location: 7, length: titleLen)
            detailAttrStr.addAttributes(attribute, range: detailRange)
            // notice
            let noticeBegin = 7 + titleLen + 9
            detailRange = NSRange.init(location: noticeBegin, length: detailAttrStr.length - noticeBegin)
            detailAttrStr.addAttributes([NSAttributedStringKey.foregroundColor:DefaultColorRed], range: detailRange)
        case .commentAskDeleted:
            let title:String = detailJSON["title"].stringValue
            let titleLen:Int = (title as NSString).length
            detailAttrStr = NSMutableAttributedString.init(string: "亲，您的帖子评论：\(title) 已被系统删除。\n在未来的72小时内，您将不能再发送帖子评论！")
            // title
            let attribute:[NSAttributedStringKey:Any] = [NSAttributedStringKey.foregroundColor:AppThemeColor,
                                          NSAttributedStringKey.underlineStyle:NSUnderlineStyle.styleSingle]
            var detailRange:NSRange = NSRange.init(location: 9, length: titleLen)
            detailAttrStr.addAttributes(attribute, range: detailRange)
            // notice
            let noticeBegin = 9 + titleLen + 9
            detailRange = NSRange.init(location: noticeBegin, length: detailAttrStr.length - noticeBegin)
            detailAttrStr.addAttributes([NSAttributedStringKey.foregroundColor:DefaultColorRed], range: detailRange)
        case .chatForbidden:
            detailAttrStr = NSMutableAttributedString.init(string: "亲，您被人举报多次，并且系统已查明该情况属实！\n在未来的72小时内，您将不能再使用留言功能！")
            // notice
            let detailRange:NSRange = NSRange.init(location: 24, length: detailAttrStr.length - 24)
            detailAttrStr.addAttributes([NSAttributedStringKey.foregroundColor:DefaultColorRed], range: detailRange)
        case .reportRejected:
            let str:String = "亲，您的建议未被系统采纳！\n喵喵衷心地感谢您的热心建议！😘"
            detailAttrStr = NSMutableAttributedString.init(string: str)
        case .adviseRejected:
            let str:String = "亲，您的反馈未被系统采纳！\n喵喵衷心地感谢您的热心反馈！😘"
            detailAttrStr = NSMutableAttributedString.init(string: str)
        case .informRejected:
            detailAttrStr = NSMutableAttributedString.init(string: "亲，您的举报已被系统查实无效！\n如果存在恶意举报行为，您的举报功能可能会被系统禁用！")
            // notice
            let detailRange:NSRange = NSRange.init(location: 16, length: detailAttrStr.length - 16)
            detailAttrStr.addAttributes([NSAttributedStringKey.foregroundColor:DefaultColorRed], range: detailRange)
        case .informForbidden:
            detailAttrStr = NSMutableAttributedString.init(string: "亲，您恶意举报他人，并且系统已查明该情况属实！\n在未来的72小时内，您将不能再使用举报功能！")
            // notice
            let detailRange:NSRange = NSRange.init(location: 24, length: detailAttrStr.length - 24)
            detailAttrStr.addAttributes([NSAttributedStringKey.foregroundColor:DefaultColorRed], range: detailRange)
        }
        let height:CGFloat = self.countContentHeight(content: detailAttrStr.string)
        
        let item:MMsgCenterNoticeItem = MMsgCenterNoticeItem.init(id: id, nickname: nickname, iconURL: iconURL, gender: genderAttrStr, senderID: senderID, createTime: time, type: type,detailStr: detailStr,detailAttrStr:detailAttrStr, contentHeight: height,isUnread: true)
        
        return item
    }
    /// 计算可变Label高度
    class func countContentHeight(content:String) -> CGFloat{
        
        var height:CGFloat = 0
        let contentStr = content as NSString
        height = contentStr.getSizeWith(UIFont.systemFont(ofSize: 14), constrainedTo: CGSize.init(width: SCREEN_WIDTH - 80, height: CGFloat.greatestFiniteMagnitude)).height
        if height >= MMsgCenterVCCell.ContentMinCountHeight{
            height -= MMsgCenterVCCell.ContentMinCountHeight
        }else{
            height = 0
        }
        height += MMsgCenterVCCell.MinHeight
        
        return height
    }
}
