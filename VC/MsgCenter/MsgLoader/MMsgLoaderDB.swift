

import UIKit

class MMsgLoader: NSObject {
    
    /// 更新APP以及极光服务器的Badge数量
    class func refreshBadge(){
        
        MMsgLoader.countUnreadMsgFor(.leaveMsg) { (leaveMsg) in
            MMsgLoader.countUnreadMsgFor(.askMsg) { (askMsg) in
                MMsgLoader.countUnreadMsgFor(.notice) { (notice) in
                    
                    let unread = leaveMsg + askMsg + notice
                    DispatchQueue.main.async{
                        MMsgLoader.setAppBadge(unread)
                    }
                }
            }
        }
    }
    class func setAppBadge(_ unread:Int){
        APP.applicationIconBadgeNumber = unread
        #if arch(arm) || arch(arm64)
            JPUSHService.setBadge(unread)
        #endif
    }
    
    class func updateUser(userInfoArray:[MLatestUserInfo]){
        
        let queue:FMDatabaseQueue = FMDatabaseQueue(url: DefaultURLForDB)
        queue.inDatabase { (db) in
            
            for info in userInfoArray{
                
                let id:String = info.id
                let icon:String = info.icon
                let name:String = info.name
                let gender:Data = db.data(forObject: info.gender)
                var updated:Bool = db.update(DBTableNameForMsgCenterLeaveMsg, condition: " senderID = '\(id)' ", dict: ["nickname":name,"iconURL":icon,"gender":gender])
                if !updated{
                    DLOG("update Msg Table <\(DBTableNameForMsgCenterLeaveMsg)> failed")
                    
                }
                updated = db.update(DBTableNameForMsgCenterAsk, condition: " senderID = '\(id)' ", dict: ["nickname":name,"iconURL":icon,"gender":gender])
                if !updated{
                    DLOG("update Msg Table <\(DBTableNameForMsgCenterAsk)> failed")
                    
                }
                updated = db.update(DBTableNameForMsgCenterNotice, condition: " senderID = '\(id)' ", dict: ["nickname":name,"iconURL":icon,"gender":gender])
                if !updated{
                    DLOG("update Msg Table <\(DBTableNameForMsgCenterNotice)> failed")
                    
                }
            }
        }
    }
    
    
    /// 统计未读消息数
    ///
    /// - Parameters:
    ///   - type: 消息列表类型
    ///   - finish: 带有未读消息数的回调
    class func countUnreadMsgFor(_ type:MsgCenterListType,_ finish:@escaping ((Int) -> Void)){
        let queue = FMDatabaseQueue(url: DefaultURLForDB)
        queue.inDatabase { (db) in
            
            let table = MMsgLoader.tableNameForType(type)
            let sql = "SELECT COUNT(*) FROM \(table) WHERE loginUserID = '\(uUserID)' AND isUnread = 1 "
            db.select(sql, { (res) in
                
                guard let r = res else{
                    DispatchQueue.main.async{
                        finish(0)
                    }
                    return
                }
                r.next()
                let count = r.int(forCol: 0)
                DispatchQueue.main.async{
                    finish(count)
                }
            })
        }
    }
    
    // MARK: - Parse from DB
    /// 获取对应消息类型数据
    ///
    /// - Parameters:
    ///   - type: 消息列表类型
    ///   - keys: 需要获取的
    ///   - pageIndex: 页索引
    ///   - finish: 带有消息数组参数的回调
    class func loadDBMsgFor(_ type:MsgCenterListType,keys:[String],pageIndex:Int,_ finish:@escaping (([AnyObject]?) -> Void)){
        
        let queue = FMDatabaseQueue(url: DefaultURLForDB)
        queue.inDatabase { (db) in
            
            db.select(from: MMsgLoader.tableNameForType(type), keys: keys, condition: "WHERE loginUserID = '\(uUserID)' ORDER BY createTime DESC LIMIT 20 OFFSET \(DefaultPageSize * pageIndex)", { (res) in
                
                guard let r = res else{
                    DispatchQueue.main.async{
                        finish(nil)
                    }
                    return
                }
//                DLOG(r.columnNameToIndexMap.debugDescription)
//                DLOG(r.resultDictionary.debugDescription)
                switch type {
                case .leaveMsg:
                    let msgs = MMsgLoader.parseLeaveMsg(r) as [AnyObject]
                    DispatchQueue.main.async{
                        finish(msgs)
                    }
                case .askMsg:
                    let msgs = MMsgLoader.parseAskMsg(r) as [AnyObject]
                    DispatchQueue.main.async{
                        finish(msgs)
                    }
                case .notice:
                    let msgs = MMsgLoader.parseNotice(r) as [AnyObject]
                    DispatchQueue.main.async{
                        finish(msgs)
                    }
                }
            })
        }
    }
    class func parseLeaveMsg(_ r:FMResultSet) -> [MMsgCenterLeaveMsgItem]{
        
        var list = [MMsgCenterLeaveMsgItem]()
        while r.next(){
            let content:NSAttributedString = r.object(forKey: "content") as? NSAttributedString ?? NSAttributedString.init(string: "")
            let contentHeight:CGFloat = r.float(forKey: "contentHeight")
            let createTime:String = r.string(forKey: "createTime")
            let gender:NSAttributedString = r.object(forKey: "gender") as? NSAttributedString ?? NSAttributedString.init(string: "")
            let iconURL:String = r.string(forKey: "iconURL")
            let id:String = r.string(forKey: "id")
            let isUnread:Bool = r.bool(forKey: "isUnread")
            let nickname:String = r.string(forKey: "nickname")
            let senderID:String = r.string(forKey: "senderID")
            
            let item = MMsgCenterLeaveMsgItem.init(id: id, nickname: nickname, iconURL: iconURL, gender: gender, senderID: senderID, createTime: createTime, content: content, contentHeight: contentHeight, isUnread: isUnread)
            list.append(item)
        }
        return list
    }
    class func parseAskMsg(_ r:FMResultSet) -> [MMsgCenterAskMsgItem]{
        
        var list = [MMsgCenterAskMsgItem]()
        while r.next(){
            let askID:String = r.string(forKey: "askID")
            let contentHeight:CGFloat = r.float(forKey: "contentHeight")
            let createTime:String = r.string(forKey: "createTime")
            let detail:NSAttributedString = r.object(forCol: 3) as? NSAttributedString ?? NSAttributedString.init(string: "")
            let gender:NSAttributedString = r.object(forKey: "gender") as? NSAttributedString ?? NSAttributedString.init(string: "")
            let iconURL:String = r.string(forKey: "iconURL")
            let id:String = r.string(forKey: "id")
            let isUnread:Bool = r.bool(forKey: "isUnread")
            let nickname:String = r.string(forKey: "nickname")
            let senderID:String = r.string(forKey: "senderID")
            let type:String = r.string(forKey: "type")
            
            let item = MMsgCenterAskMsgItem.init(id: id, askID: askID, nickname: nickname, iconURL: iconURL, gender: gender, senderID: senderID, createTime: createTime, type: MMsgCenterAskMsgType(rawValue:type)!, detail: detail, contentHeight: contentHeight, isUnread: isUnread)
            list.append(item)
        }
        return list
    }
    class func parseNotice(_ r:FMResultSet) -> [MMsgCenterNoticeItem]{
        
        var list = [MMsgCenterNoticeItem]()
        while r.next(){
            let contentHeight:CGFloat = r.float(forKey: "contentHeight")
            let createTime:String = r.string(forKey: "createTime")
            let detailAttrStr:NSAttributedString = r.object(forKey: "detailAttrStr") as? NSAttributedString ?? NSAttributedString.init(string: "")
            let detailStr:String = r.string(forKey: "detailStr")
            let gender:NSAttributedString = r.object(forKey: "gender") as? NSAttributedString ?? NSAttributedString.init(string: "")
            let iconURL:String = r.string(forKey: "iconURL")
            let id:String = r.string(forKey: "id")
            let isUnread:Bool = r.bool(forKey: "isUnread")
            let nickname:String = r.string(forKey: "nickname")
            let senderID:String = r.string(forKey: "senderID")
            let type:String = r.string(forKey: "type")
            
            let item = MMsgCenterNoticeItem.init(id: id, nickname: nickname, iconURL: iconURL, gender: gender, senderID: senderID, createTime: createTime, type: MMsgCenterNoticeType(rawValue: type)!, detailStr: detailStr, detailAttrStr: detailAttrStr, contentHeight: contentHeight, isUnread: isUnread)
            list.append(item)
        }
        return list
    }
    
    // MARK: - Set to Read
    /// 设置消息为已读
    ///
    /// - Parameters:
    ///   - type: 消息列表类型
    ///   - msgID: 消息ID
    class func setMsgToReaded(_ type:MsgCenterListType,msgID:String){
        
        let queue = FMDatabaseQueue(url: DefaultURLForDB)
        queue.inDatabase { (db) in
            
            let updated = db.update(MMsgLoader.tableNameForType(type), condition: " id = \(msgID) ", dict: ["isUnread":0])
            if !updated{
                DLOG("update Msg Table <\(type.rawValue)> failed")
                return
            }
            MMsgLoader.refreshBadge()
        }
    }
    
    // MARK: - Save to DB
    /// 留言
    class func saveLeaveMsg(_ items:[MMsgCenterLeaveMsgItem]){
        
        guard items.count != 0 else {
            return
        }
        
        let queue = FMDatabaseQueue(url: DefaultURLForDB)
        queue.inDatabase({ (db) in
            
            let loginUserID:String = uUserID
            for item in items{
                let i:MMsgCenterLeaveMsgItem = item
                _ = db.insert(into: MMsgLoader.tableNameForType(.leaveMsg),dict: ["id":i.id,"nickname":i.nickname,"iconURL":i.iconURL,"gender":db.data(forObject: i.gender),"senderID":i.senderID,"createTime":i.createTime,"content":db.data(forObject: i.content),"contentHeight":i.contentHeight,"isUnread":i.isUnread,"loginUserID":loginUserID])
            }
            MMsgLoader.refreshBadge()
            DispatchQueue.main.async{
                NotiCenter.post(name: UserDidReceiverRemoteNotification, object: nil)
            }
        })
    }
    /// 用户发送留言
    class func appendLeaveMsg(_ item:MMsgCenterLeaveMsgItem){
        
        let queue = FMDatabaseQueue(url: DefaultURLForDB)
        queue.inDatabase({ (db) in
            
            let loginUserID:String = uUserID
            let i = item
            _ = db.insert(into: MMsgLoader.tableNameForType(.leaveMsg),dict: ["id":i.id,"nickname":i.nickname,"iconURL":i.iconURL,"gender":db.data(forObject: i.gender),"senderID":i.senderID,"createTime":i.createTime,"content":db.data(forObject: i.content),"contentHeight":i.contentHeight,"isUnread":i.isUnread,"loginUserID":loginUserID])
            DispatchQueue.main.async{
                NotiCenter.post(name: UserSentLeaveMsgNotification, object: nil)
            }
        })
    }
    /// 问问
    class func saveAskMsg(_ items:[MMsgCenterAskMsgItem]){
        
        guard items.count != 0 else {
            return
        }
        
        let queue = FMDatabaseQueue(url: DefaultURLForDB)
        queue.inDatabase({ (db) in
            
            let loginUserID:String = uUserID
            for item in items{
                let i = item
                _ = db.insert(into: MMsgLoader.tableNameForType(.askMsg),dict: ["id":i.id,"askID":i.askID,"nickname":i.nickname,"iconURL":i.iconURL,"gender":db.data(forObject: i.gender),"senderID":i.senderID,"createTime":i.createTime,"type":i.type.rawValue,"detail":db.data(forObject: i.detail),"contentHeight":i.contentHeight,"isUnread":i.isUnread,"loginUserID":loginUserID])
            }
            MMsgLoader.refreshBadge()
            DispatchQueue.main.async{
                NotiCenter.post(name: UserDidReceiverRemoteNotification, object: nil)
            }
        })
    }
    /// 系统通知
    class func saveNoticeMsg(_ items:[MMsgCenterNoticeItem]){
        
        guard items.count != 0 else {
            return
        }
        
        let queue = FMDatabaseQueue(url: DefaultURLForDB)
        queue.inDatabase({ (db) in
            
            let loginUserID:String = uUserID
            for item in items{
                let i = item
                _ = db.insert(into: MMsgLoader.tableNameForType(.notice),dict: ["id":i.id,"nickname":i.nickname,"iconURL":i.iconURL,"gender":db.data(forObject: i.gender),"senderID":i.senderID,"createTime":i.createTime,"type":i.type.rawValue,"detailStr":i.detailStr,"detailAttrStr":db.data(forObject: i.detailAttrStr),"contentHeight":i.contentHeight,"isUnread":i.isUnread,"loginUserID":loginUserID])
            }
            MMsgLoader.refreshBadge()
            DispatchQueue.main.async{
                NotiCenter.post(name: UserDidReceiverRemoteNotification, object: nil)
            }
        })
    }
    
    // MARK: - Clear DB Msg
    /// 清空消息记录
    class func clearTable(_ type:MsgCenterListType){
        
        let queue = FMDatabaseQueue(url: DefaultURLForDB)
        queue.inDatabase { (db) in
            
            if !db.delete(from: MMsgLoader.tableNameForType(type), condition: "WHERE loginUserID = '\(uUserID)'"){
                DLOG("删除消息中心<\(type)>旧数据，失败！！！")
                return
            }
            DispatchQueue.main.async{
                MMsgLoader.setAppBadge(0)
            }
        }
    }
    
    /// 移除黑名单用户的消息
    ///
    /// - Parameter userID: 黑名单用户ID
    class func removeMsgForBlacklist(userID:String){
        
        let queue = FMDatabaseQueue(url: DefaultURLForDB)
        queue.inDatabase { (db) in
            
            if !db.delete(from: MMsgLoader.tableNameForType(.leaveMsg), condition: " WHERE loginUserID = '\(uUserID)' AND senderID = '\(userID)' "){
                DLOG("删除黑名单用户的消息数据，失败！！！")
                return
            }
            MMsgLoader.refreshBadge()
        }
    }
    /// 消息列表类型对应的数据库表名
    ///
    /// - Parameter type: 消息列表类型
    /// - Returns: 消息数据库表名
    class func tableNameForType(_ type:MsgCenterListType) -> String{
        
        switch type {
        case .leaveMsg:
            return DBTableNameForMsgCenterLeaveMsg
        case .askMsg:
            return DBTableNameForMsgCenterAsk
        case .notice:
            return DBTableNameForMsgCenterNotice
        }
    }
}
