

import Foundation
import SwiftyJSON

extension MMsgCenterVC{
    
    /// 查看资料
    func showUserInfo(userID:String,name:String){
        
        let uiVC = self.loadViewControllerInMainStoryboard("MUserInfoVC") as! MUserInfoVC
        uiVC.userID = userID
        uiVC.userName = name
        self.hideNaviBackBtnTitle()
        self.navigationController?.pushViewController(uiVC, animated: true)
    }
    /// 留言
    func leaveMsg(userID:String,name:String){
        
        let tsVC = self.loadViewControllerInMainStoryboard("MTextSendVC") as! MTextSendVC
        tsVC.setupWith(operationType: .leaveMsg, ID: userID,name: name)
        self.hideNaviBackBtnTitle()
        self.navigationController?.pushViewController(tsVC, animated: true)
    }
    /// 帖子详情
    func showAskDetailVC(_ askID:String){
        
        if askID.count == 0{
            self.toastFail("该帖子已被删除")
            return
        }
        
        view.makeToastActivity()
        
        HttpController.shared.askDetail(askID, successBlock: { (json) in
            self.pushToAskDetailWithJSON(json)
        }, failure: { (err) in
            self.view.hideToastActivity()
            self.toastFail(err.localizedDescription)
        })
    }
    func pushToAskDetailWithJSON(_ json:JSON){
        
        let askItemJSON:JSON = json["result"]
        let font:UIFont = UIFont.systemFont(ofSize: 14)
        let userID:String = uUserID
        let currentDate:Date = DefaultDateFormatter.date(from: stringOfNow())!
        let item:AskListItem = MAskTV.parseAskItem(askItemJSON, userID: userID, font: font, currentDate: currentDate)
        
        self.view.hideToastActivity()
        
        let askDetailVC:MAskDetailVC = self.loadViewControllerInMainStoryboard("MAskDetailVC") as! MAskDetailVC
        askDetailVC.askItem = item
        self.hideNaviBackBtnTitle()
        self.navigationController?.pushViewController(askDetailVC, animated: true)
    }
    func giveCoins(userID:String,coins:String){
        
        self.showSystemStyleAlert(title: "提示", msg: "\n确认赠送他\(coins)金币？") {
            
            self.view.makeToastActivity()
            
            HttpController.shared.coinExchange(isGive: true, userID: userID, coinCount: coins, msg: "", successBlock: { (json) in
                
                self.view.hideToastActivity()
                self.toastSuccess("赠送成功")
            }, failure: { (err) in
                
                self.view.hideToastActivity()
                self.toastFail(err.localizedDescription)
            })
        }
    }
    /// 系统通知cell点击事件
    func processNoticeTouchEvent(_ item:MMsgCenterNoticeItem){
        
        let type = item.type
        switch type {
        case .zanUser,
             .giveCoins,
             .contactAdded:
            self.showUserInfo(userID: item.senderID, name: item.nickname)
        case .wantCoins:
            let detailJSON = JSON.init(parseJSON: item.detailStr)
            let coins = detailJSON["coins"].stringValue
            let id = item.senderID
            giveCoins(userID: id, coins: coins)
        case .askInformed,
             .commentAskInformed:
            
            let detailJSON = JSON.init(parseJSON: item.detailStr)
            let operationID = detailJSON["askID"].stringValue
            self.showAskDetailVC(operationID)
            
        case .userInformed,
             .rewardForInform,
             .reportAdopted,
             .adviseAdopted,
             .askDeleted,
             .chatForbidden,
             .reportRejected,
             .adviseRejected,
             .informRejected,
             .informForbidden,
            .commentAskDeleted:
            break
        }
    }
        
}
