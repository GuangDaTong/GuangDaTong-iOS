

import UIKit

struct MLatestUserInfo {
    var id:String
    var icon:String
    var name:String
    var gender:NSAttributedString
}

class MMsgCenterVC: MBaseVC,UIScrollViewDelegate,MHorizontalTabViewDelegate {
    
    /// 推送消息跳转tab
    var selectedTabForPush:Int = -1
    // MARK: - Views
    fileprivate var horizontalTab:MHorizontalTabView! = nil
    var tabView:MHorizontalTabView{
        get{
            if horizontalTab != nil {
                return horizontalTab
            }
            horizontalTab = MHorizontalTabView()
            horizontalTab.delegate = self
            horizontalTab.setup(withTitles: ["留言","帖子消息","系统通知"])
            return horizontalTab
        }
        set{
            horizontalTab = newValue
        }
    }
    var scrollView:UIScrollView = UIScrollView.init(frame: CGRect.zero)
    let clearBtn:UIButton = UIButton.init()
    var msgTV:MMsgCenterLeaveMsgTV! = nil
    var askTV:MMsgCenterAskMsgTV! = nil
    var noticeTV:MMsgCenterNoticeTV! = nil
    var currentIndex:Int = 0
    
    var userDict:[String:MLatestUserInfo] = [String:MLatestUserInfo]()
    
    // MARK: - VC Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotiCenter.addObserver(self, selector: #selector(refreshLists), name: UserDidLoginNotification, object: nil)
        NotiCenter.addObserver(self, selector: #selector(refreshLists), name: UserDidReceiverRemoteNotification, object: nil)
        NotiCenter.addObserver(self, selector: #selector(refreshLeaveMsgList), name: UserAlteredBlacklistNotification, object: nil)
        NotiCenter.addObserver(self, selector: #selector(refreshLeaveMsgList), name: UserSentLeaveMsgNotification, object: nil)
        
        setupNaviBar()
        setupTabView()
        setupScrollView()
        setupTableView()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if selectedTabForPush != -1{
            tabView.selectIndex(selectedTabForPush)
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if let VCs = self.navigationController?.viewControllers,VCs.count > 0{
            if VCs.index(of: self) != nil{
                return
            }
            // popViewController
            let now = Date.init()
            uDefaults.set(now, forKey: "LastLeaveMsgReadTime")
            uDefaults.set(now, forKey: "LastAskMsgReadTime")
            uDefaults.set(now, forKey: "LastNoticeReadTime")
            uDefaults.synchronize()
        }
    }
    // MARK: - Setup
    func setupNaviBar(){
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.barTintColor = DefaultColorBlack
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        
        clearBtn.isHidden = true
        clearBtn.setTitle("清空", for: .normal)
        clearBtn.sizeToFit()
        clearBtn.addTarget(self, action: #selector(clearMsg), for: .touchUpInside)
        let msgItem = UIBarButtonItem.init(customView: clearBtn)
        self.navigationItem.rightBarButtonItem = msgItem
    }
    func setupTabView(){
        
        self.view.addSubview(tabView)
        tabView.snp.makeConstraints { (make) in
            make.leading.equalTo(view.snp.leading)
            make.trailing.equalTo(view.snp.trailing)
            make.top.equalTo(view.snp.top)
            make.height.equalTo(tabView.defaultHeight)
        }
    }
    func setupScrollView(){
        
        scrollView.contentSize = CGSize.init(width: GetView_Width(view: self.view) * 3, height: 0)
        scrollView.delegate = self
        scrollView.isPagingEnabled = true
        
        self.view.addSubview(scrollView)
        scrollView.snp.makeConstraints { (make) in
            make.leading.equalTo(view.snp.leading)
            make.trailing.equalTo(view.snp.trailing)
            make.top.equalTo(horizontalTab.snp.bottom).offset(4)
            make.bottom.equalTo(view.snp.bottom)
        }
    }
    func setupTableView(){
        
        msgTV = MMsgCenterLeaveMsgTV.init(frame: CGRect.zero, style: UITableViewStyle.plain)
        askTV = MMsgCenterAskMsgTV.init(frame: CGRect.zero, style: UITableViewStyle.plain)
        noticeTV = MMsgCenterNoticeTV.init(frame: CGRect.zero, style: UITableViewStyle.plain)
        
        scrollView.addSubview(msgTV)
        scrollView.addSubview(askTV)
        scrollView.addSubview(noticeTV)
        
        msgTV.ownerVC = self
        askTV.ownerVC = self
        noticeTV.ownerVC = self
        
        msgTV.separatorStyle = .none
        msgTV.snp.makeConstraints { (make) in
            make.leading.equalTo(self.scrollView.snp.leading)
            make.top.equalTo(self.scrollView.snp.top)
            make.width.equalTo(self.view.snp.width)
            make.height.equalTo(self.scrollView.snp.height)
        }
        msgTV.iconBtnPressedBlock = { row in
            
            let item:MMsgCenterLeaveMsgItem = self.msgTV.dataArr[row] as! MMsgCenterLeaveMsgItem
            self.showUserInfo(userID: item.senderID, name: item.nickname)
        }
        msgTV.cellPressedBlock = { (row) in
            
            var item:MMsgCenterLeaveMsgItem = self.msgTV.dataArr[row] as! MMsgCenterLeaveMsgItem
            if item.senderID == uUserID{
                return
            }
            
            MMsgLoader.setMsgToReaded(.leaveMsg, msgID: item.id)
            self.leaveMsg(userID: item.senderID, name: item.nickname)
            
            item.isUnread = false
            self.msgTV.dataArr[row] = item
            self.msgTV.reloadData()
            self.checkTabHighlight(listType: .leaveMsg)
        }
        msgTV.loadFinishedBlock = { type in
            
            self.checkTabHighlight(listType: .leaveMsg)
            if self.currentIndex != 0{
                return
            }
            self.showClearBtnFor(listType: type)
        }
        
        
        askTV.separatorStyle = .none
        askTV.snp.makeConstraints { (make) in
            make.leading.equalTo(self.msgTV.snp.trailing)
            make.top.equalTo(self.scrollView.snp.top)
            make.width.equalTo(self.view.snp.width)
            make.height.equalTo(self.scrollView.snp.height)
        }
        askTV.iconBtnPressedBlock = { row in
            let item:MMsgCenterAskMsgItem = self.askTV.dataArr[row] as! MMsgCenterAskMsgItem
            self.showUserInfo(userID: item.senderID, name: item.nickname)
        }
        askTV.cellPressedBlock = { (row) in
            var item:MMsgCenterAskMsgItem = self.askTV.dataArr[row] as! MMsgCenterAskMsgItem
            MMsgLoader.setMsgToReaded(.askMsg, msgID: item.id)
            self.showAskDetailVC(item.askID)
            
            item.isUnread = false
            self.askTV.dataArr[row] = item
            self.askTV.reloadData()
            self.checkTabHighlight(listType: .askMsg)
        }
        askTV.loadFinishedBlock = { type in
            
            self.checkTabHighlight(listType: .askMsg)
            if self.currentIndex != 1{
                return
            }
            self.showClearBtnFor(listType: type)
        }
        
        
        noticeTV.separatorStyle = .none
        noticeTV.snp.makeConstraints { (make) in
            make.leading.equalTo(self.askTV.snp.trailing)
            make.top.equalTo(self.scrollView.snp.top)
            make.width.equalTo(self.view.snp.width)
            make.height.equalTo(self.scrollView.snp.height)
        }
        noticeTV.iconBtnPressedBlock = { row in
            let item:MMsgCenterNoticeItem = self.noticeTV.dataArr[row] as! MMsgCenterNoticeItem
            self.showUserInfo(userID: item.senderID, name: item.nickname)
        }
        noticeTV.cellPressedBlock = { (row) in
            
            var item:MMsgCenterNoticeItem = self.noticeTV.dataArr[row] as! MMsgCenterNoticeItem
            MMsgLoader.setMsgToReaded(.notice, msgID: item.id)
            
            self.processNoticeTouchEvent(item)
            item.isUnread = false
            self.noticeTV.dataArr[row] = item
            self.noticeTV.reloadData()
            
            self.checkTabHighlight(listType: .notice)
        }
        noticeTV.loadFinishedBlock = { type in
            
            self.checkTabHighlight(listType: .notice)
            if self.currentIndex != 2{
                return
            }
            self.showClearBtnFor(listType: type)
        }
        
        if isUserNotLogin{
            delay(second: 0.5) {
                self.presentVCInMainStoryboard("MLoginNaviVC")
            }
            return
        }
        refreshLists()
    }
    // 刷新新消息
    @objc func refreshLists(){
        
        msgTV.refreshData()
        askTV.refreshData()
        noticeTV.refreshData()
    }
    @objc func refreshLeaveMsgList(){
        
        msgTV.refreshData()
    }
    func checkTabHighlight(listType type:MsgCenterListType){
        
        MMsgLoader.refreshBadge()
        
        switch type {
        case MsgCenterListType.leaveMsg:
            MMsgLoader.countUnreadMsgFor(.leaveMsg) { (unreadCount) in
                
                if unreadCount > 0{
                    self.tabView.setTab(0, badge: unreadCount)
                    return
                }
                self.tabView.setTab(0, badge: 0)
            }
            return
        case MsgCenterListType.askMsg:
            MMsgLoader.countUnreadMsgFor(.askMsg) { (unreadCount) in
                
                if unreadCount > 0{
                    self.tabView.setTab(1, badge: unreadCount)
                    return
                }
                self.tabView.setTab(1, badge: 0)
            }
            return
        case MsgCenterListType.notice:
            MMsgLoader.countUnreadMsgFor(.notice) { (unreadCount) in
                
                if unreadCount > 0{
                    self.tabView.setTab(2, badge: unreadCount)
                    return
                }
                self.tabView.setTab(2, badge: 0)
            }
            return
        }
    }
    /// 是否显示清空按钮
    func showClearBtnFor(listType type:MsgCenterListType){
        
        var tv:MMsgCenterTV! = nil
        switch type {
        case .leaveMsg:
            tv = self.msgTV
        case .askMsg:
            tv = self.askTV
        case .notice:
            tv = self.noticeTV
        }
        let isEmpty = (tv.dataArr.count == 0)
        clearBtn.isHidden = isEmpty
    }
    /// 清空历史消息
    @objc func clearMsg(){
        
        var type = MsgCenterListType.askMsg
        switch currentIndex {
        case 0:
            type = .leaveMsg
        case 1:
            type = .askMsg
        case 2:
            type = .notice
        default:
            break
        }
        
        let alert = UIAlertController(title: "提示", message: "\n清空\(type.rawValue)?", preferredStyle: .alert)
        
        let okAction = UIAlertAction.init(title: "是的", style: .destructive) { (action) in
            self.clearServerMsg(type: type)
        }
        let cancelAction = UIAlertAction(title: "取消", style: .default, handler: nil)
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        
        present(alert, animated: true, completion: nil)
        
    }
    func clearServerMsg(type:MsgCenterListType){
        
        MMsgLoader.clearTable(type)
        self.toastSuccess("已清空")
        self.view.hideToastActivity()
        
        var tv:MMsgCenterTV! = nil
        switch type {
        case .leaveMsg:
            tv = self.msgTV
        case .askMsg:
            tv = self.askTV
        case .notice:
            tv = self.noticeTV
        }
        tv.refreshData()
        self.checkTabHighlight(listType: type)
    }
    func checkUserInfo(_ IDs:[String]){
        
        HttpController.shared.userData(IDs, successBlock: { (json) in
            
            DispatchQueue.global().async {
                let jsonArray = json["result"]["list"].arrayValue
                var infoArray = [MLatestUserInfo]() //需要写入数据库
                for info in jsonArray{
                    let id = info["id"].stringValue
                    let gender = info["gender"].stringValue
                    let genderAttr = gender.attributedGenderString()
                    let newInfo = MLatestUserInfo.init(id:id,icon: info["icon"].stringValue, name: info["nickname"].stringValue, gender: genderAttr)
                    self.userDict[id] = newInfo
                    infoArray.append(newInfo)
                }
                DispatchQueue.main.async{ // 刷新列表数据
                    self.msgTV.reloadData()
                    self.askTV.reloadData()
                    self.noticeTV.reloadData()
                }
                // 更新数据库数据
                MMsgLoader.updateUser(userInfoArray: infoArray)
            }
        }, failure: { (err) in
        })
    }
    
    // MARK: - HorizontalTabView Delegate
    func horizontalTabViewDidSelect(_ index:Int){
        
        let point = CGPoint.init(x: CGFloat(index) * GetView_Width(view: self.view), y: 0)
        scrollView.setContentOffset(point, animated: true)
        scrollView.flashScrollIndicators()
        
        switch index {
        case 0:
            let isEmpty = (msgTV.dataArr.count == 0)
            if isEmpty{
                msgTV.refreshData()
            }
            showClearBtnFor(listType: .leaveMsg)
        case 1:
            let isEmpty = (askTV.dataArr.count == 0)
            if isEmpty{
                askTV.refreshData()
            }
            showClearBtnFor(listType: .askMsg)
        case 2:
            let isEmpty = (noticeTV.dataArr.count == 0)
            if isEmpty{
                noticeTV.refreshData()
            }
            showClearBtnFor(listType: .notice)
        default:
            break
        }
        currentIndex = index
    }
    // MARK: - UIScrollView Delegate
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        if scrollView == self.scrollView {
            let index = Int(targetContentOffset.pointee.x / GetView_Width(view: self.view) + 0.1)
            if horizontalTab.selectedIndex == index{
                return
            }
            horizontalTab.selectIndex(index)
        }
    }
}


