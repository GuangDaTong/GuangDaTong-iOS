

import Foundation

let DBKeysForLeaveMsg:[String:FMDBDataType] =
[
    "id":FMDBDataType.text,
    "nickname":FMDBDataType.text,
    "iconURL":FMDBDataType.text,
    "gender":FMDBDataType.blob,
    "senderID":FMDBDataType.text,
    "createTime":FMDBDataType.text,
    "content":FMDBDataType.blob,
    "contentHeight":FMDBDataType.real,
    "isUnread":FMDBDataType.bool,
    "loginUserID":FMDBDataType.text
]

let DBKeysForAskMsg:[String:FMDBDataType] =
[
    "id":FMDBDataType.text,
    "askID":FMDBDataType.text,
    "nickname":FMDBDataType.text,
    "iconURL":FMDBDataType.text,
    "gender":FMDBDataType.blob,
    "senderID":FMDBDataType.text,
    "createTime":FMDBDataType.text,
    "type":FMDBDataType.text,
    "detail":FMDBDataType.blob,
    "contentHeight":FMDBDataType.real,
    "isUnread":FMDBDataType.bool,
    "loginUserID":FMDBDataType.text
]

let DBKeysForNotice:[String:FMDBDataType] =
[
    "id":FMDBDataType.text,
    "nickname":FMDBDataType.text,
    "iconURL":FMDBDataType.text,
    "gender":FMDBDataType.blob,
    "senderID":FMDBDataType.text,
    "createTime":FMDBDataType.text,
    "type":FMDBDataType.text,
    "detailStr":FMDBDataType.text,
    "detailAttrStr":FMDBDataType.blob,
    "contentHeight":FMDBDataType.real,
    "isUnread":FMDBDataType.bool,
    "loginUserID":FMDBDataType.text
]
