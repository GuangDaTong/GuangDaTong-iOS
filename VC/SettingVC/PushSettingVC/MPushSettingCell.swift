

import UIKit

let MPushSettingCellIdentifier = "MPushSettingCell"

class MPushSettingCell: MBaseTVCell {

    var schoolPushAlterBlock:((Bool) -> Void)?
    var isSchoolPush:Bool{
        get{
            return schoolBtn.isSelected
        }
        set{
            schoolBtn.isSelected = newValue
        }
    }
    var collegePushAlterBlock:((Bool) -> Void)?
    var isCollegePush:Bool{
        get{
            return collegeBtn.isSelected
        }
        set{
            collegeBtn.isSelected = newValue
        }
    }
    
    @IBOutlet weak var schoolBtn: UIButton!
    @IBOutlet weak var collegeBtn: UIButton!
    let selectImage = UIImage.init(named: "pushSetting_selected")
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        addBottomSeparateLine()
        schoolBtn.setImage(selectImage, for: .selected)
        collegeBtn.setImage(selectImage, for: .selected)
    }
    @IBAction func collegeBtnBlock(_ sender: Any) {
        
        collegeBtn.isSelected = !collegeBtn.isSelected
        collegePushAlterBlock?(collegeBtn.isSelected)
    }
    @IBAction func schoolBtnBlock(_ sender: Any) {
        
        schoolBtn.isSelected = !schoolBtn.isSelected
        schoolPushAlterBlock?(schoolBtn.isSelected)
        
    }
    
}
