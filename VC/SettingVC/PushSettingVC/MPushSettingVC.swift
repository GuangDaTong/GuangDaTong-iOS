

import UIKit

class MPushSettingVC: MBaseVC {

    // MARK: - View
    let pushTV = BaseTV.init()
    
    // MARK: - Property
    let isSchool = "isSchoolPush"
    let isCollege = "isCollegePush"
    let isComment = "isCommentPush"
    let isLeaveMsg = "isLeaveMsgPush"
    let isActivity = "isActivityPush"
    let isSystem = "isSystemPush"
    
    var isSchoolPush = false
    var isCollegePush = false
    var isCommentPush = false
    var isLeaveMsgPush = false
    var isActivityPush = false
    var isSystemPush = false
    
    // MARK: - VC Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        loadUserSetting()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if let VCs = self.navigationController?.viewControllers,VCs.count > 0{
            if VCs.index(of: self) != nil{
                return
            }
            HttpController.shared.setPushSetting(schoolUrgent: self.isSchoolPush, collegeUrgent: self.isCollegePush, comment: self.isCommentPush, leaveWords: self.isLeaveMsgPush, activity: self.isActivityPush, system: self.isSystemPush, successBlock: { (json) in
                DLOG("*** 更新推送设置成功！***")
            }, failure: { (err) in
                DLOG(err.localizedDescription)
            })
        }
    }
    
    // MARK: - Setup
    func setupUI(){
        
        self.title = "推送设置"
        self.view.backgroundColor = UIColor.white
        setupNaviBar()
        setupTV()
    }
    func setupNaviBar(){
        
        let helpBtn = UIButton.init()
        helpBtn.setImage(UIImage.init(named: "pushSetting_help"), for: .normal)
        helpBtn.sizeToFit()
        helpBtn.addTarget(self, action: #selector(showHelpVC), for: .touchUpInside)
        let msgItem = UIBarButtonItem.init(customView: helpBtn)
        self.navigationItem.rightBarButtonItem = msgItem
    }
    func setupTV(){
        
        self.view.addSubview(pushTV)
        pushTV.snp.makeConstraints { (m) in
            m.leading.equalTo(view.snp.leading)
            m.top.equalTo(view.snp.top)
            m.trailing.equalTo(view.snp.trailing)
            m.bottom.equalTo(view.snp.bottom)
        }
        pushTV.backgroundColor = UIColor.groupTableViewBackground
        pushTV.separatorStyle = .none
        pushTV.delegate = self
        pushTV.dataSource = self
        pushTV.register(UINib.init(nibName: TVUniversalCellIdentifier, bundle: nil), forCellReuseIdentifier: TVUniversalCellIdentifier)
        pushTV.register(UINib.init(nibName: MPushSettingCellIdentifier, bundle: nil), forCellReuseIdentifier: MPushSettingCellIdentifier)
    }
    func loadUserSetting(){
        
        self.isSchoolPush = uDefaults.bool(forKey: isSchool)
        self.isCollegePush = uDefaults.bool(forKey: isCollege)
        self.isCommentPush = uDefaults.bool(forKey: isComment)
        self.isLeaveMsgPush = uDefaults.bool(forKey: isLeaveMsg)
        self.isActivityPush = uDefaults.bool(forKey: isActivity)
        self.isSystemPush = uDefaults.bool(forKey: isSystem)
    }
    @objc func showHelpVC(){
        
        let webVC = self.loadViewControllerInMainStoryboard("MWebVC") as! MWebVC
        webVC.setup(title: "关于推送设置", url: BaseURL + "/help/push")
        webVC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(webVC, animated: true)
    }
}

extension MPushSettingVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let row = indexPath.row
        if row == 0{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: MPushSettingCellIdentifier) as! MPushSettingCell
            cell.selectionStyle = .none
            cell.isSchoolPush = self.isSchoolPush
            cell.schoolPushAlterBlock = { isSchool in
                self.isSchoolPush = isSchool
                uDefaults.set(isSchool, forKey: self.isSchool)
                uDefaults.synchronize()
                self.pushTV.reloadData()
            }
            cell.isCollegePush = self.isCollegePush
            cell.collegePushAlterBlock = { isCollege in
                self.isCollegePush = isCollege
                uDefaults.set(isCollege, forKey: self.isCollege)
                uDefaults.synchronize()
                self.pushTV.reloadData()
            }
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: TVUniversalCellIdentifier) as! TVUniversalCell
        cell.selectionStyle = .none
        
        switch row {
        case 1:
            cell.setup(withType: .select, title: "评论通知", icon: nil,subtitle: "",isSelect: isCommentPush)
        case 2:
            cell.setup(withType: .select, title: "留言通知", icon: nil,subtitle: "",isSelect: isLeaveMsgPush)
        case 3:
            cell.setup(withType: .select, title: "活动通知", icon: nil,subtitle: "",isSelect: isActivityPush)
        case 4:
            cell.setup(withType: .select, title: "系统通知", icon: nil,subtitle: "",isSelect: isSystemPush)
        default:
            break
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        var key = ""
        var value = false
        let row = indexPath.row
        
        switch row {
        case 0:
            return
        case 1:
            isCommentPush = !isCommentPush
            key = isComment
            value = isCommentPush
        case 2:
            isLeaveMsgPush = !isLeaveMsgPush
            key = isLeaveMsg
            value = isLeaveMsgPush
        case 3:
            isActivityPush = !isActivityPush
            key = isActivity
            value = isActivityPush
        case 4:
            isSystemPush = !isSystemPush
            key = isSystem
            value = isSystemPush
        default:
            break
        }
        uDefaults.set(value, forKey: key)
        uDefaults.synchronize()
        pushTV.reloadData()
    }
}
