

import UIKit
import SDWebImage

class MSettingVC: MBaseVC {

    @IBOutlet weak var settingTV: BaseTV!
    @IBOutlet var footerView: UIView!
    @IBOutlet weak var quitBtn: UIButton!
    
    var isLogin:Bool = (uUserID.count != 0)
    /// 登录和未登录显示的行数量不同
    var indexOffset:Int = 0
    var isSoundEnabled:Bool = false
    var isVibrationEnabled:Bool = false
    var cacheSizeStr:String = "计算中"
    
    var loginType:LoginType = LoginType.phone
    
    fileprivate let soundKey:String = "isSoundEnabled"
    fileprivate let vibrationKey:String = "isVibrationEnabled"

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "设置"
        self.view.backgroundColor = UIColor.groupTableViewBackground
        
        if let loginType = uDefaults.string(forKey: "UserLoginType"){
            let type:LoginType = LoginType(rawValue: loginType) ?? LoginType.phone
            self.loginType = type
        }
        
        setupTV()
        loadSetting()
        countCache()
    }
    func countCache(){
        
        DispatchQueue.global().async {
            let sdCacheSize:UInt = SDImageCache.shared().getSize()
            let sizeStr:String = self.countCacheSize(sdCacheSize)
            DispatchQueue.main.async {
                self.cacheSizeStr = sizeStr
                self.settingTV.reloadData()
            }
        }
    }
    func countCacheSize(_ size:UInt) -> String{
        let K:UInt = 1 << 10
        let M:UInt = K << 10
        let G:UInt = M << 10
        if size < K{
            return "\(size)B"
        }
        if size < M{
            return "\(size / K)KB"
        }
        if size < G{
            return "\(size / M)MB"
        }
        return "\(size / G)GB"
    }
    
    func setupTV(){
        
        settingTV.separatorStyle = .none
        settingTV.register(UINib.init(nibName: TVUniversalCellIdentifier, bundle: nil), forCellReuseIdentifier: TVUniversalCellIdentifier)
        
        if isLogin{
            settingTV.tableFooterView = footerView
            quitBtn.setCornerRounded(radius: 4)
            indexOffset = 0
        }else{
            indexOffset = 2
        }
    }
    func loadSetting(){
        
        if let soundEnabled = uDefaults.object(forKey: soundKey) as? Bool{
            isSoundEnabled = soundEnabled
        }
        if let vibrationEnabled = uDefaults.object(forKey: vibrationKey) as? Bool{
            isVibrationEnabled = vibrationEnabled
        }
    }
}

extension MSettingVC{
    
    @IBAction func quitBtnPressed(_ sender: Any) {
        
        let alert = UIAlertController(title: "提示", message: "\n退出登录？", preferredStyle: .alert)
        
        let okAction = UIAlertAction.init(title: "是的", style: .destructive) { (action) in
            
            uDefaults.set("", forKey: "UserID")
            uDefaults.set("", forKey: "UserToken")
            uDefaults.set("", forKey: "UserName")
            uDefaults.set("", forKey: "UserIcon")
            uDefaults.set("", forKey: "UserCoins")
            uDefaults.set("", forKey: "UserLevel")
            uDefaults.synchronize()
            
            self.toastSuccess("已退出登录")
            NotiCenter.post(name: UserDidLogoutNotification, object: nil)
            self.settingTV.tableFooterView = nil
            self.navigationController?.popViewController(animated: true)
        }
        let cancelAction = UIAlertAction(title: "取消", style: .default, handler: nil)
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        
        present(alert, animated: true, completion: nil)
    }
    func clearCache(){
        
        self.showSystemStyleAlert(title: "提示", msg: "\n清理缓存？") {
            
            self.view.makeToastActivity()
            
            let sdCache = SDWebImageManager.shared().imageCache!
            sdCache.clearMemory()
            sdCache.clearDisk(onCompletion: {
                self.view.hideToastActivity()
                self.toastSuccess("清理完成")
                self.countCache()
            })
        }
    }
}

extension MSettingVC:UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        if isLogin && self.loginType != LoginType.phone{
            return 4 - indexOffset
        }
        return 5 - indexOffset
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell = tableView.dequeueReusableCell(withIdentifier: TVUniversalCellIdentifier) as! TVUniversalCell
        cell.selectionStyle = .none
        
        var row = indexPath.row + indexOffset
        
        if isLogin && self.loginType != LoginType.phone{
            row += 1
        }
        
        configCell(cell, row: row)
        
        return cell
    }
    func configCell(_ cell:TVUniversalCell,row:Int){
        
        switch row {
        case 0:
            cell.setup(withType: .disclosureIndicator, title: "修改密码", icon: nil)
        case 1:
            cell.setup(withType: .disclosureIndicator, title: "推送设置", icon: nil)
        case 2:
            cell.setup(withType: .select, title: "开启声音", icon: nil,
                       isSelect: isSoundEnabled)
        case 3:
            cell.setup(withType: .select, title: "开启振动", icon: nil,
                       isSelect: isVibrationEnabled)
        case 4:
            cell.setup(withType: .subtitle, title: "清理缓存", icon: nil,subtitle: cacheSizeStr)
        default:
            break
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        var row = indexPath.row + indexOffset
        if isLogin && self.loginType != LoginType.phone{
            row += 1
        }
        
        switch row {
        case 0:
            let frVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MFinishResetPwdVC") as! MFinishResetPwdVC
            frVC.isAlterPwd = true
            self.navigationController?.pushViewController(frVC, animated: true)
        case 1:
            let pushVC = MPushSettingVC()
            self.navigationController?.pushViewController(pushVC, animated: true)
        case 2:
            isSoundEnabled = !isSoundEnabled
            self.settingTV.reloadData()
            uDefaults.set(isSoundEnabled, forKey: soundKey)
            uDefaults.synchronize()
        case 3:
            isVibrationEnabled = !isVibrationEnabled
            self.settingTV.reloadData()
            uDefaults.set(isVibrationEnabled, forKey: vibrationKey)
            uDefaults.synchronize()
        case 4:
            clearCache()
        default:
            break
        }
    }
    
}
