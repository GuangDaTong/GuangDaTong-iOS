

import UIKit
import SwiftyJSON

class MUserSearchVC: MBaseVC {

    var isFirstLoad = true
    var searchBtn = UIButton.init(frame: CGRect.zero)
    let searchTV = MBaseTV.init(frame: CGRect.zero, style: UITableViewStyle.plain)
    
    var _searchBar:UISearchBar! = nil
    var searchBar:UISearchBar{
        get{
            if _searchBar != nil{
                return _searchBar
            }
            _searchBar = UISearchBar.init(frame: CGRect.zero)
            _searchBar.alpha = 0.7
            _searchBar.tintColor = UIColor.lightGray //光标颜色
            _searchBar.backgroundColor = UIColor.clear //背景色
            _searchBar.placeholder = "输入要查找的用户昵称"
            _searchBar.delegate = self
            _searchBar.enablesReturnKeyAutomatically = true
            
            for subView in self.searchBar.subviews{
                for sSubView in subView.subviews{
                    // 移除背景，防止出现黑线
                    if sSubView.isKind(of: NSClassFromString("UISearchBarBackground")!){
                        sSubView.removeFromSuperview()
                    }
                    // 设置字体颜色
                    if sSubView.isKind(of: UITextField.self){
                        let tf = sSubView as! UITextField
                        tf.textColor = UIColor.darkGray
                    }
                }
            }
            return _searchBar
        }
        set{
            _searchBar = newValue
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UIColor.groupTableViewBackground
        setupNaviBar()
        setupTV()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if isFirstLoad{
            isFirstLoad = false
            searchBar.becomeFirstResponder()
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        
        searchBar.resignFirstResponder()
        super.viewWillDisappear(animated)
    }
    // MARK: - Setup
    func setupNaviBar(){
        
        let frame = CGRect.init(x: 0, y: 0, width: SCREEN_WIDTH - 120, height: 36)
        let containerView = UIView.init(frame: frame)
        containerView.addSubview(self.searchBar)
        self.navigationItem.titleView = containerView
        self.searchBar.frame = CGRect.init(x: -15, y: 0, width: SCREEN_WIDTH - 120, height: 36)
        
        self.searchBtn.setTitle("搜索", for: .normal)
        self.searchBtn.setTitleColor(UIColor.white, for: .normal)
        self.searchBtn.addTarget(self, action: #selector(beginSearch), for: .touchUpInside)
        let searchItem = UIBarButtonItem.init(customView: self.searchBtn)
        self.navigationItem.rightBarButtonItem = searchItem
        self.searchBtn.sizeToFit()
    }
    func setupTV(){
        
        searchTV.setup(withDelegate: self)
        
        self.view.addSubview(searchTV)
        searchTV.snp.makeConstraints { (m) in
            m.top.equalTo(view.snp.top)
            m.bottom.equalTo(view.snp.bottom)
            m.leading.equalTo(view.snp.leading)
            m.trailing.equalTo(view.snp.trailing)
        }
        searchTV.separatorStyle = .none
    }
    @objc func beginSearch(){
    
        self.searchBar.resignFirstResponder()
        self.searchTV.refreshData()
    }
    func showUserInfo(userID:String,name:String){
        
        let uiVC = self.loadViewControllerInMainStoryboard("MUserInfoVC") as! MUserInfoVC
        uiVC.userID = userID
        uiVC.userName = name
        self.hideNaviBackBtnTitle()
        self.navigationController?.pushViewController(uiVC, animated: true)
    }
}


extension MUserSearchVC:MBaseTVDelegate{
    
    func loadData(pageIndex:Int){
        
        guard let keyword = searchBar.text,keyword.count != 0 else {
            self.searchTV.appendData(nil)
            return
        }
        
        HttpController.shared.searchUser(keyword: keyword, pageIndex: pageIndex, successBlock: { (json) in
            
            let jsonArr = json["result"]["data"].arrayValue
            var contactArr = [MContactItem]()
            
            for itemJSON in jsonArr{
                let item = self.parseItem(fromJSON: itemJSON)
                contactArr.append(item)
            }
            self.searchTV.appendData(contactArr)
        }) { (err) in
            
            self.searchTV.loadFailed()
            self.toastFail(err.localizedDescription)
        }
        
    }
    func parseItem(fromJSON data:JSON) -> MContactItem{
        
        let id = data["id"].stringValue
        let nickname = data["nickname"].stringValue
        let iconURL = data["icon"].stringValue
        
        let item = MContactItem.init(id: id, name: nickname, icon: iconURL)
        
        return item
    }
    
    func nibsToRegister() -> [String]{
        return [MMineContactVCCellIdentifier]
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return searchTV.dataArr.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell = tableView.dequeueReusableCell(withIdentifier: MMineContactVCCellIdentifier) as! MMineContactVCCell
        let item = searchTV.dataArr[indexPath.row] as! MContactItem
        cell.setup(item.name, iconURL: item.icon)
        cell.tintColor = DefaultColorRed
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return MMineContactVCCellHeight
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView.isEditing{
            return
        }
        let item = searchTV.dataArr[indexPath.row] as! MContactItem
        tableView.deselectRow(at: indexPath, animated: true)
        self.showUserInfo(userID: item.id, name: item.name)
    }
}

// MARK: - Delegates
extension MUserSearchVC:UISearchBarDelegate{
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText.count > DefaultSearchTextMaxLength{
            searchBar.text = String(searchText.characters.prefix(DefaultSearchTextMaxLength))
        }
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        beginSearch()
    }
}
