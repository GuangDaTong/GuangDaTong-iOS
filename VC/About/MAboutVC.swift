

import UIKit

class MAboutVC: MBaseVC,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var versionLB: UILabel!
    @IBOutlet weak var aboutTV: BaseTV!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "关于"
        versionLB.text = "广大通 \(APP_Current_Version)(\(APP_Current_Build))"
        aboutTV.delegate = self
        aboutTV.dataSource = self
        aboutTV.separatorStyle = .none
        aboutTV.register(UINib.init(nibName: TVUniversalCellIdentifier, bundle: nil), forCellReuseIdentifier: TVUniversalCellIdentifier)
    }
    // MARK: - TV Datasoure & Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:TVUniversalCell = tableView.dequeueReusableCell(withIdentifier: TVUniversalCellIdentifier) as! TVUniversalCell
        cell.titleLBWidth.constant = 150
        cell.selectionStyle = .none
        let row:Int = indexPath.row
        switch row {
        case 0:
            cell.setup(withType: .disclosureIndicator, title: "关于广大通", icon: nil)
        case 1:
            cell.setup(withType: .disclosureIndicator, title: "加入我们", icon: nil)
        case 2:
            cell.setup(withType: .disclosureIndicator, title: "使用条款", icon: nil)
        default:
            break
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        switch indexPath.row {
        case 0:
            showWebVC("关于广大通", url: BaseURL + "/aboutMew")
        case 1:
            showWebVC("加入我们", url: BaseURL + "/joinUs")
        case 2:
            showWebVC("使用条款", url: BaseURL + "/contract")
        default:
            break
        }
    }
    func showWebVC(_ title:String,url:String){
        let webVC:MWebVC = self.loadViewControllerInMainStoryboard("MWebVC") as! MWebVC
        webVC.setup(title: title, url: url)
        self.navigationController?.pushViewController(webVC, animated: true)
    }
}
