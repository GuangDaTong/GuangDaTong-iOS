

import UIKit
import SDWebImage
import SwiftyJSON

enum LoginType:String {
    case phone
    case qq
    case wechat
}

class MLoginVC: MBaseVC,UITextFieldDelegate {

    // MARK: - Constraints
    @IBOutlet weak var accountSepLineHeight: NSLayoutConstraint!
    @IBOutlet weak var pwdSepLineHeight: NSLayoutConstraint!
    // MARK: - Views
    @IBOutlet weak var accountTF: UITextField!
    @IBOutlet weak var pwdTF: UITextField!
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var wechatIV: UIImageView!
    @IBOutlet weak var qqIV: UIImageView!
    
    var loginType:LoginType = LoginType.phone
    var uid:String = ""
    var name:String = ""
    var gender:String = ""
    var selectedCollege:String = ""
    
    var instituteArr:[String] = [String]()
    var _listVC:MSelectionListVC! = nil
    var listVC:MSelectionListVC{
        get{
            if _listVC != nil {
                return _listVC
            }
            _listVC = self.loadViewControllerInMainStoryboard("MSelectionListVC") as! MSelectionListVC
            _listVC.title = "请选择学院"
            _listVC.selectedBlock = { (title) in
                
                self.selectedCollege = title
                if self.loginType == LoginType.qq{
                    self.loginByQQ()
                }else{
                    // loginByWechat()
                }
                self.listVC.selectedStr = ""
            }
            return _listVC
        }
    }
    
    /// 是否要进入注册界面
    var isRegister = false
    var failCount:Int = 0
    
    // MARK: - VC Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        let account = uDefaults.string(forKey: "UserAccount")
        if let acc = account {
            if acc.count == 0{
                loginBtn.isEnabled = false
                return
            }
            accountTF.text = acc
            let pwd = uDefaults.string(forKey: "UserPassword")
            if let p = pwd {
                pwdTF.text = p
                loginBtn.isEnabled = true
            }
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        APP.setStatusBarHidden(true, with: .fade)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if isRegister {
            isRegister = false
            return
        }
        APP.setStatusBarHidden(false, with: .none)
    }
    
    // MARK: - UI
    func setupUI(){
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        loginBtn.isEnabled = false
        accountSepLineHeight.setToOnePixel()
        pwdSepLineHeight.setToOnePixel()
        
        NotiCenter.addObserver(self, selector: #selector(checkLoginBtnStatus), name: NSNotification.Name.UITextFieldTextDidChange, object: nil)
        accountTF.delegate = self
        accountTF.setPlaceholderTextToLightGray()
        pwdTF.delegate = self
        pwdTF.setPlaceholderTextToLightGray()
        setupNaviBar()
        
        let wcTap = UITapGestureRecognizer.init(target: self, action: #selector(wechatLogin))
        wechatIV.addGestureRecognizer(wcTap)
        let qqTap = UITapGestureRecognizer.init(target: self, action: #selector(qqLogin))
        qqIV.addGestureRecognizer(qqTap)
    }
    
    func setupNaviBar(){
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.barTintColor = DefaultColorBlack
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
    }
    
    @objc func checkLoginBtnStatus(){
        
        self.loginType = LoginType.phone
        guard (accountTF.text?.count)! > 0 && (pwdTF.text?.count)! > 0 else {
            
            if accountTF.text?.count == 0 {
                
                pwdTF.text = ""
                uDefaults.set("", forKey: "UserAccount")
                uDefaults.set("", forKey: "UserPassword")
                uDefaults.synchronize()
            }
            loginBtn.isEnabled = false
            return
        }
        loginBtn.isEnabled = true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == accountTF{
            view.endEditing(true)
            pwdTF.becomeFirstResponder()
        }else{
            view.endEditing(true)
            login()
        }
        return true
    }
    // MARK: - 第三方登录
    @objc func wechatLogin(){
        
        self.loginType = LoginType.wechat
        self.showAlert("微信登录即将开放，敬请期待~")
    }
    @objc func qqLogin(){
        
        self.loginType = LoginType.qq
        UMSocialManager.default().getUserInfo(with: UMSocialPlatformType.QQ, currentViewController: self) { (data, error) in
            
            self.view.makeToastActivity()
            self.parseAuthInfo(data: data, error: error,isQQ: true)
        }
    }
    func parseAuthInfo(data:Any?,error:Error?,isQQ:Bool){
        
        if let err = error{
            self.view.hideToastActivity()
            let msg:String = err.localizedDescription
            if msg.contains("2009"){ //操作取消
                return
            }
            toastFail(err.localizedDescription)
            return
        }
        
        let res:UMSocialUserInfoResponse = data as! UMSocialUserInfoResponse
        self.uid = res.usid
        self.name = res.name
        self.gender = res.unionGender
        
        HttpController.shared.isQQRegister(uuid: uid, successBlock: { (json) in
            
            let isReg:Bool = json["result"]["isRegister"].boolValue
            if isReg{
                self.loginByQQ()
            }else{
                self.getInstitueData()
            }
        }, failure: { (err) in
            self.view.hideToastActivity()
            self.toastFail(err.localizedDescription)
        })
    }
    func loginByQQ(){
        
        HttpController.shared.loginByQQ(uid, name: name, college: selectedCollege,gender:gender, successBlock: { (json) in
            
            uDefaults.set("qq", forKey: "UserLoginType")
            uDefaults.synchronize()
            
            self.parseLoginInfo(json: json)
        }, failure: { (err) in
            
            self.view.hideToastActivity()
            self.toastFail(err.localizedDescription)
        })
    }
    /// 获取学院列表
    func getInstitueData(){
        
        HttpController.shared.instituteList(success: { (json) in
            
            let arr:[JSON] = json["result"]["colleges"].arrayValue
            self.instituteArr = [String]()
            for institute in arr {
                self.instituteArr.append(institute.stringValue)
            }
            self.listVC.list = self.instituteArr
            
            self.view.hideToastActivity()
            
            self.view.endEditing(true)
            self.navigationController?.pushViewController(self.listVC, animated: true)
            
        }, failure: {  (err) in
            self.view.hideToastActivity()
            self.toastFail(err.localizedDescription)
        })
    }
    
    // MARK: - 手机号登录
    func login(){
        
        loginBtn.isEnabled = false
        if self.failCount >= MaxLoginFailCount{
            self.toastFail("登录失败次数过多，请您稍候重试")
            return
        }
        let account:String = accountTF.text ?? ""
        if account.count != 11{
            self.toastFail("帐号应为11位手机号")
            loginBtn.isEnabled = true
            return
        }
        let pwd:String = pwdTF.text ?? ""
        if pwd.count < 6{
            self.toastFail("密码太短")
            loginBtn.isEnabled = true
            return
        }
        
        uDefaults.set(account, forKey: "UserAccount")
        uDefaults.set(pwd, forKey: "UserPassword")
        uDefaults.synchronize()
        
        view.endEditing(true)
        self.view.makeToastActivity()
        
        HttpController.shared.loginWithAccount(account, pwd: pwd, successBlock: { (json) in
            
            uDefaults.set("phone", forKey: "UserLoginType")
            uDefaults.synchronize()
            
            self.parseLoginInfo(json: json)
        }, failure: { (err) in
            
            self.view.hideToastActivity()
            self.loginBtn.isEnabled = true
            self.failCount += 1
            self.toastFail(err.localizedDescription)
        })
    }
    func parseLoginInfo(json:JSON){
        
        self.view.hideToastActivity()
        
        let r:JSON = json["result"]
        
        uDefaults.set(r["id"].stringValue, forKey: "UserID")
        uDefaults.set(r["token"].stringValue, forKey: "UserToken")
        uDefaults.set(r["nickname"].stringValue, forKey: "UserName")
        uDefaults.set(r["icon"].stringValue, forKey: "UserIcon")
        uDefaults.set(r["coins"].intValue, forKey: "UserCoins")
        uDefaults.set(r["level"].intValue, forKey: "UserLevel")
        uDefaults.set(r["college"].stringValue, forKey: "UserCollege")
        uDefaults.set(r["gender"].stringValue, forKey: "UserGender")
        uDefaults.set(r["grade"].stringValue, forKey: "UserGrade")
        uDefaults.set(r["pushUrgentSchool"].boolValue, forKey: "isSchoolPush")
        uDefaults.set(r["pushUrgentCollege"].boolValue, forKey: "isCollegePush")
        uDefaults.set(r["pushComment"].boolValue, forKey: "isCommentPush")
        uDefaults.set(r["pushLeaveWords"].boolValue, forKey: "isLeaveMsgPush")
        uDefaults.set(r["pushActivity"].boolValue, forKey: "isActivityPush")
        uDefaults.set(r["pushSystem"].boolValue, forKey: "isSystemPush")
        uDefaults.synchronize()
        
        let blacklistArray:[JSON] = r["blacklist"].arrayValue
        var blackArray:[String] = [String]()
        for item in blacklistArray{
            let userID = item["id"].stringValue
            blackArray.append(userID)
        }
        MAskFilter.shared.resetBlackList(list: blackArray)
        
        NotiCenter.post(name: UserDidLoginNotification, object: nil)
        self.toastSuccess("登录成功!")
        delay(second: 0.5, block: {
            self.dismiss(animated: true, completion:nil)
        })
    }
    
    // MARK: - Touch Events
    @IBAction func loginBtnPressed(_ sender: Any) {
        
        login()
    }
    @IBAction func registerBtnPressed(_ sender: Any) {
        
        isRegister = true
        pushVCInMainStoryboard("MRegisterVC")
    }
    @IBAction func forgetPwdBtnPressed(_ sender: Any) {
        
        let resetVC = loadViewControllerInMainStoryboard("MResetPwdVC") as! MResetPwdVC
        resetVC.phone = accountTF.text ?? ""
        self.navigationController?.pushViewController(resetVC, animated: true)
    }
    
    @IBAction func closeBtnPressed(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func viewDidTouchDown(_ sender: Any) {
        view.endEditing(true)
    }
}
