

import Foundation

extension MWebVC{
    
    func showSignupVC(){
        
        if isUserNotLogin{
            self.toastFail("您尚未登录")
            self.presentVCInMainStoryboard("MLoginNaviVC")
            return
        }
        var discoveryType:SignupType = .activity
        switch zanType {
        case .partTime:
            discoveryType = .partTime
        case .training:
            discoveryType = .training
        case .activity:
            discoveryType = .activity
        default:
            break
        }
        let suVC = MSignUpVC()
        suVC.setupWithDiscoveryID(itemID, type: discoveryType)
        self.hideNaviBackBtnTitle()
        self.navigationController?.pushViewController(suVC, animated: true)
    }
    func showContactSystemVC(){
        
        if isUserNotLogin{
            self.toastFail("您尚未登录")
            self.presentVCInMainStoryboard("MLoginNaviVC")
            return
        }
        
        let tsVC = self.loadViewControllerInMainStoryboard("MTextSendVC") as! MTextSendVC
        tsVC.setupWith(operationType: .contactSystem, ID: itemID,zanType: zanType)
        self.hideNaviBackBtnTitle()
        self.navigationController?.pushViewController(tsVC, animated: true)
    }
    @objc func showMenu(){
        
        let sheet = UIAlertController.init(title: "", message: "请选择", preferredStyle: .actionSheet)
        let shareAction = UIAlertAction.init(title: "分享", style: .default) { [weak self](action) in
            self?.showShareMenu()
        }
        sheet.addAction(shareAction)
        let collectAction = UIAlertAction.init(title: "收藏", style: .default) { [weak self](action) in
            self?.collect()
        }
        sheet.addAction(collectAction)
        if rightNavigationItemType == .menuWithFeedback {
            let wrongAction = UIAlertAction.init(title: "信息有误", style: .destructive) { [weak self] (action) in
                self?.wrongInfoFeedback()
            }
            sheet.addAction(wrongAction)
        }
        let cancelAction = UIAlertAction.init(title: "取消", style: .cancel, handler: nil)
        sheet.addAction(cancelAction)
        // iPad需要指明这个参数，而非iPad不会执行if内的语句
        if sheet.popoverPresentationController != nil{
            sheet.popoverPresentationController?.barButtonItem = self.navigationItem.rightBarButtonItem
            sheet.popoverPresentationController?.sourceView = self.view
            sheet.popoverPresentationController?.sourceRect = self.view.bounds
        }
        
        present(sheet, animated: true, completion: nil)
    }
    func showShareMenu(){
        
        let shareView = MShareView.loadFromNib()
        shareView.show()
        shareView.shareSelectedBlock = { platform in
            
            self.shareWithPlatform(platform)
        }
    }
    func shareWithPlatform(_ platform:MSharePlatformType){
        
        let image:UIImage! = UIImage.init(named: "alert_mew")
        let titleStr = "广大通"
        let descriptionStr = self.title ?? "广大通"
        var shareType = MShareType.ask
        switch self.zanType {
        case .guidance:
            shareType = .guidance
        case .summary:
            shareType = .summary
        case .club:
            shareType = .club
        case .look:
            shareType = .look
        case .activity:
            shareType = .activity
        case .partTime:
            shareType = .partTime
        case .training:
            shareType = .training
        default:
            break
        }
        let url = self.urlStr.replacingOccurrences(of: "&OSType=iOS", with: "")
        let shareMaker = MShareMaker.init(ownerVC: self, type: shareType, platform: platform, objID: itemID, title: titleStr, description: descriptionStr, url: url, image: image)
        shareMaker.startShare()
    }
    func collect(){
        
        if isUserNotLogin{
            self.toastFail("您尚未登录")
            self.presentVCInMainStoryboard("MLoginNaviVC")
            return
        }
        if self.isCollect{
            self.toastFail("您已经收藏过了")
            return
        }
        
        self.view.makeToastActivity()
        
        var collectType = CollectType.guidance
        
        switch self.zanType {
        case .guidance:
            collectType = .guidance
        case .summary:
            collectType = .summary
        case .club:
            collectType = .club
        case .look:
            collectType = .look
        case .ask:
            collectType = .ask
        case .activity:
            collectType = .activity
        case .partTime:
            collectType = .partTime
        case .training:
            collectType = .training
        default:
            break
        }
        
        HttpController.shared.collect(type:collectType,ID:itemID, successBlock: {  (json) in
            
            self.view.hideToastActivity()
            self.toastSuccess("收藏成功")
            self.isCollect = true
            self.collectBlock?()
            
            }, failure: { (err) in
                
                self.view.hideToastActivity()
                self.toastFail(err.localizedDescription)
        })
    }
    func wrongInfoFeedback(){
        
        let tsVC = self.loadViewControllerInMainStoryboard("MTextSendVC") as! MTextSendVC
        tsVC.setupWith(operationType: .reportError, ID: self.itemID,zanType: zanType)
        self.hideNaviBackBtnTitle()
        self.navigationController?.pushViewController(tsVC, animated: true)
    }
}
