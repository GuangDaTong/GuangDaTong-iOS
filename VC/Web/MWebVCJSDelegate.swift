

import UIKit
import WebKit

class MWebVCJSDelegate: NSObject,WKScriptMessageHandler {

    weak var scriptDelegate:WKScriptMessageHandler! = nil
    
    init(scriptDelegate:WKScriptMessageHandler) {
        self.scriptDelegate = scriptDelegate
    }
    
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage){
        
        self.scriptDelegate.userContentController(userContentController, didReceive: message)
    }
}
