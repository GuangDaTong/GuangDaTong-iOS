

import UIKit

class MWebBottomBar: UIView {

    var zanBlock:(() -> Void)?
    var contactBlock:(() -> Void)?
//    var signupBlock:(() -> Void)?
    
    static let defaultHeight:CGFloat = 32
    @IBOutlet weak var readCountLB: UILabel!
    @IBOutlet weak var zanCountLB: UILabel!
    @IBOutlet weak var zanIconBtn: UIButton!
    @IBOutlet weak var zanBtn: UIButton!
    @IBOutlet weak var signupBtn: UIButton!
    @IBOutlet weak var topSepLineHeight: NSLayoutConstraint!
    fileprivate var type:WebVCBottomBarType = .none
    
    var readCount:Int = 0
    var zanCount:Int = 0
    
    class func loadFromNib(withType type:WebVCBottomBarType) -> MWebBottomBar{
        
        let view = Bundle.main.loadNibNamed("MWebBottomBar", owner: nil, options: nil)?.last as! MWebBottomBar
        view.type = type
        return view
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        
        topSepLineHeight.setToOnePixel()
        zanIconBtn.setImage(UIImage.init(named: "zan_gray"), for: .normal)
        zanIconBtn.setImage(UIImage.init(named: "zan_blue"), for: .selected)
    }
    @IBAction func signupBtnPressed(_ sender: Any) {
        contactBlock?()
    }
    @IBAction func zanBtnPressed(_ sender: Any) {
        
        if zanIconBtn.isSelected{
            return
        }
        if isUserNotLogin{
            zanBlock?()
            return
        }
        setToZan()
        setupZanCount(self.zanCount + 1)
        zanBlock?()
    }
    func setupReadCount(_ count:Int){
        self.readCount = count
        readCountLB.text = "阅读 \(self.readCount)"
    }
    func setupZanCount(_ count:Int){
        self.zanCount = count
        zanCountLB.text = "\(self.zanCount)"
    }
    func setToZan(){
        
        zanIconBtn.isSelected = true
        UIView.animate(withDuration: 0.15, animations: {
            self.zanIconBtn.imageView?.layer.transform = CATransform3DMakeScale(1.1, 1.1, 0)
        }, completion: { (finish) in
            UIView.animate(withDuration: 0.15, animations: {
                self.zanIconBtn.imageView?.layer.transform = CATransform3DIdentity
            })
        })
    }
    func setupZan(isZan:Bool){
        
        zanIconBtn.isSelected = isZan
    }
}
