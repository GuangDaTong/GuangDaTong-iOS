

import Foundation
import WebKit

extension MWebVC:WKNavigationDelegate,WKUIDelegate,WKScriptMessageHandler{
    
    // MARK: - Webview Delegate
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        
        let frameInfo:WKFrameInfo? = navigationAction.targetFrame
        if frameInfo == nil{ //打开新页面显示，针对_blank情况
            if let url = navigationAction.request.url{
                decisionHandler(WKNavigationActionPolicy.cancel)
                self.mWebView.load(URLRequest.init(url: url))
                return
            }
        }
        decisionHandler(WKNavigationActionPolicy.allow)
    }
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
        
        delay(second: 0.5) {
            self.urlStr = webView.url?.absoluteString
            if webView.title?.count != 0 {
                self.title = webView.title
            }
            self.setupBottomBar()
            self.bottomBar?.isHidden = false
            self.readBlock?()
        }
        
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        
        checkGoBack()
    }
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        
        bottomBar?.isHidden = true
        self.progressView.isHidden = true
        
        checkGoBack()
    }
    /// 检查返回手势（pop/goback）
    func checkGoBack(){
        
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = !self.mWebView.canGoBack
        if self.mWebView.canGoBack{
            showLeftNavigationItem()
        }else{
            self.navigationItem.leftBarButtonItems = nil
        }
    }
    // Webview Alert弹出 http://www.jianshu.com/p/c69ef186de17
    //Alert弹框
    func webView(_ webView: WKWebView, runJavaScriptAlertPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping () -> Void) {
        let alert = UIAlertController(title: message, message: nil, preferredStyle: .alert)
        let action = UIAlertAction(title: "好的", style: .cancel) { (_) in
            completionHandler()
        }
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    //confirm弹框
    func webView(_ webView: WKWebView, runJavaScriptConfirmPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping (Bool) -> Void) {
        let alert = UIAlertController(title: message, message: nil, preferredStyle: .alert)
        let action = UIAlertAction(title: "确定", style: .default) { (_) in
            completionHandler(true)
        }
        let cancelAction = UIAlertAction(title: "取消", style: .cancel) { (_) in
            completionHandler(false)
        }
        alert.addAction(action)
        alert.addAction(cancelAction)
        present(alert, animated: true, completion: nil)
    }
    //TextInput弹框
    func webView(_ webView: WKWebView, runJavaScriptTextInputPanelWithPrompt prompt: String, defaultText: String?, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping (String?) -> Void) {
        let alert = UIAlertController(title: prompt, message: nil, preferredStyle: .alert)
        alert.addTextField(configurationHandler: { (textField) in
            
        })
        let action = UIAlertAction.init(title: "确定", style: .default, handler: { (action) in
            completionHandler(alert.textFields?.last?.text)
        })
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    
    // WKWebView多线程加载，有崩溃的可能，崩溃后会导致白屏，需重新加载内容
    // http://blog.csdn.net/tencent_bugly/article/details/54668721
    func webViewWebContentProcessDidTerminate(_ webView: WKWebView) {
        webView.reload()
    }
    
    // JS回调
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage){
        
        let body = message.body
        switch body {
        case let d as NSDictionary:
            var index = 0
            if let idx = d["imageIndex"] as? Int{
                index = idx
            }
            if let urlArray = d["imageURLArray"] as? NSArray{
                if urlArray.count <= 0{
                    return
                }
                if let array = urlArray as? [String]{
                    showImageBrowser(currentIndex: index, urls: array)
                }
            }
        default:
            DLOG(body)
        }
    }
}
