

import UIKit
import WebKit
import SnapKit

/// 导航栏右边按钮类型
///
/// - none: wu
/// - share: 分享
/// - menu: 菜单
enum WebVCRightNavigationItemType {
    case none
    case menu
    case menuWithFeedback
}

/// 底部View类型
///
/// - none: 无
/// - zan: 点赞
/// - signUp: 点赞 & 报名
enum WebVCBottomBarType {
    case none
    case zan
    case signUp
}

class MWebVC: MBaseVC {
    
    var mWebView:WKWebView! = nil
    var progressView:UIProgressView = UIProgressView.init(frame: CGRect.zero)
    /// 需要引用KVO闭包，否则会被释放
    var observeBlock: NSKeyValueObservation! = nil
    /// js代理，防止内存泄露
    var jsDelegate:MWebVCJSDelegate! = nil
    var bottomBar:MWebBottomBar?
    
    var zanBlock:(() -> Void)? //赞
    var collectBlock:(() -> Void)? //收藏
    var readBlock:(() -> Void)? // 文章阅读完毕
    
    var rightNavigationItemType:WebVCRightNavigationItemType = .none
    var bottomBarType:WebVCBottomBarType = .none
    var zanType:ZanType = .guidance
    /// 查看的项目的ID
    var itemID = ""
    var titleStr = ""
    var urlStr:String! = nil
    
    var isCollect = false
    var isZan = false
    var zanCount = 0
    var readCount = 0
    
    // MARK: - VC Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        setupWebView()
        setupRightNavigationItem()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    deinit {
        self.mWebView.load(URLRequest.init(url: URL.init(string: "about:blank")!))
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        pausePlay()
    }
    
    // MARK: - Setup
    
    /// 初始化
    ///
    /// - Parameters:
    ///   - title: 标题
    ///   - url: 连接字符串
    ///   - bottomBarType: 底部按钮
    ///   - rightNavigationItemType: 导航栏按钮
    ///   - zanType: 赞类型
    ///   - itemID: 操作对象ID
    ///   - isCollect: 是否已收藏
    ///   - isZan: 是否已赞
    ///   - readCount: 阅读数量
    ///   - zanCount: 点赞数量
    func setup(title:String,url:String,bottomBarType:WebVCBottomBarType = .none,rightNavigationItemType:WebVCRightNavigationItemType = .none,zanType:ZanType = .none,itemID:String?=nil,isCollect:Bool=false,isZan:Bool=false,readCount:Int=0,zanCount:Int=0){
        
        self.title = title
        self.itemID = itemID ?? ""
        self.zanType = zanType
        self.urlStr = url
        self.rightNavigationItemType = rightNavigationItemType
        self.bottomBarType = bottomBarType
        
        self.isCollect = isCollect
        self.isZan = isZan
        self.zanCount = zanCount
        self.readCount = readCount
    }
    func setupWebView(){
        
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        
        let conf:WKWebViewConfiguration = WKWebViewConfiguration.init()
        let prefer:WKPreferences = WKPreferences.init()
        prefer.javaScriptEnabled = true
        conf.preferences = prefer
        conf.allowsInlineMediaPlayback = true
        if #available(iOS 10.0, *) {
            conf.mediaTypesRequiringUserActionForPlayback = WKAudiovisualMediaTypes.all
        } else {
            conf.mediaPlaybackRequiresUserAction = true
        }
        
        let pauseJSString:String =
"""
var videos = document.getElementsByTagName("video");
        function pauseVideo(){
            var len = videos.length
            for(var i=0;i<len;i++){
                videos[i].pause();
            }
        }
        var audios = document.getElementsByTagName("audio");
        function pauseAudio(){
            var len = audios.length
            for(var i=0;i<len;i++){
                audios[i].pause();
            }
        }
"""
        let pauseJS:WKUserScript = WKUserScript.init(source: pauseJSString, injectionTime: WKUserScriptInjectionTime.atDocumentStart, forMainFrameOnly: true)
        
        let userContentCtrl:WKUserContentController = WKUserContentController.init()
        self.jsDelegate = MWebVCJSDelegate.init(scriptDelegate: self) //防止内存泄露
        userContentCtrl.add(jsDelegate, name: "showImage") //查看图片的JS函数
        userContentCtrl.addUserScript(pauseJS)
        conf.userContentController = userContentCtrl
        
        mWebView = WKWebView.init(frame: CGRect.zero, configuration: conf)
        mWebView.tintColor = UIColor.white
        mWebView.uiDelegate = self
        mWebView.navigationDelegate = self
        mWebView.allowsBackForwardNavigationGestures = true
        
        // urlStr = "https://www.baidu.com"
        if !urlStr.hasPrefix("http"){
            urlStr = BaseURL
        }
        if !urlStr.contains("?"){ //不含参数
            urlStr = urlStr + "?OSType=iOS"
        }else{ //已包含参数
            urlStr = urlStr + "&OSType=iOS"
        }
        
        let url:URL = URL.init(string: urlStr)!
        let req:URLRequest = URLRequest.init(url:url)
        mWebView.load(req)
        view.addSubview(mWebView)
        mWebView.snp.makeConstraints { (make) in
            make.leading.equalTo(self.view.snp.leading)
            make.trailing.equalTo(self.view.snp.trailing)
            make.top.equalTo(self.view.snp.top)
            let offset:CGFloat = (self.bottomBarType != .none) ? -MWebBottomBar.defaultHeight : 0
            make.bottom.equalTo(self.view.snp.bottom).offset(offset)
        }
        configProgressView()
    }
    func configProgressView(){
        
        self.progressView.trackTintColor = UIColor.white
        self.progressView.progressTintColor = DefaultColorGreen.withAlphaComponent(0.8)
        self.progressView.frame = CGRect.init(x: 0, y: 0, width: SCREEN_WIDTH, height: 1)
        self.progressView.transform = CGAffineTransform.init(scaleX: 1, y: 3)
        self.view.addSubview(self.progressView)
        self.progressView.progress = 0.05 //设置初始值，防止网页加载过慢时没有进度
        
        //Swift 4 KVO
        self.observeBlock = mWebView.observe(\.estimatedProgress, options: NSKeyValueObservingOptions.new.union(NSKeyValueObservingOptions.old)) { (webview, changed) in
            
            if let new = changed.newValue{
                self.progressView.isHidden = false
                self.progressView.progress = Float(new)
                if new >= 1.0{
                    delay(second: 0.3, block: {
                        self.progressView.isHidden = true
                    })
                }
            }
        }
    }
    func showLeftNavigationItem(){
        
        let goBackBtn = UIButton.init()
        let closeBtn = UIButton.init()
        
        goBackBtn.setImage(UIImage.init(named: "navi_go_back"), for: UIControlState.normal)
        goBackBtn.setTitle(" 返回", for: UIControlState.normal)
        goBackBtn.addTarget(self, action: #selector(goBack), for: UIControlEvents.touchUpInside)
        goBackBtn.sizeToFit()
        goBackBtn.contentEdgeInsets = UIEdgeInsetsMake(0, -8, 0, 8)
        
        let backItem = UIBarButtonItem.init(customView: goBackBtn)
        closeBtn.setTitle("关闭", for: UIControlState.normal)
        closeBtn.addTarget(self, action: #selector(popViewController), for: UIControlEvents.touchUpInside)
        closeBtn.sizeToFit()
        closeBtn.contentEdgeInsets = UIEdgeInsetsMake(0, -4, 0, 4)
        let closeItem = UIBarButtonItem.init(customView: closeBtn)
        
        let items:[UIBarButtonItem] = [backItem,closeItem]
        self.navigationItem.leftBarButtonItems = items
    }
    @objc func goBack(){
        self.mWebView.goBack()
    }
    func setupRightNavigationItem(){
        
        switch rightNavigationItemType {
        case .none:
            break
        case .menu:
            fallthrough
        case .menuWithFeedback:
            let menuBtn = UIButton.init()
            menuBtn.setImage(UIImage.init(named: "naviBar_menu"), for: .normal)
            menuBtn.sizeToFit()
            menuBtn.addTarget(self, action: #selector(showMenu), for: .touchUpInside)
            let msgItem = UIBarButtonItem.init(customView: menuBtn)
            self.navigationItem.rightBarButtonItem = msgItem
            break
        }
    }
    func setupBottomBar(){
        
        switch bottomBarType {
        case .none:
            break
        case .zan:
            fallthrough
        case .signUp:
            bottomBar = MWebBottomBar.loadFromNib(withType:bottomBarType)
            let bar = bottomBar!
            view.addSubview(bar)
            bar.snp.makeConstraints { (make) in
                make.leading.equalTo(view.snp.leading)
                make.trailing.equalTo(view.snp.trailing)
                make.top.equalTo(mWebView.snp.bottom)
                make.bottom.equalTo(view.snp.bottom)
            }
            bar.setupReadCount(self.readCount)
            bar.setupZanCount(self.zanCount)
            bar.setupZan(isZan: self.isZan)
            
            bar.zanBlock = {
                
                if isUserNotLogin{
                    self.toastFail("您尚未登录")
                    self.presentVCInMainStoryboard("MLoginNaviVC")
                    return
                }
                self.isZan = true
                self.zanCount += 1
                self.zanBlock?()
                
                let ID = self.itemID
                HttpController.shared.zan(type: self.zanType, ID: ID, successBlock: { (json) in
                    DLOG("赞 帖子\(ID) 成功！")
                }, failure: { (err) in
                    DLOG("赞 帖子\(ID) 失败 ！")
                })
            }
            if bottomBarType == .zan{
                bar.signupBtn.isHidden = true
                return
            }
            bar.contactBlock = {
                self.showContactSystemVC()
            }
        }
    }
    /// 暂停播放网页内的音频、视频
    func pausePlay(){
        mWebView.evaluateJavaScript("pauseVideo()") { (data, error) in
        }
        mWebView.evaluateJavaScript("pauseAudio()") { (data, error) in
        }
    }
}
