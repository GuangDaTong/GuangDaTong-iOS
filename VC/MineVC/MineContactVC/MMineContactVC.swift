

import UIKit
import SwiftyJSON

struct MContactItem {
    var id:String
    var name:String
    var icon:String
}

class MMineContactVC: MBaseVC {

    let manageBtn = UIButton.init()
    let addContactBtn = UIButton.init()
    let contactTV = MBaseTV.init(frame: CGRect.zero, style: UITableViewStyle.plain)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        NotiCenter.addObserver(self, selector: #selector(refreshContactList), name: UserDidAlterContactNotification, object: nil)
        setupUI()
    }
    deinit {
        NotiCenter.removeObserver(self)
    }
    func setupUI(){
        
        self.title = "通讯录"
        self.view.backgroundColor = UIColor.white
        setupNaviBar()
        
        contactTV.setup(withDelegate: self)
        
        self.view.addSubview(contactTV)
        contactTV.snp.makeConstraints { (m) in
            m.top.equalTo(view.snp.top)
            m.bottom.equalTo(view.snp.bottom)
            m.leading.equalTo(view.snp.leading)
            m.trailing.equalTo(view.snp.trailing)
        }
        contactTV.separatorStyle = .none
        contactTV.refreshData()
    }
    func setupNaviBar(){
        
        manageBtn.isHidden = true
        manageBtn.setTitle("删除", for: .normal)
        manageBtn.sizeToFit()
        manageBtn.addTarget(self, action: #selector(manageContact), for: .touchUpInside)
        let msgItem = UIBarButtonItem.init(customView: manageBtn)
        addContactBtn.setTitle("添加", for: .normal)
        addContactBtn.sizeToFit()
        addContactBtn.addTarget(self, action: #selector(addUser), for: .touchUpInside)
        let addItem = UIBarButtonItem.init(customView: addContactBtn)
        self.navigationItem.rightBarButtonItems = [addItem,msgItem]
    }
    @objc func addUser(){
        
        let usVC = MUserSearchVC()
        self.navigationController?.pushViewController(usVC, animated: true)
    }
    @objc func manageContact(){
        
        if contactTV.isEditing{
            manageBtn.setTitle("删除", for: .normal)
            if let indexPaths = contactTV.indexPathsForSelectedRows{
                commitToDelete(indexPaths:indexPaths)
            }
        }else{
            manageBtn.setTitle("完成", for: .normal)
        }
        
        contactTV.isEditing = !contactTV.isEditing
    }
    func commitToDelete(indexPaths:[IndexPath]){
        
        let alert = UIAlertController(title: "提示", message: "\n删除\(indexPaths.count)位联系人？", preferredStyle: .alert)
        
        let okAction = UIAlertAction.init(title: "是的", style: .destructive) { (action) in
            self.deleteFromServer(indexPaths:indexPaths)
        }
        let cancelAction = UIAlertAction(title: "取消", style: .default)
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        
        present(alert, animated: true, completion: nil)
    }
    @objc func refreshContactList(){
        self.contactTV.refreshData()
    }
    func deleteFromServer(indexPaths:[IndexPath]){
        
        var IDs = [String]()
        for indexPath in indexPaths{
            let row = indexPath.row
            let item = contactTV.dataArr[row] as! MContactItem
            IDs.append(item.id)
        }
        
        view.makeToastActivity()
        
        HttpController.shared.deleteContact(userIDs: IDs, successBlock: { (json) in
            
            self.contactTV.refreshData()
            self.view.hideToastActivity()
            self.toastSuccess("删除成功")
            
        }, failure: { (err) in
            
            self.view.hideToastActivity()
            self.toastFail(err.localizedDescription)
        })
    }
    
    func showUserInfo(userID:String,name:String){
        
        let uiVC = self.loadViewControllerInMainStoryboard("MUserInfoVC") as! MUserInfoVC
        uiVC.userID = userID
        uiVC.userName = name
        self.hideNaviBackBtnTitle()
        self.navigationController?.pushViewController(uiVC, animated: true)
    }
}

extension MMineContactVC:MBaseTVDelegate{
    
    func loadData(pageIndex:Int){
        
        HttpController.shared.contactList(pageIndex: pageIndex, successBlock: { (json) in
            
            let jsonArr = json["result"]["data"].arrayValue
            var contactArr = [MContactItem]()
            
            for itemJSON in jsonArr{
                let item = self.parseItem(fromJSON: itemJSON)
                contactArr.append(item)
            }
            self.contactTV.appendData(contactArr)
            if pageIndex == 0 && contactArr.count == 0{
                self.manageBtn.isHidden = true
            }else{
                self.manageBtn.isHidden = false
            }
            
        }, failure: { (err) in
            
            self.contactTV.loadFailed()
            self.toastFail(err.localizedDescription)
        })
    }
    func parseItem(fromJSON data:JSON) -> MContactItem{
        
        let id = data["id"].stringValue
        let nickname = data["nickname"].stringValue
        let iconURL = data["icon"].stringValue
//        let createTime = data["createTime"].stringValue
        
        let item = MContactItem.init(id: id, name: nickname, icon: iconURL)
        
        return item
    }
    
    func nibsToRegister() -> [String]{
        return [MMineContactVCCellIdentifier]
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return contactTV.dataArr.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell = tableView.dequeueReusableCell(withIdentifier: MMineContactVCCellIdentifier) as! MMineContactVCCell
        let item = contactTV.dataArr[indexPath.row] as! MContactItem
        cell.setup(item.name, iconURL: item.icon)
        cell.tintColor = DefaultColorRed
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return MMineContactVCCellHeight
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView.isEditing{
            return
        }
        let item = contactTV.dataArr[indexPath.row] as! MContactItem
        tableView.deselectRow(at: indexPath, animated: true)
        self.showUserInfo(userID: item.id, name: item.name)
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        
        let value = UITableViewCellEditingStyle.delete.rawValue |
            UITableViewCellEditingStyle.insert.rawValue
        return UITableViewCellEditingStyle.init(rawValue: value)!
    }
}
