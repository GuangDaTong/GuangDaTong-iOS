

import UIKit

let MMineContactVCCellIdentifier = "MMineContactVCCell"
let MMineContactVCCellHeight:CGFloat = 64

class MMineContactVCCell: MBaseTVCell {

    @IBOutlet weak var iconIV: UIImageView!
    @IBOutlet weak var titleLB: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        iconIV.contentMode = .scaleAspectFill
        iconIV.clipsToBounds = true
        
        iconIV.setCornerRounded(radius: 4)
        addBottomSeparateLine()
    }
    func setup(_ title:String,iconURL:String){
        titleLB.text = title
        iconIV.sd_setImage(with: URL.init(string: iconURL), placeholderImage: DefaultUserIcon)
    }

}
