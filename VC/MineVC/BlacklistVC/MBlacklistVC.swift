

import UIKit

struct BlacklistModel{
    
    var id = ""
    var icon = ""
    var name = ""
    var time = ""
}

class MBlacklistVC: MBaseVC {

    var blacklistTV = MBaseTV.init(frame: CGRect.zero, style: UITableViewStyle.plain)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }
    
    // MARK: - UI
    func setupUI(){
        
        view.backgroundColor = UIColor.white
        self.title = "黑名单"
        
        blacklistTV.setup(withDelegate: self)
        view.addSubview(blacklistTV)
        blacklistTV.snp.makeConstraints { (m) in
            m.leading.equalTo(self.view.snp.leading)
            m.top.equalTo(self.view.snp.top)
            m.trailing.equalTo(self.view.snp.trailing)
            m.bottom.equalTo(self.view.snp.bottom)
        }
        
        blacklistTV.refreshData()
    }
    func deleteFromBlacklist(index:Int,userID:String){
        
        HttpController.shared.deleteFromBlacklist(userID: userID, successBlock: { (json) in
            
            MAskFilter.shared.removeFromBlacklist(userID: userID)
            self.toastSuccess("移除成功")
            NotiCenter.post(name: UserAlteredBlacklistNotification, object: nil)
            self.blacklistTV.refreshData()
        }) { (err) in
            self.blacklistTV.loadFailed()
            self.toastFail(err.localizedDescription)
        }
    }
}

extension MBlacklistVC:MBaseTVDelegate{
    
    func loadData(pageIndex:Int){
        
        if pageIndex > 0{
            
            blacklistTV.appendData(nil)
            return
        }
        
        HttpController.shared.blacklist(successBlock: { (json) in
            
            let jsonArr = json["result"]["data"].arrayValue
            var blackArr = [BlacklistModel]()
            
            for j in jsonArr{
                let id:String = j["id"].stringValue
                let icon:String = j["icon"].stringValue
                let name:String = j["nickname"].stringValue
                let time:NSString = j["createTime"].stringValue as NSString
                let t:String = time.timeShort() ?? "数据异常"
                let item:BlacklistModel = BlacklistModel.init(id: id, icon: icon, name: name, time: t)
                blackArr.append(item)
            }
            self.blacklistTV.appendData(blackArr)
            
        }, failure: { (err) in
            
            self.blacklistTV.loadFailed()
            self.toastFail(err.localizedDescription)
        })
    }
    func nibsToRegister() -> [String]{
        return [MBlacklistVCCellID]
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return blacklistTV.dataArr.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return MBlacklistVCCellHeight
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell = tableView.dequeueReusableCell(withIdentifier: MBlacklistVCCellID) as! MBlacklistVCCell
        cell.selectionStyle = .none
        
        let row = indexPath.row
        let item = blacklistTV.dataArr[row] as! BlacklistModel
        cell.set(name: item.name, icon: item.icon, time: item.time)
        return cell
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        
        return UITableViewCellEditingStyle.delete
    }
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let del = UITableViewRowAction(style: .destructive, title: "删除",handler:{(action,idxPath) in
            
            let row = indexPath.row
            let item = self.blacklistTV.dataArr[row] as! BlacklistModel
            
            self.showSystemStyleAlert(title: "提示", msg: "\n将\(item.name)从黑名单移除？", okBlock: {
                self.deleteFromBlacklist(index:row,userID: item.id)
            })
        })
        
        return [del]
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let row = indexPath.row
        let item = blacklistTV.dataArr[row] as! BlacklistModel
        
        let uiVC = self.loadViewControllerInMainStoryboard("MUserInfoVC") as! MUserInfoVC
        uiVC.userID = item.id
        uiVC.userName = item.name
        self.hideNaviBackBtnTitle()
        self.navigationController?.pushViewController(uiVC, animated: true)
    }
}
