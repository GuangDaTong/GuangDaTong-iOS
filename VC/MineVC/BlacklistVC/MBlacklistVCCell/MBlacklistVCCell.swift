

import UIKit

let MBlacklistVCCellID = "MBlacklistVCCell"
let MBlacklistVCCellHeight:CGFloat = 56

class MBlacklistVCCell: MBaseTVCell {

    @IBOutlet weak var iconIV: UIImageView!
    @IBOutlet weak var nameLB: UILabel!
    @IBOutlet weak var timeLB: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        iconIV.setCornerRounded(radius: 4)
        addBottomSeparateLine()
    }
    
    func set(name:String,icon:String,time:String){
        nameLB.text = name
        iconIV.sd_setImage(with: URL.init(string: icon), placeholderImage: DefaultImagePlaceholder)
        timeLB.text = "拉黑时间:\(time)"
    }
}
