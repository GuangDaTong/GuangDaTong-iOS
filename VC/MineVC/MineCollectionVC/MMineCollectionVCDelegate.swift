

import Foundation

extension MMineCollectionVC:UIScrollViewDelegate,MHorizontalTabViewDelegate{
    
    // MARK: - HorizontalTabView Delegate
    func horizontalTabViewDidSelect(_ index:Int){
        let floatIndex:CGFloat = CGFloat(index)
        let viewWidth:CGFloat = GetView_Width(view: self.view)
        let point:CGPoint = CGPoint.init(x: floatIndex * viewWidth, y: 0)
        self.scrollView.setContentOffset(point, animated: true)
        self.scrollView.flashScrollIndicators()
        
        for tv in self.tvArr{
            tv.isEditing = false
        }
        self.deleteBtn.setTitle("删除", for: .normal)
        
        var tv:MBaseTV! = nil
        switch index {
        case 0:
            self.curListType = MMineCollectionListType.ask
            tv = self.askTV
        case 1:
            self.curListType = MMineCollectionListType.guidance
            tv = self.guidanceTV
        case 2:
            self.curListType = MMineCollectionListType.summary
            tv = self.summaryTV
        case 3:
            self.curListType = MMineCollectionListType.club
            tv = self.clubTV
        case 4:
            self.curListType = MMineCollectionListType.look
            tv = self.lookTV
        case 5:
            self.curListType = MMineCollectionListType.partTime
            tv = self.partTimeTV
        case 6:
            self.curListType = MMineCollectionListType.training
            tv = self.trainingTV
        case 7:
            self.curListType = MMineCollectionListType.activity
            tv = self.activityTV
        default:
            break
        }
        if tv.dataArr.count == 0{
            self.deleteBtn.isHidden = true
            tv.refreshData()
        }else{
            self.deleteBtn.isHidden = false
        }
    }
    // MARK: - UIScrollView Delegate
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        if scrollView == self.scrollView {
            let index = Int(targetContentOffset.pointee.x / GetView_Width(view: self.view) + 0.1)
            if horizontalTab.selectedIndex == index{
                return
            }
            horizontalTab.selectIndex(index)
        }
    }
}
