

import UIKit

enum MMineCollectionListType {
    case ask
    case guidance
    case summary
    case club
    case look
    case partTime
    case training
    case activity
}

class MMineCollectionVC: MBaseVC {

    // MARK: - Property
    var curListType = MMineCollectionListType.ask
    var tvArr:[MBaseTV]! = nil
    
    // MARK: - Views
    let deleteBtn = UIButton.init()
    var horizontalTab:MHorizontalTabView! = nil
    var tabView:MHorizontalTabView{
        get{
            if horizontalTab != nil {
                return horizontalTab
            }
            horizontalTab = MHorizontalTabView()
            horizontalTab.delegate = self
            horizontalTab.setup(withTitles: ["社区","特别推荐",
                                             "图文咨询","最近更新","本月热点",
                                             "面试","学习","职场"])
            return horizontalTab
        }
        set{
            horizontalTab = newValue
        }
    }
    var scrollView = UIScrollView.init(frame: CGRect.zero)
    // ask
    var askTV: MAskTV! = MAskTV.init(frame: CGRect.zero, style: UITableViewStyle.plain)
    var manager:MAskManager! = nil
    // school
    var guidanceTV = MSchoolTV.init(frame: CGRect.zero, style: UITableViewStyle.plain)
    var summaryTV = MSchoolTV.init(frame: CGRect.zero, style: UITableViewStyle.plain)
    var clubTV = MSchoolTV.init(frame: CGRect.zero, style: UITableViewStyle.plain)
    var lookTV = MSchoolTV.init(frame: CGRect.zero, style: UITableViewStyle.plain)
    // discovery
    var partTimeTV = MDiscoveryPartTimeTV.init(frame: CGRect.zero, style: UITableViewStyle.plain)
    var trainingTV = MDiscoveryTrainingTV.init(frame: CGRect.zero, style: UITableViewStyle.plain)
    var activityTV = MDiscoveryActivityTV.init(frame: CGRect.zero, style: UITableViewStyle.plain)
    
    // MARK: - VC Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "我的收藏"
        self.view.backgroundColor = UIColor.white
        setupNaviBar()
        setupTabView()
        setupScrollView()
    }
    
    // MARK: - Setup
    func setupNaviBar(){
        
        deleteBtn.isHidden = true
        deleteBtn.setTitle("删除", for: .normal)
        deleteBtn.sizeToFit()
        deleteBtn.addTarget(self, action: #selector(deleteCollection), for: .touchUpInside)
        let msgItem = UIBarButtonItem.init(customView: deleteBtn)
        self.navigationItem.rightBarButtonItem = msgItem
    }
    func setupTabView(){
        
        self.view.addSubview(tabView)
        tabView.snp.makeConstraints { (make) in
            make.leading.equalTo(view.snp.leading)
            make.trailing.equalTo(view.snp.trailing)
            make.top.equalTo(view.snp.top)
            make.height.equalTo(tabView.defaultHeight)
        }
    }
    func setupScrollView(){
        
        scrollView.contentSize = CGSize.init(width: GetView_Width(view: self.view) * 8, height: 0)
        scrollView.delegate = self
        scrollView.isPagingEnabled = true
        
        self.view.addSubview(scrollView)
        scrollView.snp.makeConstraints { (make) in
            make.leading.equalTo(view.snp.leading)
            make.trailing.equalTo(view.snp.trailing)
            make.top.equalTo(horizontalTab.snp.bottom).offset(4)
            make.bottom.equalTo(view.snp.bottom)
        }
        setupAskTV()
        setupSchoolTV()
        setupDiscoveryTV()
        tvArr = [askTV,guidanceTV,summaryTV,clubTV,lookTV,partTimeTV,trainingTV,activityTV]
        let tvLoadFinishBlock:((MBaseTV,Int) -> Void) = { tv,pageIndex in
            self.tvLoadFinished(tv:tv)
        }
        for tv in tvArr{
            tv.loadFinishBlock = tvLoadFinishBlock
        }
    }
    func setupAskTV(){
        
        manager = MAskManager.init(withOwnerVC: self, tableView: askTV)
        
        askTV.setup(withLoadType: .collectList) { [weak self] (cellIndex,touchType) in
            DLOG("cellIndex:\(cellIndex),touchType:\(touchType)")
            self?.manager.processCellTouch(withIndex: cellIndex, touchType: touchType)
        }
        
        scrollView.addSubview(askTV)
        askTV.snp.makeConstraints { (m) in
            m.top.equalTo(scrollView.snp.top)
            m.leading.equalTo(scrollView.snp.leading)
            m.width.equalTo(SCREEN_WIDTH)
            m.height.equalTo(scrollView.snp.height)
        }
    }
    func setupSchoolTV(){
        
        guidanceTV.isLoadForCollectList = true
        summaryTV.isLoadForCollectList = true
        clubTV.isLoadForCollectList = true
        lookTV.isLoadForCollectList = true
        
        scrollView.addSubview(guidanceTV)
        scrollView.addSubview(summaryTV)
        scrollView.addSubview(clubTV)
        scrollView.addSubview(lookTV)
        
        guidanceTV.listType = .guidance
        guidanceTV.cellPressedBlock = { [weak self] (index) in
            
            self?.showWebVC(listType: .guidance, index: index, bottomBarType: .zan,
                            navigationItemType: .menuWithFeedback, zanType: .guidance)
        }
        guidanceTV.separatorStyle = .none
        guidanceTV.snp.makeConstraints { (make) in
            make.leading.equalTo(askTV.snp.trailing)
            make.top.equalTo(scrollView.snp.top)
            make.width.equalTo(SCREEN_WIDTH)
            make.height.equalTo(scrollView.snp.height)
        }
        
        summaryTV.listType = .summary
        summaryTV.cellPressedBlock = { [weak self] (index) in
            
            self?.showWebVC(listType: .summary, index: index, bottomBarType: .zan,
                            navigationItemType: .menuWithFeedback, zanType: .summary)
        }
        summaryTV.separatorStyle = .none
        summaryTV.snp.makeConstraints { (make) in
            make.leading.equalTo(guidanceTV.snp.trailing)
            make.top.equalTo(scrollView.snp.top)
            make.width.equalTo(SCREEN_WIDTH)
            make.height.equalTo(scrollView.snp.height)
        }
        
        clubTV.listType = .club
        clubTV.cellPressedBlock = { [weak self] (index) in
            
            self?.showWebVC(listType: .club, index: index, bottomBarType: .zan,
                            navigationItemType: .menuWithFeedback, zanType: .club)
        }
        clubTV.separatorStyle = .none
        clubTV.snp.makeConstraints { (make) in
            make.leading.equalTo(summaryTV.snp.trailing)
            make.top.equalTo(scrollView.snp.top)
            make.width.equalTo(SCREEN_WIDTH)
            make.height.equalTo(scrollView.snp.height)
        }
        
        lookTV.listType = .look
        lookTV.cellPressedBlock = { [weak self] (index) in
            
            self?.showWebVC(listType: .look, index: index, bottomBarType: .zan,
                            navigationItemType: .menuWithFeedback, zanType: .look)
        }
        lookTV.separatorStyle = .none
        lookTV.snp.makeConstraints { (make) in
            make.leading.equalTo(clubTV.snp.trailing)
            make.top.equalTo(scrollView.snp.top)
            make.width.equalTo(SCREEN_WIDTH)
            make.height.equalTo(scrollView.snp.height)
        }
    }
    
    func setupDiscoveryTV(){
        
        partTimeTV.isLoadForCollectList = true
        trainingTV.isLoadForCollectList = true
        activityTV.isLoadForCollectList = true
        
        scrollView.addSubview(partTimeTV)
        scrollView.addSubview(trainingTV)
        scrollView.addSubview(activityTV)
        
        partTimeTV.cellPressedBlock = { [weak self] (index) in
            
            self?.showWebVC(listType: .partTime, index: index, bottomBarType: .signUp,
                            navigationItemType: .menu, zanType: .partTime)
        }
        partTimeTV.separatorStyle = .none
        partTimeTV.snp.makeConstraints { (make) in
            make.leading.equalTo(lookTV.snp.trailing)
            make.top.equalTo(scrollView.snp.top)
            make.width.equalTo(view.snp.width)
            make.height.equalTo(scrollView.snp.height)
        }
        
        trainingTV.cellPressedBlock = { [weak self] (index) in
            
            self?.showWebVC(listType: .training, index: index, bottomBarType: .signUp,
                            navigationItemType: .menu, zanType: .training)
        }
        trainingTV.separatorStyle = .none
        trainingTV.snp.makeConstraints { (make) in
            make.leading.equalTo(partTimeTV.snp.trailing)
            make.top.equalTo(scrollView.snp.top)
            make.width.equalTo(view.snp.width)
            make.height.equalTo(scrollView.snp.height)
        }
        
        activityTV.cellPressedBlock = { [weak self] (index) in
            
            self?.showWebVC(listType: .activity, index: index, bottomBarType: .signUp,
                            navigationItemType: .menu, zanType: .activity)
        }
        activityTV.separatorStyle = .none
        activityTV.snp.makeConstraints { (make) in
            make.leading.equalTo(trainingTV.snp.trailing)
            make.top.equalTo(scrollView.snp.top)
            make.width.equalTo(view.snp.width)
            make.height.equalTo(scrollView.snp.height)
        }
    }
    
}
