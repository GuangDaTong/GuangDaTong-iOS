

import Foundation

extension MMineCollectionVC{
    
    func tvLoadFinished(tv finishLoadTV:MBaseTV){
        
        var tv:MBaseTV! = nil
        switch curListType {
        case .ask:
            tv = askTV
        case .guidance:
            tv = guidanceTV
        case .summary:
            tv = summaryTV
        case .club:
            tv = clubTV
        case .look:
            tv = lookTV
        case .partTime:
            tv = partTimeTV
        case .training:
            tv = trainingTV
        case .activity:
            tv = activityTV
        }
        if finishLoadTV != tv{
            return
        }
        if finishLoadTV.dataArr.count == 0{
            deleteBtn.isHidden = true
        }else{
            deleteBtn.isHidden = false
        }
    }
    @objc func deleteCollection(){
        
        var tv:MBaseTV! = nil
        
        switch curListType {
        case .ask:
            tv = askTV
        case .guidance:
            tv = guidanceTV
        case .summary:
            tv = summaryTV
        case .club:
            tv = clubTV
        case .look:
            tv = lookTV
        case .partTime:
            tv = partTimeTV
        case .training:
            tv = trainingTV
        case .activity:
            tv = activityTV
        }
        
        if tv.isEditing{
            deleteBtn.setTitle("删除", for: .normal)
            processDeleteIn(tv: tv,listType: curListType)
        }else{
            deleteBtn.setTitle("完成", for: .normal)
        }
        
        tv.isEditing = !tv.isEditing
        tv.reloadData()
    }
    func processDeleteIn(tv:MBaseTV,listType:MMineCollectionListType){
        
        let indexPaths = tv.indexPathsForSelectedRows
        if indexPaths == nil{
            return
        }
        
        let count = indexPaths!.count
        let alert = UIAlertController(title: "提示", message: "\n确认删除\(count)个收藏？", preferredStyle: .alert)
        
        let okAction = UIAlertAction.init(title: "是的", style: .destructive) { (action) in
            
            self.deleteFromServer(tv: tv, indexPaths: indexPaths!,listType:listType)
        }
        let cancelAction = UIAlertAction(title: "取消", style: .default, handler: nil)
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        
        present(alert, animated: true, completion: nil)
    }
    func deleteFromServer(tv:MBaseTV,indexPaths:[IndexPath],listType:MMineCollectionListType){
        
        view.makeToastActivity()
        
        var deleteIDs = [String]()
        
        switch listType {
        case .ask:
            for indexPath in indexPaths{
                let row = indexPath.section
                let item = tv.dataArr[row] as! AskListItem
                deleteIDs.append(item.favorID)
            }
        case .guidance:
            fallthrough
        case .summary:
            fallthrough
        case .club:
            fallthrough
        case .look:
            for indexPath in indexPaths{
                let row = indexPath.row
                let item = tv.dataArr[row] as! SchoolListItem
                deleteIDs.append(item.favorID)
            }
        case .partTime:
            fallthrough
        case .training:
            fallthrough
        case .activity:
            for indexPath in indexPaths{
                let row = indexPath.row
                let item = tv.dataArr[row] as! DiscoveryListItem
                deleteIDs.append(item.favorID)
            }
        }
        
        HttpController.shared.deleteCollect(type: listType, IDs: deleteIDs, successBlock: { (json) in
            
            tv.refreshData()
            self.view.hideToastActivity()
            self.toastSuccess("删除成功")
        }, failure: { (err) in
            
            self.view.hideToastActivity()
            self.toastFail(err.localizedDescription)
        })
    }
    func showWebVC(listType:SchoolListType,index:Int,bottomBarType:WebVCBottomBarType,navigationItemType:WebVCRightNavigationItemType,zanType:ZanType){
        
        var tableView:MSchoolTV! = nil
        switch listType {
        case .guidance:
            tableView = guidanceTV
        case .summary:
            tableView = summaryTV
        case .club:
            tableView = clubTV
        case .look:
            tableView = lookTV
        }
        
        var item = tableView.dataArr[index] as! SchoolListItem
        guard item.id != "" else{
            self.toastFail("该内容已被删除")
            return
        }
        
        let webVC = self.loadViewControllerInMainStoryboard("MWebVC") as! MWebVC
        let url = item.link + "?id=\(item.id)"
        webVC.setup(title: item.title, url: url, bottomBarType: bottomBarType, rightNavigationItemType: navigationItemType, zanType: zanType,itemID: item.id,isCollect: item.isCollect,isZan: item.isZan,readCount: item.readCount,zanCount: item.zanCount)
        webVC.zanBlock = {
            item.zanCount += 1
            item.isZan = true
            tableView.dataArr[index] = item
        }
        webVC.collectBlock = {
            item.isCollect = true
            tableView.dataArr[index] = item
        }
        webVC.readBlock = {
            item.readCount += 1
            tableView.dataArr[index] = item
        }
        webVC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(webVC, animated: true)
    }
    func showWebVC(listType:DiscoveryListType,index:Int,bottomBarType:WebVCBottomBarType,navigationItemType:WebVCRightNavigationItemType,zanType:ZanType){
        
        var tableView:MDiscoveryTV! = nil
        switch listType {
        case .partTime:
            tableView = partTimeTV
        case .training:
            tableView = trainingTV
        case .activity:
            tableView = activityTV
        }
        var item = tableView.dataArr[index] as! DiscoveryListItem
        guard item.id != "" else{
            self.toastFail("该内容已被删除")
            return
        }
        
        let webVC = self.loadViewControllerInMainStoryboard("MWebVC") as! MWebVC
        let url = item.link + "?id=\(item.id)"
        webVC.setup(title: item.title, url: url, bottomBarType: bottomBarType, rightNavigationItemType: navigationItemType, zanType: zanType,itemID: item.id,isCollect: item.isCollect,isZan: item.isZan,readCount: item.readCount,zanCount: item.zanCount)
        webVC.zanBlock = {
            item.zanCount += 1
            item.isZan = true
            tableView.dataArr[index] = item
        }
        webVC.collectBlock = {
            item.isCollect = true
            tableView.dataArr[index] = item
        }
        webVC.readBlock = {
            item.readCount += 1
            tableView.dataArr[index] = item
        }
        webVC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(webVC, animated: true)
    }
}
