

import UIKit

class MMineVC: MBaseVC {

    @IBOutlet weak var mineTV: BaseTV!
    var mineNavigator:MMineNavigator = MMineNavigator()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "我的"
        mineNavigator.setupWith(ownerVC: self)
        mineTV.separatorStyle = .none
        mineTV.register(UINib.init(nibName: TVUniversalCellIdentifier, bundle: nil), forCellReuseIdentifier: TVUniversalCellIdentifier)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if APPDELEGATE.forceTouchLaunchType == .contact{
            APPDELEGATE.forceTouchLaunchType = .none
            mineNavigator.showVC(type: .contact)
        }
    }
}

extension MMineVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: TVUniversalCellIdentifier) as! TVUniversalCell
        cell.titleLBWidth.constant = 150
        cell.selectionStyle = .none
        
        let row = indexPath.row
        switch row {
        case 0:
            cell.setup(withType: .icon, title: "通讯录", icon: UIImage.init(named: "mine_contact"))
        case 1:
            cell.setup(withType: .icon, title: "金币收益记录", icon: UIImage.init(named: "mine_icon_record"))
        case 2:
            cell.setup(withType: .icon, title: "我的帖子", icon: UIImage.init(named: "mine_ask"))
        case 3:
            cell.setup(withType: .icon, title: "我的收藏", icon: UIImage.init(named: "mine_collection"))
        case 4:
            cell.setup(withType: .icon, title: "我的消息", icon: UIImage.init(named: "mine_msg"))
        case 5:
            cell.setup(withType: .icon, title: "黑名单", icon: UIImage.init(named: "put_into_blacklist"))
        default:
            break
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.row {
        case 0:
            mineNavigator.showVC(type: .contact)
        case 1:
            mineNavigator.showVC(type: .accountBook)
        case 2:
            mineNavigator.showVC(type: .ask)
        case 3:
            mineNavigator.showVC(type: .collection)
        case 4:
            mineNavigator.showVC(type: .msg)
        case 5:
            mineNavigator.showVC(type: .blacklist)
        default:
            break
        }
    }
}
