

import UIKit

class MMineAskVC: MBaseVC {

    var askTV: MAskTV! = nil
    var manager:MAskManager! = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "我的问问"
        self.view.backgroundColor = UIColor.white
        setupTV()
    }
    
    func setupTV(){
        
        askTV = MAskTV.init(frame: CGRect.zero, style: UITableViewStyle.plain)
        manager = MAskManager.init(withOwnerVC: self, tableView: askTV)
        
        askTV.setup(withLoadType: .mineAskList) { (cellIndex,touchType) in
            DLOG("cellIndex:\(cellIndex),touchType:\(touchType)")
            self.manager.processCellTouch(withIndex: cellIndex, touchType: touchType)
        }
        
        self.view.addSubview(askTV)
        askTV.snp.makeConstraints { (m) in
            m.top.equalTo(self.view.snp.top)
            m.leading.equalTo(self.view.snp.leading)
            m.bottom.equalTo(self.view.snp.bottom)
            m.trailing.equalTo(self.view.snp.trailing)
        }
    }

}
