

import Foundation
import SwiftyJSON

extension MMineAccountBookVC:MBaseTVDelegate{
    
    // MARK: - TV Datasoure & Delegate
    func loadData(pageIndex:Int){
        
        HttpController.shared.accountBook(pageIndex: pageIndex, successBlock: { (json) in
            
            let jsonArr = json["result"]["data"].arrayValue
            var contactArr = [MAccountBookItem]()
            
            for itemJSON in jsonArr{
                let item = self.parseItem(fromJSON: itemJSON)
                contactArr.append(item)
            }
            self.accountTV.appendData(contactArr)
            
        }, failure: { (err) in
            
            self.accountTV.loadFailed()
            self.toastFail(err.localizedDescription)
        })
    }
    func parseItem(fromJSON data:JSON) -> MAccountBookItem{
        
        let coins = data["amount"].stringValue
        let event = data["description"].stringValue
        let time = data["createTime"].stringValue
        
        let shortTime = (time as NSString).timeShort() ?? ""
        
        let item = MAccountBookItem.init(coins: coins, event: event, time: shortTime)
        
        return item
    }
    func nibsToRegister() -> [String]{
        return [MMineAccountBookVCCellIdentifier]
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return accountTV.dataArr.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: MMineAccountBookVCCellIdentifier) as! MMineAccountBookVCCell
        cell.selectionStyle = .none
        let item = accountTV.dataArr[indexPath.row] as! MAccountBookItem
        
        cell.set(coins: item.coins, event: item.event, time: item.time)
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
