

import UIKit

class MAccountBookItem {
    
    var coins:String
    var event:String
    var time:String
    
    init(coins:String,event:String,time:String) {
        self.coins = coins
        self.event = event
        self.time = time
    }
}

class MMineAccountBookVC: MBaseVC {

    // MARK: - View
    let accountTV = MBaseTV.init(frame: CGRect.zero, style: UITableViewStyle.plain)
    
    // MARK: - VC Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "金币收益记录"
        self.view.backgroundColor = UIColor.white
        setupUI()
    }
    
    // MARK: - Setup
    func setupUI(){
        
        setupNaviBar()
        setupTV()
    }
    func setupNaviBar(){
        
        let helpBtn = UIButton.init()
        helpBtn.setImage(UIImage.init(named: "pushSetting_help"), for: .normal)
        helpBtn.sizeToFit()
        helpBtn.addTarget(self, action: #selector(showHelpVC), for: .touchUpInside)
        let msgItem = UIBarButtonItem.init(customView: helpBtn)
        self.navigationItem.rightBarButtonItem = msgItem
    }
    func setupTV(){
        
        accountTV.setup(withDelegate: self)
        accountTV.separatorStyle = .none
        self.view.addSubview(accountTV)
        accountTV.snp.makeConstraints { (m) in
            m.leading.equalTo(view.snp.leading)
            m.top.equalTo(view.snp.top)
            m.trailing.equalTo(view.snp.trailing)
            m.bottom.equalTo(view.snp.bottom)
        }
        accountTV.refreshData()
    }
    @objc func showHelpVC(){
        
        let webVC = self.loadViewControllerInMainStoryboard("MWebVC") as! MWebVC
        webVC.setup(title: "关于金币收益", url: BaseURL + "/help/coin")
        webVC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(webVC, animated: true)
    }

}
