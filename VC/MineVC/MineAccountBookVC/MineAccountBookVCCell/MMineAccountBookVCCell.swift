

import UIKit

let MMineAccountBookVCCellIdentifier = "MMineAccountBookVCCell"

class MMineAccountBookVCCell: MBaseTVCell {

    @IBOutlet weak var coinsLB: UILabel!
    @IBOutlet weak var eventLB: UILabel!
    @IBOutlet weak var timeLB: UILabel!
    
    let plusColor = DefaultColorGreen
    let minusColor = DefaultColorRed
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        addBottomSeparateLine()
    }
    func set(coins:String,event:String,time:String){
        
        self.coinsLB.text = coins
        if coins.hasPrefix("-"){
            coinsLB.textColor = minusColor
        }else{
            coinsLB.textColor = plusColor
        }
        
        self.eventLB.text = event
        self.timeLB.text = time
    }

}
