

import UIKit

enum MMineNavigateType {
    
    /// 通讯录
    case contact
    /// 喵币收益记录
    case accountBook
    /// 我的问问
    case ask
    /// 我的收藏
    case collection
    /// 我的消息
    case msg
    
    case blacklist
}

class MMineNavigator: NSObject {

    weak var ownerVC:UIViewController! = nil
    func setupWith(ownerVC vc:UIViewController){
        
        ownerVC = vc
    }
    
    func showVC(type:MMineNavigateType){
        
        switch type {
        case .contact:
            showContactVC()
        case .accountBook:
            showAccountBookVC()
        case .ask:
            showAskVC()
        case .collection:
            showCollectionVC()
        case .msg:
            showMsgVC()
        case .blacklist:
            showBlacklist()
        }
    }
    func showContactVC(){
        
        let vc = MMineContactVC()
        ownerVC.navigationController?.pushViewController(vc, animated: true)
    }
    func showAccountBookVC(){
        
        let vc = MMineAccountBookVC()
        ownerVC.navigationController?.pushViewController(vc, animated: true)
    }
    func showAskVC(){
        
        let vc = MMineAskVC()
        ownerVC.navigationController?.pushViewController(vc, animated: true)
    }
    func showCollectionVC(){
        
        let vc = MMineCollectionVC()
        ownerVC.navigationController?.pushViewController(vc, animated: true)
    }
    func showMsgVC(){
        
        let msgVC = ownerVC.loadViewControllerInMainStoryboard("MMsgCenterVC") as! MMsgCenterVC
        msgVC.hidesBottomBarWhenPushed = true
        ownerVC.navigationController?.pushViewController(msgVC, animated: true)
    }
    func showBlacklist(){
        
        let vc = MBlacklistVC()
        ownerVC.navigationController?.pushViewController(vc, animated: true)
    }
}
