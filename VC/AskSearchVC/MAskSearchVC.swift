

import UIKit

class MAskSearchVC: MBaseVC {

    // MARK: - Properties
    let historyFilePath:String = NSHomeDirectory() + "/Documents/searchHistoryArr.plist"
    var searchHistoryArr = [String]()
    let MAskSearchVCCellIdentifier = "MAskSearchVCCell"
    var cellSelectBlock:((Int) -> Void)?
    
    var searchRange:AskRangeType! = nil
    var askType:AskType! = nil
    var askTag:AskTag! = nil
    
    // MARK: - Views
    let searchTV = BaseTV.init(frame: CGRect.zero, style: .grouped)
    var searchBtn = UIButton.init(frame: CGRect.zero)
    
    var _searchBar:UISearchBar! = nil
    var searchBar:UISearchBar{
        get{
            if _searchBar != nil{
                return _searchBar
            }
            _searchBar = UISearchBar.init(frame: CGRect.zero)
            _searchBar.alpha = 0.7
            _searchBar.tintColor = UIColor.lightGray //光标颜色
            _searchBar.backgroundColor = UIColor.clear //背景色
            _searchBar.placeholder = "搜索你感兴趣的问题"
            _searchBar.delegate = self
            _searchBar.enablesReturnKeyAutomatically = true
            
            for subView in self.searchBar.subviews{
                for sSubView in subView.subviews{
                    // 移除背景，防止出现黑线
                    if sSubView.isKind(of: NSClassFromString("UISearchBarBackground")!){
                        sSubView.removeFromSuperview()
                    }
                    // 设置字体颜色
                    if sSubView.isKind(of: UITextField.self){
                        let tf = sSubView as! UITextField
                        tf.textColor = UIColor.darkGray
                    }
                }
            }
            return _searchBar
        }
        set{
            _searchBar = newValue
        }
    }
    
    let searchView = MAskSearchView.loadFromNib()
    let historyClearView = MSearchHistoryHeaderView.loadFromNib()
    
    // MARK: - VC Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNaviBar()
        setupTV()
        setupSearchView()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        searchBar.becomeFirstResponder()
    }
    override func viewWillDisappear(_ animated: Bool) {
        
        searchBar.resignFirstResponder()
        super.viewWillDisappear(animated)
    }
    
    // MARK: - Setup
    func setupNaviBar(){
        
        let frame = CGRect.init(x: 0, y: 0, width: SCREEN_WIDTH - 100, height: 36)
        let containerView = UIView.init(frame: frame)
        containerView.addSubview(self.searchBar)
        self.searchBar.frame = CGRect.init(x: -16, y: 0, width: SCREEN_WIDTH - 100, height: 36)
        self.searchBar.transform = CGAffineTransform.init(scaleX: 0.9, y: 0.9)
        self.navigationItem.titleView = containerView
        
        self.searchBtn.setTitle("搜索", for: .normal)
        self.searchBtn.setTitleColor(UIColor.white, for: .normal)
        self.searchBtn.addTarget(self, action: #selector(searchBtnPressed), for: .touchUpInside)
        let searchItem = UIBarButtonItem.init(customView: self.searchBtn)
        self.navigationItem.rightBarButtonItem = searchItem
        self.searchBtn.sizeToFit()
    }
    func setupTV(){
        
        searchTV.backgroundColor = UIColor.groupTableViewBackground
        searchTV.delegate = self
        searchTV.dataSource = self
        searchTV.register(UITableViewCell.self, forCellReuseIdentifier: MAskSearchVCCellIdentifier)
        
        view.addSubview(searchTV)
        searchTV.snp.makeConstraints { (m) in
            m.leading.equalTo(view.snp.leading)
            m.trailing.equalTo(view.snp.trailing)
            m.top.equalTo(view.snp.top)
            m.bottom.equalTo(view.snp.bottom)
        }
        historyClearView.clearHistoryBlock = {
            self.alertForClearHistory()
        }
        cellSelectBlock = { row in
            
            self.searchBar.text = self.searchHistoryArr[row]
            self.searchBtnPressed()
        }
        loadSearchHistoryFromFile()
    }
    func setupSearchView(){
        
        searchTV.tableHeaderView = self.searchView
        searchView.snp.makeConstraints { (m) in
            m.width.equalTo(SCREEN_WIDTH)
            m.height.equalTo(MAskSearchView.DefaultHeight)
        }
        
        searchView.askTypeClearBlock = {
            self.askType = nil
        }
        searchView.askTagClearBlock = {
            self.askTag = nil
        }
        searchView.askTypeSelectBlock = { type in
            self.askType = type
//            DLOG(type.rawValue)
        }
        searchView.askTagSelectBlock = { tag in
            self.askTag = tag
//            DLOG(tag.rawValue)
        }
    }
    func alertForClearHistory(){
        
        let alert = UIAlertController(title: "提示", message: "\n清空搜索记录？", preferredStyle: .alert)
        
        let okAction = UIAlertAction.init(title: "确定", style: .destructive) { (action) in
            self.searchHistoryArr.removeAll()
            self.searchTV.reloadData()
            self.saveHistoryArr()
        }
        let cancelAction = UIAlertAction(title: "取消", style: .default, handler: nil)
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        
        present(alert, animated: true, completion: nil)
    }
}

// MARK: - Touch Events
extension MAskSearchVC{
    
    @objc func searchBtnPressed(){
        
        self.searchBar.resignFirstResponder()
        
        if self.searchBar.text!.count == 0 &&
            self.askType == nil &&
            self.askTag == nil{
            self.toastFail("尚未设定搜索内容")
            return
        }
        
        showAskSearchListVC(withText: self.searchBar.text!)
        
        if self.searchBar.text!.count == 0{
            return
        }
        var index = 0
        for history in searchHistoryArr{
            if history == self.searchBar.text{
                searchHistoryArr.remove(at: index)
            }
            index += 1
        }
        searchHistoryArr.insert(self.searchBar.text!, at: 0)
        self.searchTV.reloadData()
        self.searchBar.text = ""
        saveHistoryArr()
    }
    func showAskSearchListVC(withText text:String){
        
        let slVC = loadViewControllerInMainStoryboard("MAskSearchListVC") as! MAskSearchListVC
        slVC.setup(withRange:searchRange,text: text, type: askType, tag: askTag)
        hideNaviBackBtnTitle()
        self.navigationController?.pushViewController(slVC, animated: true)
    }
}

// MARK: - Delegates & Datasources
extension MAskSearchVC:UISearchBarDelegate{
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText.count > DefaultSearchTextMaxLength{
            searchBar.text = String(searchText.characters.prefix(DefaultSearchTextMaxLength))
        }
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        searchBtnPressed()
    }
}

