

import Foundation

extension MAskSearchVC:UITableViewDelegate,UITableViewDataSource{
    
    // MARK: - TV Datasoure & Delegate
    func loadSearchHistoryFromFile(){
        
        let historyArr = NSKeyedUnarchiver.unarchiveObject(withFile: historyFilePath) as? [String]
        if historyArr != nil{
            searchHistoryArr = historyArr!
            self.searchTV.reloadData()
        }
    }
    func saveHistoryArr(){
        
        DispatchQueue.global().async {
            
            let historyArr = self.searchHistoryArr
            let manager = FileManager.default
            if !manager.fileExists(atPath: self.historyFilePath){
                let created = manager.createFile(atPath: self.historyFilePath, contents: nil, attributes: nil)
                if created{
                    NSKeyedArchiver.archiveRootObject(historyArr, toFile: self.historyFilePath)
                    return
                }else{
                    DLOG("*** 创建搜索历史文件目录失败！***")
                    return
                }
            }
            let success = NSKeyedArchiver.archiveRootObject(historyArr, toFile: self.historyFilePath)
            if !success{
                DLOG("*** 保存搜索历史文件失败！***")
            }
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchHistoryArr.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let row = indexPath.row
        let cell = tableView.dequeueReusableCell(withIdentifier: MAskSearchVCCellIdentifier)!
        cell.textLabel?.text = searchHistoryArr[row]
        cell.accessoryType = .disclosureIndicator
        cell.separatorInset = UIEdgeInsetsMake(0, 16, 0, -16)
        cell.backgroundColor = tableView.backgroundColor
        cell.textLabel?.textColor = UIColor.lightGray
        cell.textLabel?.font = UIFont.systemFont(ofSize: 14)
        cell.textLabel?.numberOfLines = 2

        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        
        let text = searchHistoryArr[indexPath.row] as NSString
        var height = text.getSizeWith(UIFont.systemFont(ofSize: 14), constrainedTo: CGSize.init(width: SCREEN_WIDTH - 48, height: CGFloat.greatestFiniteMagnitude)).height + 8
        if height < 44 {
            height = 44
        }
        
        return height
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if searchHistoryArr.count > 0{
            return self.historyClearView
        }
        return nil
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if searchHistoryArr.count > 0{
            return MSearchHistoryHeaderView.DefaultHeight
        }
        return 0.1
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
        cellSelectBlock?(indexPath.row)
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.searchBar.resignFirstResponder()
    }
}
