

import UIKit

class MAskSearchListVC: MBaseVC {

    private var searchRange:AskRangeType! = nil
    private var searchText = ""
    private var askType:AskType?
    private var askTag:AskTag?
    
    var askTV: MAskTV! = nil
    var manager:MAskManager! = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "搜索结果"
        setupTV()
    }
    
    func setup(withRange range:AskRangeType,text:String,type:AskType?,tag:AskTag?){
        
        self.searchRange = range
        self.searchText = text
        self.askType = type
        self.askTag = tag
    }
    func setupTV(){
        
        askTV = MAskTV.init(frame: CGRect.zero, style: UITableViewStyle.plain)
        manager = MAskManager.init(withOwnerVC: self, tableView: askTV)
        
        askTV.setup(withLoadType: .searchList) { [weak self] (cellIndex,touchType) in
            DLOG("cellIndex:\(cellIndex),touchType:\(touchType)")
            self?.manager.processCellTouch(withIndex: cellIndex, touchType: touchType)
        }
        askTV.setupAskSearch(range: searchRange,text: searchText, type: askType, tag: askTag)
        
        self.view.addSubview(askTV)
        askTV.snp.makeConstraints { (m) in
            m.top.equalTo(self.view.snp.top)
            m.leading.equalTo(self.view.snp.leading)
            m.bottom.equalTo(self.view.snp.bottom)
            m.trailing.equalTo(self.view.snp.trailing)
        }
    }
}
