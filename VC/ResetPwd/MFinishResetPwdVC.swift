

import UIKit

class MFinishResetPwdVC: MBaseVC,UITextFieldDelegate {

    // Views
    @IBOutlet weak var oldPwdTF: UITextField!
    @IBOutlet weak var oldPwdSepLine: UIView!
    @IBOutlet weak var pwdTF: UITextField!
    @IBOutlet weak var pwdAgainTF: UITextField!
    @IBOutlet weak var finishBtn: UIButton!
    
    // Property
    var isAlterPwd = false
    var phoneStr:String! = nil
    var verifyToken = ""
    
    // Constraint
    @IBOutlet weak var oldPwdHeight: NSLayoutConstraint!
    @IBOutlet weak var oldPwdSepLineHeight: NSLayoutConstraint!
    @IBOutlet weak var pwdSepLineHeight: NSLayoutConstraint!
    @IBOutlet weak var pwdAgainSepLineHeight: NSLayoutConstraint!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if isAlterPwd{
            self.title = "修改密码"
            self.finishBtn.setTitle("确认", for: .normal)
            oldPwdTF.delegate = self
            oldPwdTF.setPlaceholderTextToLightGray()
        }else{
            oldPwdHeight.constant = 0
            oldPwdSepLine.isHidden = true
        }
        
        oldPwdSepLineHeight.setToOnePixel()
        pwdSepLineHeight.setToOnePixel()
        pwdAgainSepLineHeight.setToOnePixel()
        
        finishBtn.isEnabled = false
        
        NotiCenter.addObserver(self, selector: #selector(checkInputStatus), name: NSNotification.Name.UITextFieldTextDidChange, object: nil)
        
        pwdTF.delegate = self
        pwdTF.setPlaceholderTextToLightGray()
        pwdAgainTF.delegate = self
        pwdAgainTF.setPlaceholderTextToLightGray()
    }
    override func viewDidAppear(_ animated: Bool) {
        
        if isAlterPwd{
            oldPwdTF.becomeFirstResponder()
        }else{
            pwdTF.becomeFirstResponder()
        }
    }
    @objc func checkInputStatus(){
        
        guard pwdAgainTF.text!.count > 0 && pwdTF.text!.count > 0 else {
            
            finishBtn.isEnabled = false
            return
        }
        if isAlterPwd && oldPwdTF.text!.count == 0{
            finishBtn.isEnabled = false
            return
        }
        finishBtn.isEnabled = true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == oldPwdTF{
            view.endEditing(true)
            pwdTF.becomeFirstResponder()
        }else if textField == pwdTF{
            view.endEditing(true)
            pwdAgainTF.becomeFirstResponder()
        }else{
            view.endEditing(true)
        }
        return true
    }
    
    @IBAction func finishBtnPressed(_ sender: Any) {
        
        self.view.endEditing(true)
        
        if isAlterPwd{
            alterPwd()
        }else{
            resetPwd()
        }
    }
    func alterPwd(){
        
        let userPwd = uDefaults.string(forKey: "UserPassword")
        if oldPwdTF.text != userPwd{
            self.toastFail("旧密码不正确！")
            return
        }
        guard pwdTF.text == pwdAgainTF.text else {
            self.toastFail("两次输入的密码不匹配！")
            return
        }
        finishBtn.isEnabled = false
        view.makeToastActivity()
        
        HttpController.shared.alterPwd(newPwd: pwdTF.text!,oldPwd: oldPwdTF.text!, successBlock: { (json) in
            
            self.view.hideToastActivity()
            uDefaults.set(self.pwdTF.text, forKey: "UserPassword")
            uDefaults.synchronize()
            self.toastSuccess("修改成功！")
            delay(second: 0.5, block: {
                self.navigationController?.popViewController(animated: true)
            })
            
        }, failure: { (err) in
            
            self.view.hideToastActivity()
            self.finishBtn.isEnabled = true
            self.toastFail(err.localizedDescription)
        })
    }
    func resetPwd(){
        
        guard pwdTF.text == pwdAgainTF.text else {
            self.toastFail("两次输入的密码不匹配！")
            return
        }
        
        finishBtn.isEnabled = false
        view.makeToastActivity()
        
        HttpController.shared.resetPwd(account: phoneStr, pwd: pwdTF.text!, token: verifyToken, successBlock: { (json) in
            
            self.view.hideToastActivity()
            uDefaults.set(self.pwdTF.text, forKey: "UserPassword")
            uDefaults.synchronize()
            self.toastSuccess("修改成功！")
            delay(second: 0.5, block: {
                self.navigationController?.popToRootViewController(animated: true)
            })
            
            }, failure: { (err) in
                
                self.view.hideToastActivity()
                self.finishBtn.isEnabled = true
                self.toastFail(err.localizedDescription)
        })
    }
    @IBAction func viewDidTouchDown(_ sender: Any) {
        self.view.endEditing(true)
    }
    
}
