

import UIKit

class MResetPwdVC: MBaseVC {

    // MARK: - Views
    @IBOutlet weak var phoneTF: UITextField!
    @IBOutlet weak var codeTF: UITextField!
    @IBOutlet weak var getCodeBtn: UIButton!
    @IBOutlet weak var nextBtn: UIButton!
    
    // MARK: - Constraints
    @IBOutlet weak var phoneSepLineHeight: NSLayoutConstraint!
    @IBOutlet weak var codeSepLineHeight: NSLayoutConstraint!
    
    // MARK: - Property
    var timer:Timer! = nil
    var second:Int = 0
    var phone = ""
    
    // MARK: - VC Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
        if phone.count > 0 {
            phoneTF.text = phone
            getCodeBtn.isEnabled = true
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        APP.setStatusBarHidden(false, with: .none)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    deinit {
        killTimer()
    }
    
    // MARK: - setup
    func setup(){
        
        getCodeBtn.isEnabled = false
        
        //FIXME: <FICOW> uncomment
        nextBtn.isEnabled = false
        
        phoneSepLineHeight.setToOnePixel()
        codeSepLineHeight.setToOnePixel()
        
        NotiCenter.addObserver(self, selector: #selector(checkLoginBtnStatus), name: NSNotification.Name.UITextFieldTextDidChange, object: nil)
        
        phoneTF.setPlaceholderTextToLightGray()
        codeTF.setPlaceholderTextToLightGray()
    }
    @objc func checkLoginBtnStatus(){
        
        guard phoneTF.text?.count == 11 else {
            getCodeBtn.isEnabled = false
            nextBtn.isEnabled = false
            return
        }
        if timer == nil {
            getCodeBtn.isEnabled = true
        }
        guard codeTF.text?.count == 4 else {
            nextBtn.isEnabled = false
            return
        }
        nextBtn.isEnabled = true
    }
    func setupTimer(){
        
        killTimer()
        timer = Timer.init(timeInterval: 1, target: self, selector: #selector(countTime), userInfo: nil, repeats: true)
        second = VerifyCodeGetDuration // 5s后可以再次获取验证码
        RunLoop.current.add(timer, forMode: .commonModes)
    }
    func killTimer(){
        if timer != nil {
            timer.invalidate()
            timer = nil
        }
    }
    @objc func countTime(){
        
        second -= 1
        getCodeBtn.setTitle("\(second)s", for: .normal)
        if second == 0 {
            killTimer()
            resetGetCodeBtn()
        }
    }
    func resetGetCodeBtn(){
        getCodeBtn.isEnabled = true
        getCodeBtn.setTitle("获取短信验证码", for: .normal)
    }
    
    // MARK: - Touch Events
    @IBAction func getCodeBtnPressed(_ sender: Any) {
        
        view.endEditing(true)
        getCodeBtn.isEnabled = false
        self.view.makeToastActivity()
        
        HttpController.shared.isAccountExist(phoneTF.text!, success: { (json) in
            
            let isExist = json["result"]["isExist"].boolValue
            guard isExist else{
                self.view.hideToastActivity()
                self.getCodeBtn.isEnabled = true
                self.toastFail("帐号不存在")
                return
            }
            
            self.getVerifyCode()
            
            }, failure: { (err) in
                
                self.view.hideToastActivity()
                self.resetGetCodeBtn()
                self.toastFail(err.localizedDescription)
        })
    }
    func getVerifyCode(){
        
        DLOG(">>> 即将向手机号：\(phoneTF.text!) 发送短信验证码！")
        setupTimer()
        SMSSDK.getVerificationCode(by: SMSGetCodeMethod.init(0), phoneNumber: phoneTF.text!, zone: "86") { (err) in
            
            self.view.hideToastActivity()
            guard err == nil else{
                // 错误码说明
                // http://wiki.mob.com/%E9%94%99%E8%AF%AF%E7%A0%81%E8%AF%B4%E6%98%8E/
                self.toastFail(err!.localizedDescription)
                self.killTimer()
                return
            }
            self.showAlert("发送成功，请注意查收短信!")
        }
    }
    @IBAction func nextBtnPressed(_ sender: Any) {
        
        view.endEditing(true)
        self.nextBtn.isEnabled = false
        self.view.makeToastActivity()
        HttpController.shared.submitVerifyCode(phoneTF.text!, code: codeTF.text!, success: { (json) in
            
            self.view.hideToastActivity()
            self.nextBtn.isEnabled = true
            let verifyToken = json["result"]["verifyToken"].stringValue
            self.showCompleteInfoVC(token: verifyToken)
            }, failure: { (err) in
                
                self.view.hideToastActivity()
                self.nextBtn.isEnabled = true
                self.toastFail(err.localizedDescription)
        })
    }
    @IBAction func viewDidTouchDown(_ sender: Any) {
        view.endEditing(true)
    }
    func showCompleteInfoVC(token:String){
        
        let frVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MFinishResetPwdVC") as! MFinishResetPwdVC
        frVC.phoneStr = phoneTF.text!
        frVC.verifyToken = token
        self.navigationController?.pushViewController(frVC, animated: true)
    }
}
