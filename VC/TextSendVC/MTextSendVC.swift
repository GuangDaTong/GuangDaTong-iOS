

import UIKit
import SwiftyJSON

enum TextSendOperationType {
    case leaveMsg
    case informAsk
    case informComment
    case informUser
    case contactSystem
    case reportError
}

class MTextSendVC: MBaseVC,UIScrollViewDelegate {

    fileprivate var operationType = TextSendOperationType.leaveMsg
    fileprivate var operationID = ""
    fileprivate var operationName = ""
    fileprivate var zanType = ZanType.ask
    
    fileprivate var inputText = ""
    fileprivate var inputV = MInputView.loadFromNib()
    fileprivate let naviBtn = UIButton.init()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        inputV.textView.becomeFirstResponder()
    }
    override func viewWillDisappear(_ animated: Bool) {
        
        view.endEditing(true)
    }
    
    func setupWith(operationType type:TextSendOperationType,ID:String,name:String="",zanType:ZanType = .ask){
        
        operationType = type
        operationID = ID
        self.zanType = zanType
        self.operationName = name
    }
    func setupUI(){
        
        self.view.backgroundColor = UIColor.groupTableViewBackground
        
        switch operationType {
        case .leaveMsg:
            self.title = "给TA留言"
            setupRightNaviBtnTitle("发送")
            inputV.placeholder = "对TA说点什么吧 ~"
        case .informAsk:
            self.title = "举报帖子"
            setupRightNaviBtnTitle("完成")
            inputV.placeholder = "亲，您需要简述一下举报理由喔！"
        case .informComment:
            self.title = "举报评论"
            setupRightNaviBtnTitle("完成")
            inputV.placeholder = "亲，您需要简述一下举报理由喔！"
        case .informUser:
            self.title = "举报TA"
            setupRightNaviBtnTitle("完成")
            inputV.placeholder = "亲，您需要简述一下举报理由喔！"
        case .contactSystem:
            self.title = "我要咨询"
            setupRightNaviBtnTitle("发送")
            inputV.placeholder = "亲，请简单描述一下您想问的问题吧 ~"
        case .reportError:
            self.title = "反馈问题"
            setupRightNaviBtnTitle("完成")
            inputV.placeholder = "亲，请简单描述一下您发现的问题吧 ~"
        }
        naviBtn.isHidden = true
        
        view.addSubview(inputV)
        inputV.snp.makeConstraints { (m) in
            m.top.equalTo(view.snp.top).offset(16)
            m.leading.equalTo(view.snp.leading)
            m.trailing.equalTo(view.snp.trailing)
            
            var height = MInputView.DefaultHeight
            height = (SCREEN_HEIGHT < 667) ? height/2.0 : height
            m.height.equalTo(height)
        }
        inputV.textChangedBlock = { [weak self] (text) in
            
            self?.inputText = text
            if text.count > 0 {
                self?.naviBtn.isHidden = false
            }else{
                self?.naviBtn.isHidden = true
            }
        }
    }
    func setupRightNaviBtnTitle(_ name:String){
        
        naviBtn.setTitle(name, for: .normal)
        naviBtn.sizeToFit()
        naviBtn.addTarget(self, action: #selector(sendText), for: .touchUpInside)
        let msgItem = UIBarButtonItem.init(customView: naviBtn)
        self.navigationItem.rightBarButtonItem = msgItem
    }
    
    @objc func sendText(){
        
        view.endEditing(true)
        naviBtn.isEnabled = false
        
        switch operationType {
        case .leaveMsg:
            leaveMsg(inputText)
        case .informAsk:
            inform(withType: .ask)
        case .informUser:
            inform(withType: .user)
        case .informComment:
            inform(withType: .comment)
        case .contactSystem:
            contactSystem(inputText)
        case .reportError:
            reportError(inputText)
        }
    }
    func inform(withType type:InformType){
        
        view.makeToastActivity()
        
        HttpController.shared.inform(type:type,ID:operationID, reason: inputText, successBlock: { (json) in
            
            self.view.hideToastActivity()
            self.toastSuccess("举报成功")
            delay(second: 0.5, block: {
                self.popViewController()
            })
            
            }, failure: { (err) in
                
                self.operationFailed(err)
        })
    }
    func leaveMsg(_ msg:String){
        
        view.makeToastActivity()
        
        HttpController.shared.leaveMsg(userID: operationID, msg: msg, successBlock: { (json) in
            
            let r = json["result"]
            let dict = ["id":r["id"].stringValue,
                        "createTime":r["createTime"].stringValue,
                        "nickname":uUserName,
                        "icon":uUserIcon,
                        "gender":uDefaults.string(forKey: "UserGender") ?? "",
                        "senderID":uUserID,
                        "words":"发送给 \(self.operationName)：\n\(msg)"]
            let msgJSON = JSON.init(dict)
            var msgItem = MMsgLoader.parseLeaveMsgItem(fromJSON: msgJSON)
            msgItem.isUnread = false
            MMsgLoader.appendLeaveMsg(msgItem)
            
            self.view.hideToastActivity()
            self.toastSuccess("发送成功")
            
            delay(second: 0.5, block: {
                self.popViewController()
            })
            
            }, failure: { (err) in
                
                self.operationFailed(err)
        })
    }
    func contactSystem(_ msg:String){
        
        view.makeToastActivity()
        
        HttpController.shared.contactSystem(type: zanType, description: msg, objID: operationID, successBlock: { (json) in
            
            self.view.hideToastActivity()
            self.toastSuccess("发送成功")
            delay(second: 0.5, block: {
                self.popViewController()
            })
        }, failure: { (err) in
            
            self.operationFailed(err)
        })
    }
    func reportError(_ msg:String){
        
        view.makeToastActivity()
        
        HttpController.shared.reportError(type: zanType, description: msg, objID: operationID, successBlock: { (json) in
            
            self.view.hideToastActivity()
            self.toastSuccess("感谢您的热心反馈 ~")
            delay(second: 0.5, block: {
                self.popViewController()
            })
        }, failure: { (err) in
            
            self.operationFailed(err)
        })
    }
    
    func operationFailed(_ err:Error){
        
        self.naviBtn.isEnabled = true
        self.view.hideToastActivity()
        self.toastFail(err.localizedDescription)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        view.endEditing(true)
    }
    @IBAction func viewTouchDown(_ sender: Any) {
        
        view.endEditing(true)
    }
}

