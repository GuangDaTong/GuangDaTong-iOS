

import UIKit
import SDWebImage

class MSelfInfoVC: MBaseVC {

    @IBOutlet weak var selfTV: BaseTV!
    let iconSettingView = MUserIconSettingView.loadFromNib()
    let saveBtn = UIButton.init()
    
    var _imagePickerVC:ImagePickerVC! = nil
    var imagePickerVC:ImagePickerVC{
        get{
            if _imagePickerVC != nil{
                return _imagePickerVC
            }
            _imagePickerVC = ImagePickerVC()
            _imagePickerVC.setup(withPresent: self)
            _imagePickerVC.imagePickerController.allowsEditing = true
            _imagePickerVC.filePickedBlock = { fileInfo in
                self.parseFileInfo(fileInfo)
            }
            return _imagePickerVC
        }
        set{
            _imagePickerVC = newValue
        }
    }
    
    lazy var listVC:MSelectionListVC = self.loadSelectionListVC()
    // MARK: - Property
    let UserIconURLString = "LocalUserIconURL"
    var userName = ""
    var userCollege = ""
    var userGender = ""
    var userPhone = ""
    var userGrade = ""
    var instituteArr = [String]()
    var genderArr = ["请选择","男","女"]
    var gradeArr = [String]()
    var isAnythingChanged = false
    var selectedIcon:UIImage?
    var loginType:LoginType = LoginType.phone
    
    // MARK: - VC Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "个人信息"
        
        if let loginType = uDefaults.string(forKey: "UserLoginType"){
            let type:LoginType = LoginType(rawValue: loginType) ?? LoginType.phone
            self.loginType = type
        }
        
        setupNaviBar()
        setupTV()
    }
    func setupNaviBar(){
        
        saveBtn.setTitle("保存", for: .normal)
        saveBtn.sizeToFit()
        saveBtn.addTarget(self, action: #selector(saveUserInfo), for: .touchUpInside)
        let saveItem = UIBarButtonItem.init(customView: saveBtn)
        self.navigationItem.rightBarButtonItem = saveItem
    }
    func setupTV(){
        
        userName = uUserName
        userCollege = uDefaults.string(forKey: "UserCollege") ?? ""
        userGender = uDefaults.string(forKey: "UserGender") ?? ""
        if userGender.count == 0{
            userGender = "请选择"
        }
        userPhone = uDefaults.string(forKey: "UserAccount") ?? ""
        userGrade = uDefaults.string(forKey: "UserGrade") ?? ""
        
        selfTV.contentInset = UIEdgeInsetsMake(16, 0, 0, 0)
        selfTV.separatorStyle = .none
        selfTV.register(UINib.init(nibName: TVUniversalCellIdentifier, bundle: nil), forCellReuseIdentifier: TVUniversalCellIdentifier)
        
        let userIconURL = uUserIcon
        if userIconURL.count != 0{
            iconSettingView.setIconURL(userIconURL)
        }else{
            iconSettingView.setIconImage(DefaultUserIcon)
        }
        iconSettingView.iconPressedBlock = {
            
            if let image = self.iconSettingView.iconIV.image{
                showImageBrowser(currentIndex: 0, images: [image])
            }
        }
        iconSettingView.viewPressedBlock = {
            
            self.changeIcon()
        }
    }
    func setIcon(_ img:UIImage){
        
        DispatchQueue.global().async {
            self.isAnythingChanged = true
            self.selectedIcon = img.zipped()
            DispatchQueue.main.async{
                self.iconSettingView.setIconImage(self.selectedIcon!)
            }
        }
    }
    @objc func saveUserInfo(){
        
        if !isAnythingChanged{
            self.toastFail("您并未修改任何内容")
            return
        }
        
        self.saveBtn.isEnabled = false
        
        view.makeToastActivity()
        
        let gender = (userGender == "请选择" ? "" : userGender)
        HttpController.shared.alterSelfInfo(nickname: userName, college: userCollege, gender: gender, grade: userGrade, custom: "", iconImage: self.selectedIcon, successBlock: { (json) in
            
            let iconURL = json["result"]["iconURL"].stringValue
            if iconURL.count != 0{
                SDWebImageManager.shared().saveImage(toCache: self.selectedIcon, for: URL.init(string: iconURL))
            }
            
            uDefaults.set(self.userName, forKey: "UserName")
            uDefaults.set(iconURL, forKey: "UserIcon")
            uDefaults.set(self.userCollege, forKey: "UserCollege")
            uDefaults.set(gender, forKey: "UserGender")
            uDefaults.set(self.userGrade, forKey: "UserGrade")
            uDefaults.synchronize()
            
            self.view.hideToastActivity()
            self.toastSuccess("保存成功")
            
            delay(second: 0.5, block: {
                NotiCenter.post(name: UserDidModifyInfoNotification, object: nil)
                self.popViewController()
            })
            
        }, failure: { (err) in
            
            self.saveBtn.isEnabled = true
            self.view.hideToastActivity()
            self.toastFail(err.localizedDescription)
        })
    }
    func parseFileInfo(_ fileInfo:[String : Any]?){
        
        let mediaType:String = fileInfo?[UIImagePickerControllerMediaType] as! String
        if mediaType == (kUTTypeImage as NSString) as String {
            
            var image:UIImage? = fileInfo?[UIImagePickerControllerEditedImage] as? UIImage
            if (image == nil) {
                image = fileInfo?[UIImagePickerControllerOriginalImage] as? UIImage
            }
            let data:Data = UIImageJPEGRepresentation(image!, DefaultImageCompressionRate)!
            let img:UIImage = UIImage.init(data: data)!
            DLOG("*** Image Data.length:\(data.count) ***")
            
            DispatchQueue.main.async{
                self.setIcon(img)
            }
        }
    }
    func loadSelectionListVC() -> MSelectionListVC{
        return self.loadViewControllerInMainStoryboard("MSelectionListVC") as! MSelectionListVC
    }
}

extension MSelfInfoVC{
    
    func changeIcon(){
        
        let sheet = UIAlertController.init(title: "", message: "更改头像", preferredStyle: .actionSheet)
        let albumAction = UIAlertAction.init(title: "从相册选取", style: .default) { [weak self] (action) in
            self?.view.endEditing(true)
            self?.imagePickerVC.selectImageFromAlbum()
        }
        sheet.addAction(albumAction)
        let cancelAction = UIAlertAction.init(title: "取消", style: .cancel, handler: nil)
        sheet.addAction(cancelAction)
        
        if sheet.popoverPresentationController != nil{
            let cell = self.selfTV.cellForRow(at: IndexPath.init(row: 0, section: 1))!
            sheet.popoverPresentationController?.sourceView = cell
            var bounds = cell.bounds
            bounds.size.height /= 5.0
            sheet.popoverPresentationController?.sourceRect = bounds
        }
        
        self.present(sheet, animated: true, completion: nil)
    }
    func changeName(){
        
        let inputVC = ShortTextInputVC()
        inputVC.title = "更改昵称"
        inputVC.oldText = userName
        inputVC.textInputFinishBlock = { text in
            self.userName = text
            self.selfTV.reloadData()
        }
        self.navigationController?.pushViewController(inputVC, animated: true)
    }
    func changeInstitute(){
        
        if self.instituteArr.count != 0{
            showInstituteListVC()
            return
        }
        
        view.makeToastActivity()
        HttpController.shared.instituteList(success: {  (json) in
            
            self.view.hideToastActivity()
            let arr = json["result"]["colleges"].array
            self.instituteArr = [String]()
            for institute in arr! {
                self.instituteArr.append(institute.stringValue)
            }
            
            self.showInstituteListVC()
            
            }, failure: { (err) in
                self.view.hideToastActivity()
                self.toastFail(err.localizedDescription)
        })
    }
    func showInstituteListVC(){
        
        listVC.title = "请选择学院"
        listVC.selectedStr = userCollege
        listVC.list = self.instituteArr
        listVC.selectedBlock = { (title) in
            self.userCollege = title
            self.selfTV.reloadData()
        }
        
        self.navigationController?.pushViewController(listVC, animated: true)
    }
    func changeGender(){
        
        listVC.title = "请选择性别"
        listVC.selectedStr = userGender
        listVC.list = self.genderArr
        listVC.selectedBlock = { title in
            self.userGender = title
            self.selfTV.reloadData()
        }
        self.navigationController?.pushViewController(listVC, animated: true)
    }
    func changeGrade(){
        
        if gradeArr.count != 0{
            showGradeListVC()
            return
        }
        DispatchQueue.global().async {
            
            let format = DateFormatter.init()
            format.timeZone = NSTimeZone.system
            format.dateFormat = "yyyy"
            let currentYear = (format.string(from: Date()) as NSString).integerValue
            if currentYear >= DefaultSmallestGrade{
                
                self.gradeArr.append("其他")
                for year in (DefaultSmallestGrade...currentYear).reversed(){
                    self.gradeArr.append("\(year)级")
                }
            }else{
                self.gradeArr = ["其他"]
            }
            DispatchQueue.main.async{
                self.showGradeListVC()
            }
        }
    }
    func showGradeListVC(){
        
        listVC.title = "请选择年级"
        listVC.selectedStr = userGrade
        listVC.list = self.gradeArr
        listVC.selectedBlock = { title in
            self.userGrade = title
            self.selfTV.reloadData()
        }
        self.navigationController?.pushViewController(listVC, animated: true)
    }
}

extension MSelfInfoVC:UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            return 0
        }
        return 5
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: TVUniversalCellIdentifier) as! TVUniversalCell
        cell.selectionStyle = .none
        
        let row = indexPath.row
        switch row {
        case 0:
            cell.setup(withType: .subtitle, title: "昵称", icon: nil, subtitle: userName, isSelect: false)
        case 1:
            cell.setup(withType: .subtitleWithDisclosure, title: "学院", icon: nil, subtitle: userCollege, isSelect: false)
        case 2:
            cell.setup(withType: .subtitleWithDisclosure, title: "性别", icon: nil, subtitle: userGender, isSelect: false)
        case 3:
            switch self.loginType{
            case .phone:
                cell.setup(withType: .subtitle, title: "手机号", icon: nil, subtitle: userPhone, isSelect: false)
            case .qq:
                cell.setup(withType: .subtitle, title: "登录方式", icon: nil, subtitle: "QQ登录", isSelect: false)
            case .wechat:
                cell.setup(withType: .subtitle, title: "登录方式", icon: nil, subtitle: "微信登录", isSelect: false)
            }
            
        case 4:
            cell.setup(withType: .subtitleWithDisclosure, title: "年级", icon: nil, subtitle: userGrade, isSelect: false)
//        case 5:
//            cell.setup(withType: .disclosureIndicator, title: "更多信息", icon: nil, subtitle: "", isSelect: false)
        default:
            break
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let row = indexPath.row
        switch row {
        case 0:
            changeName()
        case 1:
            changeInstitute()
        case 2:
            changeGender()
        case 3:
            return
        case 4:
            changeGrade()
        default:
            break
        }
        isAnythingChanged = true
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0{
            return self.iconSettingView
        }
        return nil
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0{
            return MUserIconSettingView.DefaultHeight
        }
        return 0.1
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 16
    }
}
