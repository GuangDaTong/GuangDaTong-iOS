

import UIKit


/// 通用TableViewCell类的类型
///
/// - onlyTitle: Title Label
/// - disclosureIndicator: Title、DisclosureIndicator
/// - icon: Icon、Title、DisclosureIndicator
/// - subtitle: Title、Subtitle
/// - subtitleWithDisclosure: Title、Subtitle、DisclosureIndicator
enum TVUniversalCellType {
    case onlyTitle
    case disclosureIndicator
    case icon
    case subtitle
    case subtitleWithDisclosure
    case select
}

let TVUniversalCellIdentifier = "TVUniversalCell"

/// 通用TableViewCell类
class TVUniversalCell: MBaseTVCell {
    
    
    @IBOutlet weak var iconIV: UIImageView!
    @IBOutlet weak var titleLB: UILabel!
    @IBOutlet weak var subtitleLB: UILabel!
    @IBOutlet weak var disclosureIndicatorIV: UIImageView!
    @IBOutlet weak var selectIV: UIImageView!
    
    static let DefaultHeight:CGFloat = 56
    let TitleLBDefaultLeading:CGFloat = 52
    let SubtitleLBDefaultTrailing:CGFloat = 24
    
    @IBOutlet weak var titleLBLeading: NSLayoutConstraint!
    @IBOutlet weak var subtitleLBTrailing: NSLayoutConstraint!
    @IBOutlet weak var titleLBWidth: NSLayoutConstraint!
    @IBOutlet weak var sepLineHeight: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        sepLineHeight.constant = 1 / UIScreen.main.scale
    }
    
    func setup(withType type:TVUniversalCellType,title:String,icon:UIImage?,subtitle:String = "",isSelect:Bool = false){
        
        titleLB.text = title
        iconIV.isHidden = true
        subtitleLB.isHidden = true
        titleLBLeading.constant = 16
        subtitleLBTrailing.constant = SubtitleLBDefaultTrailing
        selectIV.isHidden = true
        disclosureIndicatorIV.isHidden = false
        
        switch type {
        case .onlyTitle:
            disclosureIndicatorIV.isHidden = true
        case .disclosureIndicator:
            break
        case .icon:
            titleLBLeading.constant = TitleLBDefaultLeading
            iconIV.image = icon
            iconIV.isHidden = false
        case .subtitle:
            subtitleLB.text = subtitle
            subtitleLB.isHidden = false
            disclosureIndicatorIV.isHidden = true
            subtitleLBTrailing.constant = 8
        case .subtitleWithDisclosure:
            subtitleLB.text = subtitle
            subtitleLB.isHidden = false
            selectIV.isHidden = true
        case .select:
            disclosureIndicatorIV.isHidden = true
            if isSelect{
                selectIV.isHidden = false
            }
        }
    }
}
