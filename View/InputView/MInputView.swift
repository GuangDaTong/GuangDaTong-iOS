

import UIKit

class MInputView: UIView,UITextViewDelegate {
    
    static let DefaultHeight:CGFloat = 192
    fileprivate var _placeholder = ""
    var placeholder:String{
        get{
            return _placeholder
        }
        set{
            _placeholder = newValue
            textView.setPlaceholder(text: _placeholder)
        }
    }
    
    var textChangedBlock:((String) -> Void)?
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var textCountLB: UILabel!
    
    class func loadFromNib() -> MInputView{
        
        let view = Bundle.main.loadNibNamed("MInputView", owner: nil, options: nil)?.last as! MInputView
        return view
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        
        textViewDidChange(textView)
        textView.delegate = self
        textView.contentInset = UIEdgeInsetsMake(0, 0, 25, 0)
        
        textCountLB.textColor = AppThemeColor
    }
    
    internal func textViewDidChange(_ textView: UITextView) {
        
        var count = textView.text.count
        if count > DefaultMaxSendTextLength {
            let text = String(textView.text.characters.prefix(DefaultMaxSendTextLength))
            textView.text = text
            count = DefaultMaxSendTextLength
            setTextCount(count)
            textCountLB.textColor = UIColor.lightGray
            return
        }
        setTextCount(count)
        textCountLB.textColor = AppThemeColor
    }
    fileprivate func setTextCount(_ count:Int){
        
        let text = "\(count)/\(DefaultMaxSendTextLength)"
        textCountLB.text = text
        
        textChangedBlock?(textView.text)
    }
    
    
}
