

import UIKit

class MSearchHistoryHeaderView: UIView {

    static let DefaultHeight:CGFloat = 44
    
    var clearHistoryBlock:(() -> Void)?
    
    @IBOutlet weak var clearBtn: UIButton!
    
    class func loadFromNib() -> MSearchHistoryHeaderView{
        
        let view = Bundle.main.loadNibNamed("MSearchHistoryHeaderView", owner: nil, options: nil)?.last as! MSearchHistoryHeaderView
        
        return view
    }
    
    @IBAction func clearBtnPressed(_ sender: UIButton) {
        clearHistoryBlock?()
    }

}
