
import UIKit

class MUserIconSettingView: UIView {

    var iconPressedBlock:(() -> Void)?
    var viewPressedBlock:(() -> Void)?

    static let DefaultHeight:CGFloat = 90
    @IBOutlet weak var iconIV: UIImageView!
    
    class func loadFromNib() -> MUserIconSettingView{
        
        let view = Bundle.main.loadNibNamed("MUserIconSettingView", owner: nil, options: nil)?.last as! MUserIconSettingView
        return view
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        
        iconIV.setCornerRounded(radius: 4)
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(iconTapped))
        iconIV.addGestureRecognizer(tap)
    }
    func setIconURL(_ url:String){
        iconIV.sd_setImage(with: URL.init(string: url), placeholderImage: DefaultUserIcon)
    }
    func setIconImage(_ image:UIImage){
        
        iconIV.image = image
    }
    @objc func iconTapped(){
        
        iconPressedBlock?()
    }
    @IBAction func viewBtnPressed(_ sender: Any) {
        
        viewPressedBlock?()
    }
}
