

import UIKit
import SnapKit

enum MAlertType {
    case notice
    case withOK
    case withOKCancel
}

class MAlertView: UIView {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var grayCoverView: UIView!
    @IBOutlet weak var msgLB: UILabel!
    @IBOutlet weak var okBtn: UIButton!
    @IBOutlet weak var middleOKBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var msgLBCenter: NSLayoutConstraint!
    @IBOutlet weak var containerHeight: NSLayoutConstraint!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var separatorHeight: NSLayoutConstraint!
    @IBOutlet weak var mewIconTop: NSLayoutConstraint!
    
    let MsgLBSingleLineHeight:CGFloat = 17
    var okBlock:(() -> Void)?
    var cancelBlock:(() -> Void)?
    var type:MAlertType! = nil
    static let lessHeight:CGFloat = 200.0
    static let normalHeight:CGFloat = 240.0
    
    class func loadViewWithType(type:MAlertType,msg:String) -> MAlertView{
        
        let view = Bundle.main.loadNibNamed("MAlertView", owner: nil, options: nil)?.last as! MAlertView
        
        let msgStr:NSString = msg as NSString
        let msgSize:CGSize = msgStr.getSizeWith(UIFont.systemFont(ofSize: 14), constrainedTo: CGSize.init(width: 212.0, height: CGFloat.greatestFiniteMagnitude))
        
        if msgSize.height > view.MsgLBSingleLineHeight * 2{
            view.textView.text = msg
            view.textView.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.25).cgColor
            view.textView.layer.borderWidth = 0.5
            view.textView.layer.cornerRadius = 4
            view.msgLB.isHidden = true
        }else{
            view.msgLB.text = msg
            view.textView.isHidden = true
        }
        
        view.type = type
        switch type {
        case .notice:
            view.okBtn.isHidden = true
            view.cancelBtn.isHidden = true
            view.middleOKBtn.isHidden = true
            view.msgLBCenter.constant = 12
            view.containerHeight.constant = lessHeight
            view.separatorHeight.constant = 0
        case .withOK:
            view.okBtn.isHidden = true
            view.cancelBtn.isHidden = true
            view.containerHeight.constant = normalHeight
            view.separatorHeight.setToOnePixel()
        case .withOKCancel:
            view.middleOKBtn.isHidden = true
            view.containerHeight.constant = normalHeight
            view.separatorHeight.setToOnePixel()
        }
        view.mewIconTop.adjustHeightFrom6s()
        
        return view
    }
    func reverseBtnColor(){
        
        let red = self.okBtn.currentTitleColor
        let blue = self.cancelBtn.currentTitleColor
        self.cancelBtn.setTitleColor(red, for: .normal)
        self.okBtn.setTitleColor(blue, for: .normal)
    }
    func show(){
        
        _ = UIResponder.firstResponder()?.resignFirstResponder()
        self.setToHide()
        let win:UIWindow = WIN
        win.addSubview(self)
        self.snp.makeConstraints { (make) in
            make.top.equalTo(win.snp.top)
            make.leading.equalTo(win.snp.leading)
            make.trailing.equalTo(win.snp.trailing)
            make.bottom.equalTo(win.snp.bottom)
        }
        playShowAnimation()
        
        if self.textView.isHidden == false{
            delay(second: 0.4) {
                self.textView.flashScrollIndicators()
            }
        }
        
        guard self.type == MAlertType.notice else {
            return
        }
        delay(second: 2, block: {
            self.hide()
        })
    }
    func playShowAnimation(){
        
        UIView.animate(withDuration: 0.2) {
            self.containerView.transform = CGAffineTransform.identity
            self.grayCoverView.alpha = 0.6
            self.containerView.alpha = 1
        }
    }
    func hide(){
        
        UIView.animate(withDuration: 0.2, animations: {
            self.setToHide()
        }, completion: { (finish) in
            self.removeFromSuperview()
        })
    }
    func setToHide(){
        
        self.grayCoverView.alpha = 0
        self.containerView.alpha = 0
        self.containerView.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
    }
    @IBAction func okBtnPressed(_ sender: Any) {
        
        hide()
        if okBlock != nil {
            okBlock?()
        }
    }
    @IBAction func cancelBtnPressed(_ sender: Any) {
        
        hide()
        if cancelBlock != nil {
            cancelBlock?()
        }
    }
    @IBAction func middleOKBtnPressed(_ sender: Any) {
        
        hide()
        if okBlock != nil {
            okBlock?()
        }
    }
    

}
