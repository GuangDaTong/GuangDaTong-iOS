

import UIKit

class MIntroView: UIView {

    var collectionView:UICollectionView! = nil
    var imagesArray:[UIImage] = [UIImage]()
    var isHidding:Bool = false
    
    /// 针对iOS10出现的导航栏、状态栏重叠问题，暂时先采用tab切换自动调整布局的方式解决
    var hideBlock:(() -> Void)?
    
    override func didMoveToSuperview() {
        
        if let _ = superview{
            setup()
        }
    }
    func setup(){
        
        self.frame = WIN.bounds
        loadImages()
        setupCV()
        
        APP.setStatusBarHidden(true, with: .none)
    }
    func setupCV(){
        
        let layout:UICollectionViewFlowLayout = UICollectionViewFlowLayout.init()
        layout.itemSize = CGSize.init(width: SCREEN_WIDTH, height: SCREEN_HEIGHT)
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 0
        
        collectionView = UICollectionView.init(frame: .zero, collectionViewLayout: layout)
        collectionView.backgroundColor = UIColor.colorWithHex(value: 0x4990E2, alpha: 1)
        collectionView.register(UINib.init(nibName: MIntroViewCellID, bundle: nil), forCellWithReuseIdentifier: MIntroViewCellID)
        collectionView.isPagingEnabled = true
        collectionView.delegate = self
        collectionView.dataSource = self
        
        self.addSubview(collectionView)
        collectionView.snp.makeConstraints { (m) in
            m.leading.equalTo(self.snp.leading)
            m.top.equalTo(self.snp.top)
            m.trailing.equalTo(self.snp.trailing)
            m.bottom.equalTo(self.snp.bottom)
        }
    }
    func loadImages(){
        
        imagesArray.append(UIImage.init(named: "2 intro school")!)
        imagesArray.append(UIImage.init(named: "3 intro discovery 1")!)
        imagesArray.append(UIImage.init(named: "4 intro discovery 2")!)
        imagesArray.append(UIImage.init(named: "5 intro discovery 3")!)
        imagesArray.append(UIImage.init(named: "6 intro ask 1")!)
        imagesArray.append(UIImage.init(named: "7 intro ask 2")!)
        imagesArray.append(UIImage.init(named: "8 intro ask 3")!)
        imagesArray.append(UIImage.init(named: "9 intro Mew Coin")!)
    }
}

extension MIntroView:UICollectionViewDelegate,UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 8
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell:MIntroViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: MIntroViewCellID, for: indexPath) as! MIntroViewCell
        cell.imageView.image = imagesArray[indexPath.row]
        return cell
    }
    func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {
        
        if isHidding{
            scrollView.setContentOffset(scrollView.contentOffset, animated: false)
        }
    }
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        let x:CGFloat = scrollView.contentOffset.x
        let bound = 7 * SCREEN_WIDTH
        if x > bound{
            isHidding = true //停止滑动
            APP.setStatusBarHidden(false, with: .fade)
            self.hideBlock?()
            
            UIView.animate(withDuration: 0.5, animations: {
                self.layer.transform = CATransform3DMakeTranslation(-SCREEN_WIDTH, 0, 0)
                self.alpha = 0
            }, completion: { (finish) in
                if finish{
                    
                    self.removeFromSuperview()
                }
            })
            
        }
    }
}
