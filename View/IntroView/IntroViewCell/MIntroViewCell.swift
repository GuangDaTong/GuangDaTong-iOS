//
//  MIntroViewCell.swift
//  Mew_iOS
//
//  Created by ficow on 2017/9/28.
//  Copyright © 2017年 MewMewStudio. All rights reserved.
//

import UIKit

let MIntroViewCellID = "MIntroViewCell"

class MIntroViewCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
}
