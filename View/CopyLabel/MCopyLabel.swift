
import UIKit

class MCopyLabel: UILabel {

    override var canBecomeFirstResponder: Bool{
        return true
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        configGesture()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configGesture()
    }
    func configGesture(){
        
        self.isUserInteractionEnabled = true
        let copy = UILongPressGestureRecognizer.init(target: self, action: #selector(copyText(press:)))
        self.addGestureRecognizer(copy)
    }
    @objc func copyText(press:UILongPressGestureRecognizer){
        
        if press.state == .began{
            self.becomeFirstResponder()
            let copyItem = UIMenuItem.init(title: "拷贝", action:#selector(copyToPasteboard))
            let menuVC = UIMenuController.shared
            menuVC.menuItems = [copyItem]
            if menuVC.isMenuVisible{
                return
            }
            let loc = press.location(in: self)
            let origin = self.frame.origin
            let frame = CGRect.init(x: loc.x + origin.x, y: loc.y + origin.y, width: 24, height: 24)
            menuVC.setTargetRect(frame, in: self.superview!)
            menuVC.setMenuVisible(true, animated: true)
        }
    }
    @objc func copyToPasteboard(){
        
        if let text = self.attributedText?.string{
            UIPasteboard.general.string = text
        }else{
            UIPasteboard.general.string = self.text
        }
    }
}
