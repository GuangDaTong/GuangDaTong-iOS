

import UIKit

class MAskSendView: UIView,UITextViewDelegate {

    var heightUpdateBlock:((CGFloat) -> Void)?
    var imagePickBlock:(() -> Void)?
    
    static let DefaultImageWidth:CGFloat = SCREEN_WIDTH * 0.21
    static let TitleInputViewHeight:CGFloat = 45 + 8 * 2
    static let ContentTextViewHeight:CGFloat = (SCREEN_HEIGHT < 667) ? 64 : 104
    static let DefaultHeight:CGFloat = 32 + MAskSendView.ContentTextViewHeight + MAskSendView.DefaultImageWidth + 8 + MAskSendView.TitleInputViewHeight
    
    @IBOutlet weak var titleTF: UITextField!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var textCountLB: UILabel!
    
    @IBOutlet weak var imageBtn1: UIButton!
    @IBOutlet weak var imageBtn2: UIButton!
    @IBOutlet weak var imageBtn3: UIButton!
    @IBOutlet weak var imageBtn4: UIButton!
    @IBOutlet weak var imageBtn5: UIButton!
    @IBOutlet weak var imageBtn6: UIButton!
    @IBOutlet weak var imageBtn7: UIButton!
    @IBOutlet weak var imageBtn8: UIButton!
    @IBOutlet weak var imageBtn9: UIButton!
    
    @IBOutlet weak var delelteBtn1: UIButton!
    @IBOutlet weak var delelteBtn2: UIButton!
    @IBOutlet weak var delelteBtn3: UIButton!
    @IBOutlet weak var delelteBtn4: UIButton!
    @IBOutlet weak var delelteBtn5: UIButton!
    @IBOutlet weak var delelteBtn6: UIButton!
    @IBOutlet weak var delelteBtn7: UIButton!
    @IBOutlet weak var delelteBtn8: UIButton!
    @IBOutlet weak var delelteBtn9: UIButton!
    
    @IBOutlet weak var contentTextViewHeight: NSLayoutConstraint!
    
    var imageArr = [UIImage]()
    
    var imageBtnArr:[UIButton]! = nil
    var deleteBtnArr:[UIButton]! = nil
    let defaultImage = UIImage.init(named: "askWrite_addPhoto")
    
    class func loadFromNib() -> MAskSendView{
        
        let view = Bundle.main.loadNibNamed("MAskSendView", owner: nil, options: nil)?.last as! MAskSendView
        return view
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        
        NotiCenter.addObserver(self, selector: #selector(textFieldDidChange), name: NSNotification.Name.UITextFieldTextDidChange, object: nil)
        textView.delegate = self
        imageBtnArr = [imageBtn1,imageBtn2,imageBtn3,
                       imageBtn4,imageBtn5,imageBtn6,
                       imageBtn7,imageBtn8,imageBtn9]
        for btn in imageBtnArr{
            btn.imageView?.contentMode = .scaleAspectFill
        }
        contentTextViewHeight.constant = MAskSendView.ContentTextViewHeight
        
        deleteBtnArr = [delelteBtn1,delelteBtn2,delelteBtn3,
                        delelteBtn4,delelteBtn5,delelteBtn6,
                        delelteBtn7,delelteBtn8,delelteBtn9]
    }
    deinit {
        NotiCenter.removeObserver(self)
    }
    func setTextViewPlaceholder(text:String){
        
        textView.setPlaceholder(text: text)
    }
    func addPhoto(image:UIImage){
        
        imageArr.append(image)
        refreshImages()
    }
    func addPhotos(images:[UIImage]){
        
        imageArr.append(contentsOf: images)
        refreshImages()
    }
    func deletePhoto(atIndex index:Int){
        
        imageArr.remove(at: index)
        refreshImages()
    }
    func refreshImages(){
        
        var idx = 0
        var btn = imageBtnArr[idx]
        for img in imageArr{
            
            btn = imageBtnArr[idx]
            btn.setImage(img, for: .normal)
            btn.isHidden = false
            deleteBtnArr[idx].isHidden = false
            idx += 1
        }
        let line = imageArr.count / 3 + 1
        let imageSpace = MAskSendView.DefaultImageWidth + 8
        var height = 32 + MAskSendView.ContentTextViewHeight + CGFloat(line) * imageSpace + MAskSendView.TitleInputViewHeight
        
        if idx >= 9 {
            height -= imageSpace
            self.refreshHeight(height)
            return
        }
        btn = imageBtnArr[idx]
        btn.setImage(defaultImage, for: .normal)
        btn.isHidden = false
        deleteBtnArr[idx].isHidden = true
        
        for idxx in (idx + 1)..<9{
            imageBtnArr[idxx].isHidden = true
            deleteBtnArr[idxx].isHidden = true
        }
        self.refreshHeight(height)
    }
    func refreshHeight(_ height:CGFloat){
        
        self.heightUpdateBlock?(height)
//        if let tv =  superview as? UITableView{
//            tv.beginUpdates()
//            self.frame = CGRect.init(x: 0, y: 0, width: SCREEN_WIDTH, height: height)
//            tv.endUpdates()
//        }
    }
    
    // MARK: - Btn Event
    @IBAction func imageBtnPressed(_ sender: UIButton) {
        
        var index = -1
        for btn in imageBtnArr{
            index += 1
            if sender == btn {
                break
            }
        }
        if index < imageArr.count{ //查看图片
            var images = [UIImage]()
            for img in imageArr{
                images.append(img)
            }
            showImageBrowser(currentIndex: index, images: images)
            return
        }
        imagePickBlock?() //添加图片
    }
    
    @IBAction func deleteBtnPressed(_ sender: UIButton) {
        
        DLOG("")
        var index = 0
        for btn in deleteBtnArr{
            
            if btn != sender{
                index += 1
                continue
            }
            deletePhoto(atIndex: index)
            return
        }
    }
    @objc func textFieldDidChange(){
        
        var text = titleTF.text!
        if text.count > DefaultAskTitleMaxLength{
            text = String(text.characters.prefix(DefaultAskTitleMaxLength))
        }
    }
    func textViewDidChange(_ textView: UITextView) {
        
        if textView.text.count > DefaultMaxSendTextLength{
            textView.text = String(textView.text.characters.prefix(DefaultMaxSendTextLength))
        }
        textCountLB.text = "\(textView.text.count)/\(DefaultMaxSendTextLength)"
    }
}
