

import UIKit

class MAskSearchView: UIView {
    
    static let DefaultHeight:CGFloat = 242
    
    var askTypeClearBlock:(() -> Void)?
    var askTagClearBlock:(() -> Void)?
    var askTypeSelectBlock:((AskType) -> Void)?
    var askTagSelectBlock:((AskTag) -> Void)?

    // MARK: - Views
    @IBOutlet weak var askTypeClearBtn: UIButton!
    @IBOutlet weak var askTagClearBtn: UIButton!
    
    @IBOutlet weak var normalBtn: UIButton!
    @IBOutlet weak var urgentBtn: UIButton!
    
    @IBOutlet weak var lostBtn: UIButton!
    @IBOutlet weak var foundBtn: UIButton!
    @IBOutlet weak var learnBtn: UIButton!
    @IBOutlet weak var practiceBtn: UIButton!
    @IBOutlet weak var masterBtn: UIButton!
    @IBOutlet weak var trainingBtn: UIButton!
    @IBOutlet weak var shoppingBtn: UIButton!
    
    @IBOutlet weak var foodBtn: UIButton!
    @IBOutlet weak var trafficBtn: UIButton!
    @IBOutlet weak var travelBtn: UIButton!
    @IBOutlet weak var emotionBtn: UIButton!
    @IBOutlet weak var otherBtn: UIButton!
    
    
    // MARK: - Constraints
    @IBOutlet weak var upperSepLineHeight: NSLayoutConstraint!
    @IBOutlet weak var lowerSepLineHeight: NSLayoutConstraint!
    
    
    fileprivate let selectedColor = UIColor.colorWithHex(value: 0xF6A623, alpha: 1)
    fileprivate let unselectColor = AppThemeColor
    
    fileprivate var typeBtnArr:[UIButton]! = nil
    fileprivate var tagBtnArr:[UIButton]! = nil
    fileprivate let tagList:[AskTag] = [.lost,.found,.learn,.practice,.master,
                                        .training,.shopping,.food,.traffic,.travel,
                                        .emotion,.other]
    
    class func loadFromNib() -> MAskSearchView{
        
        let view = Bundle.main.loadNibNamed("MAskSearchView", owner: nil, options: nil)?.last as! MAskSearchView
        view.frame = CGRect.init(x: 0, y: 0, width: SCREEN_WIDTH, height: MAskSearchView.DefaultHeight)
        return view
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        
        upperSepLineHeight.setToOnePixel()
        lowerSepLineHeight.setToOnePixel()
        
        typeBtnArr = [normalBtn,urgentBtn]
        for btn in typeBtnArr{
            btn.tintColor = UIColor.clear
            btn.setTitleColor(unselectColor, for: .normal)
            btn.setTitleColor(selectedColor, for: .selected)
            diselectBtn(btn)
            btn.setCornerRounded(radius: 4)
            btn.layer.borderWidth = 1.0
        }
        tagBtnArr = [lostBtn,foundBtn,learnBtn,practiceBtn,
                     masterBtn,trainingBtn,shoppingBtn,foodBtn,
                     trafficBtn,travelBtn,emotionBtn,otherBtn]
        for btn in tagBtnArr{
            btn.tintColor = UIColor.clear
            btn.setTitleColor(unselectColor, for: .normal)
            btn.setTitleColor(selectedColor, for: .selected)
            diselectBtn(btn)
            btn.setCornerRounded(radius: 4)
            btn.layer.borderWidth = 1.0
        }
        askTypeClearBtn.isHidden = true
        askTagClearBtn.isHidden = true
    }
    
    // MARK: - Btn Events
    @IBAction func askTypeClearBtnPressed(_ sender: Any) {
        
        askTypeClearBtn.isHidden = true
        askTypeClearBlock?()
        
        for btn in typeBtnArr{
            diselectBtn(btn)
        }
    }
    @IBAction func askTagClearBtnPressed(_ sender: Any) {
        
        askTagClearBtn.isHidden = true
        askTagClearBlock?()
        
        for btn in tagBtnArr{
            diselectBtn(btn)
        }
    }
    
    @IBAction func askTypeBtnPressed(_ sender: UIButton) {
        
        askTypeClearBtn.isHidden = false
        
        if sender.titleLabel?.text == "普问"{
            askTypeSelectBlock?(.normal)
        }else{
            askTypeSelectBlock?(.urgent)
        }
        
        selectBtn(sender)
        playAnimation(sender)
        
        for btn in typeBtnArr{
            if btn == sender{
                continue
            }
            diselectBtn(btn)
        }
    }
    @IBAction func askTagBtnPressed(_ sender: UIButton) {
        
        askTagClearBtn.isHidden = false
        
        for tag in tagList{
            if sender.titleLabel?.text == tag.rawValue{
                askTagSelectBlock?(tag)
            }
        }
        
        selectBtn(sender)
        playAnimation(sender)
        
        for btn in tagBtnArr{
            if btn == sender{
                continue
            }
            diselectBtn(btn)
        }
    }
    func selectBtn(_ btn:UIButton){
        
        btn.isSelected = true
        btn.layer.borderColor = selectedColor.cgColor
    }
    func diselectBtn(_ btn:UIButton){
        
        btn.isSelected = false
        btn.layer.borderColor = unselectColor.cgColor
    }
    func playAnimation(_ sender:UIView){
        
        UIView.animate(withDuration: 0.2, animations: {
            sender.transform = CGAffineTransform.init(scaleX: 0.7, y: 0.7)
        }, completion: { (finish) in
            UIView.animate(withDuration: 0.2, animations: {
                sender.transform = CGAffineTransform.identity
            })
        })
    }
    
    
    
}
