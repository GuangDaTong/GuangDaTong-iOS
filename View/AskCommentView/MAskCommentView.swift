

import UIKit

class MAskCommentView: UIView,UITextViewDelegate {

    static let DefaultHeight:CGFloat = 52
    fileprivate let TextViewDefaultHeight:CGFloat = 36
    fileprivate var originalY:CGFloat = 0
    fileprivate var offsetY:CGFloat = 0
    fileprivate let TextViewTopBottomSpace:CGFloat = 8
    fileprivate let MaxLineCount:Int = 5
    fileprivate var MaxTextViewHeight:CGFloat = 0
    
    var sendBtnPressedBlock:((_ userID:String,_ text:String) -> Void)?
    fileprivate var _placeholder = ""
    var placeholder:String{
        get{
            return _placeholder
        }
        set{
            _placeholder = newValue
            textView.setPlaceholder(text: _placeholder)
        }
    }
    fileprivate var currentCommentUserID = ""
    
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var sendBtn: UIButton!
    
    class func loadFromNib() -> MAskCommentView{
        
        let view = Bundle.main.loadNibNamed("MAskCommentView", owner: nil, options: nil)?.last as! MAskCommentView
        return view
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOpacity = 0.8
        self.layer.shadowRadius = 3
        
        let btnWidth:CGFloat = 44
        let space:CGFloat = 8
        textView.frame = CGRect.init(x: 8, y: 9, width: SCREEN_WIDTH - space * 3 - btnWidth, height: TextViewDefaultHeight)
        sendBtn.frame = CGRect.init(x: SCREEN_WIDTH - space - btnWidth, y: 8, width: btnWidth, height: TextViewDefaultHeight)
        self.setNeedsLayout()
        
        textView.delegate = self
        textView.showLightGrayBorder()
        
        sendBtn.setCornerRounded(radius: 4)
        sendBtn.isEnabled = false
        sendBtn.backgroundColor = DefaultColorGray
        
        let lineHeight:CGFloat = textView.font?.lineHeight ?? 14
        MaxTextViewHeight = lineHeight * CGFloat(MaxLineCount)
    }
    
    func startComment(withUserID id:String,name:String){
        
        originalY = GetView_Height(view: self.superview!)
        
        if id != currentCommentUserID {
            textView.text = ""
            var placeholder:String = "@\(name)"
            if id == uUserID{
                placeholder = "回复自己"
            }
            textView.setPlaceholder(text: placeholder)
        }
        currentCommentUserID = id
        
        observeKeyboardMove()
        self.textView.becomeFirstResponder()
    }
    func clearText(){
        
        self.textView.text = ""
    }
    internal func textViewDidChange(_ textView: UITextView) {
        
        let count = textView.text.count
        if count == 0 {
            sendBtn.isEnabled = false
            sendBtn.backgroundColor = DefaultColorGray
        }else{
            sendBtn.isEnabled = true
            sendBtn.backgroundColor = DefaultColorBlue
        }
        if count > DefaultMaxSendTextLength {
            TopToastView.shared.makeFailToast("内容太长，已被删减")
            let text = String(textView.text.characters.prefix(DefaultMaxSendTextLength))
            textView.text = text
        }
        
        adjustFrame()
    }
    fileprivate func adjustFrame(){
        
        let height = textView.sizeThatFits(CGSize.init(width: textView.frame.size.width, height: CGFloat.greatestFiniteMagnitude)).height
        let line = Int(height/(textView.font?.lineHeight)!)
        
        if line <= 1{
            
            textView.setFrameHeight(TextViewDefaultHeight)
            let selfHeight = TextViewDefaultHeight + TextViewTopBottomSpace * 2
            self.setFrameHeight(selfHeight)
            self.setFrameY(offsetY - selfHeight)
            sendBtn.setFrameY(selfHeight - TextViewTopBottomSpace - TextViewDefaultHeight)
            textView.scrollRangeToVisible(textView.selectedRange) //解决textView内容抖动
            return
        }
        if line <= MaxLineCount {
            
            MaxTextViewHeight = height
            textView.setFrameHeight(MaxTextViewHeight)
            let selfHeight = MaxTextViewHeight + TextViewTopBottomSpace * 2
            self.setFrameHeight(selfHeight)
            self.setFrameY(offsetY - selfHeight)
            sendBtn.setFrameY(selfHeight - TextViewTopBottomSpace - TextViewDefaultHeight)
            textView.scrollRangeToVisible(textView.selectedRange)
            return
        }
        textView.setFrameHeight(MaxTextViewHeight)
        let selfHeight = MaxTextViewHeight + TextViewTopBottomSpace * 2
        self.setFrameHeight(selfHeight)
        self.setFrameY(offsetY - selfHeight)
        sendBtn.setFrameY(selfHeight - TextViewTopBottomSpace - TextViewDefaultHeight)
        textView.scrollRangeToVisible(textView.selectedRange)
    }
    @IBAction func sendBtnPressed(_ sender: Any) {
        
        sendBtnPressedBlock?(currentCommentUserID,self.textView.text)
    }
    
    fileprivate func observeKeyboardMove(){
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChangeFrame(_:)), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    fileprivate func cancelObserveKeyboardMove(){
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    @objc fileprivate func keyboardWillChangeFrame(_ notify:NSNotification){
        
        let value = notify.userInfo?[UIKeyboardFrameEndUserInfoKey] as! NSValue
        let kbHeight = value.cgRectValue.size.height
        guard kbHeight > 0 else {
            return
        }
        
        offsetY = originalY - kbHeight
        adjustFrame()
        
        DLOG("kbHeight:\(kbHeight),offsetY:\(offsetY)")
    }
    @objc fileprivate func keyboardWillHide(_ notify:NSNotification){
        
        self.setFrameY(originalY)
        cancelObserveKeyboardMove()
        self.textView.resignFirstResponder()
    }
}
