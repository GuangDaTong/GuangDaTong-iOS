

import UIKit

enum MSharePlatformType:String {
    case qq = "QQFriend"
    case qzone = "QQZone"
    case wechat = "weChatFriend"
    case moment = "weChatCircle"
    case link
}

class MShareView: UIView {

    // MARK: - View
    @IBOutlet weak var grayBackgroundView: UIView!
    @IBOutlet weak var containerView: UIView!
    
    // MARK: - Property
    var shareSelectedBlock:((MSharePlatformType) -> Void)?
    fileprivate var platform:MSharePlatformType = MSharePlatformType.qq
    fileprivate var isUserSelect:Bool = false
    
    class func loadFromNib() -> MShareView{
        
        let view:MShareView = Bundle.main.loadNibNamed("MShareView", owner: nil, options: nil)?.last as! MShareView
        let tap:UITapGestureRecognizer = UITapGestureRecognizer.init(target: view, action: #selector(view.hide))
        view.grayBackgroundView.addGestureRecognizer(tap)
        return view
    }
    
    func show(){
        
        _ = UIResponder.firstResponder()?.resignFirstResponder()
        self.setToHide()
        WIN.addSubview(self)
        self.snp.makeConstraints { (make) in
            make.top.equalTo(WIN.snp.top)
            make.leading.equalTo(WIN.snp.leading)
            make.trailing.equalTo(WIN.snp.trailing)
            make.bottom.equalTo(WIN.snp.bottom)
        }
        containerView.setCornerRounded(radius: 8)
        playShowAnimation()
    }
    func playShowAnimation(){
        
        UIView.animate(withDuration: 0.2) {
            self.containerView.transform = CGAffineTransform.identity
            self.grayBackgroundView.alpha = 0.8
            self.containerView.alpha = 1
        }
    }
    @objc func hide(){
        
        UIView.animate(withDuration: 0.2, animations: {
            self.setToHide()
        }, completion: { (finish) in
            self.removeFromSuperview()
            if self.isUserSelect {
                self.shareSelectedBlock?(self.platform)
            }
        })
    }
    func setToHide(){
        
        self.grayBackgroundView.alpha = 0
        self.containerView.alpha = 0
        self.containerView.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
    }
    
    // MARK: - Action
    @IBAction func qqBtnPressed(_ sender: Any) {
        platform = MSharePlatformType.qq
        isUserSelect = true
        hide()
    }
    @IBAction func qzoneBtnPressed(_ sender: Any) {
        platform = MSharePlatformType.qzone
        isUserSelect = true
        hide()
    }
    @IBAction func wechatBtnPressed(_ sender: Any) {
        platform = MSharePlatformType.wechat
        isUserSelect = true
        hide()
    }
    @IBAction func momentBtnPressed(_ sender: Any) {
        platform = MSharePlatformType.moment
        isUserSelect = true
        hide()
    }
    @IBAction func linkBtnPressed(_ sender: Any) {
        platform = MSharePlatformType.link
        isUserSelect = true
        hide()
    }

}
