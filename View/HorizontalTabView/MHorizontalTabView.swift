

import UIKit

protocol MHorizontalTabViewDelegate:class {
    func horizontalTabViewDidSelect(_ index:Int)
}

class MHorizontalTabView: UIView,UICollectionViewDelegate,UICollectionViewDataSource {

    // MARK: - Config
    var defaultHeight:CGFloat = 0 //读取MHorizontalTabViewCell获取高度
    var defaultBtnWidth:CGFloat = 76
    
    // MARK: - Property
    var titleArr:[String]! = nil
    var titleList:[String]{
        get{
            return titleArr
        }
        set{
            titleArr = newValue
        }
    }
    var badgeArr:[Int]! = nil
    var selectedIndex = 0
    weak var delegate:MHorizontalTabViewDelegate?
    // MARK: - Views
    fileprivate var collectionView:UICollectionView! = nil
    fileprivate var btnArr:[UIButton]! = nil
    fileprivate var slideView:UIImageView! = nil
    // MARK: - Setup
    func setup(withTitles titles:[String]){

        assert(titles.count != 0, "MHorizontalTabView的title不能为空！")
        titleList = titles
        defaultHeight = MHorizontalTabViewCell.defaultHeight
        
        let count:Int = titleList.count
        badgeArr = [Int].init(repeating: 0, count: count)
        
        let width:CGFloat = SCREEN_WIDTH / CGFloat(count)
        if width > defaultBtnWidth{
            defaultBtnWidth = width
        }
        let layout:UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize.init(width: defaultBtnWidth, height: defaultHeight)
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 0
        
        self.backgroundColor = UIColor.white
        layer.shadowColor = UIColor.lightGray.cgColor
        layer.shadowOffset = CGSize.init(width: 0, height: 1)
        layer.shadowOpacity = 0.7
        layer.shadowRadius = 3
        
        collectionView =  UICollectionView.init(frame: CGRect.zero, collectionViewLayout: layout)
        // 防止iOS11导致头像偏移抖动
        if #available(iOS 11.0, *) {
            collectionView.contentInsetAdjustmentBehavior = .never
        }
        collectionView.alwaysBounceHorizontal = true
        collectionView.allowsSelection = false
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.backgroundColor = UIColor.clear
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib.init(nibName: MHorizontalTabViewCellIdentifier, bundle: nil), forCellWithReuseIdentifier: MHorizontalTabViewCellIdentifier)
        
        self.addSubview(collectionView)
        collectionView.snp.makeConstraints { (make) in
            make.leading.equalTo(self.snp.leading)
            make.trailing.equalTo(self.snp.trailing)
            make.top.equalTo(self.snp.top)
            make.bottom.equalTo(self.snp.bottom)
        }
    }
    
    /// select tab with bottom line shown
    ///
    /// - Parameter index: zero based index
    func selectIndex(_ index:Int){
        
        if self.delegate != nil {
            self.delegate?.horizontalTabViewDidSelect(index)
        }
        selectedIndex = index
        collectionView.reloadData()
        collectionView.layoutIfNeeded()
        let x = defaultBtnWidth * CGFloat(index)
        let rect = CGRect.init(x: x, y: 0, width: defaultBtnWidth, height: defaultHeight)
        collectionView.scrollRectToVisible(rect, animated: true)
    }
    
    /// highlight tab with a red dot
    ///
    /// - Parameters:
    ///   - index: zero based index
    ///   - highlight: highlight or not
    func setTab(_ index:Int,badge:Int){
        guard index < badgeArr.count else {
            return
        }
        badgeArr[index] = badge
        collectionView.reloadData()
    }
    
    // MARK: - CollectionView Delegate & Datasource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return titleList.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MHorizontalTabViewCellIdentifier, for: indexPath) as! MHorizontalTabViewCell
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        let aCell = cell as! MHorizontalTabViewCell
        
        let row = indexPath.row
        aCell.titleLB.text = titleList[row]
        
        let isSelect = (selectedIndex == row)
        aCell.setSelect(isSelect: isSelect)
        aCell.selectedBlock = { [weak self] in
            self?.selectedIndex = row
            self?.collectionView.reloadData()
            if self?.delegate != nil {
                self?.delegate?.horizontalTabViewDidSelect(row)
            }
        }
        aCell.setBadgeNum(badgeArr[row])
    }

}
