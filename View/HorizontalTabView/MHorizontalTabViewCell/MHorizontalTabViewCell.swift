

import UIKit

let MHorizontalTabViewCellIdentifier = "MHorizontalTabViewCell"

class MHorizontalTabViewCell: UICollectionViewCell {

    static let defaultHeight:CGFloat = 36
    
    @IBOutlet weak var titleLB: UILabel!
    @IBOutlet weak var bottomIV: UIImageView!
    @IBOutlet weak var badgeLB: UILabel!
    
    let unselectColor = DefaultColorBlack
    let selectedColor = AppThemeColor
    var selectedBlock:(() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        isAccessibilityElement = true
        
        bottomIV.setCornerRounded(radius: 1)
        
        self.badgeLB.isHidden = true
        self.badgeLB.setCornerRounded(radius: 15 / 2.0)
    }
    func setSelect(isSelect:Bool){
        
        if isSelect{
            bottomIV.isHidden = false
            titleLB.textColor = selectedColor
            return
        }
        bottomIV.isHidden = true
        titleLB.textColor = unselectColor
    }
    func setBadgeNum(_ num:Int){
        
        if num <= 0{
            self.badgeLB.isHidden = true
            return
        }
        self.badgeLB.isHidden = false
        if num < 10{
            self.badgeLB.text = "  \(num)  "
            self.badgeLB.sizeToFit()
        }else if num < 100{
            self.badgeLB.text = " \(num) "
            self.badgeLB.sizeToFit()
        }else{
            self.badgeLB.text = " 99+ "
            self.badgeLB.sizeToFit()
        }
    }
    @IBAction func cellBtnPressed(_ sender: Any) {
        if let blk = selectedBlock {
            blk()
        }
    }
}
