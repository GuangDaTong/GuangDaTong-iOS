

import UIKit

/// Toast样式类型
///
/// - success: 成功
/// - fail: 失败
enum TopToastType{
    case none
    case success
    case fail
}

class TopToastView: UIView {
    
    static let shared:TopToastView = TopToastView()
    
    var currentToastText:String = ""
    
    /// 显示成功样式的提示TopToast
    func makeSuccessToast(_ text:String){
        showToast(text: text)
    }
    /// 显示失败样式的提示TopToast
    func makeFailToast(_ text:String){
        showToast(text: text, type: .fail)
    }
    /// 显示提示TopToast，样式（成功、失败）
    func showToast(text:String,type:TopToastType = . success){
        
        // 文本相同，不重复显示
        if text == self.currentToastText{
            return
        }
        self.currentToastText = text
        
        var toast:UIView! = nil
        toast = UIView.init(frame: CGRect.zero)
        toast.isUserInteractionEnabled = true
        
        let swipeUp:UISwipeGestureRecognizer = UISwipeGestureRecognizer.init(target: self, action: #selector(hideToast))
        swipeUp.direction = .up
        toast.addGestureRecognizer(swipeUp)
        
        let tap:UITapGestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(hideToast(_:)))
        toast.addGestureRecognizer(tap)
        
        var iconName:String = ""
        var textColor:UIColor! = nil
        var backgroundColor:UIColor! = nil
        if type == .fail {
            iconName = "toast_fail_info"
            textColor = UIColor.white
            backgroundColor = UIColor.colorWithHex(value: 0xFF6B6F, alpha: 1)
        }else{
            iconName = "toast_success_info"
            textColor = UIColor.darkGray
            backgroundColor = UIColor.init(white: 0.94, alpha: 1)
        }
        toast.backgroundColor = backgroundColor
        toast.layer.shadowOpacity = 0.8
        toast.layer.shadowOffset = CGSize.init(width: 0, height: 1)
        toast.layer.shadowColor = UIColor.lightGray.cgColor
        toast.layer.shadowRadius = 2
        
        DispatchQueue.main.async{
            
            WIN.addSubview(toast)
            toast.snp.makeConstraints { (m) in
                m.leading.equalTo(WIN.snp.leading)
                m.bottom.equalTo(WIN.snp.top)
                m.trailing.equalTo(WIN.snp.trailing)
                m.height.equalTo(64)
            }
            
            let icon:UIImageView = UIImageView.init(image: UIImage.init(named: iconName))
            toast.addSubview(icon)
            icon.snp.makeConstraints { (m) in
                m.leading.equalTo(toast.snp.leading).offset(16)
                m.width.equalTo(22)
                m.height.equalTo(22)
                m.bottom.equalTo(toast.snp.bottom).offset(-9)
            }
            
            let titleLB:UILabel = UILabel.init(frame: CGRect.zero)
            titleLB.text = text
            titleLB.adjustsFontSizeToFitWidth = true
            titleLB.textColor = textColor
            toast.addSubview(titleLB)
            titleLB.sizeToFit()
            titleLB.snp.makeConstraints { (m) in
                m.leading.equalTo(icon.snp.trailing).offset(8)
                m.trailing.equalTo(toast.snp.trailing).offset(-32)
                m.height.equalTo(25)
                m.centerY.equalTo(icon.snp.centerY)
            }
            UIView.animate(withDuration: 0.2, animations: {
                toast.transform = CGAffineTransform.init(translationX: 0, y: 70)
            }, completion: { (finish) in
                UIView.animate(withDuration: 0.2) {
                    toast.transform = CGAffineTransform.init(translationX: 0, y: 64)
                }
            })
            // 自动消失
            delay(second: 2) {
                if toast.superview == nil {
                    return
                }
                UIView.animate(withDuration: 0.2, animations: {
                    toast.transform = CGAffineTransform.identity
                }) { (finish) in
                    toast.removeFromSuperview()
                    self.currentToastText = ""
                }
            }
        }
    }
    /// 隐藏提示TopToast
    @objc func hideToast(_ swipe:UIGestureRecognizer){
        
        UIView.animate(withDuration: 0.1, animations: {
            swipe.view?.transform = CGAffineTransform.identity
        }) { (finish) in
            swipe.view?.removeFromSuperview()
            self.currentToastText = ""
        }
    }
}
