import Foundation

extension AppDelegate{
    
    /// 初始化APP第一次的配置项
    func initFirstLoad(){
        
//        let isFirstLoad = "isFirstLoad"
//        if uDefaults.object(forKey: isFirstLoad) == nil{
//            uDefaults.set(true, forKey: isFirstLoad)
//        }else{
//            return
//        }
        createMsgTable()
        initSoundAndVibration()
        initPushSetting()
        uDefaults.synchronize()
    }
    /// 设置声音和振动
    func initSoundAndVibration(){
        
        let soundKey = "isSoundEnabled"
        let vibrationKey = "isVibrationEnabled"
        
        if uDefaults.object(forKey: soundKey) == nil{
            uDefaults.set(true, forKey: soundKey)
        }
        if uDefaults.object(forKey: vibrationKey) == nil{
            uDefaults.set(true, forKey: vibrationKey)
        }
    }
    /// 设置推送相关设置
    func initPushSetting(){
        
        let isSchoolPush = "isSchoolPush"
        let isCollegePush = "isCollegePush"
        let isCommentPush = "isCommentPush"
        let isLeaveMsgPush = "isLeaveMsgPush"
        let isActivityPush = "isActivityPush"
        let isSystemPush = "isSystemPush"
        
        if uDefaults.object(forKey: isSchoolPush) == nil{
            uDefaults.set(true, forKey: isSchoolPush)
        }
        if uDefaults.object(forKey: isCollegePush) == nil{
            uDefaults.set(true, forKey: isCollegePush)
        }
        if uDefaults.object(forKey: isCommentPush) == nil{
            uDefaults.set(true, forKey: isCommentPush)
        }
        if uDefaults.object(forKey: isLeaveMsgPush) == nil{
            uDefaults.set(true, forKey: isLeaveMsgPush)
        }
        if uDefaults.object(forKey: isActivityPush) == nil{
            uDefaults.set(true, forKey: isActivityPush)
        }
        if uDefaults.object(forKey: isSystemPush) == nil{
            uDefaults.set(true, forKey: isSystemPush)
        }
    }
    /// 创建消息中心数据库表
    func createMsgTable(){
        
        let q = FMDatabaseQueue(url: DefaultURLForDB)
        q.inDatabase { (db) in
            
            var created = true
            var SQL = "CREATE TABLE IF NOT EXISTS \(DBTableNameForMsgCenterLeaveMsg) (id text PRIMARY KEY ON CONFLICT REPLACE,nickname text,iconURL text,gender blob,senderID text,createTime text,content blob,contentHeight real,isUnread integer,loginUserID text)"
            created = db.executeStatements(SQL)
            assert(created, "数据库创建<留言>消息表失败")
            
            SQL = "CREATE TABLE IF NOT EXISTS \(DBTableNameForMsgCenterAsk) (id text PRIMARY KEY ON CONFLICT REPLACE,askID text,nickname text,iconURL text,gender blob,senderID text,createTime text,type text,detail blob,contentHeight real,isUnread integer,loginUserID text)"
            created = db.executeStatements(SQL)
            assert(created, "数据库创建<问问通知>消息表失败")
            
            SQL = "CREATE TABLE IF NOT EXISTS \(DBTableNameForMsgCenterNotice) (id text PRIMARY KEY ON CONFLICT REPLACE,nickname text,iconURL text,gender blob,senderID text,createTime text,type text,detailStr text,detailAttrStr blob,contentHeight real,isUnread integer,loginUserID text)"
            created = db.executeStatements(SQL)
            assert(created, "数据库创建<系统通知>消息表失败")
        }
    }
}
