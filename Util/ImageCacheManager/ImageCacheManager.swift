
import UIKit
import SDWebImage

class ImageCacheManager: SKImageCacheable {
    
    static let shared:ImageCacheManager = ImageCacheManager()
    
    func imageForKey(_ key: String) -> UIImage?{
        return SDImageCache.shared().imageFromCache(forKey:key)
    }
    func setImage(_ image: UIImage, forKey key: String){
        SDImageCache.shared().store(image, forKey: key, completion: nil)
    }
    func removeImageForKey(_ key: String){
        SDImageCache.shared().removeImage(forKey: key, withCompletion: nil)
    }
}
