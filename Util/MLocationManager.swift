

import UIKit
import CoreLocation

class MLocationManager: NSObject,CLLocationManagerDelegate {

    var locationManager:CLLocationManager! = nil
    /// 防止短时间内上传多次位置信息
    var isInfoUploading:Bool = false
    
    var isAuthorized:Bool{
        return CLLocationManager.authorizationStatus() != .denied
    }

    func beginLocate(){
        
        locationManager = CLLocationManager()
        locationManager.delegate = self
        // http://blog.csdn.net/weizhixiang/article/details/54178357
        
        //更新距离
        locationManager.distanceFilter = 100 // 更新频率，单位为m
        locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation // 定位精度的设置
        
        ////发送授权申请
        locationManager.requestWhenInUseAuthorization()
        
        if (CLLocationManager.locationServicesEnabled() && isAuthorized)
        {
            //允许使用定位服务的话，开启定位服务更新
            locationManager.startUpdatingLocation()
            DLOG("定位开始！")
            return
        }
        DLOG("定位失败，无权限！")
    }
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        switch status {
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
        default:
            break
        }
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        manager.stopUpdatingLocation()
        //获取最新的坐标
        let curLocation:CLLocation = locations.last!
        
        let geoCoder:CLGeocoder = CLGeocoder.init()
        geoCoder.reverseGeocodeLocation(curLocation) { (placemarks, error) in
            
            self.parseLocationInfo(placemarks: placemarks, error: error)
        }
    }
    func parseLocationInfo(placemarks:[CLPlacemark]?,error:Error?){
        
        if let _ = error{
            return
        }
        guard let marks = placemarks else{ return }
        guard let placemark = marks.first else { return }
        
        let country:String = placemark.country ?? ""
        let province:String = placemark.administrativeArea ?? ""
        let city:String = placemark.locality ?? ""
        let district:String = placemark.subLocality ?? ""
        let county:String = placemark.subAdministrativeArea ?? ""
        let street:String = placemark.thoroughfare ?? ""
        let name:String = placemark.name ?? ""
        
        uDefaults.set(country, forKey: "UserLocation_country")
        uDefaults.set(province, forKey: "UserLocation_province")
        uDefaults.set(city, forKey: "UserLocation_city")
        uDefaults.set(district, forKey: "UserLocation_district")
        uDefaults.set(county, forKey: "UserLocation_county")
        uDefaults.set(street, forKey: "UserLocation_street")
        uDefaults.set(name, forKey: "UserLocation_name")
        
        uDefaults.synchronize()
        
        var infoArr:[String] = [String]()
        
        infoArr.append("country:\(country)")
        infoArr.append("province:\(province)")
        infoArr.append("city:\(city)")
        infoArr.append("district:\(district)")
        infoArr.append("county:\(county)")
        infoArr.append("street:\(street)")
        infoArr.append("name:\(name)")
        DLOG(infoArr.joined(separator: "\n"))
        
        if self.isInfoUploading{
            return
        }
        self.isInfoUploading = true
        NotiCenter.post(name: UserLocationDidObtainNotification, object: nil)
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
        manager.stopUpdatingLocation()
        DLOG(error.localizedDescription)
    }
}
