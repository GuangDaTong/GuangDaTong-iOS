

import UIKit

class MUpdateChecker: NSObject {
    
    /// 是否手动检查
    fileprivate var isManualCheck:Bool = false
    /// 最新版本
    fileprivate var curVersion:String = ""
    /// 最低版本
    fileprivate var minVersion:String = ""
    /// 新版本特性
    private var updateInfo:String = ""
    
    // MARK: - App Version
    func beginCheck(isManualCheck:Bool){
        
        self.isManualCheck = isManualCheck
        if isManualCheck{
            WIN.makeToastActivity()
        }
        
        HttpController.shared.checkVersion(success: { (json) in
            
            if isManualCheck{
                WIN.hideToastActivity()
            }
            self.curVersion = json["result"]["curVersion"].stringValue
            self.minVersion = json["result"]["minVersion"].stringValue
            self.updateInfo = json["result"]["updateLog"].stringValue
            
            DispatchQueue.global().async {
                self.judgeVersion()
            }
        }, failure: { (err) in
            
            if isManualCheck{
                WIN.hideToastActivity()
            }
            DLOG(err.localizedDescription)
            TopToastView.shared.makeFailToast(err.localizedDescription)
        })
    }
    func judgeVersion(){
        
        // App当前版本
        let versionArr:[String] = APP_Current_Version.components(separatedBy: ".")
        guard versionArr.count == 3 else{
            return
        }
        DLOG("current:\(APP_Current_Version),\ncurVersion:\(curVersion),\nminVersion:\(minVersion)")
        checkMinVersion(versionArr)
    }
    func checkMinVersion(_ versionArr:[String]){
        
        let minArr = minVersion.components(separatedBy: ".")
        
        if minArr.count == 3 {
            for idx in 0...2{
                let min:Int = Int(minArr[idx])!
                let cur:Int = Int(versionArr[idx])!
                if cur >= min {
                    break
                }
                DispatchQueue.main.async {
                    self.showUpdateAlert("警告", msg: "软件版本过低，可能无法正常使用，请及时更新！")
                }
                return
            }
        }
        checkCurVersion(versionArr)
    }
    func checkCurVersion(_ versionArr:[String]){
        
        let newArr = curVersion.components(separatedBy: ".")
        
        if newArr.count == 3 {
            for idx in 0...2{
                let new:Int = Int(newArr[idx])!
                let cur:Int = Int(versionArr[idx])!
                if cur >= new {
                    break
                }
                //新版本发布
                let msg:String = self.updateInfo
                DispatchQueue.main.async {
                    self.showUpdateAlert("发现新版本\(self.curVersion)", msg: msg)
                }
                return
            }
        }
    }
    func showUpdateAlert(_ title:String,msg:String){
        
        if let vc = currentTopVC(){
            DispatchQueue.global().async {
                
                let alert = UIAlertController(title: title, message: "\n\(msg)", preferredStyle: .alert)
                
                let okAction = UIAlertAction.init(title: "现在更新", style: .destructive) { (action) in
                    APP.openURL(URL.init(string: AppStoreDownloadURL)!)
                }
                let cancelAction = UIAlertAction(title: "下次再说", style: .default, handler: nil)
                alert.addAction(cancelAction)
                alert.addAction(okAction)
                
                DispatchQueue.main.async {
                    vc.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
}
