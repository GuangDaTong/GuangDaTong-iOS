//
//  KeyboardMove.swift
//  SCNULive
//
//  Created by ficow on 2017/6/30.
//  Copyright © 2017年 scnu. All rights reserved.
//

import UIKit

extension UIViewController{
    
    // MARK: - Navigation
    func loadViewControllerInMainStoryboard(_ vcName:String) -> UIViewController{
        return loadViewController(vcName, inStoryboard: "Main")
    }
    
    func loadViewController(_ vcName:String,inStoryboard sbName:String) -> UIViewController{
        
        let vc = UIStoryboard.init(name: sbName, bundle: nil).instantiateViewController(withIdentifier: vcName)
        return vc
    }
    
    
    func pushVCInMainStoryboard(_ vc:String){
        self.pushVC(vc, inStoryboard: "Main",animated: true)
    }
    func pushVC(_ vcStr:String,inStoryboard sbName:String,animated:Bool){
        
        let vc = UIStoryboard.init(name: sbName, bundle: nil).instantiateViewController(withIdentifier: vcStr)
        self.navigationController?.pushViewController(vc, animated:animated)
    }
    
    func presentVCInMainStoryboard(_ vc:String){
        self.presentVC(vc, inStoryboard: "Main", animated: true)
    }
    func presentVC(_ vcStr:String,inStoryboard sbName:String,animated:Bool){
        
        let vc = UIStoryboard.init(name: sbName, bundle: nil).instantiateViewController(withIdentifier: vcStr)
        self.present(vc, animated: animated, completion: nil)
    }
    
    
    // MARK: - Keyboard Move
    
    /// 监测键盘移动
    func observeKeyboardMove(){
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChangeFrame(_:)), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    /// 取消监测键盘移动
    func cancelObserveKeyboardMove(){
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    @objc func keyboardWillChangeFrame(_ notify:NSNotification){
        
        let frameValue:NSValue? = notify.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue
        if frameValue == nil{
            return
        }
        let value:NSValue = frameValue!
        
        let kbHeight:CGFloat = value.cgRectValue.size.height
        guard kbHeight > 0 && kbHeight < 300 else {
            return
        }
        let first:UIView? = UIResponder.firstResponder() as? UIView
        guard first != nil else {
            return
        }
        let rect:CGRect? = first?.convert(first!.bounds, to: nil)
        guard rect != nil else {
            return
        }
        
        let r:CGRect = rect!
        let viewBottomY:CGFloat = r.origin.y + (first?.frame.size.height)!
        let kbTopY:CGFloat = SCREEN_HEIGHT - kbHeight
//        DLOG("kbHeight=\(kbHeight)")
//        DLOG("r=\(r.debugDescription)")
//        DLOG("viewBottomY=\(viewBottomY)")
//        DLOG("kbTopY=\(kbTopY)")
        var offset:CGFloat = viewBottomY - kbTopY
        guard offset > 0 else {
            self.view.transform = CGAffineTransform.identity
            return
        }
        offset += 4
        if offset > kbHeight{
            offset = kbHeight
        }
//        DLOG("offset:\(offset)")
        self.view.transform = CGAffineTransform.init(translationX: 0, y: -offset)
    }
    @objc func keyboardWillHide(_ notify:NSNotification){
        self.view.transform = CGAffineTransform.identity
    }
    
}


extension UIViewController{
    
    /// navigationController.view截图
    func snapshot() -> UIView{
        
        var view:UIView? = objc_getAssociatedObject(self, "FSAnimationTransitioningSnapshot") as? UIView
        if let v = view{
            return v
        }
        view = (self.navigationController?.view.snapshotView(afterScreenUpdates: false)!)!
        self.setSnapshot(view)
        return view!
    }
    func setSnapshot(_ snapshot:UIView?){
        objc_setAssociatedObject(self, "FSAnimationTransitioningSnapshot", snapshot, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
    }
    /// 导航栏截图
    func topSnapshot() -> UIView{
        
        if let view = objc_getAssociatedObject(self, "FSAnimationTransitioningTopSnapshot") as? UIView{
            return view
        }
        let view:UIView = (self.navigationController?.view.resizableSnapshotView(from: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 64), afterScreenUpdates: false, withCapInsets: UIEdgeInsets.zero))!
        self.setTopSnapshot(view)
        return view
    }
    func setTopSnapshot(_ snapshot:UIView?){
        objc_setAssociatedObject(self, "FSAnimationTransitioningTopSnapshot", snapshot, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
    }
    
    /// view截图
    func viewSnapshot() -> UIView{
        
        if let view = objc_getAssociatedObject(self, "FSAnimationTransitioningViewSnapshot") as? UIView{
            return view
        }
        let view:UIView = (self.navigationController?.view.resizableSnapshotView(from: CGRect.init(x: 0, y: 64, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height - 64), afterScreenUpdates: false, withCapInsets: UIEdgeInsets.zero))!
        self.setViewSnapshot(view)
        return view
    }
    func setViewSnapshot(_ snapshot:UIView?){
        objc_setAssociatedObject(self, "FSAnimationTransitioningViewSnapshot", snapshot, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
    }
}
