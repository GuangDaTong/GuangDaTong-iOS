

import UIKit

extension NSLayoutConstraint{
    
    /// 以6s屏幕高度为基准，进行高度调整
    func adjustHeightFrom6s(){
        let constant = self.constant / iPhone6_Portrait_Height * SCREEN_HEIGHT
        self.constant = constant
    }
    /// 以6s屏幕宽度为基准，进行宽度调整
    func adjustWidthFrom6s(){
        let constant = self.constant / iPhone6_Portrait_Width * SCREEN_WIDTH
        self.constant = constant
    }
    /// 设置为一像素
    func setToOnePixel(){
        self.constant = 1 / UIScreen.main.scale
    }
}
