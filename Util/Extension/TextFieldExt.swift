

import UIKit

extension UITextField{
    
    func setPlaceholderTextToLightGray(){
        
        self.setValue(UIColor.lightGray, forKeyPath: "_placeholderLabel.textColor")
    }
}
