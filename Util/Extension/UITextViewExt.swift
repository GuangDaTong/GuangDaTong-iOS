

import UIKit

extension UITextView{
    
    func setPlaceholder(text:String){
        
        if self.placeholder() != nil {
            
            let placeholderLabel = self.value(forKey: "_placeholderLabel") as? UILabel
            placeholderLabel?.text = text
            return
        }
        
        let placeholderLB = UILabel.init(frame: CGRect.zero)
        placeholderLB.textColor = UIColor.lightGray
        placeholderLB.text = text
        placeholderLB.font = self.font
        placeholderLB.numberOfLines = 0
        placeholderLB.sizeToFit()
        self.addSubview(placeholderLB)
        
        self.setValue(placeholderLB, forKey: "_placeholderLabel")
    }
    func showLightGrayBorder(){
        
        self.layer.borderColor = UIColor.colorWithHex(value: 0xDDDDDD, alpha: 1).cgColor
        self.layer.cornerRadius = 4
        self.layer.borderWidth = 0.5
    }
    func placeholder() -> String?{
        
        let placeholderLabel = self.value(forKey: "_placeholderLabel") as? UILabel
        return placeholderLabel?.text
    }
}
