//
//  Color.swift
//  SCNULive
//
//  Created by ficow on 2017/6/30.
//  Copyright © 2017年 scnu. All rights reserved.
//

import UIKit

extension UIColor{
    
    
    /// 根据十六进制数字，如0xFFFFFF，生成UIColor实例
    ///
    /// - Parameters:
    ///   - value: 代表颜色的十六进制数字
    ///   - alpha: 透明度
    /// - Returns: UIColor实例
    class func colorWithHex(value:Int,alpha:CGFloat) -> UIColor{
        
        let red = ((CGFloat)((value & 0xFF0000) >> 16))/255.0;
        let green = ((CGFloat)((value & 0xFF00) >> 8))/255.0;
        let blue = ((CGFloat)(value & 0xFF))/255.0;
        return UIColor.init(red: red, green: green, blue: blue, alpha: alpha)
    }
}
