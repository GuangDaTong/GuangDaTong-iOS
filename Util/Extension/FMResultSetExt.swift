
import Foundation

extension FMResultSet{
    
    func string(forCol col:Int) -> String{
        let str = self.string(forColumnIndex: Int32(col)) ?? ""
        return str
    }
    func string(forKey key:String) -> String{
        let str = self.string(forColumn: key) ?? ""
        return str
    }
    
    func int(forCol col:Int) -> Int{
        let i = Int(self.int(forColumnIndex: Int32(col)))
        return i
    }
    func int(forKey key:String) -> Int{
        let i = Int(self.int(forColumn: key))
        return i
    }
    
    func float(forCol col:Int) -> CGFloat{
        let f = CGFloat(self.double(forColumnIndex: Int32(col)))
        return f
    }
    func float(forKey key:String) -> CGFloat{
        let f = CGFloat(self.double(forColumn: key))
        return f
    }
    
    func bool(forCol col:Int) -> Bool{
        let b = self.bool(forColumnIndex: Int32(col))
        return b
    }
    func bool(forKey key:String) -> Bool{
        let b = self.bool(forColumn: key)
        return b
    }
    
    func data(forCol col:Int) -> Data{
        let d = self.data(forColumnIndex: Int32(col)) ?? Data()
        return d
    }
    func data(forKey key:String) -> Data{
        let d = self.data(forColumn: key) ?? Data()
        return d
    }
    
    func object(forCol col:Int) -> Any?{
        let data = self.data(forCol: col)
        return NSKeyedUnarchiver.unarchiveObject(with: data)
    }
    func object(forKey key:String) -> Any?{
        let data = self.data(forKey: key)
        return NSKeyedUnarchiver.unarchiveObject(with: data)
    }
}
