

import Foundation

extension Array where Element == Int{
    
    /// Generate UIColor with [r,g,b]
    public var RGBColor:UIColor{
        guard self.count == 3 else {
            fatalError("Specify with [R,G,B] Int values")
        }
        let divisor:CGFloat = 255
        let r:CGFloat = CGFloat(self[startIndex]) / divisor
        let g:CGFloat = CGFloat(self[startIndex.advanced(by: 1)]) / divisor
        let b:CGFloat = CGFloat(self[startIndex.advanced(by: 2)]) / divisor
        return UIColor(red: r, green: g, blue: b, alpha: 1.0)
    }
}
