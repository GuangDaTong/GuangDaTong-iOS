

import Foundation

extension String{
    
    func attributedGenderString() -> NSAttributedString{
        
        var genderText:String = ""
        var genderColor:UIColor! = nil
        if self == "男"{
            genderText = "♂"
            genderColor = DefaultColorBlue
        }else if self == "女"{
            genderText = "♀"
            genderColor = DefaultColorRed
        }else{
            genderText = ""
        }
        let genderAttrStr:NSMutableAttributedString = NSMutableAttributedString.init(string: genderText)
        let genderRange:NSRange = NSRange.init(location: 0, length: genderAttrStr.length)
        genderAttrStr.addAttributes([NSAttributedStringKey.foregroundColor:genderColor], range: genderRange)
        
        return genderAttrStr
    }
}
