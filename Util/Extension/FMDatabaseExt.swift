

import Foundation

enum FMDBDataType:String {
    case null
    case text
    case int = "INTEGER"
    case bool = "integer"
    case real
    case blob
}

extension FMDatabase{
    
    
    /// select sql
    ///
    /// - Parameters:
    ///   - sql: sql
    ///   - handler: func to process Result
    func select(_ sql:String,_ handler:((FMResultSet?) -> Void)){
        
        guard sql.count != 0 else {
            assertionFailure("select SQL can not be empty!")
            return
        }
        var res:FMResultSet! = nil
        do{
            try self.validateSQL(sql)
            res = try self.executeQuery(sql, values: nil)
            DLOG("SQL:\(sql)\nexecute succeed!")
            handler(res)
            if res != nil {res.close()}
        }catch{
            assertionFailure("failed: \(error.localizedDescription)")
            handler(nil)
        }
    }
    /// select from
    ///
    /// - Parameters:
    ///   - tableName: table name
    ///   - keyArray: key array
    ///   - condition: where condition
    ///   - handler: func to process Result
    func select(from tableName:String,keys keyArray:[String],condition:String = "",_ handler:((FMResultSet?) -> Void)){
        
        guard tableName.count != 0 else {
            assertionFailure("select from table without table name")
            return
        }
        guard keyArray.count != 0 else {
            assertionFailure("select from table without columns key")
            return
        }
        var sql = "SELECT "
        var index = 0
        let count = keyArray.count
        for col in keyArray{
            sql += "\(col)"
            index += 1
            if index != count{
                sql += ","
            }
        }
        sql += " FROM \(tableName)"
        if condition.count > 0{
            sql += " \(condition) "
        }
        var res:FMResultSet! = nil
        do{
            try self.validateSQL(sql)
            res = try self.executeQuery(sql, values: nil)
            DLOG("SQL:\(sql)\nexecute succeed!")
            handler(res)
            if res != nil {res.close()}
        }catch{
            assertionFailure("failed: \(error.localizedDescription)")
            handler(nil)
        }
    }
    
    /// insert into
    ///
    /// - Parameters:
    ///   - tableName: table name
    ///   - dict: column key and value <key:value>
    /// - Returns: success or fail
    func insert(into tableName:String,dict:[String:AnyHashable]) -> Bool{
        
        guard tableName.count != 0 else {
            assertionFailure("insert into table without table name")
            return false
        }
        guard dict.count != 0 else {
            assertionFailure("insert into table without columns key and value")
            return false
        }
        
        let sql = SQLForInsert(tableName,colDict: dict)
        var values = [AnyHashable]()
        do{
            for col in dict.keys.sorted(){
                let value = dict[col]!
                switch value {
                case let b as Bool:
                    if b{
                        values.append(1)
                    }else{
                        values.append(0)
                    }
                default:
                    values.append(value)
                }
            }
            try self.validateSQL(sql)
            try self.executeUpdate(sql, values: values)
//            DLOG("SQL:\(sql)\nvalues:\(values)\nexecute succeed!")
        }catch{
            assertionFailure("failed: \(error.localizedDescription)")
            return false
        }
        return true
    }
    
    /// SQL for insert into
    ///
    /// - Parameters:
    ///   - tableName: table name
    ///   - colDict: column key and value
    /// - Returns: success or fail
    private func SQLForInsert(_ tableName:String,colDict:[String:AnyHashable]) -> String{
        
        var sql = "INSERT INTO \(tableName) ("
        
        let count = colDict.count
        var index = 0
        for col in colDict.keys.sorted(){
            sql += "\(col)"
            index += 1
            if index != count{
                sql += ","
            }
        }
        
        sql += ") VALUES ("
        for index in 1...count{
            sql += "?"
            if index != count{
                sql += ","
            }
        }
        sql += ")"
        
        return sql
    }
    
    /// update table
    ///
    /// - Parameters:
    ///   - tableName: table name
    ///   - condition: update condition
    ///   - dict: args
    /// - Returns: success or fail
    func update(_ tableName:String,condition:String = "",dict:[String:Any]) -> Bool{
        
        if self.tableExists(tableName){
            var sql = "UPDATE \(tableName) SET "
            var values = [Any]()
            var isFirst = true
            for key in dict.keys.sorted() {
                let v = dict[key]
                values.append(v as Any)
                if isFirst{
                    isFirst = false
                }else{
                    sql += ","
                }
                switch v {
                case _ as String:
                    sql += " \(key) = ? "
                default:
                    sql += " \(key) = ? "
                }
            }
            
            do{
                if condition.count > 0{
                    sql += " WHERE \(condition)"
                }
                try self.validateSQL(sql)
                try self.executeUpdate(sql, values: values)
                DLOG("SQL:\(sql)\nexecute succeed!")
            }catch{
                assertionFailure("failed: \(error.localizedDescription)")
                return false
            }
        }else{
            DLOG("\(tableName) not exists!")
        }
        return true
    }
    
    
    /// delete from
    ///
    /// - Parameter tableName: table name
    ///   - condition: where condition
    /// - Returns: success or fail
    func delete(from tableName:String,condition:String = "") -> Bool{
        
        if self.tableExists(tableName){
            var sql = "DELETE FROM \(tableName)"
            do{
                if condition.count > 0{
                    sql += " \(condition)"
                }
                try self.validateSQL(sql)
                try self.executeUpdate(sql, values: nil)
                DLOG("SQL:\(sql)\nexecute succeed!")
            }catch{
                assertionFailure("failed: \(error.localizedDescription)")
                return false
            }
        }else{
            DLOG("\(tableName) not exists!")
        }
        return true
    }
    
    
    
    /// drop table
    ///
    /// - Parameter name: table name
    /// - Returns: success or fail
    func drop(_ tableName:String) -> Bool{
        
        if self.tableExists(tableName){
            let sql = "DROP TABLE \(tableName)"
            do{
                try self.validateSQL(sql)
                try self.executeUpdate(sql, values: nil)
                DLOG("SQL:\(sql)\nexecute succeed!")
            }catch{
                assertionFailure("failed: \(error.localizedDescription)")
                return false
            }
        }else{
            DLOG("\(tableName) not exists!")
        }
        return true
    }
    
    
    /// renew table to latest structure by copying old table to a new table
    ///
    /// - Parameters:
    ///   - tableName: table name
    ///   - colDict: column name and type
    func renewTable(_ tableName:String,colDict:[String:FMDBDataType]){
        
        let copyTable = "\(tableName)2"
        if self.tableExists(copyTable){
            _ = drop(copyTable)
        }
        _ = create("\(tableName)2", colDict: colDict)
        let insertSQL = "INSERT INTO \(copyTable) SELECT * FROM \(tableName)"
        let alterSQL = "ALTER TABLE \(copyTable) RENAME TO \(tableName)"
        do{
            try self.validateSQL(insertSQL)
            try self.executeUpdate(insertSQL, values: nil)
            DLOG("SQL:\(insertSQL)\nexecute succeed!")
            _ = drop(tableName)
            try self.validateSQL(alterSQL)
            try self.executeUpdate(alterSQL, values: nil)
            DLOG("SQL:\(alterSQL)\nexecute succeed!")
        }catch{
            assertionFailure("failed: \(error.localizedDescription)")
        }
    }
    
    /// determine whether table struct has changed
    ///
    /// - Parameters:
    ///   - tableName: table name
    ///   - colDict: column name and type
    /// - Returns: changed or not
    func isTableStructChanged(_ tableName:String,colDict:[String:FMDBDataType]) -> Bool{
        
        let r = self.getTableSchema(tableName)
        var oldCols = [String:FMDBDataType]()
        while r.next() {
            let colName = r.string(forKey: "name")
            let colType = r.string(forKey: "type")
            let type = FMDBDataType.init(rawValue: colType) ?? FMDBDataType.null
            switch type {
            case .null:
                continue
            default:
                oldCols[colName] = type
            }
        }
        r.close()
        if oldCols.count == colDict.count{
            for key in colDict.keys{
                let oldType = oldCols[key]
                let newType = colDict[key]
                if let n = newType,let o = oldType{
                    if n != o{
                        return true // type for certain key is different
                    }
                    continue
                }
                return true // key is different
            }
            return false // nothing different
        }
        return true // keys count is different
    }
    
    /// create table
    ///
    /// - Parameters:
    ///   - name: table name
    ///   - colDict: column name and type dict <name:type>
    /// - Returns: success or fail
    func create(_ tableName:String,colDict:[String:FMDBDataType]) -> Bool{
        
        if self.tableExists(tableName){
            DLOG("\(tableName) already exists!")
            if isTableStructChanged(tableName, colDict: colDict){
//                assertionFailure("需要实现更完善的表复制逻辑")
//                renewTable(tableName, colDict: colDict)
            }
            return true
        }
        guard tableName.count != 0 else {
            assertionFailure("creating table without table name")
            return false
        }
        guard colDict.count != 0 else {
            assertionFailure("creating table without columns name and type")
            return false
        }
        
        let sql = SQLForCreate(tableName,colDict: colDict)
        do{
            try self.validateSQL(sql)
            try self.executeUpdate(sql, values: nil)
            DLOG("SQL:\(sql)\nexecute succeed!")
        }catch{
            assertionFailure("failed: \(error.localizedDescription)")
            return false
        }
        return true
    }
    
    /// SQL for create table
    ///
    /// - Parameters:
    ///   - name: table name
    ///   - colDict: column name and type dict <name:type>
    /// - Returns: SQL
    private func SQLForCreate(_ tableName:String,colDict:[String:FMDBDataType]) -> String{
        
        var sql = "CREATE TABLE IF NOT EXISTS \(tableName) ("
        
        let count = colDict.count
        var index = 0
        for col in colDict.keys{
            let type = colDict[col]!
            sql += "\(col) \(type.rawValue)"
            index += 1
            if index != count{
                sql += ","
            }
        }
        sql += ")"
        return sql
    }
}


extension FMDatabase{
    
    func data(forObject obj:Any) -> Data{
        return NSKeyedArchiver.archivedData(withRootObject: obj)
    }
}
