

import UIKit

extension UIView{
    
    func setFrameX(_ x:CGFloat){
        self.frame.origin.x = x
    }
    func setFrameY(_ y:CGFloat){
        self.frame.origin.y = y
    }
    func setFrameWidth(_ width:CGFloat){
        self.frame.size.width = width
    }
    func setFrameHeight(_ height:CGFloat){
        self.frame.size.height = height
    }
    
    func setCornerRounded(radius:CGFloat){
        self.layer.cornerRadius = radius
        self.layer.masksToBounds = true
    }
}
