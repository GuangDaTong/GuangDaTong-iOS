

import UIKit


class MShareMaker: NSObject {
    
    /// 调用分享的VC
    weak var ownerVC:MBaseVC?
    var shareSuccessBlock:(() -> Void)?
    /// 分享的类型
    var shareType = MShareType.look
    /// 分享的平台
    var platform = MSharePlatformType.qq
    /// 操作的类型对应ID
    var objID:String
    var title:String
    var shareDescription:String
    var url:String
    var image:UIImage
    
    init(ownerVC:MBaseVC,type:MShareType,platform:MSharePlatformType,objID:String,title:String,description:String,url:String,image:UIImage) {
        
        self.ownerVC = ownerVC
        self.platform = platform
        self.shareType = type
        self.objID = objID
        self.title = title
        self.shareDescription = description
        self.url = url
        self.image = image
    }
    
    func startShare(){
        
        self.ownerVC?.view.makeToastActivity()
        
        DispatchQueue.global().async {
            
            var platform:UMSocialPlatformType = UMSocialPlatformType.QQ
            
            switch self.platform {
            case MSharePlatformType.qq:
                platform = UMSocialPlatformType.QQ
            case MSharePlatformType.qzone:
                platform = UMSocialPlatformType.qzone
            case MSharePlatformType.wechat:
                platform = UMSocialPlatformType.wechatSession
            case MSharePlatformType.moment:
                if self.shareType != MShareType.ask{
                    self.title = self.shareDescription
                }
                platform = UMSocialPlatformType.wechatTimeLine
            case MSharePlatformType.link:
                self.copyLink()
                return
            }
            
            self.shareToSocialPlatform(platform)
        }
    }
    func copyLink(){
        
        DispatchQueue.global().async {
            
            UIPasteboard.general.string = self.url
            
            DispatchQueue.main.async{
                self.ownerVC?.view.hideToastActivity()
                self.ownerVC?.toastSuccess("链接地址已复制到粘贴板")
            }
        }
    }
    func shareToSocialPlatform(_ platform:UMSocialPlatformType){
        
        let msgObj:UMSocialMessageObject = UMSocialMessageObject.init()
        let shareObj:UMShareWebpageObject = UMShareWebpageObject.shareObject(withTitle: self.title, descr: self.shareDescription, thumImage: self.image)
        shareObj.webpageUrl = self.url
        msgObj.shareObject = shareObj
        
        DispatchQueue.main.async {
            
            let isInstall:Bool = UMSocialManager.default().isInstall(platform)
            if !isInstall{
                self.ownerVC?.view.hideToastActivity()
                self.ownerVC?.toastFail("分享失败，尚未安装该应用")
                return
            }
            
            self.ownerVC?.view.hideToastActivity()
            UMSocialManager.default().share(to: platform, messageObject: msgObj, currentViewController: self.ownerVC) { (data, error) in
                
                self.parseShareResponse(data, error: error)
            }
        }
    }
    func parseShareResponse(_ data:Any?,error:Error?){
        
        if let err:Error = error{
            let errStr:String = err.localizedDescription
            if errStr.contains("2009") || errStr.contains("2003"){
                // self.ownerVC?.toastFail("分享取消")
                return
            }
            self.ownerVC?.toastFail(err.localizedDescription)
            return
        }
        //                    if let res = data as? UMSocialShareResponse{
        //                        self.ownerVC?.toastSuccess("分享成功")
        //                        return
        //                    }
        DLOG(data)
        self.shareSuccessBlock?()
        self.ownerVC?.toastSuccess("分享成功")
        
        DispatchQueue.global().async { // 请求接口记录分享结果
            HttpController.shared.share(self.objID, type: self.shareType, platform: self.platform, success: { (json) in
                DLOG("分享<\(self.shareType.rawValue)>\(self.objID) 成功!")
            }, failure: { (err) in
                DLOG(err.localizedDescription)
            })
        }
    }
}
