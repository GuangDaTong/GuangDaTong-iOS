

#import <UIKit/UIKit.h>

#import <MobileCoreServices/MobileCoreServices.h>
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>

@interface ImagePickerVC:NSObject <UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (strong, nonatomic) UIImagePickerController* imagePickerController;
@property (copy, nonatomic) void(^filePickedBlock)(NSDictionary<NSString *,id> * fileInfo);

- (void)setupWithPresentVC:(UIViewController*)vc;

- (void)selectImageFromCamera;
- (void)selectImageFromAlbum;

@end
