

#import "ImagePickerVC.h"
#import <objc/runtime.h>
#import "UIColor+DYColor.h"
static void* imagePickerControllerKey = &imagePickerControllerKey;
static void* filePickedBlockKey = &filePickedBlockKey;

@interface ImagePickerVC ()

@property (weak, nonatomic) UIViewController* presentingVC;

- (void)setupImagePickerController;

@end


@implementation ImagePickerVC
@dynamic imagePickerController;
@dynamic filePickedBlock;

#pragma mark - ImagePicker


- (void)setupWithPresentVC:(UIViewController*)vc{
    
    self.presentingVC = vc;
    [self setupImagePickerController];
}

// http://www.jianshu.com/p/e70a184d1f32
- (UIImagePickerController*)imagePickerController{
    
    return objc_getAssociatedObject(self, imagePickerControllerKey);
}
- (void)setupImagePickerController{
    
    UIImagePickerController* imagePickerController = [[UIImagePickerController alloc] init];
    
    imagePickerController.delegate = self;
    imagePickerController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
//    imagePickerController.showsCameraControls = NO;
    
    objc_setAssociatedObject(self, imagePickerControllerKey, imagePickerController, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void (^)(NSDictionary<NSString *,id> *))filePickedBlock{
    
    return objc_getAssociatedObject(self, filePickedBlockKey);
}
- (void)setFilePickedBlock:(void (^)(NSDictionary<NSString *,id> *))filePickedBlock{
    objc_setAssociatedObject(self, filePickedBlockKey, filePickedBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
}


- (void)selectImageFromCamera
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        self.imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        //相机类型（拍照、录像...）字符串需要做相应的类型转换
        self.imagePickerController.mediaTypes = @[(NSString *)kUTTypeImage];//@[(NSString *)kUTTypeMovie,(NSString *)kUTTypeImage];
//        self.imagePickerController.allowsEditing = YES;
        //设置摄像头模式（拍照，录制视频）为录像模式
        self.imagePickerController.cameraCaptureMode = UIImagePickerControllerCameraCaptureModePhoto;
        [self.presentingVC presentViewController:self.imagePickerController animated:YES completion:nil];
    }else{
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:@"您的设备不支持相机功能！" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"好的" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:action];
        [self.presentingVC presentViewController:alert animated:YES completion:nil];
    }
}
- (void)selectImageFromAlbum{
    
//    self.imagePickerController.allowsEditing = YES;
    self.imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self.presentingVC presentViewController:self.imagePickerController animated:YES completion:^{
        self.imagePickerController.navigationBar.tintColor = [UIColor colorFromHexString:@"4990E2"];
    }];
}
//适用获取所有媒体资源，只需判断资源类型
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
//    NSString *mediaType=[info objectForKey:UIImagePickerControllerMediaType];
    //判断资源类型
//    if ([mediaType isEqualToString:(NSString *)kUTTypeImage]){
//        //        //如果是图片
//        //        self.imageView.image = info[UIImagePickerControllerEditedImage];
//        //        //压缩图片
//        //        NSData *fileData = UIImageJPEGRepresentation(self.imageView.image, 1.0);
//        //        //保存图片至相册
//        //        UIImageWriteToSavedPhotosAlbum(self.imageView.image, self, @selector(image:didFinishSavingWithError:contextInfo:), NULL);
//        //        //上传图片
//        //        [self uploadImageWithData:fileData];
//        
//    }else{
//        //        //如果是视频
//        //        NSURL *url = info[UIImagePickerControllerMediaURL];
//        //        //播放视频
//        //        _moviePlayer.contentURL = url;
//        //        [_moviePlayer play];
//        //        //保存视频至相册（异步线程）
//        //        NSString *urlStr = [url path];
//        //
//        //        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//        //            if (UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(urlStr)) {
//        //
//        //                UISaveVideoAtPathToSavedPhotosAlbum(urlStr, self, @selector(video:didFinishSavingWithError:contextInfo:), nil);
//        //            }
//        //        });
//        //        NSData *videoData = [NSData dataWithContentsOfURL:url];
//        //        //视频上传
//        //        [self uploadVideoWithData:videoData];
//    }
    if (self.filePickedBlock) {
        self.filePickedBlock(info);
    }
    [self.imagePickerController dismissViewControllerAnimated:YES completion:nil];
}
// 图片保存完毕的回调
- (void) image:(UIImage *) image didFinishSavingWithError:(NSError *) error contextInfo: (void *)contextInf{
//    DLog(@"");
}

// 视频保存完毕的回调
- (void)video:(NSString *)videoPath didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInf{
    if (error) {
        NSLog(@"保存视频过程中发生错误，错误信息:%@",error.localizedDescription);
    }else{
        NSLog(@"视频保存成功.");
    }
}
@end
