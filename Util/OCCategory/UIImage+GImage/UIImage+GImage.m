//
//  UIImage+GImage.m
//  GGo
//
//  Created by ficow on 2017/4/12.
//  Copyright © 2017年 ficow. All rights reserved.
//

#import "UIImage+GImage.h"

@implementation UIImage (GImage)

- (UIImage *)cornerRoundedImage {

    //开始对imageView进行画图
    UIGraphicsBeginImageContextWithOptions(self.size, NO, [UIScreen mainScreen].scale);
    //使用贝塞尔曲线画出一个圆形图
    CGRect rect = CGRectMake(0, 0, self.size.width, self.size.height);
    [[UIBezierPath bezierPathWithRoundedRect:rect
                                cornerRadius:self.size.width / 5] addClip];
    [self drawInRect:rect];
    
    UIImage* image = UIGraphicsGetImageFromCurrentImageContext();
    //结束画图
    UIGraphicsEndImageContext();
    return image;
}



/**
 压缩图片
 
 @return 压缩完成的图片
 */
- (UIImage *)zippedImage{
    
    if (!self) {
        return nil;
    }
    NSData* data = [self zippedImageData];
    UIImage* image = [UIImage imageWithData:data];
    return image;
}

/**
 压图片质量
 
 @return Data
 */
- (NSData *)zippedImageData{
    if (!self) {
        return nil;
    }
    UIImage* image = [self copy];
    CGFloat maxFileSize = 1 << 19; // 上传的图片最大512KB
    CGFloat compressionRate = 0.6f;
    NSData *compressedData = UIImageJPEGRepresentation(image, compressionRate);
    while ([compressedData length] > maxFileSize) {
        compressionRate *= compressionRate;
        UIImage* compressedImage = [[self class] compressImage:image newWidth:image.size.width * compressionRate];
        compressedData = UIImageJPEGRepresentation(compressedImage, compressionRate);
    }
    return compressedData;
}

/**
 *  等比缩放本图片大小
 *
 *  @param newImageWidth 缩放后图片宽度，像素为单位
 *
 *  @return self-->(image)
 */
+ (UIImage *)compressImage:(UIImage *)image newWidth:(CGFloat)newImageWidth
{
    if (!image) return nil;
    float imageWidth = image.size.width;
    float imageHeight = image.size.height;
    float width = newImageWidth;
    float height = image.size.height/(image.size.width/width);
    
    float widthScale = imageWidth /width;
    float heightScale = imageHeight /height;
    
    // 创建一个bitmap的context
    // 并把它设置成为当前正在使用的context
    UIGraphicsBeginImageContext(CGSizeMake(width, height));
    
    if (widthScale > heightScale) {
        [image drawInRect:CGRectMake(0, 0, imageWidth /heightScale , height)];
    }
    else {
        [image drawInRect:CGRectMake(0, 0, width , imageHeight /widthScale)];
    }
    
    // 从当前context中创建一个改变大小后的图片
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    // 使当前的context出堆栈
    UIGraphicsEndImageContext();
    
    return newImage;
    
}


@end
