//
//  UIImage+GImage.h
//  GGo
//
//  Created by ficow on 2017/4/12.
//  Copyright © 2017年 ficow. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (GImage)

- (UIImage *)cornerRoundedImage;


/**
 压缩图片
 
 @return 压缩完成的图片
 */
- (UIImage *)zippedImage;

/**
 压图片质量
 
 @return 压缩完成的图片的Data
 */
- (NSData *)zippedImageData;


@end
