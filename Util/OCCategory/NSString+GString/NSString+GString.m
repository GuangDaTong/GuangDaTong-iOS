//
//  NSString+GString.m
//  GGo
//
//  Created by ficow on 2017/4/16.
//  Copyright © 2017年 ficow. All rights reserved.
//

#import "NSString+GString.h"

@implementation NSString (GString)

- (CGSize)getSizeWithFont:(UIFont *)font constrainedToSize:(CGSize)size{
    CGSize resultSize = CGSizeZero;
    if (self.length <= 0) {
        return resultSize;
    }
    NSMutableParagraphStyle *style = [NSMutableParagraphStyle new];
    style.lineBreakMode = NSLineBreakByWordWrapping;
    resultSize = [self boundingRectWithSize:CGSizeMake(floor(size.width), floor(size.height))//用相对小的 width 去计算 height / 小 heigth 算 width
                                    options:(NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin)
                                 attributes:@{NSFontAttributeName: font,
                                              NSParagraphStyleAttributeName: style}
                                    context:nil].size;
    resultSize = CGSizeMake(floor(resultSize.width + 1), floor(resultSize.height + 1));//上面用的小 width（height） 来计算了，这里要 +1
    return resultSize;
}

- (NSString*)timeShortString{
    
    if (self.length < 19 || ![self containsString:@"T"]) {
        return @"";
    }
    // 2017-04-13T10:28:32.962633+08:00 -> 2017-04-13 10:28:32
    NSString* shortTimeStr = [[self substringToIndex:19] stringByReplacingOccurrencesOfString:@"T" withString:@" "];
    return shortTimeStr;
}
- (NSString*)timeShortStringWithoutYear{
    
    // 2017-04-13 10:28:32
    NSString* shortTimeStr = [self timeShortString];
    // 2017-04-13 10:28:32 -> 4-13 10:28:32
    NSString* timeWithoutYear;
    if ([timeWithoutYear characterAtIndex:0] == '0') {
        timeWithoutYear = [shortTimeStr substringFromIndex:7];
    }else{
        timeWithoutYear = [shortTimeStr substringFromIndex:6];
    }
    return timeWithoutYear;
}
//- (NSString*)yearShortStringWithoutSecond{
//    
//    // 2017-04-13 10:28:32
//    NSString* shortTimeStr = [self timeShortString];
//    // 2017-04-13 10:28:32 -> 4-13 10:28:32
//    NSString* yearShort = [shortTimeStr substringFromIndex:2];
//    // 2017-04-13 10:28:32 -> 4-13 10:28:32
//    return [yearShort substringToIndex:yearShort.length - 3];
//}
- (NSString*)shortenTimeForDate:(NSDate*)currentDate{
    
    // e.g: 2017-04-13 10:28:32
    NSString* shortTimeStr = [self timeShortString];
    if (shortTimeStr.length == 0) {
        return @"";
    }
    
    NSDateFormatter* format = [NSDateFormatter new];
    format.timeZone = NSTimeZone.systemTimeZone;
    format.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    NSDate* oldDate = [format dateFromString:shortTimeStr];
    if (!oldDate) {
        return @"";
    }

    const NSInteger MIN = 60;
    const NSInteger HOUR = 60 * MIN;
    const NSInteger DAY = 24 * HOUR;
    
    const NSInteger offset = [currentDate timeIntervalSinceDate:oldDate];
    
    if (offset < DAY) {
        if (offset < MIN) {
            return [NSString stringWithFormat:@"刚刚"];
        }
        format.dateFormat = @"dd";
        NSInteger nowDay = [format stringFromDate:currentDate].integerValue;
        NSInteger oldDay = [format stringFromDate:oldDate].integerValue;
        
        if (nowDay == oldDay) {
            format.dateFormat = @"HH:mm";
            NSString* time = [format stringFromDate:oldDate];
            return [NSString stringWithFormat:@"今天 %@",time];
        }
        format.dateFormat = @"HH:mm";
        NSString* time = [format stringFromDate:oldDate];
        return [NSString stringWithFormat:@"昨天 %@",time];
    }
    
    const NSInteger day = offset / DAY;
    if (day < 30) {
        return [NSString stringWithFormat:@"%ld天前",(long)day];
    }
    
    const NSInteger MON = 30 * DAY;
    const NSInteger mon = offset / MON;
    if (mon < 12) {
        return [NSString stringWithFormat:@"%ld个月前",(long)mon];
    }
    
    const NSInteger YEAR = 12 * MON;
    const NSInteger year = offset / YEAR;
    return [NSString stringWithFormat:@"%ld年前",(long)year];
}

@end
