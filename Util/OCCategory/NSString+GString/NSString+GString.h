//
//  NSString+GString.h
//  GGo
//
//  Created by ficow on 2017/4/16.
//  Copyright © 2017年 ficow. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NSString (GString)

- (CGSize)getSizeWithFont:(UIFont *)font constrainedToSize:(CGSize)size;

/**
 2017-04-13T10:28:32.962633+08:00 -> 2017-04-13 10:28:32

 @return yyyy-MM-dd hh:mm:ss
 */
- (NSString*)timeShortString;

/**
 2017-04-13 10:28:32 -> 4-13 10:28:32
 
 @return MM-dd hh:mm:ss
 */
- (NSString*)timeShortStringWithoutYear;


/**
 2017-04-13T10:28:32.962633+08:00 -> 17-04-13 10:28

 @return yy-MM-dd hh:mm
 */
//- (NSString*)yearShortStringWithoutSecond;



/**
 简洁日期字符串

 @param currentDate 当前日期
 @return 今天 hh:mm、x天前、x周前、x月前、x年前
 */
- (NSString*)shortenTimeForDate:(NSDate*)currentDate;


/*
 //dateFormatter 的格式可以指定以下内容
 G: 公元时代，例如AD公元
 yy: 年的后2位
 yyyy: 完整年
 MM: 月，显示为1-12
 MMM: 月，显示为英文月份简写,如 Jan
 MMMM: 月，显示为英文月份全称，如 Janualy
 dd: 日，2位数表示，如02
 d: 日，1-2位显示，如 2
 EEE: 简写星期几，如Sun
 EEEE: 全写星期几，如Sunday
 aa: 上下午，AM/PM
 H: 时，24小时制，0-23
 K：时，12小时制，0-11
 m: 分，1-2位
 mm: 分，2位
 s: 秒，1-2位
 ss: 秒，2位
 S: 毫秒
 */
@end
