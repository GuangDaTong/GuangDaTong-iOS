//
//  UIColor+DYColor.h
//  dianyi-operation
//
//  Created by ficow on 2017/8/21.
//  Copyright © 2017年 广州市壹度软件科技有限责任公司. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (DYColor)

/** 根据十六进制颜色值生成颜色，可以有#前缀*/
+ (instancetype)colorFromHexString:(NSString *)hexString;

+ (instancetype)colorWithR:(CGFloat)red G:(CGFloat)green B:(CGFloat)blue A:(CGFloat)alpha;
@end
