

import UIKit

class MPushManager: NSObject {

    static let shared = MPushManager()
    
    func process(pushInfo:[AnyHashable : Any]){
        
        //防止推送消息异常导致的崩溃
        if let type = pushInfo["type"] as? String{
            
            var index = -1
            
            switch type {
            case "leaveWords":
                index = 0
                break
            case "ask":
                index = 1
                break
            case "notice":
                index = 2
                break
            case "urgentAsk":
                if let ID = pushInfo["id"] as? String{
                    let rangeStr = (pushInfo["range"] as? String) ?? "school"
                    if rangeStr == "college"{
                        showUrgentAsk(ID: ID,isCollege: true)
                    }else{
                        showUrgentAsk(ID: ID,isCollege: false)
                    }
                }
                return
            default:
                showSchoolVC()
                return
            }
            self.showMsgVC(tabIndex: index)
        }
    }
    func showUrgentAsk(ID:String?,isCollege:Bool){
        
        let tab:UITabBarController = self.rootVC()
        if ID == nil{
            return
        }
        let askNavi:UINavigationController = tab.viewControllers?[2] as! UINavigationController
        askNavi.popToRootViewController(animated: false)
        let askVC:MAskVC = askNavi.viewControllers.first as! MAskVC
        if isCollege{
            askVC.pushAskRangeType = AskRangeType.college
        }else{
            askVC.pushAskRangeType = AskRangeType.school
        }
        askVC.pushAskID = ID!
        tab.selectedIndex = 0 // 切换Tab（原本在问问界面也可以刷新）
        tab.selectedIndex = 2
    }
    func showSchoolVC(){
        let tab:UITabBarController = self.rootVC()
        tab.selectedIndex = 0
    }
    func showMsgVC(tabIndex:Int){
        
        let tab:UITabBarController = self.rootVC()
        tab.selectedIndex = 0
        let schoolNavi:UINavigationController = tab.viewControllers?.first as! UINavigationController
        schoolNavi.popToRootViewController(animated: false)
        let school:MSchoolVC = schoolNavi.viewControllers.first as! MSchoolVC
        school.showMsgVC(animated: false,msgTab: tabIndex)
    }
    func rootVC() -> UITabBarController{
        
        let rootNavi:UINavigationController = WIN.rootViewController as! UINavigationController
        let tab:TabVC = rootNavi.viewControllers.first as! TabVC
        return tab
    }
}
