

import UIKit
import Alamofire

let NetworkStatusChangedNotification = Notification.Name.init("NetworkStatusChangedNotification")

class ReachabilityManager: NSObject {
    
    private let manager = Alamofire.NetworkReachabilityManager(host: "www.baidu.com")
    static let shared = ReachabilityManager()
    
    func listenForReachability() {
        
        self.manager?.listener = { status in
            
            DLOG("Network Status Changed: \(status)")
            var reachable = false
            switch status {
            case .reachable(.wwan),.reachable(.ethernetOrWiFi),.unknown:
                reachable = true
            case .notReachable:
                reachable = false
            }
            DispatchQueue.main.async{
                NotiCenter.post(name: NetworkStatusChangedNotification, object: nil, userInfo: ["reachable":reachable])
            }
        }
        
        self.manager?.startListening()
    }

}
