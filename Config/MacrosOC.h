

#ifndef MacrosOC_h
#define MacrosOC_h

/// 极光推送
#ifdef DEBUG
    #define JPushAppKey @"830864c3b1e485ba9117d120"
#else
    #define JPushAppKey @"13b58bff96976aedd5307f6c"
#endif


#ifdef DEBUG
#define DLog( s, ... ) NSLog( @"\n<%@:(%d) %s>\n%@", [[NSString stringWithUTF8String:__FILE__] lastPathComponent], __LINE__,__PRETTY_FUNCTION__, [NSString stringWithFormat:(s), ##__VA_ARGS__] )
#else
#define DLog( s, ... )
#endif


#if !(TARGET_IPHONE_SIMULATOR)
/** iPhone Simulator */
#endif

#endif /* MacrosOC_h */
