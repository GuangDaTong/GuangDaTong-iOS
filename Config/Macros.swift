import UIKit
import Alamofire

let uDefaults:UserDefaults = UserDefaults.standard

/// 在GCD主线程延迟执行闭包，单位：秒
func delay(second:Double,block:@escaping (() -> Void)){
    
    DispatchQueue.main.asyncAfter(deadline: .now() + second, execute: {
        block()
    })
}

/// 当前最顶层的ViewController
func currentTopVC() -> UIViewController?{
    
    let rootNavi:UINavigationController? = WIN.rootViewController as? UINavigationController
    let tab:TabVC? = rootNavi?.viewControllers.first as? TabVC
    let navi:UINavigationController? = tab?.selectedViewController as? UINavigationController
    return navi?.topViewController
}

var isSimulator:Bool = {
    var isSim:Bool = false
    #if arch(i386) || arch(x86_64)
        isSim = true
    #endif
    return isSim
}()

//判定真机
#if arch(arm) || arch(arm64)
#endif

// MARK: - DEBUG
func ERRLOG<T>(_ msg:T,file:String = #file,function:String = #function,line:Int = #line){
    #if DEBUG
        let file:String = (file as NSString).pathComponents.last!.replacingOccurrences(of: "swift", with: "")
        let time:String = DefaultDateFormatter.string(from: Date())
        let output:String =  "‼️\(time)\n\(file)\(function)[\(line)]:\n  \(msg)\n‼️"
        print(output)
    #endif
}

func DLOG<T>(_ msg:T,file:String = #file,function:String = #function,line:Int = #line){
    #if DEBUG
        let file:String = (file as NSString).pathComponents.last!.replacingOccurrences(of: "swift", with: "")
        let time:String = DefaultDateFormatter.string(from: Date())
        let output:String =  "⚠️\(time)\n\(file)\(function)[\(line)]:\n  \(msg)"
        print(output)
    #endif
}
func sendMsgToDebugServer<T>(_ msg:T,file:String = #file,function:String = #function,line:Int = #line){
    
    let url:URL = URL.init(string: "http://192.168.1.117:8080")!
    let file:String = (file as NSString).pathComponents.last!.replacingOccurrences(of: "swift", with: "")
    let info:String =  "⚠️\(file)\(function)[\(line)]:\n"
    DLOG(info)
    Alamofire.request(url,
                      method:.post,
                      parameters: ["Info":info,"Msg":msg],
                      encoding: JSONEncoding.default,
                      headers: nil)
        .responseJSON { (response) in
            DLOG(response.debugDescription)
    }

}

// UITextField右对齐输入空格不显示 http://www.jianshu.com/p/774fde80c1b0
let NonBreaking_Space:String = String.init(Character.init(UnicodeScalar.init(0x00a0)))

// MARK: - Device
let is_iPhone:Bool = UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.phone
let is_iPad:Bool = UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad
let is_RETINA:Bool = UIScreen.main.scale >= 2.0


// MARK: - Language
let SYSTEM_LANGUAGE = NSLocale.preferredLanguages.first!


// MARK: - Application
let APP:UIApplication = UIApplication.shared
let APPDELEGATE:AppDelegate = UIApplication.shared.delegate! as! AppDelegate
let WIN:UIWindow = APPDELEGATE.window!

// MARK: - Frame
let SCREEN_BOUNDS:CGRect = UIScreen.main.bounds
let SCREEN_WIDTH:CGFloat = SCREEN_BOUNDS.width
let SCREEN_HEIGHT:CGFloat = SCREEN_BOUNDS.height

let SCREEN_MAX_LENGTH:CGFloat = max(SCREEN_WIDTH, SCREEN_HEIGHT)
let SCREEN_MIN_WIDTH:CGFloat  = min(SCREEN_WIDTH,SCREEN_HEIGHT)

let SINGLE_LINE_WIDTH:CGFloat = 1 / UIScreen.main.scale
let SINGLE_LINE_ADJUST_OFFSET:CGFloat = SINGLE_LINE_WIDTH / 2

let kStatusBarHeight:CGFloat = 20
let kNaviBarHeight:CGFloat = 44
let kTopBarHeight:CGFloat = 64
let kTabBarHeight:CGFloat = 49
let kSearchBarHeight:CGFloat = 44
let kEnglishKeyboardHeight:CGFloat = 216
let kChineseKeyboardHeight:CGFloat = 252

func GetView_Width(view: UIView) -> CGFloat { return view.frame.size.width }
func GetView_Height(view: UIView) -> CGFloat { return view.frame.size.height }
func GetView_X(view: UIView) -> CGFloat { return view.frame.origin.x }
func GetView_Y(view: UIView) -> CGFloat { return view.frame.origin.y }

let iPhone6_Portrait_Height:CGFloat = 667
let iPhone6_Portrait_Width:CGFloat = 375


// MARK: - System Version
let APP_Current_Version:String = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String // a.b.c
let APP_Current_Build:String = Bundle.main.infoDictionary!["CFBundleVersion"] as! String // build x
let IOS_SYSTEM_VERSION:NSString = UIDevice.current.systemVersion as NSString
let IOS_VERSION:Int = Int(IOS_SYSTEM_VERSION.substring(to:IOS_SYSTEM_VERSION.range(of: ".").location))!

let is_iOS8:Bool = IOS_VERSION == 8
let is_iOS9:Bool = IOS_VERSION == 9
let is_iOS10:Bool = IOS_VERSION == 10
let is_iOS11:Bool = IOS_VERSION == 11

let isAfter_IOS7:Bool = IOS_VERSION > 7
let isAfter_IOS8:Bool = IOS_VERSION > 8
let isAfter_IOS9:Bool = IOS_VERSION > 9
let isAfter_IOS10:Bool = IOS_VERSION > 10

let is_iPhone_4_OR_Less:Bool = (is_iPhone && SCREEN_MAX_LENGTH < 568.0)
let is_iPhone_5:Bool = (is_iPhone && SCREEN_MAX_LENGTH == 568.0)
let is_iPhone_6:Bool = (is_iPhone && SCREEN_MAX_LENGTH == 667.0)
let is_iPhone_6P:Bool = (is_iPhone && SCREEN_MAX_LENGTH == 736.0)
let is_iPhone_X:Bool = (is_iPhone && SCREEN_MAX_LENGTH == 812.0)

// MARK: - Document Path
let PATH_OF_DOCUMENT:String = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!


// MARK: - Color
func RGBA(red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat) -> UIColor{
    return UIColor.init(red: red/255.0, green: green/255.0, blue: blue/255.0, alpha: alpha)
}


// MARK: - Time
let SECOND_OF_EIGHT_HOUR:TimeInterval = 8 * 60 * 60
let SECOND_OF_DAY:TimeInterval = 24.0 * 60 * 60

func timeStamp() -> String{
    
    let formatter:DateFormatter = DateFormatter()
    formatter.dateStyle = .medium
    formatter.timeStyle = .short
    formatter.dateFormat = "yyyy-MM-dd HH:mm:ss SSS"
    formatter.timeZone = NSTimeZone.system
    let date:Date = formatter.date(from: formatter.string(from: NSDate() as Date)) ?? Date()
    let stamp:String = "\(Int(date.timeIntervalSince1970))"
    return stamp
}

func dateFrom(_ string:String) -> Date{
    let format:DateFormatter = DateFormatter.init()
    format.timeZone = NSTimeZone.system
    format.dateFormat = "yyyy-MM-dd"
    let date:Date = format.date(from: string) ?? Date()
    return date
}

func stringDateOfToday() -> String{
    
    let format:DateFormatter = DateFormatter.init()
    format.timeZone = NSTimeZone.system
    format.dateFormat = "yyyy-MM-dd"
    
    let nowStr:String = format.string(from: NSDate() as Date)
    let today:String = (nowStr as NSString).substring(to: 10)
    return today
}

var DefaultDateFormatter:DateFormatter = {
    let formatter:DateFormatter = DateFormatter.init()
    formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    formatter.timeZone = NSTimeZone.system
//    formatter.timeZone = TimeZone.init(secondsFromGMT: 3600 * 8)
    return formatter
}()

func stringOfNow() -> String{
    
    let nowStr:String = DefaultDateFormatter.string(from: NSDate() as Date)
    return nowStr
}
