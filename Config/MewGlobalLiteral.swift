

import Foundation

// MARK: - Config
#if DEBUG
//var BaseURL = "http://172.22.27.12"
    let BaseURL:String = "https://debug.collegemew.com"
    let SALT:String = "mewmewstudio"
#else
    let BaseURL:String = "https://www.baidu.com"
    let SALT:String = ""
#endif

//let TestImageURL:String = "https://www.collegemew.com/favicon.ico"
let AppStoreDownloadURL:String = "https://itunes.apple.com/app/apple-store/id1280429561?pt=118801394&ct=directFromApp&mt=8"

// MARK: - Global
let AppThemeColor:UIColor = UIColor.colorWithHex(value: 0x72A438, alpha: 1)
let DefaultColorRed:UIColor = UIColor.colorWithHex(value: 0xFE3824, alpha: 1)
let DefaultColorGray:UIColor = UIColor.lightGray
let DefaultColorBlack:UIColor = UIColor.colorWithHex(value: 0x232323, alpha: 1)
let DefaultColorBlue:UIColor = UIColor.colorWithHex(value: 0x4A90E2, alpha: 1) 
let DefaultColorGreen:UIColor = UIColor.colorWithHex(value: 0x72A438, alpha: 1)
let DefaultColorYellow:UIColor = UIColor.colorWithHex(value: 0xF6A623, alpha: 1)

let DefaultSchool:String = "广州大学"
let DefaultPageSize:Int = 20

let JPushDebugAppKey:String = "830864c3b1e485ba9117d120"
let JPushReleaseAppKey:String = "13b58bff96976aedd5307f6c"

let UMengAppKey:String = "596759b05312dd59af001328"
let QQAppID:String = "1106322025" //友盟分享需要使用QQ的AppID
let QQAppKey:String = "0WU7XPdiCD254UpT"
let WechatAppKey:String = "wx874e7f1494be2bae"
let WechatSecretKey:String = ""

let DefaultAnimationDuration:TimeInterval = 0.3 // Toast提示动画
let DefaultMaxSendTextLength = 2048 //最长发送文本长度
let VerifyCodeGetDuration = 90 // 验证码获取间隔时间
let UploadRegIDDelayTime:Double = 5 //延迟上传用户极光推送ID、地理位置信息
let MaxLoginFailCount:Int = 3 //登录失败尝试次数
let DefaultSeparateLineColor = UIColor.colorWithHex(value: 0xBDBDBD, alpha: 1) //分割线颜色
let DefaultAskContentMaxLine:CGFloat = 8 //问问内容行数
let DefaultUserNameMaxLength:Int = 15 //用户昵称长度
let DefaultAskTitleMaxLength:Int = 15 //问问标题长度
let DefaultSearchTextMaxLength:Int = 40 //搜索内容长度
let DefaultImageCompressionRate:CGFloat = 0.7 //图片压缩率
let DefaultSmallestGrade:Int = 2000 //年级最小数
let DefaultCoinAmountDigitCount:Int = 4 //悬赏、赠送、索要喵币的最大金额位数
let DefaultSignupTextLength:Int = 20 //报名表输入内容最大文本长度


// 数据库
let DefaultURLForDB = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask,appropriateFor: nil, create: false).appendingPathComponent("fmdb.sqlite")
let DBTableNameForSchool:String = "school"
let DBTableNameForDiscovery:String = "discovery"
let DBTableNameForAsk:String = "ask"
let DBTableNameForMsgCenterLeaveMsg:String = "leaveMsg"
let DBTableNameForMsgCenterAsk:String = "askMsg"
let DBTableNameForMsgCenterNotice:String = "noticeMsg"

// UserDefaults
var isUserNotLogin:Bool{
    get{
        return uUserID == ""
    }
}
var uUserID:String{
    get{
        return uDefaults.string(forKey: "UserID") ?? ""
    }
}
var uUserToken:String{
    get{
        return uDefaults.string(forKey: "UserToken") ?? ""
    }
}
var uUserIcon:String{
    get{
        return uDefaults.string(forKey: "UserIcon") ?? ""
    }
}
var uUserName:String{
    get{
        return uDefaults.string(forKey: "UserName") ?? ""
    }
}
var uUserCoins:String{
    get{
        return uDefaults.string(forKey: "UserCoins") ?? ""
    }
}
var isSoundEnabled:Bool{
    get{
       return uDefaults.bool(forKey: "isSoundEnabled")
    }
}
var isVibrationEnabled:Bool{
    get{
        return uDefaults.bool(forKey: "isVibrationEnabled")
    }
}

let DefaultUserIcon:UIImage = UIImage.init(named: "me_userIcon")!
let DefaultImagePlaceholder:UIImage? = UIImage.init(named: "no_image")

/// 图片链接字符串
func showImageBrowser(withURLStr url:String){

    DispatchQueue.global().async {
        
        var photos:[SKPhoto] = [SKPhoto]()
        let photo:SKPhoto = SKPhoto.photoWithImageURL(url)
        photo.shouldCachePhotoURLImage = true
        photos.append(photo)
        presentImageBrowser(withPhotos: photos,initIndex: 0)
    }
}
/// 0为起始的索引，图片链接数组
func showImageBrowser(currentIndex:Int,urls:[String]){
    
    DispatchQueue.global().async {
        
        var photos:[SKPhoto] = [SKPhoto]()
        for urlStr in urls {
            let photo:SKPhoto = SKPhoto.photoWithImageURL(urlStr)
            photo.shouldCachePhotoURLImage = true
            photos.append(photo)
        }
        presentImageBrowser(withPhotos: photos,initIndex: currentIndex)
    }
}
/// 0为起始的索引，图片对象数组
func showImageBrowser(currentIndex:Int,images:[UIImage]){
    
    DispatchQueue.global().async {
        
        var photos:[SKPhoto] = [SKPhoto]()
        for img in images {
            let photo:SKPhoto = SKPhoto.photoWithImage(img)
            photo.shouldCachePhotoURLImage = true
            photos.append(photo)
        }
        presentImageBrowser(withPhotos: photos,initIndex: currentIndex)
    }
}
func presentImageBrowser(withPhotos photos:[SKPhoto],initIndex:Int){
    
    let browser:SKPhotoBrowser = SKPhotoBrowser.init(photos: photos)
    browser.initializePageIndex(initIndex)
    
    DispatchQueue.main.async{
        let topVC:UIViewController? = currentTopVC()
        topVC?.present(browser, animated: true, completion: nil)
    }
}
// MARK: - Notification
let NotiCenter:NotificationCenter = NotificationCenter.default

let UserDidLoginNotification:Notification.Name = Notification.Name.init("UserDidLoginNotification")
let UserDidLogoutNotification:Notification.Name = Notification.Name.init("UserDidLogoutNotification")
let UserDidModifyInfoNotification:Notification.Name = Notification.Name.init("UserDidModifyInfoNotification")
let UserDidReceiverRemoteNotification:Notification.Name = Notification.Name.init("UserDidReceiverRemoteNotification")
let UserAlteredBlacklistNotification:Notification.Name = Notification.Name.init("UserAlteredBlacklistNotification")
let UserSentLeaveMsgNotification:Notification.Name = Notification.Name.init("UserSentLeaveMsgNotification")
let UserDidAlterContactNotification:Notification.Name = Notification.Name.init("UserDidAlterContactNotification")
let UserLocationDidObtainNotification:Notification.Name = Notification.Name.init("UserLocationDidObtainNotification")
