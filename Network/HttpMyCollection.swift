

import Foundation
import Alamofire
import SwiftyJSON

extension HttpController{
    
    
    /// 问问收藏列表
    ///
    /// - Parameters:
    ///   - pageIndex: 页索引
    ///   - successBlock: 成功
    ///   - failureBlock: 失败
    func askCollectList(pageIndex:Int,successBlock:@escaping ((JSON) -> Void),failure failureBlock:@escaping ((Error) -> Void)){
        
        let token:String = uUserToken
        let sign:String = (SALT + token).MD5()
        let url:String = "/list/favorite/ask"
        
        let params:Parameters = ["pageIndex":pageIndex,"pageSize":DefaultPageSize,"sign":sign]
        
        self.requestWith(url, method: .post, parameters: params, headers: nil, success: { (json) in
            successBlock(json)
        }, failure: { (err) in
            failureBlock(err)
        })
    }
    
    
    /// 校园收藏列表
    ///
    /// - Parameters:
    ///   - type: 校园列表类型
    ///   - pageIndex: 页索引
    ///   - successBlock: 成功
    ///   - failureBlock: 失败
    func schoolCollectList(type:SchoolListType,pageIndex:Int,successBlock:@escaping ((JSON) -> Void),failure failureBlock:@escaping ((Error) -> Void)){
        
        let token:String = uUserToken
        let sign:String = (SALT + token).MD5()
        
        var url:String = "/list/favorite"
        switch type {
        case .guidance:
            url += "/guidance"
        case .summary:
            url += "/summary"
        case .club:
            url += "/club"
        case .look:
            url += "/look"
        }
        
        let params:Parameters = ["pageIndex":pageIndex,"pageSize":DefaultPageSize,"sign":sign]
        
        self.requestWith(url, method: .post, parameters: params, headers: nil, success: { (json) in
            successBlock(json)
        }, failure: { (err) in
            failureBlock(err)
        })
    }
    
    
    /// 发现收藏列表
    ///
    /// - Parameters:
    ///   - type: 发现列表类型
    ///   - pageIndex: 页索引
    ///   - successBlock: 成功
    ///   - failureBlock: 失败
    func discoveryCollectList(type:DiscoveryListType,pageIndex:Int,successBlock:@escaping ((JSON) -> Void),failure failureBlock:@escaping ((Error) -> Void)){
        
        let token:String = uUserToken
        let sign:String = (SALT + token).MD5()
        
        var url:String = "/list/favorite"
        switch type {
        case .partTime:
            url += "/partTimeJob"
        case .training:
            url += "/training"
        case .activity:
            url += "/activity"
        }
        
        let params:Parameters = ["pageIndex":pageIndex,"pageSize":DefaultPageSize,"sign":sign]
        
        self.requestWith(url, method: .post, parameters: params, headers: nil, success: { (json) in
            successBlock(json)
        }, failure: { (err) in
            failureBlock(err)
        })
    }
    
    
    /// 删除收藏
    ///
    /// - Parameters:
    ///   - type: 收藏类型
    ///   - IDs: ID字符串数组
    ///   - successBlock: 成功
    ///   - failureBlock: 失败
    func deleteCollect(type:MMineCollectionListType,IDs:[String],successBlock:@escaping ((JSON) -> Void),failure failureBlock:@escaping ((Error) -> Void)){
        
        let token:String = uUserToken
        let sign:String = (SALT + token).MD5()
        var url:String = "/delete/favorite"
        switch type {
        case .ask:
            url += "/ask"
        case .guidance:
            url += "/guidance"
        case .summary:
            url += "/summary"
        case .club:
            url += "/club"
        case .look:
            url += "/look"
        case .partTime:
            url += "/partTimeJob"
        case .training:
            url += "/training"
        case .activity:
            url += "/activity"
        }
        
        let params:Parameters = ["IDs":IDs,"sign":sign]
        
        self.requestWith(url, method: .post, parameters: params, headers: nil, success: { (json) in
            successBlock(json)
        }, failure: { (err) in
            failureBlock(err)
        })
    }
}
