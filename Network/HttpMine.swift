

import Foundation
import Alamofire
import SwiftyJSON

extension HttpController{
    
    /// 我的问问
    ///
    /// - Parameters:
    ///   - pageIndex: 页索引
    ///   - successBlock: 成功
    ///   - failureBlock: 失败
    func myAskList(pageIndex:Int,successBlock:@escaping ((JSON) -> Void),failure failureBlock:@escaping ((Error) -> Void)){
        
        let token:String = uUserToken
        let sign:String = (SALT + token).MD5()
        let url:String = "/list/myAsk"
        
        let params:Parameters = ["pageIndex":pageIndex,"pageSize":DefaultPageSize,"sign":sign]
        
        self.requestWith(url, method: .post, parameters: params, headers: nil, success: { (json) in
            successBlock(json)
        }, failure: { (err) in
            failureBlock(err)
        })
    }
    
    
    /// 喵币收益记录
    ///
    /// - Parameters:
    ///   - pageIndex: 页索引
    ///   - successBlock: 成功
    ///   - failureBlock: 失败
    func accountBook(pageIndex:Int,successBlock:@escaping ((JSON) -> Void),failure failureBlock:@escaping ((Error) -> Void)){
        
        let token:String = uUserToken
        let sign:String = (SALT + token).MD5()
        let url:String = "/accountBook"
        
        let params:Parameters = ["pageIndex":pageIndex,"pageSize":DefaultPageSize,"sign":sign]
        
        self.requestWith(url, method: .post, parameters: params, headers: nil, success: { (json) in
            successBlock(json)
        }, failure: { (err) in
            failureBlock(err)
        })
    }
    
    
    /// 通讯录
    ///
    /// - Parameters:
    ///   - pageIndex: 页索引
    ///   - successBlock: 成功
    ///   - failureBlock: 失败
    func contactList(pageIndex:Int,successBlock:@escaping ((JSON) -> Void),failure failureBlock:@escaping ((Error) -> Void)){
        
        let token:String = uUserToken
        let sign:String = (SALT + token).MD5()
        let url:String = "/list/friends"
        
        let params:Parameters = ["pageIndex":pageIndex,"pageSize":DefaultPageSize,"sign":sign]
        
        self.requestWith(url, method: .post, parameters: params, headers: nil, success: { (json) in
            successBlock(json)
        }, failure: { (err) in
            failureBlock(err)
        })
    }
    
    /// 搜索用户
    ///
    /// - Parameters:
    ///   - keyword: 关键词
    ///   - pageIndex: 页索引
    func searchUser(keyword:String,pageIndex:Int,successBlock:@escaping ((JSON) -> Void),failure failureBlock:@escaping ((Error) -> Void)){
        
        let token:String = uUserToken
        let sign:String = (SALT + token).MD5()
        let url:String = "/list/search/users"
        
        let params:Parameters = ["key":keyword,"pageIndex":pageIndex,"pageSize":DefaultPageSize,"sign":sign]
        
        self.requestWith(url, method: .post, parameters: params, headers: nil, success: { (json) in
            successBlock(json)
        }, failure: { (err) in
            failureBlock(err)
        })
    }
    
    
    /// 删除（多个）联系人
    ///
    /// - Parameters:
    ///   - userIDs: 联系人ID字符串数组
    ///   - successBlock: 成功
    ///   - failureBlock: 失败
    func deleteContact(userIDs:[String],successBlock:@escaping ((JSON) -> Void),failure failureBlock:@escaping ((Error) -> Void)){
        
        let token:String = uUserToken
        let sign:String = (SALT + token).MD5()
        let url:String = "/delete/friends"
        
        let params:Parameters = ["IDs":userIDs,"sign":sign]
        
        self.requestWith(url, method: .post, parameters: params, headers: nil, success: { (json) in
            successBlock(json)
        }, failure: { (err) in
            failureBlock(err)
        })
    }
    
    
    /// 我的黑名单
    func blacklist(successBlock:@escaping ((JSON) -> Void),failure failureBlock:@escaping ((Error) -> Void)){
        
        let token:String = uUserToken
        let sign:String = (SALT + token).MD5()
        let url:String = "/list/blacklist"
        
        let params:Parameters = ["sign":sign]
        
        self.requestWith(url, method: .post, parameters: params, headers: nil, success: { (json) in
            successBlock(json)
        }, failure: { (err) in
            failureBlock(err)
        })
    }
    
    /// 从黑名单删除
    ///
    /// - Parameters:
    ///   - userID: 用户ID
    func deleteFromBlacklist(userID:String,successBlock:@escaping ((JSON) -> Void),failure failureBlock:@escaping ((Error) -> Void)){
        
        let token:String = uUserToken
        let sign:String = (SALT + token).MD5()
        let url:String = "/delete/blacklist"
        
        let params:Parameters = ["IDs":[userID],"sign":sign]
        
        self.requestWith(url, method: .post, parameters: params, headers: nil, success: { (json) in
            successBlock(json)
        }, failure: { (err) in
            failureBlock(err)
        })
    }
}
