

import Foundation
import Alamofire
import SwiftyJSON

/// 举报类型
///
/// - ask: 问问
/// - comment: 评论
/// - user: 用户
enum InformType {
    case ask
    case comment
    case user
}

/// 赞类型
enum ZanType:String {
    case none
    case user = "users"
    case guidance
    case summary
    case club
    case look
    case ask
    case activity
    case partTime = "partTimeJob"
    case training
}

/// 收藏类型
enum CollectType:String{
    case guidance
    case summary
    case club
    case look
    case ask
    case activity
    case partTime = "partTimeJob"
    case training
}

/// 分享类型
enum MShareType:String{
    case guidance
    case summary
    case club
    case look
    case ask
    case activity
    case partTime = "partTimeJob"
    case training
    case app = "App"
}

extension HttpController{
    
    /// 检查APP版本
    ///
    /// - Parameters:
    ///   - success: 成功
    ///   - failure: 失败
    func checkVersion(success:@escaping ((JSON) -> Void),failure:@escaping ((Error) -> Void)){
        
        let url:String = "/checkVersion"
        let sign:String = SALT.MD5()
        let paras:Parameters = ["OSType":"ios","sign":sign]
        requestWith(url, method: .post, parameters: paras, headers: nil, success: { (json) in
            success(json)
        }, failure: { (err) in
            failure(err)
        })
        
    }
    
    
    /// 分享
    ///
    /// - Parameters:
    ///   - objID: objID
    ///   - type: 分享类型
    ///   - platform: 分享平台
    func share(_ objID:String,type:MShareType,platform:MSharePlatformType,success:@escaping ((JSON) -> Void),failure:@escaping ((Error) -> Void)){
        
        let url:String = "/share/" + type.rawValue
        let token:String = uUserToken
        let sign:String = (SALT + objID).MD5()
        let paras:Parameters = ["objectID":objID,"shareWay":platform.rawValue,"token":token,"sign":sign]
        
        requestWith(url, method: .post, parameters: paras, headers: nil, success: { (json) in
            success(json)
        }, failure: { (err) in
            failure(err)
        })
        
    }
    
    
    /// 上传用户信息(Location,RegistrationID)
    ///
    /// - Parameters:
    ///   - regID: 极光推送RegistrationID
    ///   - infoDict: 参数字典
    func uploadUserInfo(_ regID:String,locationDict:Parameters,success:@escaping ((JSON) -> Void),failure:@escaping ((Error) -> Void)){
        
        let url:String = "/pushInfo"
        let userID:String = uUserID
        let sign:String = (SALT + regID).MD5()
        var params:Parameters = ["userID":userID,"regID":regID,
                                "OSType":"ios","version":APP_Current_Version,
                                "sign":sign]
        
        for location in locationDict{
            params[location.key] = location.value
        }
        
        requestWith(url, method: .post, parameters: params, headers: nil, success: { (json) in
            success(json)
        }, failure: { (err) in
            failure(err)
        })
        
    }
    
    
    /// 获取喵币数量、发帖子扣喵币规则
    ///
    /// - Parameters:
    ///   - successBlock: 成功
    ///   - failureBlock: 失败
    func getCoins(successBlock:@escaping ((Int,MAskRuleItem) -> Void),failure failureBlock:@escaping ((Error) -> Void)){
        
        if isUserNotLogin{
            return
        }
        
        let token:String = uUserToken
        let sign:String = (SALT + token).MD5()
        let url:String = "/getCoinsAndRules"
        
        let params:Parameters = ["sign":sign]
        
        self.requestWith(url, method: .post, parameters: params, headers: nil, success: { (json) in
            
            let res:JSON = json["result"]
            let coins:Int = res["coins"].intValue
            
            let rules:JSON = res["rules"]
            let normal:JSON = rules["normal"]
            let normalItem:MAskSubRuleItem = MAskSubRuleItem.init(college: normal["college"].intValue, school: normal["school"].intValue)
            let urgent:JSON = rules["urgent"]
            let urgentItem:MAskSubRuleItem = MAskSubRuleItem.init(college: urgent["college"].intValue, school: urgent["school"].intValue)
            let item:MAskRuleItem = MAskRuleItem.init(normal: normalItem, urgent: urgentItem)
            successBlock(coins,item)
        }, failure: { (err) in
            failureBlock(err)
        })
    }
    
    /// 举报
    ///
    /// - Parameters:
    ///   - type: 举报类型
    ///   - ID: ID
    ///   - reason: 举报理由
    ///   - successBlock: 成功
    ///   - failureBlock: 失败
    func inform(type:InformType,ID:String,reason:String,successBlock:@escaping ((JSON) -> Void),failure failureBlock:@escaping ((Error) -> Void)){
        
        let token:String = uUserToken
        let sign:String = (SALT + token).MD5()
        var url:String = ""
        switch type {
        case .ask:
            url = "/inform/ask"
        case .comment:
            url = "/inform/commentAsk"
        case .user:
            url = "/inform/users"
            break
        }
        
        let params:Parameters = ["id":ID,"reason":reason,"sign":sign]
        
        self.requestWith(url, method: .post, parameters: params, headers: nil, success: { (json) in
            successBlock(json)
        }, failure: { (err) in
            failureBlock(err)
        })
    }
    
    
    /// 赞
    ///
    /// - Parameters:
    ///   - type: 类型
    ///   - ID: ID
    ///   - successBlock: 成功
    ///   - failureBlock: 失败
    func zan(type:ZanType,ID:String,successBlock:@escaping ((JSON) -> Void),failure failureBlock:@escaping ((Error) -> Void)){
        
        let token:String = uUserToken
        let sign:String = (SALT + token).MD5()
        let url:String = "/zan/" + type.rawValue
        
        let params:Parameters = ["id":ID,"sign":sign]
        
        self.requestWith(url, method: .post, parameters: params, headers: nil, success: { (json) in
            successBlock(json)
        }, failure: { (err) in
            failureBlock(err)
        })
    }
    
    
    /// 收藏
    ///
    /// - Parameters:
    ///   - type: 类型
    ///   - ID: ID
    ///   - successBlock: 成功
    ///   - failureBlock: 失败
    func collect(type:CollectType,ID:String,successBlock:@escaping ((JSON) -> Void),failure failureBlock:@escaping ((Error) -> Void)){
        
        let token:String = uUserToken
        let sign:String = (SALT + token).MD5()
        let url:String = "/favorite/" + type.rawValue
        
        let params:Parameters = ["id":ID,"sign":sign]
        
        self.requestWith(url, method: .post, parameters: params, headers: nil, success: { (json) in
            successBlock(json)
        }, failure: { (err) in
            failureBlock(err)
        })
    }
    
    
    /// 信息有误
    ///
    /// - Parameters:
    ///   - type: 类型
    ///   - description: 描述文字
    ///   - objID: 信息有误的校园item的ID
    ///   - successBlock: 成功
    ///   - failureBlock: 失败
    func reportError(type:ZanType,description:String,objID:String,successBlock:@escaping ((JSON) -> Void),failure failureBlock:@escaping ((Error) -> Void)){
        
        let token:String = uUserToken
        let sign:String = (SALT + objID).MD5()
        var url:String = "/report"
        switch type {
        case .guidance:
            url += "/guidance"
        case .summary:
            url += "/summary"
        case .club:
            url += "/club"
        case .look:
            url += "/look"
        default:
            break
        }
        
        let params:Parameters = ["description":description,"objectID":objID,"token":token,"sign":sign]
        
        self.requestWith(url, method: .post, parameters: params, headers: nil, success: { (json) in
            successBlock(json)
        }, failure: { (err) in
            failureBlock(err)
        })
    }
    
    
    /// 联系客服
    ///
    /// - Parameters:
    ///   - type: 类型
    ///   - description: 描述
    ///   - objID: 发现item的ID
    ///   - successBlock: 成功
    ///   - failureBlock: 失败
    func contactSystem(type:ZanType,description:String,objID:String,successBlock:@escaping ((JSON) -> Void),failure failureBlock:@escaping ((Error) -> Void)){
        
        let token:String = uUserToken
        let sign:String = (SALT + token).MD5()
        var url:String = "/contactService"
        switch type {
        case .partTime:
            url += "/partTimeJob"
        case .activity:
            url += "/activity"
        case .training:
            url += "/training"
        default:
            break
        }
        
        let params:Parameters = ["words":description,"objectID":objID,"sign":sign]
        
        self.requestWith(url, method: .post, parameters: params, headers: nil, success: { (json) in
            successBlock(json)
        }, failure: { (err) in
            failureBlock(err)
        })
    }
}
