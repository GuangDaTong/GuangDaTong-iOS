

import Foundation
import Alamofire
import SwiftyJSON

extension HttpController{
    
    /// 登录
    ///
    /// - Parameters:
    ///   - account: 用户名
    ///   - pwd: 密码
    func loginWithAccount(_ account:String,pwd:String,successBlock:@escaping ((JSON) -> Void),failure failureBlock:@escaping ((Error) -> Void)){
        
        let sign:String = (SALT + pwd).MD5()
        let url:String = "/login"
        let params:Parameters = ["account":account,"password":pwd,"sign":sign]
        
        self.requestWith(url, method: .post, parameters: params, headers: nil, success: { (json) in
            successBlock(json)
        }, failure: { (err) in
            failureBlock(err)
        })
    }
    
    
    /// 是否已经用QQ登录过
    ///
    /// - Parameters:
    ///   - uuid: QQ帐号UID
    func isQQRegister(uuid:String,successBlock:@escaping ((JSON) -> Void),failure failureBlock:@escaping ((Error) -> Void)){
        
        let sign:String = (SALT + uuid).MD5()
        let url:String = "/isQQRegister"
        let params:Parameters = ["uuid":uuid,"sign":sign]
        
        self.requestWith(url, method: .post, parameters: params, headers: nil, success: { (json) in
            successBlock(json)
        }, failure: { (err) in
            failureBlock(err)
        })
    }
    
    
    /// 通过QQ登录
    ///
    /// - Parameters:
    ///   - uuid: QQ帐号UID
    ///   - name: 用户昵称
    ///   - college: 用户的学院信息
    func loginByQQ(_ uuid:String,name:String,college:String,gender:String,successBlock:@escaping ((JSON) -> Void),failure failureBlock:@escaping ((Error) -> Void)){
        
        let sign:String = (SALT + uuid).MD5()
        let url:String = "/login/qq"
        let params:Parameters = ["uuid":uuid,"nickname":name,"college":college,"gender":gender,"sign":sign]
        
        self.requestWith(url, method: .post, parameters: params, headers: nil, success: { (json) in
            successBlock(json)
        }, failure: { (err) in
            failureBlock(err)
        })
    }
    
    
    /// 注册
    ///
    /// - Parameters:
    ///   - account: 帐号
    ///   - pwd: 密码
    ///   - institute: 学院
    ///   - success: 成功
    ///   - failure: 失败
    func register(account:String,pwd:String,institute:String,token:String,success:@escaping ((JSON) -> Void),failure:@escaping ((Error) -> Void)){
        
        let url:String = "/register"
        let sign:String = (SALT + token).MD5()
        let paras:Parameters = ["account":account,"password":pwd,"college":institute,"school":DefaultSchool,"sign":sign]
        requestWith(url, method: .post, parameters: paras, headers: nil, success: { (json) in
            success(json)
        }, failure: { (err) in
            failure(err)
        })
    }
    
    
    /// 获取学院列表
    ///
    /// - Parameters:
    ///   - success: 成功
    ///   - failure: 失败
    func instituteList(success:@escaping ((JSON) -> Void),failure:@escaping ((Error) -> Void)){
        
        let url:String = "/list/college"
        let sign:String = SALT.MD5()
        let paras:Parameters = ["school":DefaultSchool,"sign":sign]
        requestWith(url, method: .post, parameters: paras, headers: nil, success: { (json) in
            success(json)
        }, failure: { (err) in
            failure(err)
        })
    }
    
    /// 提交验证码
    ///
    /// - Parameters:
    ///   - phone: 手机号
    ///   - code: 验证码
    ///   - success: 成功
    ///   - failure: 失败
    func submitVerifyCode(_ phone:String,code:String,success:@escaping ((JSON) -> Void),failure:@escaping ((Error) -> Void)){
        
        let url:String = "/checkVerifyCode"
        let sign:String = (SALT + phone).MD5()
        let paras:Parameters = ["phone":phone,"code":code,"sign":sign]
        requestWith(url, method: .post, parameters: paras, headers: nil, success: { (json) in
            success(json)
        }, failure: { (err) in
            failure(err)
        })
        
    }
    
    
    /// 手机号是否已注册
    ///
    /// - Parameters:
    ///   - phone: 手机号
    ///   - success: 成功
    ///   - failure: 失败
    func isAccountExist(_ phone:String,success:@escaping ((JSON) -> Void),failure:@escaping ((Error) -> Void)){
        
        let url:String = "/isAccountExist"
        let sign:String = (SALT + phone).MD5()
        let paras:Parameters = ["account":phone,"sign":sign]
        requestWith(url, method: .post, parameters: paras, headers: nil, success: { (json) in
            success(json)
        }, failure: { (err) in
            failure(err)
        })
        
    }
    
    /// 重置密码
    ///
    /// - Parameters:
    ///   - account: 用户名
    ///   - pwd: 密码
    ///   - token: 后端验证码，不同于userToken
    ///   - successBlock: 成功
    ///   - failureBlock: 失败
    func resetPwd(account:String,pwd:String,token:String,successBlock:@escaping ((JSON) -> Void),failure failureBlock:@escaping ((Error) -> Void)){
        
        let sign:String = (SALT + token).MD5()
        let url:String = "/resetPassword"
        let params:Parameters = ["account":account,"password":pwd,"sign":sign]
        
        self.requestWith(url, method: .post, parameters: params, headers: nil, success: { (json) in
            successBlock(json)
        }, failure: { (err) in
            failureBlock(err)
        })
    }
    
    
    /// 修改密码
    ///
    /// - Parameters:
    ///   - newPwd: 新密码
    ///   - oldPwd: 旧密码
    ///   - successBlock: 成功
    ///   - failureBlock: 失败
    func alterPwd(newPwd:String,oldPwd:String,successBlock:@escaping ((JSON) -> Void),failure failureBlock:@escaping ((Error) -> Void)){
        
        let token:String = uUserToken
        let sign:String = (SALT + token).MD5()
        let url:String = "/alterPassword"
        let params:Parameters = ["newPwd":newPwd,"oldPwd":oldPwd,"sign":sign]
        
        self.requestWith(url, method: .post, parameters: params, headers: nil, success: { (json) in
            successBlock(json)
        }, failure: { (err) in
            failureBlock(err)
        })
    }
    
    
    /// 其他用户的资料
    ///
    /// - Parameters:
    ///   - id: 用户ID
    ///   - successBlock: 成功
    ///   - failureBlock: 失败
    func userInfo(id:String,successBlock:@escaping ((JSON) -> Void),failure failureBlock:@escaping ((Error) -> Void)){
        
        let token:String = uUserToken
        let sign:String = (SALT + id).MD5()
        let url:String = "/data/person"
        let params:Parameters = ["token":token,"id":id,"sign":sign]
        
        self.requestWith(url, method: .post, parameters: params, headers: nil, success: { (json) in
            successBlock(json)
        }, failure: { (err) in
            failureBlock(err)
        })
    }
    
    
    /// 修改个人信息
    ///
    /// - Parameters:
    ///   - nickname: 昵称
    ///   - college: 学院
    ///   - gender: 性别
    ///   - grade: 年级
    ///   - custom: 更多信息
    ///   - iconImage: 头像图片
    ///   - successBlock: 成功
    ///   - failureBlock: 失败
    func alterSelfInfo(nickname:String,college:String,gender:String,grade:String,custom:String,iconImage:UIImage?,successBlock:@escaping ((JSON) -> Void),failure failureBlock:@escaping ((Error) -> Void)){
        
        DispatchQueue.global().async {
            
            let token:String = uUserToken
            let sign:String = (SALT + token).MD5()
            let url:String = "/alterData"
            let params:[String:String] = ["nickname":nickname,"college":college,
                                          "gender":gender,"grade":grade,
                                          "custom":custom,"sign":sign]
            DLOG(">>> url: \(url)\n\tparams: \(params.debugDescription)\n")
            
            var files:[MewFileInfo] = [MewFileInfo]()
            
            if let icon = iconImage{
                let compressedImage:Data = UIImageJPEGRepresentation(icon, 0.8)!
                let file:MewFileInfo = MewFileInfo.init(name: "\(Date()).jpg", data:compressedImage, argName: "icon", mimeType: "image/jpeg")
                files.append(file)
            }
            self.upload(files: files, toURL: url, withParameters: params, success: { (json) in
                
                DLOG(json.stringValue)
                successBlock(json)
            }, failure: { (err) in
                DLOG(err)
                failureBlock(err)
            })
        }
    }
    
    
    /// 添加为联系人
    ///
    /// - Parameters:
    ///   - userID: 用户ID
    ///   - successBlock: 成功
    ///   - failureBlock: 失败
    func addToContact(userID:String,successBlock:@escaping ((JSON) -> Void),failure failureBlock:@escaping ((Error) -> Void)){
        
        let token:String = uUserToken
        let sign:String = (SALT + token).MD5()
        let url:String = "/addFriend"
        let params:Parameters = ["friendID":userID,"sign":sign]
        
        self.requestWith(url, method: .post, parameters: params, headers: nil, success: { (json) in
            successBlock(json)
        }, failure: { (err) in
            failureBlock(err)
        })
        
    }
    
    /// 加入黑名单
    ///
    /// - Parameters:
    ///   - userID: 用户ID
    ///   - successBlock: 成功
    ///   - failureBlock: 失败
    func addToBlacklist(userID:String,successBlock:@escaping ((JSON) -> Void),failure failureBlock:@escaping ((Error) -> Void)){
        
        let token:String = uUserToken
        let sign:String = (SALT + token).MD5()
        let url:String = "/blacklist"
        let params:Parameters = ["userID":userID,"sign":sign]
        
        self.requestWith(url, method: .post, parameters: params, headers: nil, success: { (json) in
            successBlock(json)
        }, failure: { (err) in
            failureBlock(err)
        })
        
    }
    
    /// 消息中心获取用户信息用于更新
    ///
    /// - Parameters:
    ///   - IDs: 用户ID
    ///   - successBlock: 成功
    ///   - failureBlock: 失败
    func userData(_ IDs:[String],successBlock:@escaping ((JSON) -> Void),failure failureBlock:@escaping ((Error) -> Void)){
        
        let sign:String = SALT.MD5()
        let url:String = "/list/userData"
        let params:Parameters = ["IDs":IDs,"sign":sign]
        
        self.requestWith(url, method: .post, parameters: params, headers: nil, success: { (json) in
            successBlock(json)
        }, failure: { (err) in
            failureBlock(err)
        })
    }
}
