

import Foundation
import Alamofire
import SwiftyJSON

extension HttpController{
    
    /// 发现列表
    ///
    /// - Parameters:
    ///   - type: 发现类型
    ///   - pageIndex: 页索引
    ///   - successBlock: 成功
    ///   - failureBlock: 失败
    func discovery(type:DiscoveryListType,pageIndex:Int,successBlock:@escaping ((JSON) -> Void),failure failureBlock:@escaping ((Error) -> Void)){
        
        let sign:String = SALT.MD5()
        let token:String = uUserToken
        var url:String = ""
        switch type {
        case .partTime:
            url = "/list/partTimeJob"
        case .training:
            url = "/list/training"
        case .activity:
            url = "/list/activity"
        }
        
        let params:Parameters = ["token":token,"pageIndex":pageIndex,"pageSize":DefaultPageSize,"sign":sign]
        
        self.requestWith(url, method: .post, parameters: params, headers: nil, success: { (json) in
            successBlock(json)
        }, failure: { (err) in
            failureBlock(err)
        })
    }
    
    
    /// 报名
    ///
    /// - Parameters:
    ///   - type: 报名类型
    ///   - itemID: 发现item的ID
    ///   - userName: 真实姓名
    ///   - phone: 手机号
    ///   - personCount: 人数
    ///   - qq: QQ号
    ///   - wechat: 微信号
    ///   - successBlock: 成功
    ///   - failureBlock: 失败
    func signUp(type:SignupType,itemID:String,userName:String,phone:String,personCount:String,qq:String,wechat:String,successBlock:@escaping ((JSON) -> Void),failure failureBlock:@escaping ((Error) -> Void)){
        
        let token:String = uUserToken
        let sign:String = (SALT + token).MD5()
        var url:String = ""
        switch type {
        case .partTime:
            url = "/signUp/partTimeJob"
        case .training:
            url = "/signUp/training"
        case .activity:
            url = "/signUp/activity"
        }
        
        let params:Parameters = ["id":itemID,"name":userName,"phone":phone,
                                 "participationCount":personCount,
                                 "qq":qq,"weChat":wechat,"sign":sign]
        
        self.requestWith(url, method: .post, parameters: params, headers: nil, success: { (json) in
            successBlock(json)
        }, failure: { (err) in
            failureBlock(err)
        })
    }
}
