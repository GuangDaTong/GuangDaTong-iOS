

import Foundation
import Alamofire
import SwiftyJSON

/// 问问类型
enum AskType:String {
    case normal = "普问"
    case urgent = "急问"
}

/// 问问标签
enum AskTag:String {
    case lost = "寻物启事"
    case found = "失物招领"
    case learn = "学习"
    case practice = "实习"
    case master = "考研"
    case training = "培训"
    case shopping = "购物"
    case food = "美食"
    case traffic = "交通"
    case travel = "游玩"
    case emotion = "情感"
    case other = "其他"
}

/// 问问范围
enum AskRangeType:String {
    case college = "college"
    case school = "school"
    case allCollege = "all college"
    case allSchool = "all school"
}

extension HttpController{
    
    /// 问问主列表
    ///
    /// - Parameters:
    ///   - type: 问问范围类型
    ///   - pageIndex: 页索引
    ///   - successBlock: 成功
    ///   - failureBlock: 失败
    func askList(range:AskRangeType,pageIndex:Int,successBlock:@escaping ((JSON) -> Void),failure failureBlock:@escaping ((Error) -> Void)){
        
        let sign:String = SALT.MD5()
        let url:String = "/list/ask"
        
        let params:Parameters = ["range":range.rawValue,"pageIndex":pageIndex,"pageSize":DefaultPageSize,"sign":sign,"token":uUserToken ]
        
        self.requestWith(url, method: .post, parameters: params, headers: nil, success: { (json) in
            successBlock(json)
        }, failure: { (err) in
            failureBlock(err)
        })
    }
    
    
    /// 删除问问
    ///
    /// - Parameters:
    ///   - askIDs: 问问ID字符串数组
    ///   - successBlock: 成功
    ///   - failureBlock: 失败
    func deleteAsk(_ askIDs:[String],successBlock:@escaping ((JSON) -> Void),failure failureBlock:@escaping ((Error) -> Void)){
        
        let token:String = uUserToken
        let sign:String = (SALT + token).MD5()
        let url:String = "/delete/ask"
        
        let params:Parameters = ["IDs":askIDs,"sign":sign]
        
        self.requestWith(url, method: .post, parameters: params, headers: nil, success: { (json) in
            successBlock(json)
        }, failure: { (err) in
            failureBlock(err)
        })
    }
    
    
    /// 问问评论列表
    ///
    /// - Parameters:
    ///   - askID: 问问ID
    ///   - pageIndex: 页索引
    ///   - successBlock: 成功
    ///   - failureBlock: 失败
    func askCommentList(_ askID:String,pageIndex:Int,successBlock:@escaping ((JSON) -> Void),failure failureBlock:@escaping ((Error) -> Void)){
        
        let sign:String = (SALT + askID).MD5()
        let url:String = "/list/comment/ask"
        
        let params:Parameters = ["id":askID,"pageIndex":pageIndex,"pageSize":DefaultPageSize,"sign":sign]
        
        self.requestWith(url, method: .post, parameters: params, headers: nil, success: { (json) in
            successBlock(json)
        }, failure: { (err) in
            failureBlock(err)
        })
    }
    
    
    /// 评论问问
    ///
    /// - Parameters:
    ///   - askID: 问问ID
    ///   - commentReceiverID: 被评论人ID
    ///   - comment: 评论内容
    ///   - successBlock: 成功
    ///   - failureBlock: 失败
    func sendComment(_ askID:String,commentReceiverID:String,comment:String,successBlock:@escaping ((JSON) -> Void),failure failureBlock:@escaping ((Error) -> Void)){
        
        let token:String = uUserToken
        let sign:String = (SALT + token).MD5()
        let url:String = "/comment/ask"
        
        let params:Parameters = ["id":askID,"commentReceiverID":commentReceiverID,"content":comment,"sign":sign]
        
        self.requestWith(url, method: .post, parameters: params, headers: nil, success: { (json) in
            successBlock(json)
        }, failure: { (err) in
            failureBlock(err)
        })
    }
    
    
    /// 采纳/删除问问
    ///
    /// - Parameters:
    ///   - askID: 问问ID
    ///   - commentID: 评论ID
    ///   - isDelete: 采纳/删除
    ///   - successBlock: 成功
    ///   - failureBlock: 失败
    func acceptOrDeleteComment(_ askID:String,commentID:String,isDelete:Bool,successBlock:@escaping ((JSON) -> Void),failure failureBlock:@escaping ((Error) -> Void)){
        
        let token:String = uUserToken
        let sign:String = (SALT + token).MD5()
        let url:String = (isDelete ? "/delete/commentAsk" : "/adopt/ask")
        
        var params:Parameters! = nil
        if isDelete{
            params = ["id":commentID,"sign":sign]
        }else{
            params = ["askID":askID,"commentID":commentID,"sign":sign]
        }
        
        self.requestWith(url, method: .post, parameters: params, headers: nil, success: { (json) in
            successBlock(json)
        }, failure: { (err) in
            failureBlock(err)
        })
    }
    
    
    /// 发问问
    ///
    /// - Parameters:
    ///   - type: 类型
    ///   - title: 标题
    ///   - label: 标签
    ///   - range: 范围
    ///   - reward: 悬赏
    ///   - content: 内容
    ///   - images: 图片数组
    ///   - successBlock: 成功
    ///   - failureBlock: 失败
    func sendAsk(type:String,title:String,label:String,range:AskRangeType,reward:String,content:String,images:[UIImage],successBlock:@escaping ((JSON) -> Void),failure failureBlock:@escaping ((Error) -> Void)){
        
        DispatchQueue.global().async {
            
            let token:String = uUserToken
            let sign:String = (SALT + token).MD5()
            let url:String = "/send/ask"
            var params:[String:String] = ["type":type,"title":title,"label":label,"range":range.rawValue,
                                          "reward":reward,"images count":"\(images.count)",
                "content":content,"sign":sign]
            DLOG(">>> url: \(BaseURL + url)\n\tparams: \(params.debugDescription)\n")
            
            var files:[MewFileInfo] = [MewFileInfo]()
            var index:Int = 1
            var sizeDict:[String:Dictionary<String, Int>] = [String:Dictionary<String, Int>]()
            for image in images{
                
                let argName:String = "image\(index)"
                let width:Int = Int(image.size.width)
                let height:Int = Int(image.size.height)
                let dict:[String:Int] = ["width":width,"height":height]
                sizeDict[argName] = dict
                
                let compressedImage:Data = image.zippedImageData() ?? Data()
                let file:MewFileInfo = MewFileInfo.init(name: "ask_image_\(Date())_from_iOS.jpg", data:compressedImage, argName: argName, mimeType: "image/jpeg")
                files.append(file)
                index += 1
            }
            var sizeData:Data! = nil
            
            do{
                sizeData = try JSONSerialization.data(withJSONObject: sizeDict, options: JSONSerialization.WritingOptions.prettyPrinted)
            }catch _ {
                sizeData = Data.init()
            }
            params["size"] = String.init(data: sizeData, encoding: .utf8)
            
            self.upload(files: files, toURL: url, withParameters: params, success: { (json) in
                
                successBlock(json)
            }, failure: { (err) in
                
                failureBlock(err)
            })
        }
    }
    
    /// 某条问问的详情
    ///
    /// - Parameters:
    ///   - askID: 问问ID
    ///   - successBlock: 成功
    ///   - failureBlock: 失败
    func askDetail(_ askID:String,successBlock:@escaping ((JSON) -> Void),failure failureBlock:@escaping ((Error) -> Void)){
        
        let token:String = uUserToken
        let sign:String = (SALT + askID).MD5()
        let url:String = "/data/ask"
        
        let params:Parameters = ["id":askID,"token":token,"sign":sign]
        
        self.requestWith(url, method: .post, parameters: params, headers: nil, success: { (json) in
            successBlock(json)
        }, failure: { (err) in
            failureBlock(err)
        })
    }
    
    
    /// 问问搜索列表
    ///
    /// - Parameters:
    ///   - text: 搜索文本
    ///   - type: 问问类型
    ///   - tag: 问问标签
    ///   - range: 问问可见范围
    ///   - pageIndex: 页索引
    ///   - successBlock: 成功
    ///   - failureBlock: 失败
    func askSearchList(text:String,type:AskType?,tag:AskTag?,range:AskRangeType,pageIndex:Int,successBlock:@escaping ((JSON) -> Void),failure failureBlock:@escaping ((Error) -> Void)){
        
        let sign:String = SALT.MD5()
        let url:String = "/list/search/ask"
        
        var params:Parameters = ["key":text,"range":range.rawValue,
                                 "pageIndex":pageIndex,"pageSize":DefaultPageSize,
                                 "sign":sign,"token":uUserToken ]
        if let t = type{
            params["type"] = t.rawValue
        }
        if let t = tag{
            params["label"] = t.rawValue
        }
        
        self.requestWith(url, method: .post, parameters: params, headers: nil, success: { (json) in
            successBlock(json)
        }, failure: { (err) in
            failureBlock(err)
        })
    }
}
