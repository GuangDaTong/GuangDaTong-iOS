

import Foundation
import Alamofire
import SwiftyJSON

enum MsgCenterListType:String {
    case leaveMsg = "留言"
    case askMsg = "问问消息"
    case notice = "系统通知"
}

extension HttpController{
    
    
    /// 推送设置开关
    ///
    /// - Parameters:
    ///   - schoolUrgent: 学校急问
    ///   - collegeUrgent: 学院急问
    ///   - comment: 评论
    ///   - leaveWords: 留言
    ///   - activity: 活动
    ///   - system: 系统
    ///   - successBlock: 成功
    ///   - failureBlock: 失败
    func setPushSetting(schoolUrgent:Bool,collegeUrgent:Bool,comment:Bool,leaveWords:Bool,activity:Bool,system:Bool,successBlock:@escaping ((JSON) -> Void),failure failureBlock:@escaping ((Error) -> Void)){
        
        let token:String = uUserToken
        let sign:String = (SALT + token).MD5()
        let url:String = "/setPush"
        
        let params:Parameters = ["urgentSchool":schoolUrgent,"urgentCollege":collegeUrgent,
                                 "comment":comment,"leaveWords":leaveWords,"activity":activity,"system":system,"sign":sign]
        
        self.requestWith(url, method: .post, parameters: params, headers: nil, success: { (json) in
            successBlock(json)
        }, failure: { (err) in
            failureBlock(err)
        })
    }
    /// 消息中心消息列表
    ///
    /// - Parameters:
    ///   - type: 消息列表类型
    ///   - pageIndex: 页索引
    ///   - successBlock: 成功
    ///   - failureBlock: 失败
    func msgCenter(type:MsgCenterListType,pageIndex:Int,successBlock:@escaping ((JSON) -> Void),failure failureBlock:@escaping ((Error) -> Void)){
        
        let token:String = uUserToken
        let sign:String = (SALT + token).MD5()
        var url:String = ""
        switch type {
        case .leaveMsg:
            url = "/list/leaveWords"
        case .askMsg:
            url = "/list/askMsg"
        case .notice:
            url = "/list/notices"
        }
        
        let params:Parameters = ["pageIndex":pageIndex,"pageSize":DefaultPageSize,"sign":sign]
        
        self.requestWith(url, method: .post, parameters: params, headers: nil, success: { (json) in
            
            DispatchQueue.global().async {
                successBlock(json)
            }
        }, failure: { (err) in
            failureBlock(err)
        })
    }
    
    
    /// 清空消息
    ///
    /// - Parameters:
    ///   - type: 类型
    ///   - successBlock: 成功
    ///   - failureBlock: 失败
    func clearMsg(type:MsgCenterListType,successBlock:@escaping ((JSON) -> Void),failure failureBlock:@escaping ((Error) -> Void)){
        
        let token:String = uUserToken
        let sign:String = (SALT + token).MD5()
        var url:String = ""
        switch type {
        case .leaveMsg:
            url = "/clear/leaveWords"
        case .askMsg:
            url = "/clear/askMsg"
        case .notice:
            url = "/clear/notices"
        }
        
        let params:Parameters = ["sign":sign]
        
        self.requestWith(url, method: .post, parameters: params, headers: nil, success: { (json) in
            successBlock(json)
        }, failure: { (err) in
            failureBlock(err)
        })
    }
    
    
    /// 发送留言
    ///
    /// - Parameters:
    ///   - userID: 用户ID
    ///   - msg: 消息内容
    ///   - successBlock: 成功
    ///   - failureBlock: 失败
    func leaveMsg(userID:String,msg:String,successBlock:@escaping ((JSON) -> Void),failure failureBlock:@escaping ((Error) -> Void)){
        
        let token:String = uUserToken
        let sign:String = (SALT + token).MD5()
        let url:String = "/leaveWords"
        
        let params:Parameters = ["receiverID":userID,"words":msg,"sign":sign]
        
        self.requestWith(url, method: .post, parameters: params, headers: nil, success: { (json) in
            successBlock(json)
        }, failure: { (err) in
            failureBlock(err)
        })
    }
    
    
    /// 赠送/索要喵币
    ///
    /// - Parameters:
    ///   - isGive: 是否为赠送
    ///   - userID: 用户ID
    ///   - coinCount: 喵币数量
    ///   - msg: 留言
    ///   - successBlock: 成功
    ///   - failureBlock: 失败
    func coinExchange(isGive:Bool,userID:String,coinCount:String,msg:String,successBlock:@escaping ((JSON) -> Void),failure failureBlock:@escaping ((Error) -> Void)){
        
        let token = uUserToken
        let sign = (SALT + token).MD5()
        var url = ""
        var params:Parameters! = ["receiverID":userID,"words":msg,"amount":coinCount,"sign":sign]
        if isGive{
            url = "/giveCoins"
            params["receiverID"] = userID
        }else{
            url = "/wantCoins"
            params["giverID"] = userID
        }
        
        self.requestWith(url, method: .post, parameters: params, headers: nil, success: { (json) in
            successBlock(json)
        }, failure: { (err) in
            failureBlock(err)
        })
    }
}
