

import Foundation
import Alamofire
import SwiftyJSON

enum SchoolListType:Int {
    case guidance = 0
    case summary
    case club
    case look
}

extension HttpController{
    
    /// 校园导航列表
    ///
    /// - Parameters:
    ///   - type: 校园导航类型
    ///   - pageIndex: 页索引
    ///   - successBlock: 成功
    ///   - failureBlock: 失败
    func school(type:SchoolListType,pageIndex:Int,successBlock:@escaping ((JSON) -> Void),failure failureBlock:@escaping ((Error) -> Void)){
        
        let sign:String = SALT.MD5()
        let token:String = uUserToken
        var url:String = ""
        switch type {
        case .guidance:
            url = "/list/guidance"
        case .summary:
            url = "/list/summary"
        case .club:
            url = "/list/club"
        case .look:
            url = "/list/look"
        }
        
        let params:Parameters = ["token":token,"pageIndex":pageIndex,"pageSize":DefaultPageSize,"sign":sign]
        
        self.requestWith(url, method: .post, parameters: params, headers: nil, success: { (json) in
            successBlock(json)
        }, failure: { (err) in
            failureBlock(err)
        })
    }
    /// 广告
    ///
    /// - Parameters:
    ///   - successBlock: 成功
    ///   - failureBlock: 失败
    func advertisement(successBlock:@escaping ((JSON) -> Void),failure failureBlock:@escaping ((Error) -> Void)){

        let sign:String = SALT.MD5()
        let url:String = "/list/advertisement"
        let params:Parameters = ["school": DefaultSchool,"sign":sign,]
        
        self.requestWith(url, method: .post, parameters: params, headers: nil, success: { (json) in
            successBlock(json)
        }, failure: { (err) in
            failureBlock(err)
        })
    }
}
