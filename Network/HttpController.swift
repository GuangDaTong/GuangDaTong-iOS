

import Foundation
import Alamofire
import SwiftyJSON

class MewFileInfo:NSObject {
    var name = ""
    var argName = ""
    var mimeType = ""
    var data:Data! = nil
    init(name:String,data:Data,argName:String = "",mimeType:String = "") {
        self.name = name
        self.data = data
        self.argName = argName
        self.mimeType = mimeType
    }
}

class HttpController:NSObject{
    
    static let shared:HttpController = HttpController()
    var isNetworkAvailable:Bool = true
    
    fileprivate override init(){
        super.init()
        
        NotiCenter.addObserver(self, selector: #selector(networkStatusChanged), name: NetworkStatusChangedNotification, object: nil)
    }
    @objc func networkStatusChanged(notify:Notification){
        
        if let able = notify.userInfo?["reachable"] as? Bool {
            self.isNetworkAvailable = able
//            if !able{
//                WIN.makeFailToast("网络连接已断开")
//            }
        }
    }
    
    func download(withURL url:String,success successBlock:@escaping ((URL) -> Void),failure failureBlock:@escaping ((Error,URL?) -> Void)){
        
        DLOG(">>> File Download url: \(url)\n")
        let destination:DownloadRequest.DownloadFileDestination = DownloadRequest.suggestedDownloadDestination()
        Alamofire.download(url, to: destination).response { (response) in
            
            if let err:Error = response.error {
                DispatchQueue.main.async{
                    failureBlock(err,response.destinationURL)
                }
                return
            }
            if let url:URL = response.destinationURL {
                DispatchQueue.main.async{
                    successBlock(url)
                }
                return
            }
            DispatchQueue.main.async{
                failureBlock(NSError.init(domain: "com.mew", code: -1, userInfo: [NSLocalizedDescriptionKey:"下载内容解析异常！"]),nil)
            }
        }
    }
    
    
    /// 上传文件
    ///
    /// - Parameters:
    ///   - files: 文件数组
    ///   - url: 链接地址
    ///   - parameters: 参数字典
    ///   - successBlock: 成功
    ///   - failureBlock: 失败
    func upload(files:[MewFileInfo],toURL url:String,withParameters parameters:[String:String]?,success successBlock:@escaping ((JSON) -> Void),failure failureBlock:@escaping ((Error) -> Void)){
        
        DispatchQueue.global().async {
            
            Alamofire.upload(
                multipartFormData: { formData in
                    
                    if let params = parameters{
                        for key in params.keys{
                            let param = params[key]!
                            formData.append(param.data(using: .utf8) ?? Data(), withName: key)
                        }
                    }
                    for file in files{
                        
                        formData.append(file.data, withName: file.argName, fileName: file.name, mimeType: file.mimeType)
                    }
            },
                to: BaseURL + url,
                encodingCompletion: { encodingResult in
                    switch encodingResult {
                    case .success(let upload, _, _):
                        upload.responseJSON { response in
                            
                            DLOG(">>> url: \(url)\n\tstatusCode:\(response.response?.statusCode ?? -1)\n")
                            
                            self.processResponse(response, success: successBlock, failure: failureBlock)
                        }
                    case .failure(let encodingError):
                        
                        DLOG(">>> url: \(url)\n\tError:\(encodingError.localizedDescription)")
                        DispatchQueue.main.async{
                            failureBlock(encodingError)
                        }
                    }
            })
        }
    }
    
    
    /// 请求
    ///
    /// - Parameters:
    ///   - url: 链接
    ///   - method: 方法
    ///   - parameters: 参数
    ///   - headers: 请求头部
    ///   - successBlock: 成功
    ///   - failureBlock: 失败
    func requestWith(_ url:String,method:HTTPMethod,parameters:Parameters?,headers: HTTPHeaders?,success successBlock:@escaping ((JSON) -> Void),failure failureBlock:@escaping ((Error) -> Void)){
        
        guard isNetworkAvailable else {
            let error:NSError = NSError.init(domain: "com.mew", code: 01, userInfo: [NSLocalizedDescriptionKey:"尚未连接到网络"])
            failureBlock(error)
            return
        }
        
        let URL:String = BaseURL + url
//        DLOG(">>> url: \(URL)\n\tmethod: \(method)\n\tparams: \(parameters.debugDescription)\n\theaders: \(headers.debugDescription)\n")
        
        DispatchQueue.global().async {
            
            Alamofire.request(URL,
                              method: method,
                              parameters: parameters,
                              encoding: JSONEncoding.default,
                              headers: nil)
                .responseJSON { (response) in
                    
                    DLOG(">>> url: \(URL)\n\tmethod: \(method)\n\tparams: \(parameters.debugDescription)\n\theaders: \(headers.debugDescription)\n\tstatusCode:\(response.response?.statusCode ?? -1)\n")
                    
                    self.processResponse(response, success: successBlock, failure: failureBlock)
            }
        }
    }
    
    func processResponse(_ response:DataResponse<Any>,success successBlock:@escaping ((JSON) -> Void),failure failureBlock:@escaping ((Error) -> Void)){
        
        switch response.result {
        case .success(let value):
            
            let json:JSON = JSON(value)
            
            DLOG(">>> url: \(response.request!.url!.absoluteString)\nJSON:\n\(json.debugDescription)")
            
            guard json["code"].stringValue == "200" else {
                
                if json.type == .null {
                    DispatchQueue.main.async{
                        failureBlock(NSError.init(domain: "", code: -1, userInfo: [NSLocalizedDescriptionKey : "呃,服务器返回了垃圾🌚"]))
                    }
                    return
                }
                DispatchQueue.main.async{
                    let code:Int = json["code"].intValue
                    let msg:String = json["msg"].stringValue
                    failureBlock(NSError.init(domain: "", code: code, userInfo: [NSLocalizedDescriptionKey : msg]))
                    if code == 201{
                        
                        uDefaults.set("", forKey: "UserID")
                        uDefaults.set("", forKey: "UserToken")
                        uDefaults.set("", forKey: "UserName")
                        uDefaults.set("", forKey: "UserIcon")
                        uDefaults.set("", forKey: "UserCoins")
                        uDefaults.set("", forKey: "UserLevel")
                        uDefaults.synchronize()
                        TopToastView.shared.makeFailToast(msg)
                        NotiCenter.post(name: UserDidLogoutNotification, object: nil)
                    }
                }
                return
            }
            DispatchQueue.main.async{
                successBlock(json)
            }
            return
            
        case .failure(let error):
            
            DLOG(error.localizedDescription)
            
            let err:NSError = error as NSError
            if response.response?.statusCode == 502{
                DispatchQueue.main.async{
                    failureBlock(NSError.init(domain: "", code: err.code, userInfo: [NSLocalizedDescriptionKey : "呀！服务器跪了，求烧纸 -_-+"]))
                }
                return
            }
            
            guard err.code != 4 else{
                // 1.连接需要登录的WIFI时，如果没有登录，连接会超时
                // 2.HTTP状态码为404
                DispatchQueue.main.async{
                    failureBlock(NSError.init(domain: "", code: err.code, userInfo: [NSLocalizedDescriptionKey : "Sorry,找不到内容..."]))
                }
                return
            }
            DispatchQueue.main.async{
                failureBlock(NSError.init(domain: "", code: err.code, userInfo: [NSLocalizedDescriptionKey : error.localizedDescription]))
            }
        }
    }
}
