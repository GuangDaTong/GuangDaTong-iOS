import UIKit
import UserNotifications

enum ForceTouchLaunchType {
    case none
    case writeAsk
    case contact
    case msgCenter
    case mine
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var reachabilityManager:ReachabilityManager! = nil
    var forceTouchLaunchType:ForceTouchLaunchType = ForceTouchLaunchType.none
    // MARK: - App Life Cycle
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

        setupUMMob()
        setupBugly()
        setupUMShare()
        
        reachabilityManager = ReachabilityManager.shared
        reachabilityManager.listenForReachability()
        initDB()
        MAskFilter.shared.configFilter()
        setupPhotoBrowserOptions()

        initFirstLoad()
        
        #if arch(arm) || arch(arm64)
            setupJPush(withApplication: application, options: launchOptions)
        #endif
        
        loadNotification(withOptions: launchOptions)
        
        return true
    }
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
}

extension AppDelegate{
    
    // MARK: - 初始化数据库
    func initDB(){
        
        let database = FMDatabase(url: DefaultURLForDB)
        guard database.open() else {
            print("*** Unable to open database ***")
            return
        }
    }
    // MARK: - 友盟应用统计 http://mobile.umeng.com/
    func setupUMMob(){
        
        // 友盟应用分析初始化 http://dev.umeng.com/analytics/ios-doc/integration
        UMAnalyticsConfig.sharedInstance().appKey = UMengAppKey
        #if DEBUG
            MobClick.setLogEnabled(true)
            UMAnalyticsConfig.sharedInstance().channelId = "Debug"
        #else
            UMAnalyticsConfig.sharedInstance().channelId = "AppStore"
        #endif
        MobClick.setAppVersion("\(APP_Current_Version)(\(APP_Current_Build))") //正确的APP版本
        MobClick.setCrashReportEnabled(false) //记录崩溃
        MobClick.start(withConfigure: UMAnalyticsConfig.sharedInstance())
    }
    /// 腾讯Bugly 崩溃分析 https://bugly.qq.com/
    func setupBugly(){
        
        let config = BuglyConfig.init()
        #if DEBUG
            config.debugMode = true
            config.channel = "Debug"
        #else
            config.channel = "AppStore"
        #endif
        
        config.viewControllerTrackingEnable = true
        config.blockMonitorEnable = true
        config.blockMonitorTimeout = 2
        config.unexpectedTerminatingDetectionEnable = true
        config.version = APP_Current_Version
        config.reportLogLevel = .warn
        Bugly.setUserIdentifier("893539")
        
        Bugly.start(withAppId: "9adbf3873e", config: config)
        
        Bugly.setUserValue(uUserID, forKey: "UserID")
        Bugly.setUserValue(uUserName, forKey: "UserName")
        Bugly.setUserValue(Bugly.sdkVersion(), forKey: "SDK_Version")
        Bugly.setUserValue(Bugly.buglyDeviceId(), forKey: "DeviceID")
    }
    // MARK: - 友盟分享
    func setupUMShare(){
        
        // 友盟分享文档  http://dev.umeng.com/social/ios/quick-integration#1_2
        #if DEBUG
            UMSocialManager.default().openLog(true)
        #endif
        let umShare = UMSocialManager.default()!
        umShare.umSocialAppkey = UMengAppKey
        umShare.setPlaform(.QQ, appKey: QQAppID, appSecret: "", redirectURL: BaseURL)
        umShare.setPlaform(.wechatSession, appKey: WechatAppKey, appSecret: WechatSecretKey, redirectURL: BaseURL)
        umShare.setPlaform(.wechatTimeLine, appKey: WechatAppKey, appSecret: WechatSecretKey, redirectURL: BaseURL)
    }
    func setupPhotoBrowserOptions(){
        
        SKPhotoBrowserOptions.displayCounterLabel = false
        SKPhotoBrowserOptions.displayBackAndForwardButton = false                 // back / forward button will be hidden
        SKPhotoBrowserOptions.displayHorizontalScrollIndicator = false            // horizontal scroll bar will be hidden
        SKPhotoBrowserOptions.displayVerticalScrollIndicator = false              // vertical scroll bar will be hidden
        SKPhotoBrowserOptions.displayStatusbar = true
        SKPhotoBrowserOptions.displayCloseButton = false
        SKPhotoBrowserOptions.enableSingleTapDismiss = true
        // 统一使用SDWebImage的缓存
        SKCache.sharedCache.imageCache = ImageCacheManager.shared
    }
    // 分享后的回调
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        
        return UMSocialManager.default().handleOpen(url)
    }
    func application(_ application: UIApplication, handleOpen url: URL) -> Bool {
        
        return UMSocialManager.default().handleOpen(url)
    }
    // 通过Universal Links打开App，
    // https://developer.apple.com/library/content/documentation/General/Conceptual/AppSearch/UniversalLinks.html#//apple_ref/doc/uid/TP40016308-CH12-SW1
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
        
        return true
    }
    @available(iOS 9.0, *) // 3D Touch Menu
    func application(_ application: UIApplication, performActionFor shortcutItem: UIApplicationShortcutItem, completionHandler: @escaping (Bool) -> Void) {
        
        completionHandler(handleQuickAction(shortcutItem: shortcutItem))
    }
    @available(iOS 9.0, *)
    func handleQuickAction(shortcutItem:UIApplicationShortcutItem) -> Bool{
        
        let type:String = shortcutItem.localizedTitle
        
        let rootNavi:UINavigationController = WIN.rootViewController as! UINavigationController
        let tab:TabVC = rootNavi.viewControllers.first as! TabVC
        
        if isUserNotLogin {
            let vc = currentTopVC() as? MBaseVC
            vc?.toastFail("您尚未登录")
            tab.presentVCInMainStoryboard("MLoginNaviVC")
            return true
        }
        
        let vc = currentTopVC() as? MBaseVC
        
        switch type {
        case "发问问":
            if vc is MAskWriteVC{
                self.forceTouchLaunchType = .none
                return true
            }
            tab.selectedIndex = 1
            tab.selectedIndex = 2
            vc?.navigationController?.popToRootViewController(animated: false)
            self.forceTouchLaunchType = .writeAsk
        case "通讯录":
            if vc is MMineContactVC{
                self.forceTouchLaunchType = .none
                return true
            }
            tab.selectedIndex = 1
            tab.selectedIndex = 3
            vc?.navigationController?.popToRootViewController(animated: false)
            self.forceTouchLaunchType = .contact
        case "消息中心":
            if vc is MMsgCenterVC{
                self.forceTouchLaunchType = .none
                return true
            }
            tab.selectedIndex = 1
            tab.selectedIndex = 0
            vc?.navigationController?.popToRootViewController(animated: false)
            self.forceTouchLaunchType = .msgCenter
        case "我的":
            if vc is MMineVC{
                self.forceTouchLaunchType = .none
                return true
            }
            tab.selectedIndex = 1
            tab.selectedIndex = 3
            vc?.navigationController?.popToRootViewController(animated: false)
            self.forceTouchLaunchType = .mine
        default:
            break
        }
        return true
    }
}

